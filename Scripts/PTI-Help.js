﻿class PTIHelp {

    static GetPageHelp(PageName) {
        var HelpSectionTemplate = "<button onclick=\"ToggleAccordion('~SectionId~');\" class=\"w3-button w3-light-grey w3-block w3-left-align\">~SectionHeadingText~</button>";
        HelpSectionTemplate += "   <div id=\"~SectionId~\" class=\"w3-hide w3-border\" >";
        HelpSectionTemplate += "     <div class=\"w3-container\">";
        HelpSectionTemplate += "       <p>~SectionText~</p>";
        HelpSectionTemplate += "     </div>";
        HelpSectionTemplate += "   </div> ";
        $.ajax({
            url: g_root + "/AjaxFunctions/Help/AllPageHelp",
            type: "POST",
            data: PageName,
            contentType: "text/plain; charset=UTF-8",
            success: function (data) {
                var jsonData = JSON.parse(data);
                if (jsonData.HelpSections) {
                    var SectionCount = 0;
                    var HelpContentHTML = "";
                    $(jsonData.HelpSections).each(function () {
                        SectionCount++;
                        var SectionId = "HelpSection_" + SectionCount.toString();
                        var ThisSectionHTML = HelpSectionTemplate.replace(/~SectionId~/ig, SectionId);
                        ThisSectionHTML = ThisSectionHTML.replace(/~SectionHeadingText~/ig, this.SectionHeading);
                        ThisSectionHTML = ThisSectionHTML.replace(/~SectionText~/ig, this.SectionText);
                        HelpContentHTML += ThisSectionHTML;
                    });
                    $("#HelpContent").html(HelpContentHTML);
                }
            }
        });
    }
}