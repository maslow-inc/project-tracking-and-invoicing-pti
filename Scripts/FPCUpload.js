﻿//Page Ready
var FPCUploadTimer;

$(function () {
    StartCheckingUploadStatus();
});

function StartCheckingUploadStatus() {
    CheckFPCUploadStatus();
    FPCUploadTimer = setInterval(CheckFPCUploadStatus, 15000);
}

function CheckFPCUploadStatus() {
    var dt = new Date();
    //console.log("CheckFPCUploadStatus " + dt.toString());
    $("#chkStatus").show();
    $.ajax({
        url: g_root + "/AjaxFunctions/InvoicingPeriod/GetFPCUploadStatus",
        type: "POST",
        data: "{}",
        contentType: "text/plain; charset=UTF-8",
        success: function (data) {
            if (data.trim().length > 2) {
                $(".invoicingperiodbutton").addClass("w3-disabled");
                $("#btnFPCUpload").attr("title", "FPC Upload Currently Running...");
                $("#FPCUploadRunningSpinner").show();
                jData = JSON.parse(data);
                var StatusHTML = "<h5 style='margin-left:60px;'>FPC Upload Process Status:</h5><ol>";
                $(jData).each(function () {
                    var PreviousRecord = JSON.parse(this.PreviousRecord);
                    if (PreviousRecord.IsError == "true") {
                        StatusHTML += "<ul><li style='color:red;'>" + this.DTS + ": " + PreviousRecord.LogEvent + "</li></ul>";
                    }
                    else {
                        StatusHTML += "<li style='font-size:10px;'>" + this.DTS + ": " + PreviousRecord.LogEvent;
                    }
                });
                StatusHTML += "</ol><label id='chkStatus' style='font-size:12px;display:none;'>Updating status...</label>";
                $("#FPCUploadStatus").html(StatusHTML);
            }
            else {
                window.clearInterval(FPCUploadTimer);
                $(".invoicingperiodbutton").removeClass("w3-disabled");
                $("#btnFPCUpload").attr("title", "");
                $("#FPCUploadRunningSpinner").hide();
                GetLastFPCUploadForPeriod();
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            var errorMsg = "Error getting FPC Upload Status. " + textStatus + ": " + errorThrown;
            alert(errorMsg);
        }
    });
}

function GetLastFPCUploadForPeriod() {
    $.ajax({
        url: g_root + "/AjaxFunctions/InvoicingPeriod/GetLastFPCUploadDTSForOpenPeriod",
        type: "POST",
        data: "{}",
        contentType: "text/plain; charset=UTF-8",
        success: function (data) {
            if (data.trim().length > 2) {
                var jData = JSON.parse(data);
                var LastFPCUploadDTS = jData[0].DTS;
                var LastFPCUploadEmp = jData[0].EmployeeName;
                $("#FPCUploadStatus").html("<p style='font-size:10px;margin-left:55px;'>FPC Upload for the current open period last<br />completed by " + LastFPCUploadEmp + ": " + LastFPCUploadDTS + "</p>");
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            var errorMsg = "Error getting FPC Upload Status. " + textStatus + ": " + errorThrown;
            alert(errorMsg);
        }
    });
}

function btnModalFPCUploadContinue_Click() {
    if (confirm("Once started, the FPC Upload process will run in the background until complete. Progress will be displayed below the FPC Upload button.\r\n\r\nYou may leave this page and return to check the upload progress at any time.\r\n\r\nDo you wish to continue?")) {
        $("#divModalFPCUpload").hide();
        RunFPCUpload().then(
            function (response) {
                if (response == "started") {
                    StartCheckingUploadStatus();
                }
                else {
                    alert(response);
                }
            },
            function (errormsg) {
                alert('Error starting FPC Upload process: ' + errormsg);
            }
        );
    }
}

function RunFPCUpload() {
    return new Promise(function (resolve, reject) {
        $.ajax({
            url: g_root + "/AjaxFunctions/InvoicingPeriod/RunFPCUpload",
            type: "POST",
            data: "{}",
            contentType: "text/plain; charset=UTF-8",
            success: function (data) {
                response = data.trim();
                if (response == "started") {
                    resolve(response);
                }
                else {
                    reject(response);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                var errorMsg = "Error in RunFPCUpload. " + textStatus + ": " + errorThrown;
                console.log(errorMsg);
                reject(errorMsg);
            }
        });
    });
}
