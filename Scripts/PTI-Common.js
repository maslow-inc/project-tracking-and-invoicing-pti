﻿
/*HTML Functions*/
function htmlEncode(value) {
    //create a in-memory div, set it's inner text(which jQuery automatically encodes)
    //then grab the encoded contents back out. The div never exists on the page.
    return $('<div/>').text(value).html();
}

function htmlDecode(value) {
    return $('<div/>').html(value).text();
}

function FormatDateDisplay(sDate, DateStyle) {
    var sLocale = "en-us";
    var sYear = "numeric";
    var sMonth = "short";
    var sDay = "numeric";

    switch (DateStyle) {
        case "short":
            sMonth = "2-digit";
            sDay = "2-digit";
            break;
        case "datebox":
            sLocale = "en-CA";
            sMonth = "2-digit";
            sDay = "2-digit";
            break;
        default:
    }
    var returnDate = new Date(sDate).toLocaleDateString(sLocale, { year: sYear, month: sMonth, day: sDay });
    if (returnDate == "Invalid Date") returnDate = "";
    return returnDate;
}

function openTab(tabBtn, tabClass, tabID) {
    //Hide and deactivate all tabs
    $("." + tabClass).hide();
    $(".tabBtn").removeClass("w3-nei");

    //Show selected tab
    $("#" + tabID).show();
    $(tabBtn).addClass("w3-nei");

    //Set clicked tab in cookie as last tab selected for page refresh
    var Tab = $(tabBtn).data("tab");
    setCookie("LastOpenedTab", Tab, 1);

}

function FilterList(SearchBox) {
    var filter = $(SearchBox).val().toUpperCase();
    var li = $(SearchBox).siblings('ul').first().children(); //Get all <li> elements from list
    $(li).each(function () {
        var itemVal = $(this).text().toUpperCase();
        if (itemVal.indexOf(filter) > -1) {
            $(this).show();
        }
        else {
            $(this).hide();
        }
    });
}

function AddOptionToList(ListName, Value, Text) {
    $('#' + ListName).append($('<option/>', {
        value: Value,
        text: Text
    }));
}

function SortByProperty(prop) {
    return function (a, b) {
        if (a[prop] > b[prop]) {
            return 1;
        } else if (a[prop] < b[prop]) {
            return -1;
        }
        return 0;
    }
}

/*Data Functions*/
function GetMetadataByType(TypeName) {
    return new Promise(function (resolve, reject) {
        $.ajax({
            url: g_root + "/AjaxFunctions/GetMetadataByType/" + TypeName,
            type: "GET",
            contentType: "text/plain; charset=UTF-8",
            success: function (data) {
                var jData = [];
                if (data) {
                    jData = JSON.parse(data.trim());
                }
                resolve(jData);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                var errorMsg = "Error in GetMetadataByType. " + textStatus + ": " + errorThrown;
                console.log(errorMsg);
                reject(errorMsg);
            }
        });
    });
}