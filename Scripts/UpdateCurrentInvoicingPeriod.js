﻿var g_DVPChanges;

$(function () {
    //Page loaded
    InitializeDisplay();
});

function InitializeDisplay() {
    var InvoicingPeriodLabel = "<strong>Current Open Period:</strong>&nbsp;<span style='text-decoration-line:underline;'>" + g_CurrentOpenPeriodDisplay + "</span>";
    $("#InvoicingPeriodHeader").html(InvoicingPeriodLabel);
    if (g_CurrentOpenPeriodId == '') {
        $("#DivMain").html("<h3>This feature is only available when there is an open invoicing period.</h3>");
    }

    //Hide NEI footer for more vertical space
    $("footer.w3-theme-d1").hide();
}

function GetDVPChanges() {
    $(".DataLoadedDisplay").hide();
    $("#spnLoading").show();

    $.ajax({
        url: g_root + "/AjaxFunctions/InvoicingPeriod/GetDVPUpdates",
        type: "GET",
        contentType: "text/plain; charset=UTF-8",
        success: function (data) {
            g_DVPChanges = JSON.parse(data.trim());
            if (g_DVPChanges.length > 0) {
                $("#spnResultsCount").html("Subphases that differ from DVP: " + g_DVPChanges.length);
                BuildDVPChangesTable();
            }
            else {
                alert("DVP values have not changed since the PTI invoicing period was opened or last updated.");
            }
            $("#spnLoading").hide();
        }
    });
}

function BuildDVPChangesTable() {
    var ShowHideGlyph = "<span class='showhide' style='cursor:pointer;font-weight:bold;display:none;' title='Show/Hide Detail'>-</span>"; //showhide controls hidden for now. Can be updated for future enhancement if desired.
    var ShowHideEvent = " onclick='ShowHideDetail(this);'";
    var ClientRow = "<tr class='client thin'><td class='dataentryshowhide' ~showhideevent~>~showhideglyph~</td><td><strong>~ClientName~</strong></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>";
    var ProjectRow = "<tr class='project thin'><td class='dataentryshowhide' ~showhideevent~>~showhideglyph~</td><td>~ProjectName~</td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>";
    var PhaseRow = "<tr class='phase thin'><td class='dataentryshowhide' ~showhideevent~>~showhideglyph~</td><td class='phasedesc'>~PhaseName~</td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>";
    var SubphaseRow = "<tr class='subphase thin'><td class='dataentryshowhide' ~showhideevent~>~showhideglyph~</td><td class='subphasedesc'>~SubphaseName~</td><td>~PTI_Spent_Total~</td><td>~DVP_Spent_Total~</td><td class='diffcol'>~Spent_Total_Diff~</td><td>~PTI_Exp_Unbilled~</td><td>~DVP_Exp_Unbilled~</td><td class='diffcol'>~Exp_Unbilled_Diff~</td><td>~PTI_WIP_Total~</td><td>~DVP_WIP_Total~</td><td class='diffcol'>~WIP_Total_Diff~</td><td>~PTI_WIP_Current~</td><td>~DVP_WIP_Current~</td><td class='diffcol'>~WIP_Current_Diff~</td><td>~PTI_WIP_Held~</td><td>~DVP_WIP_Held~</td><td class='diffcol'>~WIP_Held_Diff~</td></tr>";

    var LastClientName = "";
    var LastProjectName = "";
    var LastPhaseName = "";
    var WorkRow = "";
    var TableBodyHTML = "";
    $(g_DVPChanges).each(function () {
        var ClientName = this.ClientName;
        var ProjectName = this.ProjectName;
        var PhaseName = this.PhaseName;
        var SubphaseName = this.SubphaseName;

        //ClientRow
        if (LastClientName != ClientName) {
            WorkRow = ClientRow.replace(/~ClientName~/ig, ClientName).replace(/~showhideevent~/ig, ShowHideEvent).replace(/~showhideglyph~/ig, ShowHideGlyph); //Row ShowHideDetail column;
            TableBodyHTML += WorkRow;
            LastClientName = ClientName;
        }
        //ProjectRow
        if (LastProjectName != ProjectName) {
            WorkRow = ProjectRow.replace(/~ProjectName~/ig, ProjectName).replace(/~showhideevent~/ig, ShowHideEvent).replace(/~showhideglyph~/ig, ShowHideGlyph); //Row ShowHideDetail column;;
            TableBodyHTML += WorkRow;
            LastProjectName = ProjectName;
        }
        //PhaseRow
        if (LastPhaseName != PhaseName) {
            WorkRow = PhaseRow.replace(/~PhaseName~/ig, PhaseName).replace(/~showhideevent~/ig, ShowHideEvent).replace(/~showhideglyph~/ig, ShowHideGlyph); //Row ShowHideDetail column;;
            TableBodyHTML += WorkRow;
            LastPhaseName = PhaseName;
        }
        //Subphase Row
        WorkRow = SubphaseRow.replace(/~SubphaseName~/ig, SubphaseName).replace(/~showhideevent~/ig, "").replace(/~showhideglyph~/ig, ""); //Not Row ShowHideDetail column;;
        WorkRow = WorkRow.replace(/~PTI_Spent_Total~/ig, fmtMoney(this.PTI_SPENT_TOTAL)).replace(/~DVP_Spent_Total~/ig, fmtMoney(this.DVP_SPENT_TOTAL)).replace(/~Spent_Total_Diff~/ig, fmtMoney(this.SPENT_TOTAL_DIFF));
        WorkRow = WorkRow.replace(/~PTI_Exp_Unbilled~/ig, fmtMoney(this.PTI_EXP_UNBILLED)).replace(/~DVP_Exp_Unbilled~/ig, fmtMoney(this.DVP_EXP_UNBILLED)).replace(/~Exp_Unbilled_Diff~/ig, fmtMoney(this.EXP_UNBILLED_DIFF));
        WorkRow = WorkRow.replace(/~PTI_WIP_Total~/ig, fmtMoney(this.PTI_WIP_TOTAL)).replace(/~DVP_WIP_Total~/ig, fmtMoney(this.DVP_WIP_TOTAL)).replace(/~WIP_Total_Diff~/ig, fmtMoney(this.WIP_TOTAL_DIFF));
        WorkRow = WorkRow.replace(/~PTI_WIP_Current~/ig, fmtMoney(this.PTI_WIP_CURRENT)).replace(/~DVP_WIP_Current~/ig, fmtMoney(this.DVP_WIP_CURRENT)).replace(/~WIP_Current_Diff~/ig, fmtMoney(this.WIP_CURRENT_DIFF));
        WorkRow = WorkRow.replace(/~PTI_WIP_Held~/ig, fmtMoney(this.PTI_WIP_HELD)).replace(/~DVP_WIP_Held~/ig, fmtMoney(this.DVP_WIP_HELD)).replace(/~WIP_Held_Diff~/ig, fmtMoney(this.WIP_HELD_DIFF));

        TableBodyHTML += WorkRow;
    });

    $("#tblValuesData").html(TableBodyHTML);

    //Highlight cells with differences
    $("td.diffcol").each(function () {
        if ($(this).html() != "$0.00") {
            $(this).addClass("highlight");
        }
    });

    $(".PageStartDisplay").hide();
    $(".DataLoadedDisplay").show();

    var thisTable = FormatTable("tblValues", false, "", false, true);

}

function btnCancel_click() {

    var bCancel;

    if ($("#btnCancel").html() == "Finish") {
        bCancel = true
    }
    else {
        bCancel = confirm('Are you sure you want to cancel?');
    }

    if (bCancel){
        location.reload();
    }
}

function ExportTable() {
    //Set up Modal Table Data
    $("#modalTableData").html("<table id='tblModalExport' style='font-size:9px;'><thead id='tblModalExportHead'></thead><tbody id='tblModalExportBody'></tbody></table>");
    //Remove show/hide th from table head row
    WorkingHTML = $("#tblValuesHead").html().replace(/<th.*.dataentryshowhide.*?th>/ig, "");
    $("#tblModalExportHead").html(WorkingHTML);
    //Remove show/hide td from table body rows
    WorkingHTML = $("#tblValuesData").html().replace(/<td class="dataentryshowhide.*?td>/ig, "");
    WorkingHTML = WorkingHTML.replace(/display:.*?none;/ig, ""); //Unhide any rows that were collapsed in main display
    $("#tblModalExportBody").html(WorkingHTML);

    //Make datatable with output buttons
    FormatTable("tblModalExport", false, "PTI-DVP Differences - " + g_CurrentOpenPeriodDisplay, true, false);

    $("#divModal").show();

}

function FormatTable(tblId, orderable, OutputFilename, ShowOutputButtons, AllowScroll) {
    var OutputButtons = [];
    if (ShowOutputButtons) {
        OutputButtons = [
            'copy',
            {
                extend: 'excel',
                text: 'Excel',
                title: '',
                filename: OutputFilename
            },
            {
                extend: 'print',
                text: 'Print',
                title: OutputFilename
            }
        ];
    }
    var DatatableSettings = {
        "preDrawCallback": function (settings) {
            pageScrollPos = $('div.dataTables_scrollBody:visible').scrollTop();
        },
        "drawCallback": function (settings) {
            $('div.dataTables_scrollBody:visible').scrollTop(pageScrollPos);
        },
        stateSave: true,
        paging: false,
        ordering: orderable,
        scrollY: 656,
        scrollX: true,
        autoWidth: true,
        scrollCollapse: true,
        buttons: OutputButtons,
        dom: 'Blfrtip'
    };
    if (!AllowScroll) {
        DatatableSettings.scrollY = "";
        DatatableSettings.scrollCollapse = false;
    }

    var dt = $("#" + tblId).DataTable(DatatableSettings);

    RemoveTableFeatures();

    return dt;
}

function RemoveTableFeatures(tblId) {
    //Remove Search
    $(".dataTables_filter").hide();

    //Remove Row Info
    $(".dataTables_info").hide();

    //Position button panel
    $(".dt-buttons").css("margin-top", "26px");

    //Style export buttons
    $(".dt-button").addClass("w3-button w3-teal w3-small");
}

function UpdateValues() {
    if (!$("#btnUpdateValues").hasClass("w3-disabled")) {
        var bContinue = confirm("This will update " + g_DVPChanges.length + " records in the PTI database for this invoicing period. Do you want to continue?");

        if (bContinue) {
            var TotalRowsUpdated = 0;
            $(g_DVPChanges).each(function () {
                var rowId = this.InvoicingPeriodDataId;
                //Spent Total
                if (this.SPENT_TOTAL_DIFF != '0.0000') {
                    UpdatePTIValue(rowId, "SpentTotal", this.DVP_SPENT_TOTAL);
                }
                //Exp Unbilled
                if (this.EXP_UNBILLED_DIFF != '0.0000') {
                    UpdatePTIValue(rowId, "ExpenseUnbilled", this.DVP_EXP_UNBILLED);
                }
                //WIP Total
                if (this.WIP_TOTAL_DIFF != '0.0000') {
                    UpdatePTIValue(rowId, "WIP_Total", this.DVP_WIP_TOTAL);
                }
                //WIP Current
                if (this.WIP_CURRENT_DIFF != '0.0000') {
                    UpdatePTIValue(rowId, "WIP_Current", this.DVP_WIP_CURRENT);
                }
                //WIP HELD
                if (this.WIP_HELD_DIFF != '0.0000') {
                    UpdatePTIValue(rowId, "WIP_Held", this.DVP_WIP_HELD);
                }
                TotalRowsUpdated += 1;
            });
            $("#btnUpdateValues").addClass("w3-disabled").attr("title", "PTI rows have already been updated with values shown in this view.");
            $("#btnCancel").html("Finish");
            alert(TotalRowsUpdated + " record(s) were updated for this invoicing period.");
        }
    }
}

function UpdatePTIValue(RowId, FieldName, NewValue) {
    return new Promise(function (resolve, reject) {
        $.ajax({
            url: g_root + "/AjaxFunctions/SaveValue",
            type: "POST",
            data: JSON.stringify({ "Id": RowId, "TableName": "InvoicingPeriodData", "FieldName": FieldName, "NewValue": NewValue }),
            contentType: "text/plain; charset=UTF-8",
            success: function (data) {
                response = data.trim();
                if (response == "success") {
                    resolve(response);
                }
                else {
                    reject(response);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                var errorMsg = "Error in SaveValue. " + textStatus + ": " + errorThrown;
                console.log(errorMsg);
                reject(errorMsg);
            }
        });
    });
}


function fmtMoney(val) {
    var money = new Intl.NumberFormat('en-US', {
        style: 'currency',
        currency: 'USD'
        // These options will to round to whole numbers as needed.
        //minimumFractionDigits: 0, // (this suffices for whole numbers, but will print 2500.10 as $2,500.1)
        //maximumFractionDigits: 0, // (causes 2500.99 to be printed as $2,501)
    });
    return money.format(val);
}