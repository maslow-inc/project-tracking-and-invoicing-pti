﻿var g_InvoicingChangeRefreshPage = window.location.href;

$(function () {
    var PeriodLabel = $("#lblCurrentActivePeriodName").html().replace(/<.*>/ig, "") + "\r\n\r\n";
    if ($("#lblCurrentActivePeriodName").data("periodopen") == "False") {
        //This is a closed invoicing period
        $("#divActivePeriod").removeClass("w3-light-gray");
        $("#divActivePeriod").addClass("w3-orange");
        $("#divActivePeriod").attr("title", PeriodLabel + "\tThis period is CLOSED.\r\n\tAll projects are READ-ONLY.\r\n\r\nClick to select another period.");
    }
    else {
        //Invoicing period is open
        $("#divActivePeriod").addClass("w3-light-gray");
        $("#divActivePeriod").removeClass("w3-orange");
        $("#divActivePeriod").attr("title", PeriodLabel + "\tThis period is OPEN.\r\n\r\nClick to select a another period.");
    }
});

function LoadClosedInvoicingPeriods() {
    $.ajax({
        url: g_root + "/AjaxFunctions/GetAllRows/InvoicingPeriod",
        type: "POST",
        data: "",
        contentType: "text/plain; charset=UTF-8",
        success: function (data) {
            var jData = JSON.parse(data.trim());
            var ClosedInvoicingPeriods = jData.filter(x => { return x.StatusId === g_InvoicingPeriodClosedStatus });
            $("#selClosedInvoicingPeriods").html("");
            AddOptionToList("selClosedInvoicingPeriods", 0, "-- Select a closed period --");
            if (ClosedInvoicingPeriods.length > 0) {
                ClosedInvoicingPeriods.sort(SortByProperty("InvoicingPeriodName"));
                $(ClosedInvoicingPeriods).each(function () {
                    var PeriodStartDate = FormatDateDisplay(this.StartDate);
                    var PeriodEndDate = FormatDateDisplay(this.EndDate);
                    AddOptionToList("selClosedInvoicingPeriods", this.Id, this.InvoicingPeriodName + " (" + PeriodStartDate + " - " + PeriodEndDate + ")");
                });
                if (g_UserSelectedInvoicingPeriod.length > 0) {
                    var jUserSelectedInvoicingPeriod = JSON.parse(g_UserSelectedInvoicingPeriod);
                    $("#selClosedInvoicingPeriods").val(jUserSelectedInvoicingPeriod.Id);
                }
            }
            else {
                AddOptionToList("selClosedInvoicingPeriods", "0", "No Closed Invoicing Periods Found");
            }
        }
    });
};

function ReturnToCurrentInvoicingPeriod() {
    InvoicingPeriodAction("ReturnToCurrentInvoicingPeriod", JSON.stringify({}), '');
}

function SetClosedInvoicingPeriod() {
    var ClosedInvoicingPeriodId = $("#selClosedInvoicingPeriods").val();
    if (ClosedInvoicingPeriodId == "0") {
        //alert("Select a closed invoicing period.");
    }
    else {
        var PeriodId = $("#selClosedInvoicingPeriods").val();
        var PeriodName = $("#selClosedInvoicingPeriods option:selected").text();
        var PostData = { "Id": PeriodId, "Name": PeriodName };
        InvoicingPeriodAction("SetPeriodForSession", JSON.stringify(PostData), '');
    }
}

function InvoicingPeriodAction(ActionName, ActionData, SuccessAlertMsg) {
    $("button").attr("title", "Invoicing Period Action in Progress. Please wait...");
    $("button").addClass("w3-disabled");
    $.ajax({
        url: g_root + "/AjaxFunctions/InvoicingPeriod/" + ActionName,
        type: "POST",
        data: ActionData,
        contentType: "text/plain; charset=UTF-8",
        success: function (data) {
            if (data.trim().toLowerCase() == "success") {
                if (SuccessAlertMsg.length > 0) {
                    alert(SuccessAlertMsg);
                }
                window.location = g_InvoicingChangeRefreshPage;
            }
            else {
                alert(data.trim());
            }
            $("button").attr("title", "");
            $("button").removeClass("w3-disabled");
        },
        error: function (jqXHR, textStatus, errorThrown) {
            var errorMsg = "Error performing invoicing period action '" + ActionName + "'. " + textStatus + ": " + errorThrown;
            alert(errorMsg);
            $("button").attr("title", "");
            $("button").removeClass("w3-disabled");
        }
    });
}

/*Cookie Functions*/
function setCookie(cname, cvalue, exdays) {
    const d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    let expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function getCookie(cname) {
    let name = cname + "=";
    let ca = document.cookie.split(';');
    for (let i = 0; i < ca.length; i++) {
        let c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

function checkCookie() {
    let user = getCookie("username");
    if (user != "") {
        alert("Welcome again " + user);
    } else {
        user = prompt("Please enter your name:", "");
        if (user != "" && user != null) {
            setCookie("username", user, 365);
        }
    }
}