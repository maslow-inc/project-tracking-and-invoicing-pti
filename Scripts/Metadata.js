﻿//MetadataType variables
var MetadataTypeTable; //variable for datatable
var MetadataTypeHTMLTemplate = "<li class='w3-display-container'><label class='editable' id='lblEdit_~rowid~' title='Edit' onclick='EditFieldClick(this);'>~value~</label> <span data-rowid='~rowid~' onclick='DeleteRecord(this, \"MetadataType\");' title='Delete' class='w3-button w3-transparent w3-display-right'>&times;</span></li>";
var MetadataTypeJSON;

//Metadata variables
var MetadataTable; //variable for datatable
var MetadataHTMLTemplate = "<tr><td><a href='#edit' onclick='OpenMetadataEditForm(this);' ~editlink~><i class='fa fa-pencil-square-o' aria-hidden='true'></i></a><a href='#delete' ~deletelink~ style='margin-left:10px;' data-rowid='~rowid~' onclick='DeleteRecord(this, \"Metadata\");'><i class='fa fa-times' aria-hidden='true'></i></a><td>~type~</td><td>~value~</td><td style='text-align:center;'><a href='javascript:/**/;' data-rowid='~rowid~' title='View/Edit Connections' onclick='OpenMetadataPredicatesEditForm(this);'>~connections~</a></td><td style='text-align:center;'>~activeflag~</td><td style='text-align:center;'>~systemflag~</td></tr>";
var MetadataJSON;

var rowCheckIcon = "<i class='fa fa-check' aria-hidden='true'></i>";

$(function () {
    //Load Data
    LoadData();
});

function LoadData() {
    LoadList("MetadataType", MetadataTypeHTMLTemplate, "MetadataTypeList");
    window.setTimeout(function () {
        //Wait for MetadataType to load before loading Metadata
        LoadList("Metadata", MetadataHTMLTemplate, "MetadataList");
    }, 450);
}

function LoadList(TableName, RowTemplate, TargetListId) {
    var postData = {};
    $.ajax({
        url: g_root + "/AjaxFunctions/GetAllRows/" + TableName,
        type: "POST",
        data: JSON.stringify(postData),
        contentType: "text/plain; charset=UTF-8",
        success: function (data) {
            var jData = JSON.parse(data.trim());
            if (TableName == "MetadataType") {
                jData = jData.sort(SortByProperty("Name"));
                MetadataTypeJSON = jData;
                $("#MetadataTypeList").html("");
            }
            if (TableName == "Metadata") {
                jData = jData.sort(SortByProperty("Value"));
                MetadataJSON = jData;
                try {
                    MetadataTable.destroy();
                    $("#MetadataList").html("");
                } catch (e) {
                    //console.log("Error destroying MetadataTable: '" + e.Message + "'");
                }
            }
            //console.log(jData);
            $(jData).each(function () {
                var ThisRow;
                var bOkToDisplay = true;
                if (TableName == "MetadataType") {
                    var DisplayName = htmlEncode(this.Name);
                    ThisRow = RowTemplate.replace(/~rowid~/ig, this.Id).replace(/~value~/ig, DisplayName);
                }
                if (TableName == "Metadata") {
                    var bActive = (this.ActiveFlag == "True");
                    var bSystem = (this.SystemFlag == "True");
                    var DisplayValue = htmlEncode(this.Value);
                    var DisplayTypeName = htmlEncode(MetadataTypeJSON.find(x => { return x.Id === this.TypeId }).Name);
                    var Predicates = [];
                    if (this.Predicates.trim().length > 0) {
                        Predicates = JSON.parse(this.Predicates);
                    }
                    ThisRow = RowTemplate.replace(/~rowid~/ig, this.Id).replace(/~value~/ig, DisplayValue);
                    ThisRow = ThisRow.replace(/~connections~/ig, Predicates.length.toString());
                    ThisRow = ThisRow.replace(/~type~/ig, DisplayTypeName);
                    ThisRow = ThisRow.replace(/~activeflag~/ig, (bActive) ? rowCheckIcon : "");
                    ThisRow = ThisRow.replace(/~systemflag~/ig, (bSystem) ? rowCheckIcon : "")
                    ThisRow = ThisRow.replace(/~editlink~/ig, (bSystem) ? "class='w3-disabled' title='Edit unavailable for System Metadata'" : "title='Edit' data-rowdata='" + escape(JSON.stringify(this)) + "'");
                    ThisRow = ThisRow.replace(/~deletelink~/ig, (bSystem) ? "class='w3-disabled' title='Delete unavailable for System Metadata'" : "title='Delete'");
                }
                if (bOkToDisplay) {
                    $("#" + TargetListId).append(ThisRow);
                }
            });
            InitDataTable(TableName);
        }
    });
}

function OpenMetadataEditForm(FormOpener) {
    if (!$(FormOpener).hasClass("w3-disabled")) {
        //Set up Metadata Types select list
        $("#selMetadataType").html("");
        AddOptionToList("selMetadataType", "0", "Select a Type");
        $(MetadataTypeJSON).each(function () {
            AddOptionToList("selMetadataType", this.Id, this.Name);
        });

        if ($(FormOpener).data("rowdata")) {
            //Editing Existing Record
            var CurrentRowData = JSON.parse(unescape($(FormOpener).data("rowdata")));
            $("#txtMetadataId").val(CurrentRowData.Id);
            $("#selMetadataType").val(CurrentRowData.TypeId);
            $("#txtMetadataValue").val(CurrentRowData.Value);
            $("#txtMetadataPredicates").val(CurrentRowData.Predicates);
            $("#chkActiveFlag").prop("checked", (CurrentRowData.ActiveFlag == "True"));
            $("#chkSystemFlag").prop("checked", (CurrentRowData.SystemFlag == "True"));
            $('#MetadataEditForm').show();
        }
        else {
            //Adding New Record
            $("#txtMetadataId").val("0");
            $("#selMetadataType").val("0");
            $("#txtMetadataValue").val("");
            $("#txtMetadataPredicates").val("");
            $("#chkActiveFlag").prop("checked", true);
            $("#chkSystemFlag").prop("checked", false);
            $('#MetadataEditForm').show();
        }
    }
}

function OpenMetadataPredicatesEditForm(FormOpener) {
    var RowId = $(FormOpener).data("rowid");
    $("#MetadataPredicatesRowId").val(RowId);
    var RowPredicates = [];
    var RowData = MetadataJSON.find(x => { return x.Id === RowId });
    if (RowData) {
        var ThisRowType = MetadataTypeJSON.find(x => { return x.Id === RowData.TypeId }).Name;
        $("#MetadataPredicatesHeader").html("Metadata Connections for " + htmlEncode(ThisRowType) + " '" + htmlEncode(RowData.Value) + "'");
        if (RowData.Predicates.trim().length > 0) {
            RowPredicates = JSON.parse(RowData.Predicates);
        }
    }

    //Metadata Select Options
    var MetadataSelectOptions = "";
    var AllowedConnections = MetadataJSON.filter(x => { return x.Id !== RowId });
    var ConnectionOptionList = []
    $(AllowedConnections).each(function () {
        var ConnectionType = MetadataTypeJSON.find(x => { return x.Id == this.TypeId }).Name;
        var ThisOption = { "OptionValue": this.Id, "ConnectionType": ConnectionType, "OptionText": this.Value, "OptionDisplayText": ConnectionType + ": " + this.Value };
        ConnectionOptionList.push(ThisOption);
    });
    ConnectionOptionList = ConnectionOptionList.sort(SortByProperty("OptionDisplayText"));
    $(ConnectionOptionList).each(function () {
        MetadataSelectOptions += "<option data-connectiontype='" + htmlEncode(this.ConnectionType) + "' data-objectname='" + htmlEncode(this.OptionText) + "' value='" + htmlEncode(this.OptionValue) + "'>" + htmlEncode(this.OptionDisplayText) + "</option>";
    });

    //Predicate Select Options
    var PredicateSelectOptions = "";
    var PredicateTypeId = MetadataTypeJSON.find(x => { return x.Name === "MetadataPredicate" }).Id;
    var AllPredicates = MetadataJSON.filter(x => { return x.TypeId === PredicateTypeId });
    AllPredicates = AllPredicates.sort(SortByProperty("Value"));
    $(AllPredicates).each(function () {
        PredicateSelectOptions += "<option value='" + htmlEncode(this.Id) + "'>" + htmlEncode(this.Value) + "</option>";
    });

    //Display Existing Predicates
    var AllPredicateRowsHTML = "<h5>Current Connections</h5>";
    AllPredicateRowsHTML += "<div id='divAllPredicateRows' class='w3-round w3-padding w3-border' style='margin-top:-8px;'>";
    $(RowPredicates).each(function () {
        var PredicateLabel = MetadataJSON.find(x => { return x.Id.toString().toLowerCase() === this.Predicate.toString().toLowerCase() }).Value;
        var ConnectedObject = MetadataJSON.find(x => { return x.Id.toString().toLowerCase() === this.Object.toString().toLowerCase() });
        var ConnectedObjectName = ConnectedObject.Value;
        var ConnectedObjectTypeName = MetadataTypeJSON.find(x => { return x.Id == ConnectedObject.TypeId }).Name;
        AllPredicateRowsHTML += BuildPredicateRowHTML(this.Predicate, PredicateLabel, this.Object, ConnectedObjectTypeName, ConnectedObjectName);
    });
    AllPredicateRowsHTML += "</div>";

    //New Predicate Fields
    var NewPredicateFormHTML = "<h5>Add New Connection</h5>";
    NewPredicateFormHTML += "<div class='w3-round w3-padding w3-border' style='margin-top:-8px;'>";
    NewPredicateFormHTML += "<table class='w3-table'><thead><tr><th>Connection</th><th>Connected to</th></tr></thead><tbody><tr>";
    NewPredicateFormHTML += "<td><select id='selConnectionPredicates'>" + PredicateSelectOptions + "</select></td>";
    NewPredicateFormHTML += "<td><select id='selConnectionObjects' style='max-width:30vw;'>" + MetadataSelectOptions + "</select></td>";
    NewPredicateFormHTML += "<td><a href='javascript:/**/' title='Add connection' style='color:green;' onclick='AddMetadataPredicate(this);'><i class='fa fa-plus' aria-hidden='true'></i></a></td>";
    NewPredicateFormHTML += "</tr></tbody></table>";
    NewPredicateFormHTML += "</div>";

    $("#divMetadataPredicates").html(AllPredicateRowsHTML + NewPredicateFormHTML);

    //Show modal form
    $('#MetadataPredicatesEditForm').show();
}

function BuildPredicateRowHTML(PredicateId, PredicateLabel, ObjectId, ObjectType, ObjectLabel) {
    var PredicateRowHTML = "<div class='metadatapredicate' data-predicateid='" + htmlEncode(PredicateId) + "' data-objectid='" + htmlEncode(ObjectId) + "'>";
    PredicateRowHTML += "<a href='javascript:/**/' title='Delete connection' style='color:red;' onclick='DeleteMetadataPredicate(this);'><i class='fa fa-times' aria-hidden='true'></i></a>";
    PredicateRowHTML += "<label style='margin-left:12px;'>" + htmlEncode(PredicateLabel) + "&nbsp;<i class='fa fa-long-arrow-right' aria-hidden='true'></i>&nbsp;" + htmlEncode(ObjectType) + ":&nbsp;" + htmlEncode(ObjectLabel) + "</label>";
    PredicateRowHTML += "</div>";
    return PredicateRowHTML;
}

function AddMetadataPredicate(ClickedLink) {
    var PredicateId = $("#selConnectionPredicates option:selected").val();
    var PredicateLabel = $("#selConnectionPredicates option:selected").text();
    var ConnectionObjectId = $("#selConnectionObjects option:selected").val();
    var ConnectionObjectLabel = $("#selConnectionObjects option:selected").data("objectname");
    var ConnectionObjectType = $("#selConnectionObjects option:selected").data("connectiontype");
    var NewConnection = BuildPredicateRowHTML(PredicateId, PredicateLabel, ConnectionObjectId, ConnectionObjectType, ConnectionObjectLabel);
    var NewConnectionCheck = NewConnection.toLowerCase().replace(/\'/ig, "").replace(/\"/ig, "");
    var AllConnectionsCheck = $("#divAllPredicateRows").html().toLowerCase().replace(/\'/ig, "").replace(/\"/ig, "");
    var iDupeCheck = AllConnectionsCheck.indexOf(NewConnectionCheck);
    if (iDupeCheck == -1) {
        $("#divAllPredicateRows").append(NewConnection);
    }
    else {
        alert("Can't add connection as it would create a duplicate.");
    }
}

function SaveMetadataPredicates() {
    var NewPredicates = [];
    $(".metadatapredicate:visible").each(function () {
        var ThisPredicate = { "Predicate": $(this).data("predicateid"), "Object": $(this).data("objectid") };
        NewPredicates.push(ThisPredicate);
    });
    var RowId = $("#MetadataPredicatesRowId").val();
    $.ajax({
        url: g_root + "/AjaxFunctions/SaveValue",
        type: "POST",
        data: JSON.stringify({ "Id": RowId, "TableName": "Metadata", "FieldName": "Predicates", "NewValue": JSON.stringify(NewPredicates) }),
        contentType: "text/plain; charset=UTF-8",
        success: function (data) {
            if (data.trim().toLowerCase() == "success") {
                //Save Successful, hide dialog
                $('#MetadataPredicatesEditForm').hide();
            }
            else {
                alert(data.trim());
            }
        }
    });
}

function DeleteMetadataPredicate(ClickedLink) {
    if (confirm("Are you sure you want to remove this connection? Doing so may negatively impact system functionality.")) {
        $(ClickedLink).parent().hide();
    }
}

function InitDataTable(TableName) {
    if (TableName == "Metadata") {
        MetadataTable = $("#tblMetadata").DataTable({
            scrollY: 340,
            scrollCollapse: true,
            stateSave: true,
            "columnDefs": [
                { "orderable": false, "targets": [0, 4, 5] },
                { "width": "50px", "targets": [0, 4, 5] }
            ]
        });
    }
}

function NewMetadataTypeRow() {
    var RowId = "0";
    var NewRowHTML = MetadataTypeHTMLTemplate.replace(/~rowid~/ig, RowId).replace(/~value~/ig, "");
    $("#MetadataTypeList").append(NewRowHTML);
    var NewRowLabel = $("#lblEdit_" + RowId);
    $(NewRowLabel).data("isnew", true);
    $(NewRowLabel).trigger("click");
}

function EditFieldClick(EditField) {
    var RowId = $(EditField).attr("id").replace(/lblEdit_/ig, "");
    var RowVal = $(EditField).text();
    var EditBoxId = "txtEdit_" + RowId;
    var SaveButtonHTML = "<button type='button' id='btnSave_" + RowId + "' class='w3-disabled' title='Save' style='margin-left:5px;' onclick='SaveMetadataType(\"" + RowId + "\");'><i style='color:green;' class='fa fa-check' aria-hidden='true'></i></button>";
    var UndoButtonHTML = "<button type='button' id='btnUndo_" + RowId + "' title='Undo' style='margin-left:5px;' onclick='UndoChanges(\"" + RowId + "\");'><i style='color:blue;' class='fa fa-undo' aria-hidden='true'></i></button>";
    var EditBoxHTML = "<input type='text' id='" + EditBoxId + "' onkeyup='TextEditKeyup(this, event.key);' />";
    var EditSpanHTML = "<span id='spnEdit_" + RowId + "'>" + EditBoxHTML + SaveButtonHTML + UndoButtonHTML + "</span>";
    $(EditField).after(EditSpanHTML);
    $(EditField).hide();
    $("#" + EditBoxId).val(RowVal);
    $("#" + EditBoxId).focus().select();
}

function UndoChanges(RowId) {
    var EditSpan = $("#spnEdit_" + RowId);
    var EditLabel = $(EditSpan).siblings("label:first");
    $(EditLabel).show();
    $(EditSpan).remove();
    if ($(EditLabel).data("isnew")) {
        var RowToRemove = $(EditLabel).parent();
        $(RowToRemove).remove();
    }
}

function SaveMetadataType(RowId) {
    var SaveButton = $("#btnSave_" + RowId);
    if (!$(SaveButton).hasClass("w3-disabled")) {
        var NewValue = $("#txtEdit_" + RowId).val();

        //Save
        var RowData = { "Id": RowId, "Name": NewValue };
        $.ajax({
            url: g_root + "/AjaxFunctions/SaveRecord/MetadataType",
            type: "POST",
            data: JSON.stringify(RowData),
            contentType: "text/plain; charset=UTF-8",
            success: function (data) {
                if (data.trim().toLowerCase() == "success") {
                    //Save Successful, Update label and show it. Hide edit span
                    LoadData();
                }
                else {
                    alert(data.trim());
                }
            }
        });
    }
}

function SaveMetadata() {
    var Id = $("#txtMetadataId").val();
    var TypeId = $("#selMetadataType").val();
    var Value = $("#txtMetadataValue").val();
    var Predicates = $("#txtMetadataPredicates").val();
    var ActiveFlag = ($("#chkActiveFlag").prop("checked")) ? "1" : "0";
    var SystemFlag = ($("#chkSystemFlag").prop("checked")) ? "1" : "0";
    if (TypeId == "0" || Value.trim().length == 0) {
        alert("Type and Value are required.");
        return;
    }
    if (SystemFlag == "1") {
        if (!confirm("System values are not modifiable. Do you want to continue?")) {
            return;
        }
    }
    var RowData = { "Id": Id, "TypeId": TypeId, "Value": Value, "Predicates": Predicates, "ActiveFlag": ActiveFlag, "SystemFlag": SystemFlag };
    $.ajax({
        url: g_root + "/AjaxFunctions/SaveRecord/Metadata",
        type: "POST",
        data: JSON.stringify(RowData),
        contentType: "text/plain; charset=UTF-8",
        success: function (data) {
            if (data.trim().toLowerCase() == "success") {
                //Save Successful. Hide form and reload Metadata table
                $('#MetadataEditForm').hide();
                LoadList("Metadata", MetadataHTMLTemplate, "MetadataList");
            }
            else {
                alert(data.trim());
            }
        }
    });
}

function DeleteRecord(DeleteElement, TableName) {
    if (!$(DeleteElement).hasClass("w3-disabled")) {
        if (confirm("Are you sure you want to delete this record?")) {
            var RowId = $(DeleteElement).data("rowid");
            $.ajax({
                url: g_root + "/AjaxFunctions/DeleteRecord/" + TableName,
                type: "POST",
                data: RowId,
                contentType: "text/plain; charset=UTF-8",
                success: function (data) {
                    data = data.trim();
                    if (data.toLowerCase() == "success") {
                        //Save Successful
                        if (TableName.trim().toLowerCase() == "metadatatype") {
                            LoadData();
                        }
                        if (TableName.trim().toLowerCase() == "metadata") {
                            LoadList("Metadata", MetadataHTMLTemplate, "MetadataList");
                        }
                    }
                    else {
                        alert(data);
                    }
                }
            });
        }
    }
}

function TextEditKeyup(editbox, key) {
    var RowId = $(editbox).attr("id").replace(/txtEdit_/ig, "");
    if (key == 'Enter') {
        SaveMetadataType(RowId);
    }
    else if (key == 'Escape') {
        UndoChanges(RowId);
    }
    else {
        if ($(editbox).val().trim().length > 0) {
            $("#btnSave_" + RowId).removeClass("w3-disabled");
        }
        else {
            $("#btnSave_" + RowId).addClass("w3-disabled");
        }
    }
}