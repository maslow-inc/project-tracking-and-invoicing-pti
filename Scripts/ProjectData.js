﻿//Global script variables
var g_ProjectData;
var g_ProjectOnHold;
var g_ProjectStatus = {};
var g_PageKey;

//Page DataTable variables
var g_tblETC;
var g_tblPrjDetails;
var g_tblInvoicing;

//Tab Issues VariblesUsed to collect issues found by rules check
var g_ETCIssues = [];
var g_ProjectDetailsIssues = [];
var g_InvoicingIssues = [];

//Data Formatters
function fmtMoney(val) {
    var money = new Intl.NumberFormat('en-US', {
        style: 'currency',
        currency: 'USD'
        // These options will to round to whole numbers as needed.
        //minimumFractionDigits: 0, // (this suffices for whole numbers, but will print 2500.10 as $2,500.1)
        //maximumFractionDigits: 0, // (causes 2500.99 to be printed as $2,501)
    });
    return money.format(val);
}

function fmtPct(val) {
    var percent = new Intl.NumberFormat('en-US', {
        style: 'percent'
    });
    return percent.format(val);
}

function fmtNum(val, decimals) {
    var num = new Intl.NumberFormat('en-US', {
        minimumFractionDigits: decimals,
        maximumFractionDigits: decimals
    });
    return num.format(val);
}

function rawNum(val) {
    val = val.replace(/\$/ig, "").replace(/\%/ig, "").replace(/,/ig, "");
    return +val;
}

function CalcPct(dividend, divisor) {
    var quotient = 0;
    if (+divisor != 0) {
        quotient = dividend / divisor;
    }
    return fmtPct(quotient);
}

function empName(id) {
    var Name = "";
    var ThisEmp = g_AllEmployees.find(x => { return x.Id === id });
    if (ThisEmp) {
        Name = ThisEmp.LastName.trim() + ", " + ThisEmp.FirstName.trim();
    }
    return Name;
}

$(function () {
    //page ready
    CheckProjectId();

    //Capture keys on page
    $("#PageBody").on("keydown", function () {
        g_PageKey = event.key;
        if (event.shiftKey) g_PageKey = "Shift_" + g_PageKey;
    });
    $("#PageBody").on("click", function () {
        g_PageKey = "";
    });

    //Hide NEI footer for more vertical space
    $("footer.w3-theme-d1").hide();

    //Make modal dialog draggable with jqueryui
    $(".draggable").draggable({ handle: "#ModalHeading" });

    LoadProjectData();

    //Editable Datatable Cells//
    // Activate an inline edit on click of a table cell
    $('.w3-table').on('click', 'tbody td.editable', function (e) {
        var guid = NewGUID();
        if ($(this).data("guid")) {
            //this cell has a guid so it has been edited before
            CellEditOn($(this));
        }
        else {
            //first time editing, add guid to cell then edit
            $(this).data("guid", guid);
            CellEditOn($(this));
        }
    });

    //Click last opened tab
    var LastOpenedTab = getCookie("LastOpenedTab");
    var TabToClick = "";
    $("button").each(function () {
        if ($(this).data("tab") == LastOpenedTab) {
            TabToClick = this;
        }
    });
    if (TabToClick != "") {
        window.setTimeout(function () { $(TabToClick).trigger("click"); }, 500);
    }

});

function CheckProjectId() {
    var id = $("#id").val();
    if (id != g_InvoicingPeriodProjectIdForSelectedPeriod) {
        var CurrentPageURL = window.location.href;
        var RedirectPageURL;
        if (g_InvoicingPeriodProjectIdForSelectedPeriod.trim().length > 0) {
            RedirectPageURL = CurrentPageURL.replace(id, g_InvoicingPeriodProjectIdForSelectedPeriod);
        }
        else {
            alert("This project is not available in the selected invoicing period.\r\n\r\nReturning to All Projects list.");
            RedirectPageURL = CurrentPageURL.replace("ProjectData/" + id, "Projects");
        }
        window.location = RedirectPageURL;
    }
}

function LoadProjectData() {
    var id = $("#id").val();
    if (id == g_InvoicingPeriodProjectIdForSelectedPeriod) {
        $.ajax({
            url: g_root + "/AjaxFunctions/Project/AllData",
            type: "POST",
            data: $("#id").val(),
            contentType: "text/plain; charset=UTF-8",
            success: function (data) {
                g_ProjectData = JSON.parse(data.trim());
                //console.log("g_ProjectData:");
                //console.log(g_ProjectData);

                if (g_ProjectData.ProjectManagerName) {
                    //Project Manager Display Name
                    $("#lblEmployeeName").html(g_ProjectData.ProjectManagerName);

                    //Project On Hold
                    g_ProjectOnHold = (g_ProjectData.Project[0].OnHold == "Y");

                    //Load Project Status Object
                    g_ProjectStatus.Status = GetMetadataName(g_ProjectData.Project[0].StatusId);
                    g_ProjectStatus.InvoicingStatus = GetMetadataName(g_ProjectData.Project[0].InvoicingStatusId);
                    g_ProjectStatus.ApprovalStatus = GetMetadataName(g_ProjectData.Project[0].ApprovalStatusId);

                    //Set Client Name in Each Table
                    $(".lblClientName").each(function () {
                        $(this).html(g_ProjectData.ProjectClientName);
                    });

                    //Update Admin Flags
                    SetProjectFlag("DVPDataEntryComplete");
                    SetProjectFlag("InvoiceSentToPM");

                    //Update Carry All Notes Forward Flags
                    SetProjectFlag("ETCCarryNotesForward");
                    if ($("#chkETCCarryNotesForward").prop("checked")) $("#btnETCCarryNotesForward").addClass("w3-disabled");
                    SetProjectFlag("ProjectCarryNotesForward");
                    if ($("#chkProjectCarryNotesForward").prop("checked")) $("#btnProjectCarryNotesForward").addClass("w3-disabled");
                    SetProjectFlag("InvoicingCarryNotesForward");
                    if ($("#chkInvoicingCarryNotesForward").prop("checked")) $("#btnInvoicingCarryNotesForward").addClass("w3-disabled");

                    //Estimate to Complete table
                    BuildETCTable();

                    //Project Details table
                    BuildPrjDetailsTable();

                    //Invoicing Table
                    BuildInvoicingTable();

                    //Adjust page based on project status
                    UpdatePageDisplay();
                }
                else {
                    history.back();
                }
            }
        });
    }
}

function GetMetadataName(Id) {
    var Name = "";
    var Metadata = g_TableMetadata.find(x => { return x.MetadataId === Id });
    if (Metadata) {
        Name = Metadata.MetadataName;
    }
    return Name;
}

function SaveValue(RowId, FieldName, NewValue, TableName) {
    //TableName is optional. Default to InvoicingPeriodData as that is where the bulk of data are saved except for notes and questions/responses
    if (!TableName) {
        TableName = "InvoicingPeriodData";
    }
    return new Promise(function (resolve, reject) {
        $.ajax({
            url: g_root + "/AjaxFunctions/SaveValue",
            type: "POST",
            data: JSON.stringify({ "Id": RowId, "TableName": TableName, "FieldName": GetFieldNameFromAlias(FieldName), "NewValue": NewValue}),
            contentType: "text/plain; charset=UTF-8",
            success: function (data) {
                response = data.trim();
                if (response == "success") {
                    resolve(response);
                }
                else {
                    reject(response);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                var errorMsg = "Error in SaveValue. " + textStatus + ": " + errorThrown;
                console.log(errorMsg);
                reject(errorMsg);
            }
        });
    });
}

function GetFieldNameFromAlias(Alias) {
    var ReturnVal = Alias;
    var FieldAliases = [{ "Alias": "CurrentRate", "FieldName": "AvgBillRate" }, { "Alias": "EstimateToComplete", "FieldName": "ETC" }, { "Alias": "PhaseMgrUpdated", "FieldName": "PhaseMgrEmployeeId_Updated" }, { "Alias": "EstEndDateUpdated", "FieldName": "EstEndDate_Updated" }, { "Alias": "RevenueMethodUpdated", "FieldName": "RevenueMethod_Updated" }, {"Alias": "WIPToClose", "FieldName": "WIP_To_Close"}];
    var FoundAlias = FieldAliases.find(x => { return x.Alias === Alias });
    if (FoundAlias) {
        ReturnVal = FoundAlias.FieldName;
    }
    return ReturnVal;
}

function PerformCustomCalculation(Cell) {
    var CellParent = $(Cell).parent();
    var ProjectId = $(CellParent).data("projectid");
    var PhaseId = $(CellParent).data("phaseid");
    var SubphaseId = $(CellParent).data("subphaseid");
    var DetailId = $(CellParent).data("detailid");
    var SubphaseDetail = g_ProjectData.Detail.find(x => { return x.InvoicingPeriodProjectId === ProjectId && x.InvoicingPeriodPhaseId === PhaseId && x.InvoicingPeriodSubphaseId === SubphaseId });
    var CellId = $(Cell).attr("id");
    var FieldName = CellId.split("_")[0].replace(/subphase/ig, "");
    var DataRowId = CellId.split("_")[1];
    var SpentTotal = rawNum($("#subphaseSpentTotal_" + DataRowId).html());
    var SubphaseCompensation = rawNum($("#subphaseCompensation_" + DataRowId).html());
    var EstimateToComplete;
    var SubphaseCalcFPC;
    
    switch (FieldName.trim().toLowerCase()) {
        case "estimatetocomplete":
            SaveValue(DataRowId, "HoursToComplete", "0");
            $("#subphaseHoursToComplete_" + DataRowId).html("0.00");
            SaveValue(DataRowId, "EstimateExpenses", "0");
            $("#subphaseEstimateExpenses_" + DataRowId).html("$0.00");
            SaveValue(DataRowId, "Risk", "0");
            $("#subphaseRisk_" + DataRowId).html("$0.00");
            //Update Suphase Calc EAC and Calc FPC
            EstimateToComplete = rawNum($("#subphaseEstimateToComplete_" + DataRowId).html());
            $("#subphaseCalcEAC_" + DataRowId).html(fmtMoney(+EstimateToComplete + +SpentTotal)); //Calc EAC = [Estimate to Complete] + [Spent Total]
            //Calc FPC (see Rule 11): If Compensation = 0 then Calc FPC = 0% else Calc FPC = [Spent Total] / [Calc EAC] %
            //Rule 11 changed per Paul P 2022-04-22: If ETC = 0 then Calc FPC = 100% else Calc FPC = [Spent Total] / [Calc EAC] %
            SubphaseCalcFPC = (fmtMoney(+EstimateToComplete) == "$0.00") ? CalcPct(1,1) : CalcPct(+SpentTotal, (+EstimateToComplete + +SpentTotal));
            $("#subphaseCalcFPC_" + DataRowId).html(SubphaseCalcFPC); //Calc FPC = [Spent Total] / [Calc EAC] %
            break;
        case "hourstocomplete":
            //Round to nearest .25
            var WorkingHoursToComplete = rawNum($("#subphaseHoursToComplete_" + DataRowId).html()).toString();
            var WorkingHoursToComplete = (Math.round(WorkingHoursToComplete * 4) / 4).toFixed(2);
            $("#subphaseHoursToComplete_" + DataRowId).html(WorkingHoursToComplete);
        case "currentrate":
        case "estimateexpenses":
        case "risk":
            //Estimate to Complete = ([# Hours to Complete] * [Current Rate]) + [Estimated Expenses] + [Risk]
            var HoursToComplete = rawNum($("#subphaseHoursToComplete_" + DataRowId).html());
            var CurrentRate = rawNum($("#subphaseCurrentRate_" + DataRowId).html());
            var EstimateExpenses = rawNum($("#subphaseEstimateExpenses_" + DataRowId).html());
            var Risk = rawNum($("#subphaseRisk_" + DataRowId).html());
            EstimateToComplete = (+HoursToComplete * +CurrentRate) + +EstimateExpenses + +Risk;
            EstimateToComplete = rawNum(fmtNum(EstimateToComplete));
            SaveValue(DataRowId, "EstimateToComplete", EstimateToComplete);
            $("#subphaseEstimateToComplete_" + DataRowId).html(fmtMoney(EstimateToComplete));
            //Update Suphase Calc EAC and Calc FPC
            $("#subphaseCalcEAC_" + DataRowId).html(fmtMoney(+EstimateToComplete + +SpentTotal)); //Calc EAC = [Estimate to Complete] + [Spent Total]
            //Calc FPC (see Rule 11): If Compensation = 0 then Calc FPC = 0% else Calc FPC = [Spent Total] / [Calc EAC] %
            //Rule 11 changed per Paul P 2022-04-22: If ETC = 0 then Calc FPC = 100% else Calc FPC = [Spent Total] / [Calc EAC] %
            SubphaseCalcFPC = (fmtMoney(+EstimateToComplete) == "$0.00") ? CalcPct(1, 1) : CalcPct(+SpentTotal, (+EstimateToComplete + +SpentTotal));
            $("#subphaseCalcFPC_" + DataRowId).html(SubphaseCalcFPC); //Calc FPC = [Spent Total] / [Calc EAC] %
            break;
        case "invoicingnewinvoicedpct":
            var NewInvoicedPct = rawNum($("#subphaseInvoicingNewInvoicedPct_" + DataRowId).html());
            NewInvoicedPct = Math.round(NewInvoicedPct);
            if (NewInvoicedPct.toString() == "NaN") {
                //Invalid number
                $("#subphaseInvoicingNewInvoicedPct_" + DataRowId).html("0%");
            }
            else if (+NewInvoicedPct < 0 || +NewInvoicedPct > 100) {
                //Only allow 0 - 100 pct
                $("#subphaseInvoicingNewInvoicedPct_" + DataRowId).html("0%");
            }
            else {
                //Calculate Amt to Invoice = (NewInvoicedPct * Compensation) - Invoiced
                var Invoiced = rawNum($("#subphaseInvoicingInvoicedTotal_" + DataRowId).html());
                var AmtToInvoice = ((+NewInvoicedPct / 100) * SubphaseCompensation) - Invoiced;
                if (+AmtToInvoice >= 0) {
                    $("#subphaseInvoicingAmountToInvoice_" + DataRowId).html(fmtMoney(AmtToInvoice));
                    SaveValue(DataRowId, "AmountToInvoice", AmtToInvoice);
                    SubphaseDetail.NewInvoicedPct = NewInvoicedPct;
                }
                else {
                    //Amt to Invoice may not be negative
                    $("#subphaseInvoicingNewInvoicedPct_" + DataRowId).html("0%");
                }
            }
            break;
        default:
    }

}

function ProjectOnHold_click() {
    if (!$("#btnProjectOnHold").hasClass("w3-disabled")) {
        var ProjectNotes = {};
        if (g_ProjectData.Project[0].Notes.length > 0) {
            ProjectNotes = JSON.parse(g_ProjectData.Project[0].Notes);
        }
        var btnText = $("#btnProjectOnHold").text();
        if (confirm(btnText + "?")) {
            if (btnText.toLowerCase().indexOf("cancel") > -1) {
                //Remove On-Hold note for project
                delete ProjectNotes["On-Hold"];
            }
            else {
                ProjectNotes["On-Hold"] = btnText;
            }
        }
        var DataRowId = g_ProjectData.Project[0].Id;
        SaveValue(DataRowId, "Notes", JSON.stringify(ProjectNotes), "InvoicingPeriodProject").then(
            function (response) {
                if (response == "success") {
                    g_ProjectData.Project[0].Notes = JSON.stringify(ProjectNotes);
                    UpdateProjectOnHoldDisplay();
                }
                else {
                    alert(response);
                }
            },
            function (errormsg) {
                alert('Error saving change: ' + errormsg);
            }
        );

    }

}

function UpdateProjectOnHoldDisplay() {
    var ProjectNotes = "";
    if (g_ProjectData.Project[0].Notes.length > 0) {
        ProjectNotes = JSON.parse(g_ProjectData.Project[0].Notes);
    }

    if (g_ProjectOnHold) {
        if (ProjectNotes["On-Hold"]) {
            if (ProjectNotes["On-Hold"] == "Take Project Off Hold") {
                $("#btnProjectOnHold").text("Cancel Take Project Off Hold");
            }
            else {
                $("#btnProjectOnHold").text("Take Project Off Hold");
            }
        }
        else {
            $("#btnProjectOnHold").text("Take Project Off Hold");
        }
    }
    else {
        if (ProjectNotes["On-Hold"] == "Put Project On Hold") {
            $("#btnProjectOnHold").text("Cancel Put Project On Hold");
        }
        else {
            $("#btnProjectOnHold").text("Put Project On Hold");
        }
    }
}

function InvoiceAllWIP() {
    if (!$("#btnInvoiceAllWIP").hasClass("w3-disabled")) {
        if (confirm("Are you sure you want to invoice all WIP?")) {
            $(".wipcheck.warn").each(function () {
                if ($(this).attr("id").indexOf("subphaseWIPCheck") > -1) {
                    var AmtToInvoiceId = $(this).attr("id").replace(/subphaseWIPCheck/ig, "subphaseInvoicingAmountToInvoice");
                    var AmtToInvoice = rawNum($("#" + AmtToInvoiceId).html());
                    var RowId = AmtToInvoiceId.replace(/subphaseInvoicingAmountToInvoice_/ig, "");
                    var NewAmtToInvoice = 0 - rawNum($(this).html()) + AmtToInvoice;
                    SaveValue(RowId, "AmountToInvoice", NewAmtToInvoice).then(
                        function (response) {
                            if (response == "success") {
                                $("#" + AmtToInvoiceId).html(fmtMoney(NewAmtToInvoice));
                                RecalculateTable("tblInvoicing");
                            }
                            else {
                                alert(response);
                            }
                        },
                        function (errormsg) {
                            alert('Error updating AmountToInvoice in Invoice All WIP Process: ' + errormsg);
                        }
                    );
                }
            });
        }
    }
}

function CarryNotesForward(btn, NotesType) {
    if (!$(btn).hasClass("w3-disabled")) {
        if (confirm("Are you sure you want to populate the notes on this tab with the ones from the prior Invoicing Period?")) {
            var InvoicingPeriodProjectId = $("#id").val();
            var PostData = { "InvoicingPeriodProjectId": InvoicingPeriodProjectId, "NotesType": NotesType };
            $.ajax({
                url: g_root + "/AjaxFunctions/Project/CarryNotesForward",
                type: "POST",
                data: JSON.stringify(PostData),
                contentType: "text/plain; charset=UTF-8",
                success: function (data) {
                    if (data.trim() == "success") {
                        //Save hidden checkbox flag indicating notes have been carried forward
                        var chkId = $(btn).attr("id").replace(/btn/ig, "chk");
                        var chkFlag = $("#" + chkId);
                        $(chkFlag).prop("checked", true);
                        location.reload();
                    }
                    else {
                        alert(data);
                    }
                }
            });
        }
    }
}

function btnSubmit_click() {
    //Update status based on current project status in g_ProjectStatus
    var bConfirmed = false;
    var PostData;
    switch (g_ProjectStatus.Status) {
        case "Not Started":
        case "Started":
            if (confirm("Are you sure you want to Submit this project?")) {
                bConfirmed = true;
                PostData = { "Id": $("#id").val(), "StatusType": "Status", "Status": "Submitted" };
            }
            break;
        case "Submitted":
            alert("This project has already been submitted.");
        default:
    }

    if (bConfirmed) {
        $.ajax({
            url: g_root + "/AjaxFunctions/Project/UpdateStatus",
            type: "POST",
            data: JSON.stringify(PostData),
            contentType: "text/plain; charset=UTF-8",
            success: function (data) {
                if (data.trim() == "success") {
                    history.back();
                }
                else {
                    alert(data);
                }
            }
        });

    }
}

function btnApprove_click() {
    //Update status based on current project statuses in g_ProjectStatus
    var bConfirmed = false;
    var PostData;
    if (g_ProjectStatus.Status == "Submitted" && g_ProjectStatus.ApprovalStatus == "Not Reviewed") {
        if (confirm("Are you sure you want to Approve this project for Invoicing Review?")) {
            bConfirmed = true;
            PostData = { "Id": $("#id").val(), "StatusType": "ApprovalStatus", "Status": "Approved" };
        }
    }
    if (g_ProjectStatus.ApprovalStatus == "Approved" && (g_ProjectStatus.InvoicingStatus == "Not Started" || g_ProjectStatus.InvoicingStatus == "Questions" || g_ProjectStatus.InvoicingStatus.trim().length == 0)) {
        if (confirm("Are you sure you want to Approve this project for Final Invoicing?")) {
            bConfirmed = true;
            PostData = { "Id": $("#id").val(), "StatusType": "InvoicingStatus", "Status": "Approved" };
        }
    }
    if (bConfirmed) {
        $.ajax({
            url: g_root + "/AjaxFunctions/Project/UpdateStatus",
            type: "POST",
            data: JSON.stringify(PostData),
            contentType: "text/plain; charset=UTF-8",
            success: function (data) {
                if (data.trim() == "success") {
                    history.back();
                }
                else {
                    alert(data);
                }
            }
        });

    }

}

function btnUndo_click() {
    var ButtonMode = $("#btnUndo").data("mode");
    var PostData = { "Id": $("#id").val(), "UndoAction": ButtonMode };
    $.ajax({
        url: g_root + "/AjaxFunctions/Project/UndoStatus",
        type: "POST",
        data: JSON.stringify(PostData),
        contentType: "text/plain; charset=UTF-8",
        success: function (data) {
            if (data.trim() == "success") {
                history.back();
            }
            else {
                alert(data);
            }
        }
    });
}

function BuildETCTable() {
    var NotesTitle = "Add/Edit Notes";
    var TableBody = $("#tblETCData");
    var RowTemplate = "<tr class='etctab ~rowclass~ thin' data-projectid='~projectid~' data-phaseid='~phaseid~' data-subphaseid='~subphaseid~' data-detailid='~detailid~' data-metadataextension='~metadataextension~'><td class='dataentryshowhide' ~showhideevent~>~showhideglyph~</td><td class='etctab ~descclass~'>~desc~</td><td>~phasestatus~</td><td>~manager~</td><td class='etctab moneycell'>~tentelimit~</td><td class='etctab moneycell'>~compensation~</td><td class='etctab moneycell'>~invoiced~</td><td class='etctab moneycell'>~spenttotal~</td><td class='etctab pctcell'>~spentpct~</td><td class='etctab moneycell'>~variance~</td><td class='etctab numcell ~editable~ ~highlight~' data-customcalcs='true'>~hourstocomplete~</td><td class='etctab moneycell ~editable~' data-customcalcs='true'>~currentrate~</td><td class='etctab moneycell ~editable~ ~highlight~' data-customcalcs='true'>~estimateexpenses~</td><td class='etctab moneycell ~editable~ ~highlight~' data-customcalcs='true'>~risk~</td><td class='etctab moneycell ~editable~' data-customcalcs='true'>~estimatetocomplete~</td><td class='etctab moneycell'>~calceac~</td><td class='etctab pctcell'>~calcfpc~</td><td class='etctab moneycell'>~prioreac~</td><td class='etctab buttoncell'>~buttons~</tr>";
    var ProjectButtonsHTML = "<center><a href='javascript:/**/' class='noteslink' data-tab='ETC' data-rowtype='Project' data-id='~projectid~' title='" + NotesTitle + "' onclick='OpenModal(\"Notes\", this);'><i class='fa fa-file-o'></i></a><span id='ETCQuestion_~projectid~'></span><center>";
    var PhaseButtonsHTML = "<center><a href='javascript:/**/' class='noteslink' data-tab='ETC' data-rowtype='Phase' data-id='~phaseid~' title='" + NotesTitle + "' onclick='OpenModal(\"Notes\", this);'><i class='fa fa-file-o'></i></a><span id='ETCQuestion_~phaseid~'></span><center>";
    var SubphaseButtonsHTML = "<center><a href='javascript:/**/' class='noteslink' data-tab='ETC' data-rowtype='Subphase' data-id='~subphaseid~' title='" + NotesTitle + "' onclick='OpenModal(\"Notes\", this);'><i class='fa fa-file-o'></i></a><span id='ETCQuestion_~subphaseid~'></span><center>";

    //Table Rows
    $(TableBody).html("");
    var Projects = g_ProjectData.Project;
    $(Projects).each(function () {
        var ProjectId = this.Id;

        //Previous period Project Data Row
        var ProjectDataPreviousPeriod = g_ProjectDataPreviousPeriod[0];
        var LastMonthDataRow = RowTemplate.replace(/~buttons~/ig, ""); //No Buttons
        LastMonthDataRow = LastMonthDataRow.replace(/~projectid~/ig, ProjectId).replace(/~phaseid~/ig, '').replace(/~subphaseid~/ig, '').replace(/~detailid~/ig, ''); //Row Project/Phase/Subphase/Detail Ids
        LastMonthDataRow = LastMonthDataRow.replace(/~metadataextension~/ig, ''); //Metadata Extension Data
        LastMonthDataRow = LastMonthDataRow.replace(/~showhideevent~/ig, "").replace(/~showhideglyph~/ig, ""); //Row ShowHideDetail column
        LastMonthDataRow = LastMonthDataRow.replace(/~rowclass~/ig, "projectpriorperiod").replace(/~descclass~/ig, "maindesc"); //Row Class
        LastMonthDataRow = LastMonthDataRow.replace(/~editable~/ig, "").replace(/~highlight~/ig, ""); //Editable/Highlight Cell Classes
        LastMonthDataRow = LastMonthDataRow.replace(/~phasestatus~/ig, ""); //Phase Status
        LastMonthDataRow = LastMonthDataRow.replace(/~manager~/ig, ""); //Manager
        if (g_ProjectDataPreviousPeriod) {
            var prevTENTELimit = 0, prevCompensation = 0, prevInvoicedTotal = 0, prevSpentTotal = 0, prevHoursToComplete = 0, prevEstimateExpenses = 0, prevRisk = 0, prevETC = 0, prevPhaseEAC = 0;
            $(g_ProjectDataPreviousPeriod).each(function () {
                prevTENTELimit += +this.TENTELimit;
                prevCompensation += +this.Compensation;
                prevInvoicedTotal += +this.InvoicedTotal;
                prevSpentTotal += +this.SpentTotal;
                prevHoursToComplete += +this.HoursToComplete;
                prevEstimateExpenses += +this.EstimateExpenses;
                prevRisk += +this.Risk;
                prevETC += +this.ETC;
                prevPhaseEAC += +this.PhaseEAC;
            });
            LastMonthDataRow = LastMonthDataRow.replace(/~desc~/ig, "<span style='font-weight:bold;float:right;'>Prior Month Totals</span>"); //Desc
            LastMonthDataRow = LastMonthDataRow.replace(/>~tentelimit~/ig, " id='projectTENTELimitPriorPeriod_" + ProjectId + "'>" + fmtMoney(prevTENTELimit)); //Total T&E NTE Limit
            LastMonthDataRow = LastMonthDataRow.replace(/>~compensation~/ig, " id='projectCompensationPriorPeriod_" + ProjectId + "'>" + fmtMoney(prevCompensation)); //Compensation
            LastMonthDataRow = LastMonthDataRow.replace(/>~invoiced~/ig, " id='projectInvoicedTotalPriorPeriod_" + ProjectId + "'>" + fmtMoney(prevInvoicedTotal)); //Invoiced
            LastMonthDataRow = LastMonthDataRow.replace(/>~spenttotal~/ig, " id='projectSpentTotalPriorPeriod_" + ProjectId + "'>" + fmtMoney(prevSpentTotal)); //Spent Total
            LastMonthDataRow = LastMonthDataRow.replace(/>~spentpct~/ig, " id='projectSpentPCTPriorPeriod_" + ProjectId + "'>" + CalcPct(prevSpentTotal, prevCompensation)); //Spent PCT = [Spent Total] / [Compensation] %
            LastMonthDataRow = LastMonthDataRow.replace(/>~variance~/ig, " id='projectVariancePriorPeriod_" + ProjectId + "'>" + fmtMoney(prevCompensation - prevSpentTotal)); //Variance = [Compensation] - [Spent Total]
            LastMonthDataRow = LastMonthDataRow.replace(/>~hourstocomplete~/ig, " id='projectHoursToCompletePriorPeriod_" + ProjectId + "'>" + fmtMoney(prevHoursToComplete)); //Hours to Complete
            LastMonthDataRow = LastMonthDataRow.replace(/>~currentrate~/ig, " id='projectCurrentRatePriorPeriod_" + ProjectId + "'>"); //Current Rate - show blank for prior period
            LastMonthDataRow = LastMonthDataRow.replace(/>~estimateexpenses~/ig, " id='projectEstimateExpensesPriorPeriod_" + ProjectId + "'>" + fmtMoney(prevEstimateExpenses)); //Estimate Expenses
            LastMonthDataRow = LastMonthDataRow.replace(/>~risk~/ig, " id='projectRiskPriorPeriod_" + ProjectId + "'>" + fmtMoney(prevRisk)); //Risk
            LastMonthDataRow = LastMonthDataRow.replace(/>~estimatetocomplete~/ig, " id='projectEstimateToCompletePriorPeriod_" + ProjectId + "'>" + fmtMoney(prevETC)); //Estimate to Complete
            LastMonthDataRow = LastMonthDataRow.replace(/>~calceac~/ig, " id='projectCalcEACPriorPeriod_" + ProjectId + "'>" + fmtMoney(+prevETC + +prevSpentTotal)); //Calc EAC = [Estimate to Complete] + [Spent Total]
            LastMonthDataRow = LastMonthDataRow.replace(/>~calcfpc~/ig, " id='projectCalcFPCPriorPeriod_" + ProjectId + "'>"); //Calc FPC - show blank for prior period
            LastMonthDataRow = LastMonthDataRow.replace(/>~prioreac~/ig, " id='projectPriorEACPriorPeriod_" + ProjectId + "'>" + fmtMoney(prevPhaseEAC)); //Prior EAC
        }
        else {
            LastMonthDataRow = LastMonthDataRow.replace(/~desc~/ig, "<span style='font-weight:bold;float:right;'>Prior Month Totals Unavailable</span>"); //Desc
            LastMonthDataRow = LastMonthDataRow.replace(/>~tentelimit~/ig, " id='projectTENTELimitPriorPeriod_" + ProjectId + "'>"); //Total T&E NTE Limit
            LastMonthDataRow = LastMonthDataRow.replace(/>~compensation~/ig, " id='projectCompensationPriorPeriod_" + ProjectId + "'>"); //Compensation
            LastMonthDataRow = LastMonthDataRow.replace(/>~invoiced~/ig, " id='projectInvoicedTotalPriorPeriod_" + ProjectId + "'>"); //Invoiced
            LastMonthDataRow = LastMonthDataRow.replace(/>~spenttotal~/ig, " id='projectSpentTotalPriorPeriod_" + ProjectId + "'>"); //Spent Total
            LastMonthDataRow = LastMonthDataRow.replace(/>~spentpct~/ig, " id='projectSpentPCTPriorPeriod_" + ProjectId + "'>"); //Spent PCT
            LastMonthDataRow = LastMonthDataRow.replace(/>~variance~/ig, " id='projectVariancePriorPeriod_" + ProjectId + "'>"); //Variance
            LastMonthDataRow = LastMonthDataRow.replace(/>~hourstocomplete~/ig, " id='projectHoursToCompletePriorPeriod_" + ProjectId + "'>"); //Hours to Complete
            LastMonthDataRow = LastMonthDataRow.replace(/>~currentrate~/ig, " id='projectCurrentRatePriorPeriod_" + ProjectId + "'>"); //Current Rate - show blank for prior period
            LastMonthDataRow = LastMonthDataRow.replace(/>~estimateexpenses~/ig, " id='projectEstimateExpensesPriorPeriod_" + ProjectId + "'>"); //Estimate Expenses
            LastMonthDataRow = LastMonthDataRow.replace(/>~risk~/ig, " id='projectRiskPriorPeriod_" + ProjectId + "'>"); //Risk
            LastMonthDataRow = LastMonthDataRow.replace(/>~estimatetocomplete~/ig, " id='projectEstimateToCompletePriorPeriod_" + ProjectId + "'>"); //Estimate to Complete
            LastMonthDataRow = LastMonthDataRow.replace(/>~calceac~/ig, " id='projectCalcEACPriorPeriod_" + ProjectId + "'>"); //Calc EAC
            LastMonthDataRow = LastMonthDataRow.replace(/>~calcfpc~/ig, " id='projectCalcFPCPriorPeriod_" + ProjectId + "'>"); //Calc FPC - show blank for prior period
            LastMonthDataRow = LastMonthDataRow.replace(/>~prioreac~/ig, " id='projectPriorEACPriorPeriod_" + ProjectId + "'>"); //Prior EAC
        }
        $(TableBody).append(LastMonthDataRow);

        //This period project row
        var ProjectRow = RowTemplate.replace(/~buttons~/ig, ProjectButtonsHTML); //Append Buttons
        ProjectRow = ProjectRow.replace(/~projectid~/ig, ProjectId).replace(/~phaseid~/ig, '').replace(/~subphaseid~/ig, '').replace(/~detailid~/ig, ''); //Row Project/Phase/Subphase/Detail Ids
        ProjectRow = ProjectRow.replace(/~metadataextension~/ig, ''); //Metadata Extension Data
        ProjectRow = ProjectRow.replace(/~showhideevent~/ig, "").replace(/~showhideglyph~/ig, ""); //Row ShowHideDetail column
        ProjectRow = ProjectRow.replace(/~rowclass~/ig, "project").replace(/~descclass~/ig, "maindesc"); //Row Class
        ProjectRow = ProjectRow.replace(/~editable~/ig, "").replace(/~highlight~/ig, ""); //Editable/Highlight Cell Classes
        ProjectRow = ProjectRow.replace(/~desc~/ig, this.WBS1 + " " + this.Name); //Desc
        ProjectRow = ProjectRow.replace(/~phasestatus~/ig, ""); //Phase Status
        ProjectRow = ProjectRow.replace(/~manager~/ig, ""); //Manager
        ProjectRow = ProjectRow.replace(/>~tentelimit~/ig, " id='projectTENTELimit_" + ProjectId + "'>" + fmtMoney("0")); //Total T&E NTE Limit - this gets replaced in the subphase loop below
        ProjectRow = ProjectRow.replace(/>~compensation~/ig, " id='projectCompensation_" + ProjectId + "'>"); //Compensation
        ProjectRow = ProjectRow.replace(/>~invoiced~/ig, " id='projectInvoicedTotal_" + ProjectId + "'>"); //Invoiced
        ProjectRow = ProjectRow.replace(/>~spenttotal~/ig, " id='projectSpentTotal_" + ProjectId + "'>"); //Spent Total
        ProjectRow = ProjectRow.replace(/>~spentpct~/ig, " id='projectSpentPCT_" + ProjectId + "'>"); //Spent PCT
        ProjectRow = ProjectRow.replace(/>~variance~/ig, " id='projectVariance_" + ProjectId + "'>"); //Variance
        ProjectRow = ProjectRow.replace(/>~hourstocomplete~/ig, " id='projectHoursToComplete_" + ProjectId + "'>"); //Hours to Complete
        ProjectRow = ProjectRow.replace(/>~currentrate~/ig, " id='projectCurrentRate_" + ProjectId + "'>"); //Current Rate
        ProjectRow = ProjectRow.replace(/>~estimateexpenses~/ig, " id='projectEstimateExpenses_" + ProjectId + "'>"); //Estimate Expenses
        ProjectRow = ProjectRow.replace(/>~risk~/ig, " id='projectRisk_" + ProjectId + "'>"); //Risk
        ProjectRow = ProjectRow.replace(/>~estimatetocomplete~/ig, " id='projectEstimateToComplete_" + ProjectId + "'>"); //Estimate to Complete
        ProjectRow = ProjectRow.replace(/>~calceac~/ig, " id='projectCalcEAC_" + ProjectId + "'>"); //Calc EAC
        ProjectRow = ProjectRow.replace(/>~calcfpc~/ig, " id='projectCalcFPC_" + ProjectId + "'>"); //Calc FPC
        ProjectRow = ProjectRow.replace(/>~prioreac~/ig, " id='projectPriorEAC_" + ProjectId + "'>"); //Prior EAC

        $(TableBody).append(ProjectRow);

        var Phases = g_ProjectData.Phase.filter(x => { return x.InvoicingPeriodProjectId === ProjectId });
        Phases = Phases.sort(SortByProperty("WBS2"));
        $(Phases).each(function () {
            var ShowHideGlyph = "<span class='showhide' style='cursor:pointer;font-weight:bold;' title='Show/Hide Detail'>-</span>";
            var PhaseId = this.Id;
            var PhaseRow = RowTemplate.replace(/~buttons~/ig, PhaseButtonsHTML); //Append Buttons
            PhaseRow = PhaseRow.replace(/~projectid~/ig, ProjectId).replace(/~phaseid~/ig, PhaseId).replace(/~subphaseid~/ig, '').replace(/~detailid~/ig, ''); //Row Project/Phase/Subphase/Detail Ids
            PhaseRow = PhaseRow.replace(/~metadataextension~/ig, ''); //Metadata Extension Data
            PhaseRow = PhaseRow.replace(/~showhideevent~/ig, " onclick='ShowHideDetail(this);'").replace(/~showhideglyph~/ig, ShowHideGlyph); //Row ShowHideDetail column
            PhaseRow = PhaseRow.replace(/~rowclass~/ig, "phase").replace(/~descclass~/ig, "phasedesc"); //Row Class
            PhaseRow = PhaseRow.replace(/~editable~/ig, "").replace(/~highlight~/ig, ""); //Editable/Highlight Cell Classes
            PhaseRow = PhaseRow.replace(/~desc~/ig, this.WBS2 + " " + this.Name); //Desc
            PhaseRow = PhaseRow.replace(/~phasestatus~/ig, ""); //Phase Status
            PhaseRow = PhaseRow.replace(/~manager~/ig, ""); //Manager
            PhaseRow = PhaseRow.replace(/~tentelimit~/ig, ""); //Total T&E NTE Limit is blank at subphase level
            PhaseRow = PhaseRow.replace(/>~compensation~/ig, " id='phaseCompensation_" + PhaseId + "'>"); //Compensation
            PhaseRow = PhaseRow.replace(/>~invoiced~/ig, " id='phaseInvoicedTotal_" + PhaseId + "'>"); //Invoiced
            PhaseRow = PhaseRow.replace(/>~spenttotal~/ig, " id='phaseSpentTotal_" + PhaseId + "'>"); //Spent Total
            PhaseRow = PhaseRow.replace(/>~spentpct~/ig, " id='phaseSpentPCT_" + PhaseId + "'>"); //Spent PCT
            PhaseRow = PhaseRow.replace(/>~variance~/ig, " id='phaseVariance_" + PhaseId + "'>"); //Variance
            PhaseRow = PhaseRow.replace(/>~hourstocomplete~/ig, " id='phaseHoursToComplete_" + PhaseId + "'>"); //Hours to Complete
            PhaseRow = PhaseRow.replace(/>~currentrate~/ig, " id='phaseCurrentRate_" + PhaseId + "'>"); //Current Rate
            PhaseRow = PhaseRow.replace(/>~estimateexpenses~/ig, " id='phaseEstimateExpenses_" + PhaseId + "'>"); //Estimate Expenses
            PhaseRow = PhaseRow.replace(/>~risk~/ig, " id='phaseRisk_" + PhaseId + "'>"); //Risk
            PhaseRow = PhaseRow.replace(/>~estimatetocomplete~/ig, " id='phaseEstimateToComplete_" + PhaseId + "'>"); //Estimate to Complete
            PhaseRow = PhaseRow.replace(/>~calceac~/ig, " id='phaseCalcEAC_" + PhaseId + "'>"); //Calc EAC
            PhaseRow = PhaseRow.replace(/>~calcfpc~/ig, " id='phaseCalcFPC_" + PhaseId + "'>"); //Calc FPC
            PhaseRow = PhaseRow.replace(/>~prioreac~/ig, " id='phasePriorEAC_" + PhaseId + "'>"); //Prior EAC

            $(TableBody).append(PhaseRow);

            var Subphases = g_ProjectData.Subphase.filter(x => { return x.InvoicingPeriodPhaseId === PhaseId });
            Subphases = Subphases.sort(SortByProperty("WBS3"));
            $(Subphases).each(function () {
                var SubphaseId = this.Id;
                var SubphaseDetail = g_ProjectData.Detail.find(x => { return x.InvoicingPeriodProjectId === ProjectId && x.InvoicingPeriodPhaseId === PhaseId && x.InvoicingPeriodSubphaseId === SubphaseId });
                var SubphaseRow = RowTemplate.replace(/~buttons~/ig, SubphaseButtonsHTML); //Append Buttons
                SubphaseRow = SubphaseRow.replace(/~projectid~/ig, ProjectId).replace(/~phaseid~/ig, PhaseId).replace(/~subphaseid~/ig, SubphaseId).replace(/~detailid~/ig, SubphaseDetail.Id); //Row Project/Phase/Subphase/Detail Ids
                SubphaseRow = SubphaseRow.replace(/~metadataextension~/ig, SubphaseDetail.MetadataExtension); //Metadata Extension Data
                SubphaseRow = SubphaseRow.replace(/~showhideevent~/ig, "").replace(/~showhideglyph~/ig, ""); //Row ShowHideDetail column
                SubphaseRow = SubphaseRow.replace(/~rowclass~/ig, "subphase").replace(/~descclass~/ig, "subphasedesc"); //Row Class
                SubphaseRow = SubphaseRow.replace(/~editable~/ig, "editable").replace(/~highlight~/ig, "highlight"); //Editable/Highlight Cell Classes
                SubphaseRow = SubphaseRow.replace(/~desc~/ig, (this.WBS3 + " " + this.Name).trim()); //Desc
                SubphaseRow = SubphaseRow.replace(/>~phasestatus~/ig, " id='subphaseETCPhaseStatus_" + SubphaseDetail.Id + "'>" + SubphaseDetail.PhaseStatus); //Phase Status
                SubphaseRow = SubphaseRow.replace(/~manager~/ig, this.ManagerName); //Manager
                $("#projectTENTELimit_" + ProjectId).html(fmtMoney(SubphaseDetail.TENTELimit)); //Show Total T&E NTE Limit at Project level
                SubphaseRow = SubphaseRow.replace(/>~tentelimit~/ig, ""); //Total T&E NTE Limit is blank at subphase level
                SubphaseRow = SubphaseRow.replace(/>~compensation~/ig, " id='subphaseCompensation_" + SubphaseDetail.Id + "'>" + fmtMoney(SubphaseDetail.Compensation)); //Compensation
                SubphaseRow = SubphaseRow.replace(/>~invoiced~/ig, " id='subphaseInvoicedTotal_" + SubphaseDetail.Id + "'>" + fmtMoney(SubphaseDetail.InvoicedTotal)); //Invoiced
                SubphaseRow = SubphaseRow.replace(/>~spenttotal~/ig, " id='subphaseSpentTotal_" + SubphaseDetail.Id + "'>" + fmtMoney(SubphaseDetail.SpentTotal)); //Spent Total
                SubphaseRow = SubphaseRow.replace(/>~spentpct~/ig, " id='subphaseSpentPCT_" + SubphaseDetail.Id + "'>" + CalcPct(SubphaseDetail.SpentTotal, SubphaseDetail.Compensation)); //Spent PCT = [Spent Total] / [Compensation] %
                SubphaseRow = SubphaseRow.replace(/>~variance~/ig, " id='subphaseVariance_" + SubphaseDetail.Id + "'>" + fmtMoney(SubphaseDetail.Compensation - SubphaseDetail.SpentTotal)); //Variance = [Compensation] - [Spent Total]
                SubphaseRow = SubphaseRow.replace(/>~hourstocomplete~/ig, " id='subphaseHoursToComplete_" + SubphaseDetail.Id + "'>" + fmtNum(SubphaseDetail.HoursToComplete, 2)); //Hours to Complete
                SubphaseRow = SubphaseRow.replace(/>~currentrate~/ig, " id='subphaseCurrentRate_" + SubphaseDetail.Id + "'>" + fmtMoney(SubphaseDetail.AvgBillRate)); //Current Rate
                SubphaseRow = SubphaseRow.replace(/>~estimateexpenses~/ig, " id='subphaseEstimateExpenses_" + SubphaseDetail.Id + "'>" + fmtMoney(SubphaseDetail.EstimateExpenses)); //Estimate Expenses
                SubphaseRow = SubphaseRow.replace(/>~risk~/ig, " id='subphaseRisk_" + SubphaseDetail.Id + "'>" + fmtMoney(SubphaseDetail.Risk)); //Risk
                SubphaseRow = SubphaseRow.replace(/>~estimatetocomplete~/ig, " id='subphaseEstimateToComplete_" + SubphaseDetail.Id + "'>" + fmtMoney(SubphaseDetail.ETC)); //Estimate to Complete
                SubphaseRow = SubphaseRow.replace(/>~calceac~/ig, " id='subphaseCalcEAC_" + SubphaseDetail.Id + "'>" + fmtMoney(+SubphaseDetail.ETC + +SubphaseDetail.SpentTotal)); //Calc EAC = [Estimate to Complete] + [Spent Total]
                //Calc FPC (see Rule 11): If Compensation = 0 then Calc FPC = 0% else Calc FPC = [Spent Total] / [Calc EAC] %
                //Rule 11 changed per Paul P 2022-04-22: If ETC = 0 then Calc FPC = 100% else Calc FPC = [Spent Total] / [Calc EAC] %
                SubphaseCalcFPC = (fmtMoney(+SubphaseDetail.ETC) == "$0.00") ? CalcPct(1, 1) : CalcPct(+SubphaseDetail.SpentTotal, (+SubphaseDetail.ETC + +SubphaseDetail.SpentTotal));
                SubphaseRow = SubphaseRow.replace(/>~calcfpc~/ig, " id='subphaseCalcFPC_" + SubphaseDetail.Id + "'>" + SubphaseCalcFPC);
                SubphaseRow = SubphaseRow.replace(/>~prioreac~/ig, " id='subphasePriorEAC_" + SubphaseDetail.Id + "'>" + fmtMoney(SubphaseDetail.PhaseEAC)); //Prior EAC

                $(TableBody).append(SubphaseRow);

            });
        });
    });
    g_tblETC = MakeDataTable("tblETC", false, "", false, true);

    RecalculateTable("tblETC");
}

function BuildPrjDetailsTable() {
    var NotesTitle = "Add/Edit Notes";
    var TableBody = $("#tblPrjDetailsData");
    var RowTemplate = "<tr class='prjtab ~rowclass~ thin' data-projectid='~projectid~' data-phaseid='~phaseid~' data-subphaseid='~subphaseid~' data-detailid='~detailid~' data-metadataextension='~metadataextension~'><td class='dataentryshowhide' ~showhideevent~>~showhideglyph~</td><td class='prjtab ~descclass~'>~desc~</td><td class='prjtab moneycell'>~aramt~</td><td>~phasestatus~</td><td class='prjtab phasestatuslist ~editable~'>~newphasestatus~</td><td>~manager~</td><td class='prjtab employeelist ~editable~'>~newmanager~</td><td class='prjtab datecell'>~estimatedenddate~</td><td class='prjtab datecell ~editable~ ~highlight~'>~newestimatedenddate~</td><td>~revenuemethod~</td><td class='prjtab revenuemethodlist ~editable~'>~newrevenuemethod~</td><td class='prjtab buttoncell'>~buttons~</td></tr>";
    var ProjectButtonsHTML = "<center><a href='javascript:/**/' class='noteslink' data-tab='Project' data-rowtype='Project' data-id='~projectid~' title='" + NotesTitle + "' onclick='OpenModal(\"Notes\", this);'><i class='fa fa-file-o'></i></a><span id='ProjectQuestion_~projectid~'></span><center>";
    var PhaseButtonsHTML = "<center><a href='javascript:/**/' class='noteslink' data-tab='Project' data-rowtype='Phase' data-id='~phaseid~' title='" + NotesTitle + "' onclick='OpenModal(\"Notes\", this);'><i class='fa fa-file-o'></i></a><span id='ProjectQuestion_~phaseid~'></span><center>";
    var SubphaseButtonsHTML = "<center><a href='javascript:/**/' class='noteslink' data-tab='Project' data-rowtype='Subphase' data-id='~subphaseid~' title='" + NotesTitle + "' onclick='OpenModal(\"Notes\", this);'><i class='fa fa-file-o'></i></a><span id='ProjectQuestion_~subphaseid~'></span><center>";

    //Table Rows
    $(TableBody).html("");
    var Projects = g_ProjectData.Project;
    $(Projects).each(function () {
        var ProjectId = this.Id;
        var ProjectRow = RowTemplate.replace(/~buttons~/ig, ProjectButtonsHTML); //Append Buttons
        ProjectRow = ProjectRow.replace(/~projectid~/ig, ProjectId).replace(/~phaseid~/ig, '').replace(/~subphaseid~/ig, '').replace(/~detailid~/ig, ''); //Row Project/Phase/Subphase/Detail Ids
        ProjectRow = ProjectRow.replace(/~metadataextension~/ig, ''); //Metadata Extension Data
        ProjectRow = ProjectRow.replace(/~showhideevent~/ig, "").replace(/~showhideglyph~/ig, ""); //Row ShowHideDetail column
        ProjectRow = ProjectRow.replace(/~rowclass~/ig, "project").replace(/~descclass~/ig, "maindesc"); //Row Class
        ProjectRow = ProjectRow.replace(/~editable~/ig, "").replace(/~highlight~/ig, ""); //Editable/Highlight Cell Classes
        ProjectRow = ProjectRow.replace(/~desc~/ig, this.WBS1 + " " + this.Name); //Desc
        ProjectRow = ProjectRow.replace(/>~aramt~/ig, " id='projectPrjDetailARAmt_" + ProjectId + "'>"); //AR Amount
        ProjectRow = ProjectRow.replace(/~phasestatus~/ig, ""); //Phase Status
        ProjectRow = ProjectRow.replace(/~newphasestatus~/ig, ""); //New Phase Status
        ProjectRow = ProjectRow.replace(/~manager~/ig, ""); //Manager
        ProjectRow = ProjectRow.replace(/~newmanager~/ig, ""); //New Phase Manager
        ProjectRow = ProjectRow.replace(/~estimatedenddate~/ig, ""); //Estimated End Date
        ProjectRow = ProjectRow.replace(/~newestimatedenddate~/ig, ""); //New Estimated End Date
        ProjectRow = ProjectRow.replace(/~revenuemethod~/ig, ""); //Revenue Method
        ProjectRow = ProjectRow.replace(/~newrevenuemethod~/ig, ""); //New Revenue Method

        $(TableBody).append(ProjectRow);

        var Phases = g_ProjectData.Phase.filter(x => { return x.InvoicingPeriodProjectId === ProjectId });
        Phases = Phases.sort(SortByProperty("WBS2"));
        $(Phases).each(function () {
            var ShowHideGlyph = "<span class='showhide' style='cursor:pointer;font-weight:bold;' title='Show/Hide Detail'>-</span>";
            var PhaseId = this.Id;
            var PhaseRow = RowTemplate.replace(/~buttons~/ig, PhaseButtonsHTML); //Append Buttons
            PhaseRow = PhaseRow.replace(/~projectid~/ig, ProjectId).replace(/~phaseid~/ig, PhaseId).replace(/~subphaseid~/ig, '').replace(/~detailid~/ig, ''); //Row Project/Phase/Subphase/Detail Ids
            PhaseRow = PhaseRow.replace(/~metadataextension~/ig, ''); //Metadata Extension Data
            PhaseRow = PhaseRow.replace(/~showhideevent~/ig, " onclick='ShowHideDetail(this);'").replace(/~showhideglyph~/ig, ShowHideGlyph); //Row ShowHideDetail column
            PhaseRow = PhaseRow.replace(/~rowclass~/ig, "phase").replace(/~descclass~/ig, "phasedesc"); //Row Class
            PhaseRow = PhaseRow.replace(/~editable~/ig, "").replace(/~highlight~/ig, ""); //Editable/Highlight Cell Classes
            PhaseRow = PhaseRow.replace(/~desc~/ig, this.WBS2 + " " + this.Name); //Desc
            PhaseRow = PhaseRow.replace(/>~aramt~/ig, " id='phasePrjDetailARAmt_" + PhaseId + "'>"); //AR Amount
            PhaseRow = PhaseRow.replace(/~phasestatus~/ig, ""); //Phase Status
            PhaseRow = PhaseRow.replace(/~newphasestatus~/ig, ""); //New Phase Status
            PhaseRow = PhaseRow.replace(/~manager~/ig, ""); //Manager
            PhaseRow = PhaseRow.replace(/~newmanager~/ig, ""); //New Phase Manager
            PhaseRow = PhaseRow.replace(/~estimatedenddate~/ig, ""); //Estimated End Date
            PhaseRow = PhaseRow.replace(/~newestimatedenddate~/ig, ""); //New Estimated End Date
            PhaseRow = PhaseRow.replace(/~revenuemethod~/ig, ""); //Revenue Method
            PhaseRow = PhaseRow.replace(/~newrevenuemethod~/ig, ""); //New Revenue Method

            $(TableBody).append(PhaseRow);

            var Subphases = g_ProjectData.Subphase.filter(x => { return x.InvoicingPeriodPhaseId === PhaseId });
            Subphases = Subphases.sort(SortByProperty("WBS3"));
            $(Subphases).each(function () {
                var SubphaseId = this.Id;
                var SubphaseDetail = g_ProjectData.Detail.find(x => { return x.InvoicingPeriodProjectId === ProjectId && x.InvoicingPeriodPhaseId === PhaseId && x.InvoicingPeriodSubphaseId === SubphaseId });
                var SubphaseRow = RowTemplate.replace(/~buttons~/ig, SubphaseButtonsHTML); //Append Buttons
                SubphaseRow = SubphaseRow.replace(/~projectid~/ig, ProjectId).replace(/~phaseid~/ig, PhaseId).replace(/~subphaseid~/ig, SubphaseId).replace(/~detailid~/ig, SubphaseDetail.Id); //Row Project/Phase/Subphase/Detail Ids
                SubphaseRow = SubphaseRow.replace(/~metadataextension~/ig, SubphaseDetail.MetadataExtension); //Metadata Extension Data
                SubphaseRow = SubphaseRow.replace(/~showhideevent~/ig, "").replace(/~showhideglyph~/ig, ""); //Row ShowHideDetail column
                SubphaseRow = SubphaseRow.replace(/~rowclass~/ig, "subphase").replace(/~descclass~/ig, "subphasedesc"); //Row Class
                SubphaseRow = SubphaseRow.replace(/~editable~/ig, "editable").replace(/~highlight~/ig, "highlight"); //Editable/Highlight Cell Classes
                SubphaseRow = SubphaseRow.replace(/~desc~/ig, (this.WBS3 + " " + this.Name).trim()); //Desc
                SubphaseRow = SubphaseRow.replace(/>~aramt~/ig, " id='subphasePrjDetailARAmt_" + SubphaseDetail.Id + "'>" + fmtMoney(SubphaseDetail.AR_Amount)); //AR Amount
                SubphaseRow = SubphaseRow.replace(/>~phasestatus~/ig, " id='subphasePrjDetailPhaseStatus_" + SubphaseDetail.Id + "'>" + SubphaseDetail.PhaseStatus); //Phase Status
                SubphaseRow = SubphaseRow.replace(/>~newphasestatus~/ig, " id='subphaseNewPhaseStatus_" + SubphaseDetail.Id + "'>" + SubphaseDetail.NewPhaseStatus); //New Phase Status
                SubphaseRow = SubphaseRow.replace(/~manager~/ig, this.ManagerName); //Manager
                SubphaseRow = SubphaseRow.replace(/>~newmanager~/ig, " id='subphasePhaseMgrUpdated_" + SubphaseDetail.Id + "' data-empid='" + SubphaseDetail.PhaseMgrEmployeeId_Updated + "'>" + empName(SubphaseDetail.PhaseMgrEmployeeId_Updated)); //New Phase Manager
                SubphaseRow = SubphaseRow.replace(/>~estimatedenddate~/ig, " id='subphaseEstEndDate_" + SubphaseDetail.Id + "'>" + FormatDateDisplay(SubphaseDetail.EstEndDate, "short")); //Estimated End Date
                SubphaseRow = SubphaseRow.replace(/>~newestimatedenddate~/ig, " id='subphaseEstEndDateUpdated_" + SubphaseDetail.Id + "'>" + FormatDateDisplay(SubphaseDetail.EstEndDate_Updated, "short")); //New Estimated End Date
                SubphaseRow = SubphaseRow.replace(/>~revenuemethod~/ig, " id='subphaseRevenueMethod_" + SubphaseDetail.Id + "'>" + SubphaseDetail.RevenueMethod); //Revenue Method
                SubphaseRow = SubphaseRow.replace(/>~newrevenuemethod~/ig, " id='subphaseRevenueMethodUpdated_" + SubphaseDetail.Id + "'>" + SubphaseDetail.RevenueMethod_Updated); //New Revenue Method

                $(TableBody).append(SubphaseRow);

            });
        });
    });
    g_tblPrjDetails = MakeDataTable("tblPrjDetails", false, "", false, true);

    RecalculateTable("tblPrjDetails");
}

function BuildInvoicingTable() {
    var NotesTitle = "Add/Edit Notes";
    var TableBody = $("#tblInvoicingData");
    var RowTemplate = "<tr class='invtab ~rowclass~ thin' data-projectid='~projectid~' data-phaseid='~phaseid~' data-subphaseid='~subphaseid~' data-detailid='~detailid~' data-metadataextension='~metadataextension~'><td class='dataentryshowhide' ~showhideevent~>~showhideglyph~</td><td class='invtab ~descclass~'>~desc~</td><td>~phasestatus~</td><td class='invtab moneycell'>~compensation~</td><td class='invtab moneycell'>~spenttotal~</td><td class='invtab pctcell'>~spentpct~</td><td class='invtab moneycell'>~invoiced~</td><td class='invtab pctcell'>~invoicedpct~</td><td class='invtab pctcell nodbsave ~editable~' data-customcalcs='true'>~newinvoicedpct~</td><td class='invtab moneycell'>~wiptotal~</td><td class='invtab moneycell'>~wipcurrent~</td><td class='invtab moneycell'>~wipheld~</td><td class='invtab moneycell'>~expenseunbilled~</td><td class='invtab moneycell wipcheck'>~wipcheck~</td><td class='invtab moneycell ~editable~ ~highlight~'>~amttoinvoice~</td><td class='invtab moneycell ~editable~ ~highlight~'>~wiptoclose~</td><td class='invtab moneycell ~editable~ ~highlight~'>~amttohold~</td><td class='invtab moneycell ~editable~ ~highlight~'>~amttodelete~</td><td class='invtab moneycell ~editable~ ~highlight~'>~amttotransfer~</td><td class='invtab buttoncell'>~buttons~</td></tr>";
    var ProjectButtonsHTML = "<center><a href='javascript:/**/' class='noteslink' data-tab='Invoicing' data-rowtype='Project' data-id='~projectid~' title='" + NotesTitle + "' onclick='OpenModal(\"Notes\", this);'><i class='fa fa-file-o'></i></a><span id='InvoicingQuestion_~projectid~'></span><center>";
    var PhaseButtonsHTML = "<center><a href='javascript:/**/' class='noteslink' data-tab='Invoicing' data-rowtype='Phase' data-id='~phaseid~' title='" + NotesTitle + "' onclick='OpenModal(\"Notes\", this);'><i class='fa fa-file-o'></i></a><span id='InvoicingQuestion_~phaseid~'></span><center>";
    var SubphaseButtonsHTML = "<center><a href='javascript:/**/' class='noteslink' data-tab='Invoicing' data-rowtype='Subphase' data-id='~subphaseid~' title='" + NotesTitle + "' onclick='OpenModal(\"Notes\", this);'><i class='fa fa-file-o'></i></a><span id='InvoicingQuestion_~subphaseid~'></span><center>";

    //Table Rows
    $(TableBody).html("");
    var Projects = g_ProjectData.Project;
    $(Projects).each(function () {
        var ProjectId = this.Id;
        var ProjectRow = RowTemplate.replace(/~buttons~/ig, ProjectButtonsHTML); //Append Buttons
        ProjectRow = ProjectRow.replace(/~projectid~/ig, ProjectId).replace(/~phaseid~/ig, '').replace(/~subphaseid~/ig, '').replace(/~detailid~/ig, ''); //Row Project/Phase/Subphase/Detail Ids
        ProjectRow = ProjectRow.replace(/~metadataextension~/ig, ''); //Metadata Extension Data
        ProjectRow = ProjectRow.replace(/~showhideevent~/ig, "").replace(/~showhideglyph~/ig, ""); //Row ShowHideDetail column
        ProjectRow = ProjectRow.replace(/~rowclass~/ig, "project").replace(/~descclass~/ig, "maindesc"); //Row Class
        ProjectRow = ProjectRow.replace(/~editable~/ig, "").replace(/~highlight~/ig, ""); //Editable/Highlight Cell Classes
        ProjectRow = ProjectRow.replace(/~desc~/ig, this.WBS1 + " " + this.Name); //Desc
        ProjectRow = ProjectRow.replace(/>~phasestatus~/ig, " id='projectInvoicingPhaseStatus_" + ProjectId + "'>"); //Phase Status
        ProjectRow = ProjectRow.replace(/>~compensation~/ig, " id='projectInvoicingCompensation_" + ProjectId + "'>"); //Compensation
        ProjectRow = ProjectRow.replace(/>~spenttotal~/ig, " id='projectInvoicingSpentTotal_" + ProjectId + "'>"); //Spent Total
        ProjectRow = ProjectRow.replace(/>~spentpct~/ig, " id='projectInvoicingSpentPCT_" + ProjectId + "'>"); //Spent PCT
        ProjectRow = ProjectRow.replace(/>~invoiced~/ig, " id='projectInvoicingInvoicedTotal_" + ProjectId + "'>"); //Invoiced
        ProjectRow = ProjectRow.replace(/>~invoicedpct~/ig, " id='projectInvoicingInvoicedPCT_" + ProjectId + "'>"); //Invoiced PCT
        ProjectRow = ProjectRow.replace(/>~newinvoicedpct~/ig, " id='projectInvoicingNewInvoicedPct_" + ProjectId + "'>"); //New Invoiced PCT
        ProjectRow = ProjectRow.replace(/>~wiptotal~/ig, " id='projectInvoicingWIPTotal_" + ProjectId + "'>"); //WIP Total
        ProjectRow = ProjectRow.replace(/>~wipcurrent~/ig, " id='projectInvoicingWIPCurrent_" + ProjectId + "'>"); //WIP Current
        ProjectRow = ProjectRow.replace(/>~wipheld~/ig, " id='projectInvoicingWIPHeld_" + ProjectId + "'>"); //WIP Held
        ProjectRow = ProjectRow.replace(/>~expenseunbilled~/ig, " id='projectInvoicingExpenseUnbilled_" + ProjectId + "'>"); //Expense Unbilled
        ProjectRow = ProjectRow.replace(/>~wipcheck~/ig, " id='projectWIPCheck_" + ProjectId + "'>"); //WIP Check
        ProjectRow = ProjectRow.replace(/>~amttoinvoice~/ig, " id='projectInvoicingAmountToInvoice_" + ProjectId + "'>"); //Amount to Invoice
        ProjectRow = ProjectRow.replace(/>~wiptoclose~/ig, " id='projectInvoicingWIPToClose_" + ProjectId + "'>"); //WIP to close
        ProjectRow = ProjectRow.replace(/>~amttohold~/ig, " id='projectInvoicingAmountToHold_" + ProjectId + "'>"); //Amount to Hold
        ProjectRow = ProjectRow.replace(/>~amttodelete~/ig, " id='projectInvoicingAmountToDelete_" + ProjectId + "'>"); //Amount to Delete
        ProjectRow = ProjectRow.replace(/>~amttotransfer~/ig, " id='projectInvoicingAmountToTransfer_" + ProjectId + "'>"); //Amount to Transfer

        $(TableBody).append(ProjectRow);

        var Phases = g_ProjectData.Phase.filter(x => { return x.InvoicingPeriodProjectId === ProjectId });
        Phases = Phases.sort(SortByProperty("WBS2"));
        $(Phases).each(function () {
            var ShowHideGlyph = "<span class='showhide' style='cursor:pointer;font-weight:bold;' title='Show/Hide Detail'>-</span>";
            var PhaseId = this.Id;
            var PhaseRow = RowTemplate.replace(/~buttons~/ig, PhaseButtonsHTML); //Append Buttons
            PhaseRow = PhaseRow.replace(/~projectid~/ig, ProjectId).replace(/~phaseid~/ig, PhaseId).replace(/~subphaseid~/ig, '').replace(/~detailid~/ig, ''); //Row Project/Phase/Subphase/Detail Ids
            PhaseRow = PhaseRow.replace(/~metadataextension~/ig, ''); //Metadata Extension Data
            PhaseRow = PhaseRow.replace(/~showhideevent~/ig, " onclick='ShowHideDetail(this);'").replace(/~showhideglyph~/ig, ShowHideGlyph); //Row ShowHideDetail column
            PhaseRow = PhaseRow.replace(/~rowclass~/ig, "phase").replace(/~descclass~/ig, "phasedesc"); //Row Class
            PhaseRow = PhaseRow.replace(/~editable~/ig, "").replace(/~highlight~/ig, ""); //Editable/Highlight Cell Classes
            PhaseRow = PhaseRow.replace(/~desc~/ig, this.WBS2 + " " + this.Name); //Desc
            PhaseRow = PhaseRow.replace(/>~phasestatus~/ig, " id='projectInvoicingPhaseStatus_" + PhaseId + "'>"); //Phase Status
            PhaseRow = PhaseRow.replace(/>~compensation~/ig, " id='phaseInvoicingCompensation_" + PhaseId + "'>"); //Compensation
            PhaseRow = PhaseRow.replace(/>~spenttotal~/ig, " id='phaseInvoicingSpentTotal_" + PhaseId + "'>"); //Spent Total
            PhaseRow = PhaseRow.replace(/>~spentpct~/ig, " id='phaseInvoicingSpentPCT_" + PhaseId + "'>"); //Spent PCT
            PhaseRow = PhaseRow.replace(/>~invoiced~/ig, " id='phaseInvoicingInvoicedTotal_" + PhaseId + "'>"); //Invoiced
            PhaseRow = PhaseRow.replace(/>~invoicedpct~/ig, " id='phaseInvoicingInvoicedPCT_" + PhaseId + "'>"); //Invoiced PCT
            PhaseRow = PhaseRow.replace(/>~newinvoicedpct~/ig, " id='phaseInvoicingNewInvoicedPct_" + PhaseId + "'>"); //New Invoiced PCT
            PhaseRow = PhaseRow.replace(/>~wiptotal~/ig, " id='phaseInvoicingWIPTotal_" + PhaseId + "'>"); //WIP Total
            PhaseRow = PhaseRow.replace(/>~wipcurrent~/ig, " id='phaseInvoicingWIPCurrent_" + PhaseId + "'>"); //WIP Current
            PhaseRow = PhaseRow.replace(/>~wipheld~/ig, " id='phaseInvoicingWIPHeld_" + PhaseId + "'>"); //WIP Held
            PhaseRow = PhaseRow.replace(/>~expenseunbilled~/ig, " id='phaseInvoicingExpenseUnbilled_" + PhaseId + "'>"); //Expense Unbilled
            PhaseRow = PhaseRow.replace(/>~wipcheck~/ig, " id='phaseWIPCheck_" + PhaseId + "'>"); //WIP Check
            PhaseRow = PhaseRow.replace(/>~amttoinvoice~/ig, " id='phaseInvoicingAmountToInvoice_" + PhaseId + "'>"); //Amount to Invoice
            PhaseRow = PhaseRow.replace(/>~wiptoclose~/ig, " id='phaseInvoicingWIPToClose_" + PhaseId + "'>"); //WIP to Close
            PhaseRow = PhaseRow.replace(/>~amttohold~/ig, " id='phaseInvoicingAmountToHold_" + PhaseId + "'>"); //Amount to Hold
            PhaseRow = PhaseRow.replace(/>~amttodelete~/ig, " id='phaseInvoicingAmountToDelete_" + PhaseId + "'>"); //Amount to Delete
            PhaseRow = PhaseRow.replace(/>~amttotransfer~/ig, " id='phaseInvoicingAmountToTransfer_" + PhaseId + "'>"); //Amount to Transfer

            $(TableBody).append(PhaseRow);

            var Subphases = g_ProjectData.Subphase.filter(x => { return x.InvoicingPeriodPhaseId === PhaseId });
            Subphases = Subphases.sort(SortByProperty("WBS3"));
            $(Subphases).each(function () {
                var SubphaseId = this.Id;
                var SubphaseDetail = g_ProjectData.Detail.find(x => { return x.InvoicingPeriodProjectId === ProjectId && x.InvoicingPeriodPhaseId === PhaseId && x.InvoicingPeriodSubphaseId === SubphaseId });
                var SubphaseRow = RowTemplate.replace(/~buttons~/ig, SubphaseButtonsHTML); //Append Buttons
                SubphaseRow = SubphaseRow.replace(/~projectid~/ig, ProjectId).replace(/~phaseid~/ig, PhaseId).replace(/~subphaseid~/ig, SubphaseId).replace(/~detailid~/ig, SubphaseDetail.Id); //Row Project/Phase/Subphase/Detail Ids
                SubphaseRow = SubphaseRow.replace(/~metadataextension~/ig, SubphaseDetail.MetadataExtension); //Metadata Extension Data
                SubphaseRow = SubphaseRow.replace(/~showhideevent~/ig, "").replace(/~showhideglyph~/ig, ""); //Row ShowHideDetail column
                SubphaseRow = SubphaseRow.replace(/~rowclass~/ig, "subphase").replace(/~descclass~/ig, "subphasedesc"); //Row Class
                SubphaseRow = SubphaseRow.replace(/~editable~/ig, "editable").replace(/~highlight~/ig, "highlight"); //Editable/Highlight Cell Classes
                SubphaseRow = SubphaseRow.replace(/~desc~/ig, (this.WBS3 + " " + this.Name).trim()); //Desc
                SubphaseRow = SubphaseRow.replace(/>~phasestatus~/ig, " id='subphaseInvoicingPhaseStatus_" + SubphaseDetail.Id + "'>" + SubphaseDetail.PhaseStatus); //Phase Status
                SubphaseRow = SubphaseRow.replace(/>~compensation~/ig, " id='subphaseInvoicingCompensation_" + SubphaseDetail.Id + "'>" + fmtMoney(SubphaseDetail.Compensation)); //Compensation
                SubphaseRow = SubphaseRow.replace(/>~spenttotal~/ig, " id='subphaseInvoicingSpentTotal_" + SubphaseDetail.Id + "'>" + fmtMoney(SubphaseDetail.SpentTotal)); //Spent Total
                SubphaseRow = SubphaseRow.replace(/>~spentpct~/ig, " id='subphaseInvoicingSpentPCT_" + SubphaseDetail.Id + "'>" + CalcPct(SubphaseDetail.SpentTotal, SubphaseDetail.Compensation)); //Spent PCT = [Spent Total] / [Compensation] %
                SubphaseRow = SubphaseRow.replace(/>~invoiced~/ig, " id='subphaseInvoicingInvoicedTotal_" + SubphaseDetail.Id + "'>" + fmtMoney(SubphaseDetail.InvoicedTotal)); //Invoiced
                SubphaseRow = SubphaseRow.replace(/>~invoicedpct~/ig, " id='subphaseInvoicingInvoicedPCT_" + SubphaseDetail.Id + "'>" + CalcPct(SubphaseDetail.InvoicedTotal, SubphaseDetail.Compensation)); //Invoiced PCT = [Invoiced] / [Compensation] %
                SubphaseRow = SubphaseRow.replace(/>~newinvoicedpct~/ig, " id='subphaseInvoicingNewInvoicedPct_" + SubphaseDetail.Id + "'>" + CalcPct(+SubphaseDetail.InvoicedTotal + +SubphaseDetail.AmountToInvoice, SubphaseDetail.Compensation)); //New Invoiced PCT = ([Invoiced] + [Amount to Invoice]) / [Compensation] %
                SubphaseRow = SubphaseRow.replace(/>~wiptotal~/ig, " id='subphaseInvoicingWIPTotal_" + SubphaseDetail.Id + "'>" + fmtMoney(SubphaseDetail.WIP_Total)); //WIP Total
                SubphaseRow = SubphaseRow.replace(/>~wipcurrent~/ig, " id='subphaseInvoicingWIPCurrent_" + SubphaseDetail.Id + "'>" + fmtMoney(SubphaseDetail.WIP_Current)); //WIP Current
                SubphaseRow = SubphaseRow.replace(/>~wipheld~/ig, " id='subphaseInvoicingWIPHeld_" + SubphaseDetail.Id + "'>" + fmtMoney(SubphaseDetail.WIP_Held)); //WIP Held
                SubphaseRow = SubphaseRow.replace(/>~expenseunbilled~/ig, " id='subphaseInvoicingExpenseUnbilled_" + SubphaseDetail.Id + "'>" + fmtMoney(SubphaseDetail.ExpenseUnbilled)); //Expense Unbilled
                var SubphaseWIPCheck = (+SubphaseDetail.AmountToInvoice + +SubphaseDetail.WIP_To_Close + +SubphaseDetail.AmountToHold + +SubphaseDetail.AmountToDelete + +SubphaseDetail.AmountToTransfer) - (+SubphaseDetail.WIP_Total + +SubphaseDetail.ExpenseUnbilled);
                SubphaseRow = SubphaseRow.replace(/>~wipcheck~/ig, " id='subphaseWIPCheck_" + SubphaseDetail.Id + "'>" + fmtMoney(SubphaseWIPCheck)); //WIP Check = (Amount to Invoice + WIP to Close Out + Amount to Hold + Amount to Delete + Amount to Transfer) – (WIP Total + Expense Unbilled)
                SubphaseRow = SubphaseRow.replace(/>~amttoinvoice~/ig, " id='subphaseInvoicingAmountToInvoice_" + SubphaseDetail.Id + "'>" + fmtMoney(SubphaseDetail.AmountToInvoice)); //Amount to Invoice
                SubphaseRow = SubphaseRow.replace(/>~wiptoclose~/ig, " id='subphaseInvoicingWIPToClose_" + SubphaseDetail.Id + "'>" + fmtMoney(SubphaseDetail.WIP_To_Close)); //WIP to Close
                SubphaseRow = SubphaseRow.replace(/>~amttohold~/ig, " id='subphaseInvoicingAmountToHold_" + SubphaseDetail.Id + "'>" + fmtMoney(SubphaseDetail.AmountToHold)); //Amount to Hold
                SubphaseRow = SubphaseRow.replace(/>~amttodelete~/ig, " id='subphaseInvoicingAmountToDelete_" + SubphaseDetail.Id + "'>" + fmtMoney(SubphaseDetail.AmountToDelete)); //Amount to Delete
                SubphaseRow = SubphaseRow.replace(/>~amttotransfer~/ig, " id='subphaseInvoicingAmountToTransfer_" + SubphaseDetail.Id + "'>" + fmtMoney(SubphaseDetail.AmountToTransfer)); //Amount to Transfer

                //Update Billing Terms label
                $("#lblBillingTerms").html(SubphaseDetail.BillingTerms);

                $(TableBody).append(SubphaseRow);

            });
        });
    });
    g_tblInvoicing = MakeDataTable("tblInvoicing", false, "", false, true);

    RecalculateTable("tblInvoicing");
}

function CheckRuleOverrides(CurrentCell) {
    var CellParent = $(CurrentCell).parent();
    var ProjectId = $(CellParent).data("projectid");
    var PhaseId = $(CellParent).data("phaseid");
    var SubphaseId = $(CellParent).data("subphaseid");
    var DetailId = $(CellParent).data("detailid");
    var SubphaseDetail = g_ProjectData.Detail.find(x => { return x.InvoicingPeriodProjectId === ProjectId && x.InvoicingPeriodPhaseId === PhaseId && x.InvoicingPeriodSubphaseId === SubphaseId });

    //ETC Rules 1 and 2
    if (CellParent.hasClass("ETCRule1") || CellParent.hasClass("ETCRule2")) {
        var RuleToOverride = (CellParent.hasClass("ETCRule1")) ? "ETCRule1" : "ETCRule2";
        var RuleOverrideId = g_RuleOverrides.find(x => { return x.Value === RuleToOverride }).Id.toString().toUpperCase();
        var MetadataExtension = $(CellParent).data("metadataextension");
        if (MetadataExtension.toString().length > 0) {
            if ($.inArray(RuleOverrideId, MetadataExtension.MetadataIds) == -1) {
                MetadataExtension.MetadataIds.push(RuleOverrideId);
                SaveValue(DetailId, "MetadataExtension", JSON.stringify(MetadataExtension), "InvoicingPeriodData");
                $(CellParent).data("metadataextension", MetadataExtension);
                SubphaseDetail.MetadataExtension = MetadataExtension;
            }
        }
        else {
            MetadataExtension = { "MetadataIds": [RuleOverrideId] };
            SaveValue(DetailId, "MetadataExtension", JSON.stringify(MetadataExtension), "InvoicingPeriodData");
            $(CellParent).data("metadataextension", MetadataExtension);
            SubphaseDetail.MetadataExtension = MetadataExtension;
        }
    }
}

function RecalculateTable(TableId) {
    switch (TableId) {
        case "tblETC":
            RecalculateETCTable();
            break;
        case "tblPrjDetails":
            RecalculatePrjDetailsTable();
            break;
        case "tblInvoicing":
            RecalculateInvoicingTable();
            break;
        default:
    }
    CheckRules(TableId);
}

function RecalculateETCTable() {
    var TableId = "tblETC";
    var ProjectRows = $("#" + TableId + " tr.project");
    var PhaseRows = $("#" + TableId + " tr.phase");

    //Rollup Subphases to Phases
    $(PhaseRows).each(function () {
        var PhaseId = $(this).data("phaseid");
        var PhaseCompensation = 0;
        var PhaseInvoicedTotal = 0;
        var PhaseSpentTotal = 0;
        var PhaseHoursToComplete = 0;
        var PhaseCurrentRate = 0;
        var PhaseEstimateExpenses = 0;
        var PhaseRisk = 0;
        var PhaseEstimateToComplete = 0;
        var PhasePriorEAC = 0;
        $("#" + TableId + " tr.subphase[data-phaseid='" + PhaseId + "']").each(function () {
            var DetailId = $(this).data("detailid");
            PhaseCompensation += rawNum($("#subphaseCompensation_" + DetailId).html());
            var SubphaseInvoicedTotal = rawNum($("#subphaseInvoicedTotal_" + DetailId).html());
            PhaseInvoicedTotal += SubphaseInvoicedTotal;
            PhaseSpentTotal += rawNum($("#subphaseSpentTotal_" + DetailId).html());
            PhaseHoursToComplete += rawNum($("#subphaseHoursToComplete_" + DetailId).html());
            PhaseCurrentRate += rawNum($("#subphaseCurrentRate_" + DetailId).html());
            PhaseEstimateExpenses += rawNum($("#subphaseEstimateExpenses_" + DetailId).html());
            PhaseRisk += rawNum($("#subphaseRisk_" + DetailId).html());
            PhaseEstimateToComplete += rawNum($("#subphaseEstimateToComplete_" + DetailId).html());
            PhasePriorEAC += rawNum($("#subphasePriorEAC_" + DetailId).html());
        });
        $("#phaseCompensation_" + PhaseId).html(fmtMoney(PhaseCompensation));
        $("#phaseSpentTotal_" + PhaseId).html(fmtMoney(PhaseSpentTotal));
        $("#phaseSpentPCT_" + PhaseId).html(CalcPct(PhaseSpentTotal, PhaseCompensation)); //Spent PCT = [Spent Total] / [Compensation] %
        $("#phaseInvoicedTotal_" + PhaseId).html(fmtMoney(PhaseInvoicedTotal));
        $("#phaseVariance_" + PhaseId).html(fmtMoney(PhaseCompensation - PhaseSpentTotal)); //Variance = [Compensation] - [Spent Total]
        $("#phaseHoursToComplete_" + PhaseId).html(fmtNum(PhaseHoursToComplete, 2));
        $("#phaseCurrentRate_" + PhaseId).html(fmtMoney(PhaseCurrentRate));
        $("#phaseEstimateExpenses_" + PhaseId).html(fmtMoney(PhaseEstimateExpenses));
        $("#phaseRisk_" + PhaseId).html(fmtMoney(PhaseRisk));
        $("#phaseEstimateToComplete_" + PhaseId).html(fmtMoney(PhaseEstimateToComplete));
        $("#phaseCalcEAC_" + PhaseId).html(fmtMoney(+PhaseEstimateToComplete + +PhaseSpentTotal)); //Calc EAC = [Estimate to Complete] + [Spent Total]
        //Rule 11 changed per Paul P 2022-04-22: If ETC = 0 then Calc FPC = 100% else Calc FPC = [Spent Total] / [Calc EAC] %
        var PhaseCalcFPC = (fmtMoney(+PhaseEstimateToComplete) == "$0.00") ? CalcPct(1, 1) : CalcPct(PhaseSpentTotal, (+PhaseEstimateToComplete + +PhaseSpentTotal));
        $("#phaseCalcFPC_" + PhaseId).html(PhaseCalcFPC);
        $("#phasePriorEAC_" + PhaseId).html(fmtMoney(PhasePriorEAC));
    });

    //Rollup Phases to Project
    $(ProjectRows).each(function () {
        var ProjectId = $(this).data("projectid");
        var ProjectCompensation = 0;
        var ProjectInvoicedTotal = 0;
        var ProjectSpentTotal = 0;
        var ProjectHoursToComplete = 0;
        var ProjectCurrentRate = 0;
        var ProjectEstimateExpenses = 0;
        var ProjectRisk = 0;
        var ProjectEstimateToComplete = 0;
        var ProjectPriorEAC = 0;
        $("#" + TableId + " tr.phase[data-projectid='" + ProjectId + "']").each(function () {
            var PhaseId = $(this).data("phaseid");
            ProjectCompensation += rawNum($("#phaseCompensation_" + PhaseId).html());
            ProjectInvoicedTotal += rawNum($("#phaseInvoicedTotal_" + PhaseId).html());
            ProjectSpentTotal += rawNum($("#phaseSpentTotal_" + PhaseId).html());
            ProjectHoursToComplete += rawNum($("#phaseHoursToComplete_" + PhaseId).html());
            ProjectCurrentRate += rawNum($("#phaseCurrentRate_" + PhaseId).html());
            ProjectEstimateExpenses += rawNum($("#phaseEstimateExpenses_" + PhaseId).html());
            ProjectRisk += rawNum($("#phaseRisk_" + PhaseId).html());
            ProjectEstimateToComplete += rawNum($("#phaseEstimateToComplete_" + PhaseId).html());
            ProjectPriorEAC += rawNum($("#phasePriorEAC_" + PhaseId).html());
        });
        $("#projectCompensation_" + ProjectId).html(fmtMoney(ProjectCompensation));
        $("#projectInvoicedTotal_" + ProjectId).html(fmtMoney(ProjectInvoicedTotal));
        $("#projectSpentTotal_" + ProjectId).html(fmtMoney(ProjectSpentTotal));
        $("#projectSpentPCT_" + ProjectId).html(CalcPct(ProjectSpentTotal, ProjectCompensation)); //Spent PCT = [Spent Total] / [Compensation] %
        $("#projectVariance_" + ProjectId).html(fmtMoney(ProjectCompensation - ProjectSpentTotal)); //Variance = [Compensation] - [Spent Total]
        $("#projectHoursToComplete_" + ProjectId).html(fmtNum(ProjectHoursToComplete, 2));
        $("#projectCurrentRate_" + ProjectId).html("&nbsp;"); //Project Current Rate Not Displayed at Project Level
        $("#projectEstimateExpenses_" + ProjectId).html(fmtMoney(ProjectEstimateExpenses));
        $("#projectRisk_" + ProjectId).html(fmtMoney(ProjectRisk));
        $("#projectEstimateToComplete_" + ProjectId).html(fmtMoney(ProjectEstimateToComplete));
        $("#projectCalcEAC_" + ProjectId).html(fmtMoney(+ProjectEstimateToComplete + +ProjectSpentTotal)); //Calc EAC = [Estimate to Complete] + [Spent Total]
        $("#projectCalcFPC_" + ProjectId).html("&nbsp;"); //Calc FPC Not Displayed at Project Level
        $("#projectPriorEAC_" + ProjectId).html(fmtMoney(ProjectPriorEAC));
    });
}

function RecalculatePrjDetailsTable() {
    var TableId = "tblPrjDetails";
    var ProjectRows = $("#" + TableId + " tr.project");
    var PhaseRows = $("#" + TableId + " tr.phase");

    //Rollup Subphases to Phases
    $(PhaseRows).each(function () {
        var PhaseId = $(this).data("phaseid");
        var PhaseARAmt = 0;
        $("#" + TableId + " tr.subphase[data-phaseid='" + PhaseId + "']").each(function () {
            var DetailId = $(this).data("detailid");
            PhaseARAmt += rawNum($("#subphasePrjDetailARAmt_" + DetailId).html());
        });
        $("#phasePrjDetailARAmt_" + PhaseId).html(fmtMoney(PhaseARAmt));
    });

    //Rollup Phases to Project
    $(ProjectRows).each(function () {
        var ProjectId = $(this).data("projectid");
        var ProjectARAmt = 0;
        $("#" + TableId + " tr.phase[data-projectid='" + ProjectId + "']").each(function () {
            var PhaseId = $(this).data("phaseid");
            ProjectARAmt += rawNum($("#phasePrjDetailARAmt_" + PhaseId).html());
        });

        $("#projectPrjDetailARAmt_" + ProjectId).html(fmtMoney(ProjectARAmt));
    });
}

function RecalculateInvoicingTable() {
    var TableId = "tblInvoicing";
    var ProjectRows = $("#" + TableId + " tr.project");
    var PhaseRows = $("#" + TableId + " tr.phase");

    //Rollup Subphases to Phases
    $(PhaseRows).each(function () {
        var PhaseId = $(this).data("phaseid");
        var PhaseCompensation = 0;
        var PhaseSpentTotal = 0;
        var PhaseInvoicedTotal = 0;
        var PhaseWIPCheck = 0;
        var PhaseAmountToInvoice = 0;
        var PhaseWIPTotal = 0;
        var PhaseWIPCurrent = 0;
        var PhaseWIPHeld = 0;
        var PhaseExpenseUnbilled = 0;
        var PhaseWIPToClose = 0;
        var PhaseAmountToHold = 0;
        var PhaseAmountToDelete = 0;
        var PhaseAmountToTransfer = 0;
        $("#tblInvoicing tr.subphase[data-phaseid='" + PhaseId + "']").each(function () {
            var DetailId = $(this).data("detailid");
            var SubphaseCompensation = rawNum($("#subphaseInvoicingCompensation_" + DetailId).html());
            PhaseCompensation += SubphaseCompensation;
            PhaseSpentTotal += rawNum($("#subphaseInvoicingSpentTotal_" + DetailId).html());
            var SubphaseInvoicedTotal = rawNum($("#subphaseInvoicingInvoicedTotal_" + DetailId).html());
            PhaseInvoicedTotal += SubphaseInvoicedTotal;
            var SubphaseAmountToInvoice = rawNum($("#subphaseInvoicingAmountToInvoice_" + DetailId).html());
            PhaseAmountToInvoice += SubphaseAmountToInvoice;
            PhaseWIPTotal += rawNum($("#subphaseInvoicingWIPTotal_" + DetailId).html());
            PhaseWIPCurrent += rawNum($("#subphaseInvoicingWIPCurrent_" + DetailId).html());
            PhaseWIPHeld += rawNum($("#subphaseInvoicingWIPHeld_" + DetailId).html());
            PhaseExpenseUnbilled += rawNum($("#subphaseInvoicingExpenseUnbilled_" + DetailId).html());
            PhaseWIPToClose += rawNum($("#subphaseInvoicingWIPToClose_" + DetailId).html());
            PhaseAmountToHold += rawNum($("#subphaseInvoicingAmountToHold_" + DetailId).html());
            PhaseAmountToDelete += rawNum($("#subphaseInvoicingAmountToDelete_" + DetailId).html());
            PhaseAmountToTransfer += rawNum($("#subphaseInvoicingAmountToTransfer_" + DetailId).html());
            var SubphaseWIPCheck = (rawNum($("#subphaseInvoicingAmountToInvoice_" + DetailId).html()) + rawNum($("#subphaseInvoicingWIPToClose_" + DetailId).html()) + rawNum($("#subphaseInvoicingAmountToHold_" + DetailId).html()) + rawNum($("#subphaseInvoicingAmountToDelete_" + DetailId).html()) + rawNum($("#subphaseInvoicingAmountToTransfer_" + DetailId).html())) - (rawNum($("#subphaseInvoicingWIPTotal_" + DetailId).html()) + rawNum($("#subphaseInvoicingExpenseUnbilled_" + DetailId).html()));
            $("#subphaseInvoicingNewInvoicedPct_" + DetailId).html(CalcPct((+SubphaseInvoicedTotal + +SubphaseAmountToInvoice), SubphaseCompensation)); //New Invoiced PCT = ([Invoiced] + [Amount to Invoice]) / [Compensation] %
            $("#subphaseWIPCheck_" + DetailId).html(fmtMoney(SubphaseWIPCheck)); //WIP Check = (Amount to Invoice + WIP to Close Out + Amount to Hold + Amount to Delete + Amount to Transfer) – (WIP Total + Expense Unbilled)
        });
        $("#phaseInvoicingCompensation_" + PhaseId).html(fmtMoney(PhaseCompensation));
        $("#phaseInvoicingSpentTotal_" + PhaseId).html(fmtMoney(PhaseSpentTotal));
        $("#phaseInvoicingSpentPCT_" + PhaseId).html(CalcPct(PhaseSpentTotal, PhaseCompensation)); //Spent PCT = [Spent Total] / [Compensation] %
        $("#phaseInvoicingInvoicedTotal_" + PhaseId).html(fmtMoney(PhaseInvoicedTotal));
        $("#phaseInvoicingInvoicedPCT_" + PhaseId).html(CalcPct(PhaseInvoicedTotal, PhaseCompensation)); //Invoiced PCT = [Invoiced Total] / [Compensation] %
        PhaseWIPCheck = (PhaseAmountToInvoice + PhaseWIPToClose + PhaseAmountToHold + PhaseAmountToDelete + PhaseAmountToTransfer) - (PhaseWIPTotal + PhaseExpenseUnbilled);
        $("#phaseWIPCheck_" + PhaseId).html(fmtMoney(PhaseWIPCheck)); //WIP Check = (Amount to Invoice + WIP to Close Out + Amount to Hold + Amount to Delete + Amount to Transfer) – (WIP Total + Expense Unbilled)
        $("#phaseInvoicingAmountToInvoice_" + PhaseId).html(fmtMoney(PhaseAmountToInvoice));
        $("#phaseInvoicingNewInvoicedPct_" + PhaseId).html(CalcPct((+PhaseInvoicedTotal + +PhaseAmountToInvoice), PhaseCompensation)); //New Invoiced PCT = ([Invoiced] + [Amount to Invoice]) / [Compensation] %
        $("#phaseInvoicingWIPTotal_" + PhaseId).html(fmtMoney(PhaseWIPTotal));
        $("#phaseInvoicingWIPCurrent_" + PhaseId).html(fmtMoney(PhaseWIPCurrent));
        $("#phaseInvoicingWIPHeld_" + PhaseId).html(fmtMoney(PhaseWIPHeld));
        $("#phaseInvoicingExpenseUnbilled_" + PhaseId).html(fmtMoney(PhaseExpenseUnbilled));
        $("#phaseInvoicingWIPToClose_" + PhaseId).html(fmtMoney(PhaseWIPToClose));
        $("#phaseInvoicingAmountToHold_" + PhaseId).html(fmtMoney(PhaseAmountToHold));
        $("#phaseInvoicingAmountToDelete_" + PhaseId).html(fmtMoney(PhaseAmountToDelete));
        $("#phaseInvoicingAmountToTransfer_" + PhaseId).html(fmtMoney(PhaseAmountToTransfer));
    });

    //Rollup Phases to Project
    $(ProjectRows).each(function () {
        var ProjectId = $(this).data("projectid");
        var ProjectCompensation = 0;
        var ProjectSpentTotal = 0;
        var ProjectInvoicedTotal = 0;
        var ProjectWIPCheck = 0;
        var ProjectAmountToInvoice = 0;
        var ProjectWIPTotal = 0;
        var ProjectWIPCurrent = 0;
        var ProjectWIPHeld = 0;
        var ProjectExpenseUnbilled = 0;
        var ProjectWIPToClose = 0;
        var ProjectAmountToHold = 0;
        var ProjectAmountToDelete = 0;
        var ProjectAmountToTransfer = 0;
        $("#" + TableId + " tr.phase[data-projectid='" + ProjectId + "']").each(function () {
            var PhaseId = $(this).data("phaseid");
            ProjectCompensation += rawNum($("#phaseInvoicingCompensation_" + PhaseId).html());
            ProjectSpentTotal += rawNum($("#phaseInvoicingSpentTotal_" + PhaseId).html());
            ProjectInvoicedTotal += rawNum($("#phaseInvoicingInvoicedTotal_" + PhaseId).html());
            ProjectAmountToInvoice += rawNum($("#phaseInvoicingAmountToInvoice_" + PhaseId).html());
            ProjectWIPTotal += rawNum($("#phaseInvoicingWIPTotal_" + PhaseId).html());
            ProjectWIPCurrent += rawNum($("#phaseInvoicingWIPCurrent_" + PhaseId).html());
            ProjectWIPHeld += rawNum($("#phaseInvoicingWIPHeld_" + PhaseId).html());
            ProjectExpenseUnbilled += rawNum($("#phaseInvoicingExpenseUnbilled_" + PhaseId).html());
            ProjectWIPToClose += rawNum($("#phaseInvoicingWIPToClose_" + PhaseId).html());
            ProjectAmountToHold += rawNum($("#phaseInvoicingAmountToHold_" + PhaseId).html());
            ProjectAmountToDelete += rawNum($("#phaseInvoicingAmountToDelete_" + PhaseId).html());
            ProjectAmountToTransfer += rawNum($("#phaseInvoicingAmountToTransfer_" + PhaseId).html());
        });
        $("#projectInvoicingCompensation_" + ProjectId).html(fmtMoney(ProjectCompensation));
        $("#projectInvoicingSpentTotal_" + ProjectId).html(fmtMoney(ProjectSpentTotal));
        $("#projectInvoicingSpentPCT_" + ProjectId).html(CalcPct(ProjectSpentTotal, ProjectCompensation)); //Spent PCT = [Spent Total] / [Compensation] %
        $("#projectInvoicingInvoicedTotal_" + ProjectId).html(fmtMoney(ProjectInvoicedTotal));
        $("#projectInvoicingInvoicedPCT_" + ProjectId).html(CalcPct(ProjectInvoicedTotal, ProjectCompensation)); //Invoiced PCT = [Invoiced Total] / [Compensation] %
        ProjectWIPCheck = ProjectAmountToInvoice + ProjectWIPToClose + ProjectAmountToHold + ProjectAmountToDelete + ProjectAmountToTransfer - ProjectWIPTotal - ProjectExpenseUnbilled;
        $("#projectWIPCheck_" + ProjectId).html(fmtMoney(ProjectWIPCheck)); //WIP Check = (Amount to Invoice + WIP to Close Out + Amount to Hold + Amount to Delete + Amount to Transfer) – (WIP Total + Expense Unbilled)
        $("#projectInvoicingAmountToInvoice_" + ProjectId).html(fmtMoney(ProjectAmountToInvoice));
        $("#projectInvoicingNewInvoicedPct_" + ProjectId).html(CalcPct((+ProjectInvoicedTotal + +ProjectAmountToInvoice), ProjectCompensation)); //New Invoiced PCT = ([Invoiced] + [Amount to Invoice]) / [Compensation] %
        $("#projectInvoicingWIPTotal_" + ProjectId).html(fmtMoney(ProjectWIPTotal));
        $("#projectInvoicingWIPCurrent_" + ProjectId).html(fmtMoney(ProjectWIPCurrent));
        $("#projectInvoicingWIPHeld_" + ProjectId).html(fmtMoney(ProjectWIPHeld));
        $("#projectInvoicingExpenseUnbilled_" + ProjectId).html(fmtMoney(ProjectExpenseUnbilled));
        $("#projectInvoicingWIPToClose_" + ProjectId).html(fmtMoney(ProjectWIPToClose));
        $("#projectInvoicingAmountToHold_" + ProjectId).html(fmtMoney(ProjectAmountToHold));
        $("#projectInvoicingAmountToDelete_" + ProjectId).html(fmtMoney(ProjectAmountToDelete));
        $("#projectInvoicingAmountToTransfer_" + ProjectId).html(fmtMoney(ProjectAmountToTransfer));
    });

    //WIP Check
    $("#btnInvoiceAllWIP").addClass("w3-disabled");
    $("td.wipcheck").each(function () {
        var CheckVal = rawNum($(this).text());
        $(this).removeClass("warn");
        if (CheckVal < 0) {
            $(this).addClass("warn");
            $("#btnInvoiceAllWIP").removeClass("w3-disabled");
        }
    });
}

function CheckRules(TableID) {
    var Projects = g_ProjectData.Project;
    
    //Clear Issues Variables
    if (TableID == "tblETC") g_ETCIssues = [];
    if (TableID == "tblPrjDetails") g_ProjectDetailsIssues = [];
    if (TableID == "tblInvoicing") g_InvoicingIssues = [];

    //Loop through tables to check rules
    $(Projects).each(function () {
        var ProjectId = this.Id;
        var Phases = g_ProjectData.Phase.filter(x => { return x.InvoicingPeriodProjectId === ProjectId });
        Phases = Phases.sort(SortByProperty("WBS2"));
        var Subphases, PhaseId, SubphaseId, SubphaseDetail, SubphaseDetailId;
        var SubphasesMatchRuleOne = true; //default to true and then set to false if any subphase >= 100% or if project spent% >= 25%
        var SubphasesMatchRuleTwo = true; //default to true and then set to false if any subphase >= 60% or if project spent% not between 25% and 50%
        $(Phases).each(function () {
            PhaseId = this.Id;

            if (TableID == "tblETC") {
                //ETC Rule 11 changed per Paul P 2022-04-22: If ETC = 0 then Calc FPC = 100% else Calc FPC = [Spent Total] / [Calc EAC] %
                if ($("#phaseEstimateToComplete_" + PhaseId).html() == "$0.00") {
                    $("#phaseCalcFPC_" + PhaseId).css("color","red");
                }
                else {
                    $("#phaseCalcFPC_" + PhaseId).css("color","black");
                }
            }

            Subphases = g_ProjectData.Subphase.filter(x => { return x.InvoicingPeriodPhaseId === PhaseId });
            Subphases = Subphases.sort(SortByProperty("WBS3"));
            $(Subphases).each(function () {
                SubphaseId = this.Id;
                SubphaseDetail = g_ProjectData.Detail.find(x => { return x.InvoicingPeriodProjectId === ProjectId && x.InvoicingPeriodPhaseId === PhaseId && x.InvoicingPeriodSubphaseId === SubphaseId });
                SubphaseDetailId = SubphaseDetail.Id;

                var SubphaseNotesJSON = this.Notes.trim();
                var SubphaseNotesETC = "";
                var SubphaseNotesInvoicing = "";
                if (SubphaseNotesJSON.length > 0) {
                    objSubphaseNotes = JSON.parse(SubphaseNotesJSON);
                    if (objSubphaseNotes.ETC) {
                        SubphaseNotesETC = objSubphaseNotes.ETC;
                    }
                    if (objSubphaseNotes.Invoicing) {
                        SubphaseNotesInvoicing = objSubphaseNotes.Invoicing;
                    }
                }

                switch (TableID) {
                    case "tblETC":
                        var ProjectSpentPCT = rawNum($("#projectSpentPCT_" + ProjectId).html());
                        var SubphaseSpentPCT = rawNum($("#subphaseSpentPCT_" + SubphaseDetailId).html());
                        var SubphaseRisk = rawNum($("#subphaseRisk_" + SubphaseDetailId).html());

                        //Rule 1: If Spent % at L2 (Project Level) is less than 25% and Spent % for ALL L4 subphases are less than 100%, then ETC for each L4 subphase is equal to Calc Budget Variance. Data entry cells for # Hours to Complete, Current Rate, Estimated Expenses, and Risk should appear dark grey. Message: “ETC values are not required at this stage of the project.”
                        if (+ProjectSpentPCT >= 25) {
                            SubphasesMatchRuleOne = false;
                        }
                        if (SubphasesMatchRuleOne == true && +SubphaseSpentPCT >= 100) {
                            SubphasesMatchRuleOne = false;
                        }
                        //Rule 2: If Spent % at L2 is less than 50% and Spent % for ALL L4 subphases are less than 60%, then ETC for each L4 subphase is equal to Calc Budget Variance. Data entry cells for # Hours to Complete, Current Rate, Estimated Expenses, and Risk should appear dark grey. Message: “ETC values are not required at this stage of the project, but if you have reason to believe at this stage of the project that EAC will deviate from compensation, enter values accordingly.”
                        if (+ProjectSpentPCT >= 50 || +ProjectSpentPCT < 25) {
                            SubphasesMatchRuleTwo = false;
                        }
                        if (SubphasesMatchRuleTwo == true && +SubphaseSpentPCT >= 60) {
                            SubphasesMatchRuleTwo = false;
                        }
                        //Rule 8: When entering Risk, if the number is greater than zero, a pop-up box should appear with two fields for data entry and a dialogue box with the text: “Please enter the cost of the risk and provide a description.” The cost field should be a currency field. The description field should be a text field with a maximum of 250 characters.
                        if (+SubphaseRisk != 0 && SubphaseNotesETC.length == 0) {
                            AppendProjectIssue("ETC", GetRuleMessage("ETCRule8"));
                        }
                        //ETC Rule 11 changed per Paul P 2022-04-22: If ETC = 0 then Calc FPC = 100% else Calc FPC = [Spent Total] / [Calc EAC] %
                        if ($("#subphaseEstimateToComplete_" + SubphaseDetailId).html() == "$0.00") {
                            $("#subphaseCalcFPC_" + SubphaseDetailId).css("color", "red");
                        }
                        else {
                            $("#subphaseCalcFPC_" + SubphaseDetailId).css("color", "black");
                        }
                        //Rule 14: Once all data is entered at the L4 subphase level, the PM will be able to select the Project Details tab to review project details tab. If Spent Total is greater the Calc EAC, the following text: “Calc EAC is less than Spent Total. In order to proceed, update ETC” should appear in the review issues box.
                        if (rawNum($("#projectSpentTotal_" + ProjectId).html()) > rawNum($("#projectCalcEAC_" + ProjectId).html())) {
                            AppendProjectIssue("ETC", GetRuleMessage("ETCRule14"));
                        }
                        //Rule 15: 
                        /*
                         At the completion of the ETC calculation the PM will be able to select the Project Details tab to review project details tab.If the formula([Prior EAC] - [Calc EAC]) / [Prior EAC] is greater than 5 % or less then - 5 %, a dialogue box should appear with the following dialogue message and two fields for additional data.

                        “Please select from the drop - down box the reason for the change from prior EAC and give a written description.) ”

                        The first field should be a drop - down box with a list of explanations.These will be provided, and we should have the capability to expand this list on the fly.The second field should be a standard text field up to 250 characters.
                        
                        Change Note: As of 2022-02-11, only a question response is required. Added description in notes field is optional.
                        */
                        var ETCPriorEAC = rawNum($("#projectPriorEAC_" + ProjectId).html());
                        var ETCCalcEAC = rawNum($("#projectCalcEAC_" + ProjectId).html());
                        var ETC_PCT_Check = rawNum(CalcPct((+ETCPriorEAC - +ETCCalcEAC), ETCPriorEAC));
                        if (Math.abs(+ETC_PCT_Check) > 5) {
                            CheckRuleQuestions("ETC", 'Project', ProjectId, "ETCRule15");
                        }
                        else {
                            RemoveRuleQuestions("ETC", 'Project', ProjectId, "ETCRule15");
                        }

                        //Rules higher than 100 have been added after the initial PTI release
                        //Rule 101:
                        /*
                         If Current Rate is $0.00 then disable # Hours to Complete
                         */
                        var CurrentRate = rawNum($("#subphaseCurrentRate_" + SubphaseDetailId).html());
                        if (+CurrentRate == 0) {
                            ChangeCellByRule($("#subphaseHoursToComplete_" + SubphaseDetailId), false, "ETCRule101", true);
                        }
                        else {
                            $("#subphaseHoursToComplete_" + SubphaseDetailId).addClass("editable");
                            var TitleToRemove = GetRuleMessage("ETCRule101");
                            var UpdatedTitle = $("#subphaseHoursToComplete_" + SubphaseDetailId).attr("title");
                            if (UpdatedTitle) {
                                UpdatedTitle = UpdatedTitle.replace(TitleToRemove, "");
                                while (UpdatedTitle.indexOf("\n\n") > -1) {
                                    UpdatedTitle = UpdatedTitle.replace(/\n\n/ig, "\n"); //remove double newlines
                                }
                                $("#subphaseHoursToComplete_" + SubphaseDetailId).attr("title", UpdatedTitle);
                            }
                        }

                        //Rule 102:
                        /*
                         If Compensation > $0.00, Spent Total = $0.00, ETC = $0.00 and FPC = 100%, highlight Compensation cell bright orange
                         */
                        if (rawNum($("#subphaseCompensation_" + SubphaseDetailId).html()) > 0 && rawNum($("#subphaseSpentTotal_" + SubphaseDetailId).html()) == 0 && rawNum($("#subphaseEstimateToComplete_" + SubphaseDetailId).html()) == 0 && rawNum($("#subphaseCalcFPC_" + SubphaseDetailId).html()) == 100) {
                            $("#subphaseCompensation_" + SubphaseDetailId).addClass("hltOrange");
                            if (SubphaseNotesETC.length == 0) {
                                AppendProjectIssue("ETC", GetRuleMessage("ETCRule102"));
                            }
                        }
                        else {
                            $("#subphaseCompensation_" + SubphaseDetailId).removeClass("hltOrange");
                        }

                        break;
                    case "tblPrjDetails":
                        var SubphaseStatus = $("#subphasePrjDetailPhaseStatus_" + SubphaseDetailId).html();
                        var SubphaseFPC = rawNum($("#subphaseCalcFPC_" + SubphaseDetailId).html());
                        var SubphaseEstEndDate = new Date($("#subphaseEstEndDate_" + SubphaseDetailId).html());
                        var TodaysDate = new Date();
                        //Rule 1: If active and L4 Subphase Level end date is prior to current day and FPC is less than 100%, the field Updated End Date is mandatory and should be highlighted in yellow.
                        if (!(+SubphaseFPC < 100 && SubphaseEstEndDate < TodaysDate && SubphaseStatus.toLowerCase() == "active")) {
                            ChangeCellByRule($("#subphaseEstEndDateUpdated_" + SubphaseDetailId), true, "PrjDetailRule1", false);
                        }
                        //Rule 2: If not active or L4 Subphase Level end date is in the future, the field Updated End Date is not mandatory, and the field should be highlighted in dark gray.
                        if (SubphaseEstEndDate > TodaysDate || SubphaseStatus.toLowerCase() != "active") {
                            ChangeCellByRule($("#subphaseEstEndDateUpdated_" + SubphaseDetailId), true, "PrjDetailRule2", false);
                        }
                        break;
                    case "tblInvoicing":
                        //Rule 1: If data is entered in “Amt to Hold” column, a dialogue box should appear requesting a reason for holding WIP. The reasons should be listed in a dropdown box and the PM will be required to enter one reason for each subphase line. A list of reasons will be provided.
                        if (+rawNum($("#subphaseInvoicingAmountToHold_" + SubphaseDetailId).html()) != 0) {
                            CheckRuleQuestions("Invoicing", 'Subphase', SubphaseId, "InvoicingRule1");
                        }
                        else {
                            RemoveRuleQuestions("Invoicing", 'Subphase', SubphaseId, "InvoicingRule1");
                        }
                        //Rule 2: If data is entered in “Amt to Transfer” column, a dialogue box should appear requesting the project number, phase and subphase data where the WIP should be transferred.
                        if (+rawNum($("#subphaseInvoicingAmountToTransfer_" + SubphaseDetailId).html()) != 0 && SubphaseNotesInvoicing.length == 0) {
                            //Add to project issues to be displayed
                            AppendProjectIssue("Invoicing", GetRuleMessage("InvoicingRule2"));
                        }
                        //Rule 4: If the PM enters a value in “Amt to Delete” column, a dialogue box should appear requesting a reason for deleting WIP.
                        if (+rawNum($("#subphaseInvoicingAmountToDelete_" + SubphaseDetailId).html()) != 0) {
                            CheckRuleQuestions("Invoicing", 'Subphase', SubphaseId, "InvoicingRule4");
                        }
                        else {
                            RemoveRuleQuestions("Invoicing", 'Subphase', SubphaseId, "InvoicingRule4");
                        }
                        //Rule 5: The sum of Amount to Invoice and Invoiced cannot exceed compensation on projects with a revenue method of FPC and TENTE. A dialogue box should pop up with the statement: Amount to Invoice and Invoiced is greater than compensation. Please correct your entry. An exception to this rule must exist because there are some subphases where we have already invoiced more than compensation for the project types above.
                        //Rule 5 Note From Paul 2022-02-09: I will need to think about a little. This only happened because we didn’t have controls in place on the accounting side and it could be that there will be a time when we are expecting a change order to hit and we want to invoice in advance so maybe it is just a warning and the updated % Invoiced appears in red when it exceeds 100%
                        var AmtToInvoicePlusInvoiced = +rawNum($("#subphaseInvoicingAmountToInvoice_" + SubphaseDetailId).html()) + +rawNum($("#subphaseInvoicingInvoicedTotal_" + SubphaseDetailId).html());
                        var InvoicingCompensation = +rawNum($("#subphaseInvoicingCompensation_" + SubphaseDetailId).html())
                        var ProjectRevenueMethod = $("#subphaseRevenueMethod_" + SubphaseDetailId).html().trim().toUpperCase();
                        if (AmtToInvoicePlusInvoiced > InvoicingCompensation && (ProjectRevenueMethod == "FPC" || ProjectRevenueMethod == "TENTE")) {
                            ChangeCellByRule($("#subphaseInvoicingNewInvoicedPct_" + SubphaseDetailId), true, "InvoicingRule5", false);
                            $("#subphaseInvoicingNewInvoicedPct_" + SubphaseDetailId).css("color", "red");
                        }
                        else {
                            //Clear color and hover title
                            $("#subphaseInvoicingNewInvoicedPct_" + SubphaseDetailId).css("color", "black").attr("title", "");
                        }
                        //Rule 6: The sum of amount to invoice, WIP to be closed to the project, WIP Held, WIP delete, and WIP transferred must be equal to or greater than the sum of WIP Total and Expense Unbilled. If not, a dialogue box should generate stating “there are WIP and Expenses not accounted for. Please revise your entries. 
                        if (+rawNum($("#subphaseWIPCheck_" + SubphaseDetailId).html()) < 0) {
                            AppendProjectIssue("Invoicing", GetRuleMessage("InvoicingRule6"));
                        }
                        //If WIP Check is equal to zero the fields Amt to Invoice, WIP to close out, Amt to Hold, Amt to Delete, and Amt to Transfer,  should not be highlighted in yellow.
                        $("#subphaseInvoicingAmountToInvoice_" + SubphaseDetailId).removeClass("highlight");
                        $("#subphaseInvoicingWIPToClose_" + SubphaseDetailId).removeClass("highlight");
                        $("#subphaseInvoicingAmountToHold_" + SubphaseDetailId).removeClass("highlight");
                        $("#subphaseInvoicingAmountToDelete_" + SubphaseDetailId).removeClass("highlight");
                        $("#subphaseInvoicingAmountToTransfer_" + SubphaseDetailId).removeClass("highlight");
                        if (+rawNum($("#subphaseWIPCheck_" + SubphaseDetailId).html()) != 0) {
                            $("#subphaseInvoicingAmountToInvoice_" + SubphaseDetailId).addClass("highlight");
                            $("#subphaseInvoicingWIPToClose_" + SubphaseDetailId).addClass("highlight");
                            $("#subphaseInvoicingAmountToHold_" + SubphaseDetailId).addClass("highlight");
                            $("#subphaseInvoicingAmountToDelete_" + SubphaseDetailId).addClass("highlight");
                            $("#subphaseInvoicingAmountToTransfer_" + SubphaseDetailId).addClass("highlight");
                        }

                        /*
                         If Compensation > $0.00, Spent Total = $0.00, ETC = $0.00 and FPC = 100%, highlight Invoicing Compensation cell bright orange
                         */
                        if (rawNum($("#subphaseCompensation_" + SubphaseDetailId).html()) > 0 && rawNum($("#subphaseSpentTotal_" + SubphaseDetailId).html()) == 0 && rawNum($("#subphaseEstimateToComplete_" + SubphaseDetailId).html()) == 0 && rawNum($("#subphaseCalcFPC_" + SubphaseDetailId).html()) == 100) {
                            $("#subphaseInvoicingCompensation_" + SubphaseDetailId).addClass("hltOrange");
                        }
                        else {
                            $("#subphaseInvoicingCompensation_" + SubphaseDetailId).removeClass("hltOrange");
                        }

                        break;
                    default:
                }

            });
        });

        //If ETC Rules 1 or 2 were met, set Subphase ETC to be Subphase Variance for ALL subphases in ALL phasesand recalculate table
        if (TableID == "tblETC" && (SubphasesMatchRuleOne || SubphasesMatchRuleTwo)) {
            $(Phases).each(function () {
                PhaseId = this.Id;
                Subphases = g_ProjectData.Subphase.filter(x => { return x.InvoicingPeriodPhaseId === PhaseId });
                Subphases = Subphases.sort(SortByProperty("WBS3"));
                $(Subphases).each(function () {
                    SubphaseId = this.Id;
                    SubphaseDetail = g_ProjectData.Detail.find(x => { return x.InvoicingPeriodProjectId === ProjectId && x.InvoicingPeriodPhaseId === PhaseId && x.InvoicingPeriodSubphaseId === SubphaseId });
                    SubphaseDetailId = SubphaseDetail.Id;
                    SubphaseDetailId = SubphaseDetail.Id;

                    //Update Subphase Cells
                    var RuleForCellChange = (SubphasesMatchRuleOne) ? "ETCRule1" : "ETCRule2";
                    ChangeCellByRule($("#subphaseHoursToComplete_" + SubphaseDetailId), true, RuleForCellChange, false);
                    ChangeCellByRule($("#subphaseCurrentRate_" + SubphaseDetailId), true, RuleForCellChange, false);
                    ChangeCellByRule($("#subphaseEstimateExpenses_" + SubphaseDetailId), true, RuleForCellChange, false);
                    ChangeCellByRule($("#subphaseRisk_" + SubphaseDetailId), true, RuleForCellChange, false);

                    //Update Subphase ETC to equal Variance if not overridden
                    var bSetETCToMatchVariance = true;
                    var Rule1Override = g_RuleOverrides.find(x => { return x.Value === "ETCRule1" });
                    var Rule1OverrideId = (Rule1Override) ? Rule1Override.Id : "";
                    var Rule2Override = g_RuleOverrides.find(x => { return x.Value === "ETCRule2" });
                    var Rule2OverrideId = (Rule2Override) ? Rule2Override.Id : "";
                    var SubphaseMetadataExtension = JSON.stringify(SubphaseDetail.MetadataExtension).toLowerCase();
                    if (SubphaseMetadataExtension.trim().length > 0) {
                        if (SubphaseMetadataExtension.indexOf(Rule1OverrideId) > -1 || SubphaseMetadataExtension.indexOf(Rule2OverrideId) > -1) {
                            bSetETCToMatchVariance = false;
                        }
                    }
                    if (bSetETCToMatchVariance) {
                        var SubphaseVariance = rawNum($("#subphaseVariance_" + SubphaseDetailId).html());
                        var SubphaseETC = rawNum($("#subphaseEstimateToComplete_" + SubphaseDetailId).html());
                        if (+SubphaseETC != +SubphaseVariance) {
                            $("#subphaseEstimateToComplete_" + SubphaseDetailId).html(fmtMoney(SubphaseVariance));
                            SaveValue(SubphaseDetailId, "ETC", SubphaseVariance).then(
                                function (response) {
                                    if (response == "success") {
                                        RecalculateTable(TableID);
                                    }
                                    else {
                                        alert(response);
                                    }
                                },
                                function (errormsg) {
                                    alert('Error updating ETC based on rules: ' + errormsg);
                                }
                            );
                        }
                    }
                });
            });
        }
    });

    UpdatePageDisplay();
}

function CheckRuleQuestions(Tab, RowType, RowId, RuleName) {
    var QuestionLinkHTML = "&nbsp;<a href='javascript:/**/' class='questionslink' data-tab='" + Tab + "' data-rowtype='" + RowType + "' data-id='" + RowId + "' title='Review Questions' onclick='OpenModal(\"Questions\", this);'><i class='fa fa-question-circle' style='color:~questioniconcolor~;'></i></a>";
    var QuestionIconColor = "Black";
    var OpenQuestionIconColor = "Red";
    var OpenQuestionMessage = "There are unanswered questions."

    var DataRow = g_ProjectData[RowType].find(x => { return x.Id === RowId });

    var RowQuestions = {};

    //Load RowQuestions if Questions already exist
    if (DataRow) {
        if (DataRow.Questions.toString().length > 0) {
            //Questions will be a string when first loaded from database. Convert to JSON object for use
            if (typeof DataRow.Questions == "string") DataRow.Questions = JSON.parse(DataRow.Questions);
            RowQuestions = DataRow.Questions;
        }
    }

    //Get Question and Responses for Rule
    var QuestionsForRule = GetRelatedObjects("Rules", RuleName, "Has Question", "Questions");
    
    //Check Responses to set icon color appropriately
    $(QuestionsForRule).each(function () {
        var QuestionId = this.Id;
        var QuestionType = Tab + "Questions";

        //Add question with empty response if it hasn't been added
        if (!RowQuestions[QuestionType]) RowQuestions[QuestionType] = [];
        var RowQuestionsForThisType = RowQuestions[QuestionType].find(x => { return x.QuestionId === QuestionId });
        if (!RowQuestionsForThisType) {
            RowQuestions[QuestionType].push({ "QuestionId": QuestionId, "ResponseId": "" });
            QuestionIconColor = OpenQuestionIconColor;
        }
        if (RowQuestions[QuestionType]) {
            $(RowQuestions[QuestionType]).each(function () {
                if (this.ResponseId.length == 0) QuestionIconColor = OpenQuestionIconColor; //Question has no response
            });
        }
        DataRow.Questions = RowQuestions;
    });

    //Note unanswered questions in project issues
    if (QuestionIconColor == OpenQuestionIconColor) {
        AppendProjectIssue(Tab, OpenQuestionMessage);
    }

    //Show Question Link
    QuestionLinkHTML = QuestionLinkHTML.replace(/~questioniconcolor~/ig, QuestionIconColor);
    $("#" + Tab + "Question_" + RowId).html(QuestionLinkHTML);
}

function RemoveRuleQuestions(Tab, RowType, RowId, RuleName) {
    var DataRow = g_ProjectData[RowType].find(x => { return x.Id === RowId });

    var RowQuestions = {};

    //Load RowQuestions if questions exist for row
    if (DataRow) {
        if (DataRow.Questions.toString().length > 0) {
            //Questions will be a string when first loaded from database. Convert to JSON object for use
            if (typeof DataRow.Questions == "string") DataRow.Questions = JSON.parse(DataRow.Questions);
            RowQuestions = DataRow.Questions;
        }
    }
    var QuestionType = Tab + "Questions";
    if (RowQuestions[QuestionType]) {
        var QuestionsForRule = GetRelatedObjects("Rules", RuleName, "Has Question", "Questions");
        var NewQuestionsForRule = [];
        $(QuestionsForRule).each(function () {
            var QuestionForRule = this;
            $(RowQuestions[QuestionType]).each(function () {
                if (QuestionForRule.Id != this.QuestionId) {
                    NewQuestionsForRule.push(this);
                }
            });
        });
        RowQuestions[QuestionType] = NewQuestionsForRule;

        //Update Questions Value in database
        var SaveToTableName = "InvoicingPeriod" + RowType;
        SaveValue(DataRow.Id, "Questions", JSON.stringify(DataRow.Questions), SaveToTableName);

        //Hide question link if no questions to answer
        if (RowQuestions[QuestionType].length == 0) {
            $("#" + Tab + "Question_" + RowId).html("");
        }
    }
}

function LoadModalQuestionsForRow(QuestionLink) {
    var RowType = $(QuestionLink).data("rowtype");
    var RowId = $(QuestionLink).data("id");
    var Tab = $(QuestionLink).data("tab");
    var DataRow = g_ProjectData[RowType].find(x => { return x.Id === RowId });
    var QuestionType = Tab + "Questions";

    //Questions will be a string when first loaded from database. Convert to JSON object for use
    if (typeof DataRow.Questions == "string") DataRow.Questions = JSON.parse(DataRow.Questions);
    var QuestionsToLoad = DataRow.Questions[QuestionType];

    $("#modalQuestions").html("");
    if (QuestionsToLoad) {
        $(QuestionsToLoad).each(function () {
            var QuestionId = this.QuestionId;
            var ResponseId = this.ResponseId;
            var ThisQuestion = g_RuleQuestions.find(x => { return x.Id === QuestionId });
            if (ThisQuestion) {
                var QuestionText = ThisQuestion.Value;
                var ThisQuestionBlock = "<div>";
                ThisQuestionBlock += "<h4>" + htmlEncode(QuestionText) + "</h4>";
                var ResponsesToQuestion = GetRelatedObjects("Questions", QuestionText, "Has Response", "Responses");
                ResponsesToQuestion = ResponsesToQuestion.sort(SortByProperty("Value")); //Display Stuff
                var ResponseSelect = "<select class='questionselect' data-questionid='" + QuestionId + "' class='w3-select' style='margin-bottom: 10px;'><option value='0'>- No Response Selected -</option>";
                $(ResponsesToQuestion).each(function () {
                    var ResponseSelected = (ResponseId == this.Id) ? " selected" : "";
                    ResponseSelect += "<option value='" + this.Id + "'" + ResponseSelected + ">" + htmlEncode(this.Value) + "</option>";
                });
                ResponseSelect += "</select>";
                ThisQuestionBlock += ResponseSelect + "</div>";
                var UpdatedQuestionHTML = $("#modalQuestions").html() + ThisQuestionBlock;
                $("#modalQuestions").html(UpdatedQuestionHTML);
            }
        });
    }
    

}

function ChangeCellByRule(Cell, MakeGray, RuleName, Disable) {

    $(Cell).removeClass("gray");
    if (MakeGray) {
        $(Cell).addClass("gray");
    }

    if (Disable) {
        $(Cell).removeClass("editable");
    }

    var CellTitle = GetRuleMessage(RuleName);
    if ($(Cell).attr("title")) {
        if ($(Cell).attr("title").indexOf(CellTitle) == -1) {
            CellTitle = CellTitle + "\n\n" + $(Cell).attr("title");
            $(Cell).attr("title", CellTitle);
        }
    }
    else {
        $(Cell).attr("title", CellTitle);
    }

    //Add RuleName as class to cell's row
    var CellRow = $(Cell).parent();
    $(CellRow).addClass(RuleName);
}

function AppendProjectIssue(IssueType, IssueText) {
    switch (IssueType) {
        case "ETC":
            if ($.inArray(IssueText, g_ETCIssues) == -1) {
                g_ETCIssues.push(IssueText);
            }
            break;
        case "PrjDetails":
            if ($.inArray(IssueText, g_ProjectDetailsIssues) == -1) {
                g_ProjectDetailsIssues.push(IssueText);
            }
            break;
        case "Invoicing":
            if ($.inArray(IssueText, g_InvoicingIssues) == -1) {
                g_InvoicingIssues.push(IssueText);
            }
            break;
        default:
    }
}

function FormatProjectIssues(IssueType) {
    var Result = "";
    var WorkingArray;
    switch (IssueType) {
        case "ETC":
            WorkingArray = g_ETCIssues;
            break;
        case "ProjectDetails":
            WorkingArray = g_ProjectDetailsIssues;
            break;
        case "Invoicing":
            WorkingArray = g_InvoicingIssues;
            break;
        default:
    }

    $(WorkingArray).each(function () {
        Result += "<p>" + this + "</p>";
    });

    if (Result == "") {
        Result = "<p>No issues found.</p>";
    }

    return Result;
}

function GetRuleMessage(RuleName) {
    var ReturnMessages = [];
    var RuleMessage = GetRelatedObjects("Rules", RuleName, "Has Message", "Messages");
    if (RuleMessage.length > 0) {
        $(RuleMessage).each(function () {
            ReturnMessages.push(this.Value);
        });
    }

    return ReturnMessages.join(" ");
}

function GetRuleQuestion(RuleName) {
    var ReturnQuestions = [];
    var RuleQuestion = GetRelatedObjects("Questions", RuleName, "Is Question For Rule", "Rules");
    if (RuleQuestion.length > 0) {
        $(RuleQuestion).each(function () {
            ReturnQuestions.push(this.Value);
        });
    }

    return ReturnQuestions.join(" ");
}

function GetRelatedObjects(SubjectList, SubjectValue, PredicateName, ObjectList) {
    var ReturnObjects = [];

    //Subject
    var SubjectCollection;
    if (SubjectList == "Rules") SubjectCollection = g_Rules;
    if (SubjectList == "Messages") SubjectCollection = g_RuleMessages;
    if (SubjectList == "Questions") SubjectCollection = g_RuleQuestions;
    if (SubjectList == "Responses") SubjectCollection = g_QuestionResponses;
    var Subject = (SubjectValue.trim().length > 0) ? SubjectCollection.find(x => { return x.Value === SubjectValue }) : null;

    //Predicate
    var PredicateId = g_MetadataPredicates.find(x => { return x.Value === PredicateName }).Id.toUpperCase();

    //Object
    var ObjectCollection;
    if (ObjectList == "Rules") ObjectCollection = g_Rules;
    if (ObjectList == "Messages") ObjectCollection = g_RuleMessages;
    if (ObjectList == "Questions") ObjectCollection = g_RuleQuestions;
    if (ObjectList == "Responses") ObjectCollection = g_QuestionResponses;

    if (Subject) {
        var Predicates = JSON.parse(Subject.Predicates);
        var RelatedObjectId;
        $(Predicates).each(function () {
            if (this.Predicate.toUpperCase() == PredicateId.toUpperCase()) {
                RelatedObjectId = this.Object;
                $(ObjectCollection).each(function () {
                    if (this.Id.toUpperCase() == RelatedObjectId.toUpperCase()) {
                        ReturnObjects.push(this);
                    }
                });
            }
        });
    }
    return ReturnObjects;
}


function SetNotesStyle(NotesLink) {
    var NotesIcon = $(NotesLink).children().first();
    var Id = $(NotesLink).data("id");
    var RowType = $(NotesLink).data("rowtype");
    var Tab = $(NotesLink).data("tab");
    var DataRow = g_ProjectData[RowType].find(x => { return x.Id === Id });
    $(NotesIcon).removeClass("fa-file-text");
    $(NotesIcon).removeClass("fa-file-o");
    $(NotesIcon).addClass("fa-file-o");
    if (DataRow.Notes.length > 0) {
        var Notes = JSON.parse(DataRow.Notes)[Tab];
        if (Notes) {
            if (Notes.length > 0) {
                $(NotesIcon).removeClass("fa-file-o");
                $(NotesIcon).addClass("fa-file-text");
            }
        }
    }
}

function UpdatePageDisplay() {
    var CurrentSessionInvoicingPeriodIsActive = $("#lblCurrentActivePeriodName").data("periodopen") == "True";

    //Change style of Notes links where notes exist
    $(".noteslink").each(function () {
        SetNotesStyle(this);
    });

    //Admin checkboxes
    $("#chkDVPDataEntryComplete").attr("disabled", true);
    $("#chkInvoiceSentToPM").attr("disabled", true);
    if (g_UserPermission > 1 && CurrentSessionInvoicingPeriodIsActive) {
        $("#chkDVPDataEntryComplete").attr("disabled", false);
        $("#chkInvoiceSentToPM").attr("disabled", false);
    }

    //Project On Hold Button
    UpdateProjectOnHoldDisplay();

    if (CurrentSessionInvoicingPeriodIsActive && ((g_ETCIssues.length > 0 || g_ProjectDetailsIssues.length > 0 || g_InvoicingIssues.length > 0) && g_ProjectStatus.InvoicingStatus != "Approved")) {
        $("#btnSubmit").addClass("w3-hide");
        $("#btnApprove").addClass("w3-hide");
        $("#btnUndo").addClass("w3-hide");
        $("#btnFinalApproval").addClass("w3-hide");
        $("#btnReviewIssues").removeClass("w3-hide");
    }
    else {
        $("#btnReviewIssues").addClass("w3-hide");
        $("#btnSubmit").removeClass("w3-hide");
        switch (g_ProjectStatus.Status) {
            case 'Submitted':
                //Read-Only for PMs, Review for submission by Admins and above
                $("#btnSubmit").addClass("w3-hide"); //Hide Submit button
                
                if (g_UserPermission < 2) {
                    //Make read-only for PMs
                    $("td.editable").removeClass("editable");
                    $("#btnInvoiceAllWIP").addClass("w3-disabled");
                    $("#btnProjectOnHold").addClass("w3-disabled");
                    $(".carry-notes-forward").addClass("w3-disabled");
                    $("#btnModalSave").addClass("w3-disabled-all"); //Disable Save button for Modal Dialog (Notes and Questions/Responses)
                    
                    if (g_ProjectStatus.ApprovalStatus != "Approved") {
                        //Allow PM to unsubmit if project is not approved
                        ShowUndoButton("Unsubmit");

                        //Allow PM Team Lead (PMTL) to Approve if project is not approved
                        if (g_IsTeamLead) {
                            $("#btnApprove").removeClass("w3-hide");
                        }
                    }
                }
                else {
                    //Show Approve button for Admins and above
                    $("#btnApprove").removeClass("w3-hide");

                    //Allow return to PM if project is not approved
                    if (g_UserPermission > 2 && g_ProjectStatus.ApprovalStatus != "Approved") {
                        ShowUndoButton("ReturnToPM");
                    }
                }
                break;
            default:
        }
        if (g_ProjectStatus.ApprovalStatus == "Approved") {
            if (g_UserPermission < 4) {
                //Only OWNER group can edit fields on the Estimate to Complete tab (ETC) after the ApprovalStatus = Approved (Enhancements Sept 2022)
                $("td.etctab").removeClass("editable");
                $("#btnModalSave").addClass("w3-disabled-etc"); //Disable Save button for Modal Dialog (Notes and Questions/Responses)
            }
        }
        if (g_ProjectStatus.InvoicingStatus == "Approved" || !CurrentSessionInvoicingPeriodIsActive) {
            //After invoicing approval or if invoicing period is closed then read-only for all
            $("td.editable").removeClass("editable");
            $("#btnInvoiceAllWIP").addClass("w3-disabled");
            $("#btnProjectOnHold").addClass("w3-disabled");
            $(".carry-notes-forward").addClass("w3-disabled");
            $("#btnSubmit").addClass("w3-hide"); //Hide Submit button
            $("#btnApprove").addClass("w3-hide");
            if (CurrentSessionInvoicingPeriodIsActive) { //Only show Final Approval button if invoicing period is active
                $("#btnFinalApproval").removeClass("w3-hide");
            }
            $("#btnModalSave").addClass("w3-disabled-all"); //Disable Save button for Modal Dialog (Notes and Questions/Responses)

            //Allow Admins to unapprove if invoicing period is open
            if (g_UserPermission > 2 && CurrentSessionInvoicingPeriodIsActive) {
                ShowUndoButton("Unapprove");
            }
        }
    }
}

function ShowUndoButton(ButtonMode) {
    switch (ButtonMode) {
        case "Unsubmit":
            $("#btnUndo").html("Unsubmit").data("mode", ButtonMode).removeClass("w3-hide");
            break;
        case "ReturnToPM":
            $("#btnUndo").html("Return To PM").data("mode", ButtonMode).removeClass("w3-hide");
            break;
        case "Unapprove":
            $("#btnUndo").html("Unapprove").data("mode", ButtonMode).removeClass("w3-hide");
            break;
        default:
            $("#btnUndo").html("Unsubmit/Return to PM/ Unapprove").data("mode", "undefined");
            break;
    }
}

function MakeDataTable(tblId, orderable, OutputFilename, ShowOutputButtons, AllowScroll) {
    var OutputButtons = [];
    if (ShowOutputButtons) {
        OutputButtons = [
            'copy',
            {
                extend: 'excel',
                text: 'Excel',
                title: '',
                filename: OutputFilename
            },
            {
                extend: 'print',
                text: 'Print',
                title: OutputFilename
            }
        ];
    }
    var DatatableSettings = {
        "preDrawCallback": function (settings) {
            pageScrollPos = $('div.dataTables_scrollBody:visible').scrollTop();
        },
        "drawCallback": function (settings) {
            $('div.dataTables_scrollBody:visible').scrollTop(pageScrollPos);
        },
        stateSave: true,
        paging: false,
        ordering: orderable,
        scrollY: 686,
        scrollX: true,
        autoWidth: true,
        scrollCollapse: true,
        buttons: OutputButtons,
        dom: 'Blfrtip'
    };
    if (!AllowScroll) {
        DatatableSettings.scrollY = "";
        DatatableSettings.scrollCollapse = false;
    }

    var dt = $("#" + tblId).DataTable(DatatableSettings);
    
    FormatDatatableDisplay();

    return dt;
}

function FormatDatatableDisplay(tblId) {
    //Remove Search
    $(".dataTables_filter").hide();

    //Remove Row Info
    $(".dataTables_info").hide();

    //Position button panel
    $(".dt-buttons").css("margin-top", "26px");

    //Style export buttons
    $(".dt-button").addClass("w3-button w3-teal w3-small");
}

function ShowHideAll(ShowOrHide) {
    $("span.showhide").each(function () {
        if (ShowOrHide == "Show") {
            $(this).html("+");
        }
        else {
            $(this).html("-");
        }
        $(this).parent().trigger("click");
    });
}

function ShowHideDetail(EventButton) {
    var GlyphSpan = $(EventButton).children()[0];
    var bHide = ($(GlyphSpan).html() == "-") ? true : false;
    if (bHide) {
        $(GlyphSpan).html("+");
    }
    else {
        $(GlyphSpan).html("-");
    }

    var PhaseRow = $(EventButton).parent();
    var PhaseId = $(PhaseRow).data("phaseid");
    $("tr.subphase").each(function () {
        if ($(this).data("phaseid") == PhaseId) {
            if (bHide) {
                $(this).addClass("PhaseCollapsed");
                $(this).hide();
            }
            else {
                $(this).removeClass("PhaseCollapsed");
                if (!$(this).hasClass("HiddenByStatus")) {
                    $(this).show();
                }
            }
        }
    });
}

function ToggleSubphaseStatusDisplay() {
    var lblActiveSubphases = "Show Active Status Only";
    var lblAllSubphases = "Show All Statuses";
    var DisplayStatus;
    var CurrentLabel = $(".lnkActiveSubphaseToggle").html();
    if (CurrentLabel == lblActiveSubphases) {
        //Hide Dormant/Inactive Subphases
        $(".lnkActiveSubphaseToggle").html(lblAllSubphases);
        DisplayStatus = "Active";
    }
    else {
        //Show All Subphases
        $(".lnkActiveSubphaseToggle").html(lblActiveSubphases);
        DisplayStatus = "All";
    }
    $("tr.subphase").each(function () {
        var bActive = $(this).html().indexOf(">Active</td>") > -1;
        if (DisplayStatus == "All") {
            $(this).removeClass("HiddenByStatus");
            if (!$(this).hasClass("PhaseCollapsed")) {
                $(this).show();
            }
        }
        else if (bActive && DisplayStatus == "Active") {
            $(this).removeClass("HiddenByStatus");
            if (!$(this).hasClass("PhaseCollapsed")) {
                $(this).show();
            }
        }
        else {
            $(this).addClass("HiddenByStatus");
            $(this).hide();
        }
    });
}

//Editable Table Fields
function CellEditOn(cell) {
    if ($(cell).html().indexOf("<input") == -1 && $(cell).html().indexOf("<select") == -1) {
        //Only add input control if not already in edit mode
        var guid = $(cell).data("guid");
        var cellvalue = $(cell).text();
        if ($(cell).hasClass("moneycell") || $(cell).hasClass("numcell") || $(cell).hasClass("pctcell")) cellvalue = rawNum(cellvalue);
        var boxWidth = fmtNum((rawNum($(cell).css("width").replace(/px/ig, "")) * .9), 0) + "px";
        var inputType = "text";
        if ($(cell).hasClass("datecell")) {
            inputType = "date";
            cellvalue = FormatDateDisplay(cellvalue, "datebox");
        }
        var editbox = "<input type='" + inputType + "' class='tbleditbox' style='width:" + boxWidth + "' onblur='if(g_PageKey != \"Enter\"){CellEditOff(this);}' data-origval='" + cellvalue + "' onkeyup='CellEditKeyup(this);' id='" + guid + "'></input>";

        //Select Lists
        if ($(cell).hasClass("employeelist")) {
            editbox = "<select id='" + guid + "' class='tbleditbox' style='width:" + boxWidth + "' onchange='CellEditOff(this);' onblur='CellEditOff(this);'>" + EmployeeListOptions() + "</select>";
            cellvalue = $(cell).data("empid");
        }
        if ($(cell).hasClass("revenuemethodlist")) {
            editbox = "<select id='" + guid + "' class='tbleditbox' style='width:" + boxWidth + "' onchange='CellEditOff(this);' onblur='CellEditOff(this);'>" + RevenueMethodOptions() + "</select>";
        }
        if ($(cell).hasClass("phasestatuslist")) {
            editbox = "<select id='" + guid + "' class='tbleditbox' style='width:" + boxWidth + "' onchange='CellEditOff(this);' onblur='CellEditOff(this);'>" + PhaseStatusOptions() + "</select>";
        }
        $(cell).html(editbox);
        $("#" + guid).val(cellvalue);
        $("#" + guid).focus();
        $("#" + guid).select();
    }
}

function EmployeeListOptions() {
    var Options = "<option value=''>--Not Selected--</option>";
    $(g_AllEmployees).each(function () {
        var OptionText = this.LastName.trim() + ", " + this.FirstName.trim();
        Options += "<option value='" + this.Id + "'>" + OptionText + "</option>";
    });
    return Options;
}

function RevenueMethodOptions() {
    var Options = "<option value=''>--Not Selected--</option>";
    $(g_RevenueMethods).each(function () {
        var OptionText = this.RevenueMethod.trim();
        Options += "<option value='" + this.RevenueMethod.trim() + "'>" + OptionText + "</option>";
    });
    return Options;
}

function PhaseStatusOptions() {
    var Options = "<option value=''>--Not Selected--</option>";
    $(g_PhaseStatuses).each(function () {
        var OptionText = this.PhaseStatus.trim();
        Options += "<option value='" + this.PhaseStatus.trim() + "'>" + OptionText + "</option>";
    });
    return Options;
}

function GetRowNotesText(RowType, Tab, Id) {
    var ReturnNotesText = "";
    var DataRow = g_ProjectData[RowType].find(x => { return x.Id === Id });
    if (DataRow.Notes.length > 0) {
        ReturnNotesText = JSON.parse(DataRow.Notes)[Tab];
    }
    return ReturnNotesText;
}

function OpenModal(ModalMode, Opener) {
    //Hide all modal sections
    $("#modalNotes").addClass("w3-hide");
    $("#modalIssues").addClass("w3-hide");
    $("#modalQuestions").addClass("w3-hide");
    $("#modalTableData").addClass("w3-hide");
    $("#btnModalCancel").addClass("w3-hide");
    $("#btnModalClose").addClass("w3-hide");
    $("#btnModalSave").addClass("w3-hide");

    //Set Modal Save Reference
    //var Tab = $(Opener).data("tab");
    var Tab = ($(Opener).data("tab")) ? $(Opener).data("tab") : "all";
    var Id = $(Opener).data("id");
    var RowType = $(Opener).data("rowtype");
    var DataRow;
    var saveref = { "ModalMode": ModalMode, "Tab": Tab, "Id": Id };
    $("#divModal").data("saveref", saveref);

    //Handle read-only conditions
    $("#btnModalSave").removeClass("w3-disabled");
    var disabledClass = "w3-disabled-" + Tab.toLowerCase();
    if ($("#btnModalSave").hasClass("w3-disabled-all") || $("#btnModalSave").hasClass(disabledClass)) {
        $("#btnModalSave").addClass("w3-disabled");
    }

    switch (ModalMode) {
        case "Notes":
            saveref.RowType = RowType;
            DataRow = g_ProjectData[RowType].find(x => { return x.Id === Id });
            $("#txtNotes").val(GetRowNotesText(RowType, Tab, Id));
            $("#ModalHeading").html(Tab + " " + RowType + " Notes: " + htmlEncode(DataRow.Name));
            $("#btnModalCancel").removeClass("w3-hide");
            $("#btnModalSave").removeClass("w3-hide");
            $("#modalNotes").removeClass("w3-hide");
            $("#divModal").show();
            $("#txtNotes").focus();

            break;
        case "Issues":
            $("#ModalHeading").html("Project Issues");
            $("#btnModalClose").removeClass("w3-hide");

            $("#ETCIssuesContent").html(FormatProjectIssues("ETC"));
            $("#ProjectDetailIssuesContent").html(FormatProjectIssues("ProjectDetails"));
            $("#InvoicingIssuesContent").html(FormatProjectIssues("Invoicing"));

            $("#modalIssues").removeClass("w3-hide");

            //Prefix tab label with asterisk where there are issues
            $("#btnModalIssuesTabETC").html((g_ETCIssues.length > 0) ? "*" + $("#btnModalIssuesTabETC").html().replace(/\*/ig, "") : $("#btnModalIssuesTabETC").html().replace(/\*/ig, ""));
            $("#btnModalIssuesTabProjectDetail").html((g_ProjectDetailsIssues.length > 0) ? "*" + $("#btnModalIssuesTabProjectDetail").html().replace(/\*/ig, "") : $("#btnModalIssuesTabProjectDetail").html().replace(/\*/ig, ""));
            $("#btnModalIssuesTabInvoicing").html((g_InvoicingIssues.length > 0) ? "*" + $("#btnModalIssuesTabInvoicing").html().replace(/\*/ig, "") : $("#btnModalIssuesTabInvoicing").html().replace(/\*/ig, ""));

            $("#divModal").show();
            if (g_ETCIssues.length > 0) {
                $("#btnModalIssuesTabETC").trigger("click");
            }
            else if (g_ProjectDetailsIssues.length > 0) {
                $("#btnModalIssuesTabProjectDetail").trigger("click");
            }
            else if (g_InvoicingIssues.length > 0) {
                $("#btnModalIssuesTabInvoicing").trigger("click");
            }
            else {
                $("#btnModalIssuesTabETC").trigger("click");
            }
            break;
        case "Questions":
            saveref.RowType = RowType;
            DataRow = g_ProjectData[RowType].find(x => { return x.Id === Id });
            $("#ModalHeading").html(Tab + " " + RowType + " Questions: " + htmlEncode(DataRow.Name));
            $("#modalQuestions").removeClass("w3-hide");
            $("#btnModalCancel").removeClass("w3-hide");
            $("#btnModalSave").removeClass("w3-hide");

            LoadModalQuestionsForRow(Opener);

            $("#divModal").show();
            break;
        case "ExportTable":
            var WorkingHTML;
            var TableId;
            var ExportFilenamePrefix;
            $("#modalTableData").removeClass("w3-hide");
            $("#btnModalClose").removeClass("w3-hide");

            var ActiveTab = $(".tabBtn.w3-nei").data("tab");
            $("#modalTableData").html("<table id='tblModalExport' style='font-size:9px;'><thead id='tblModalExportHead'></thead><tbody id='tblModalExportBody'></tbody></table>");
            switch (ActiveTab) {
                case "ETC":
                    TableId = "tblETC";
                    $("#ModalHeading").html("ETC Export View: " + g_ProjectData.Project[0].WBS1 + " " + g_ProjectData.ProjectClientName + " (" + g_ProjectData.ProjectManagerName + ")");
                    ExportFilenamePrefix = "Estimate To Complete";
                    break;
                case "Project":
                    TableId = "tblPrjDetails";
                    $("#ModalHeading").html("Project Details Export View: " + g_ProjectData.Project[0].WBS1 + " " + g_ProjectData.ProjectClientName + " (" + g_ProjectData.ProjectManagerName + ")");
                    ExportFilenamePrefix = "Project Details";
                    break;
                case "Invoicing":
                    TableId = "tblInvoicing";
                    $("#ModalHeading").html("Invoicing Export View: " + g_ProjectData.Project[0].WBS1 + " " + g_ProjectData.ProjectClientName + " (" + g_ProjectData.ProjectManagerName + ")");
                    ExportFilenamePrefix = "Invoicing";
                    break;
                default:
            }
            //Remove show/hide th from table head row
            WorkingHTML = $("#" + TableId + "Head").html().replace(/<th.*.dataentryshowhide.*?th>/ig, "");
            $("#tblModalExportHead").html(WorkingHTML);
            //Remove show/hide td from table body rows and update notes and questions classes for replacement below
            WorkingHTML = $("#" + TableId + "Data").html().replace(/<td class="dataentryshowhide.*?td>/ig, "").replace(/noteslink/ig, "exportnoteslink").replace(/questionslink/ig, "exportquestionslink");
            WorkingHTML = WorkingHTML.replace(/display:.*?none;/ig, ""); //Unhide any rows that were collapsed in main display
            $("#tblModalExportBody").html(WorkingHTML);
            
            //Replace Notes icon with Notes Text
            $(".exportnoteslink").each(function () {
                var Tab = $(this).data("tab");
                var RowType = $(this).data("rowtype");
                var RowId = $(this).data("id");
                var NotesContainer = $(this).parent();
                var RowNotes = GetRowNotesText(RowType, Tab, RowId);
                $(this).remove();
                if (RowNotes) {
                    if (RowNotes.trim().length > 0) {
                        $(NotesContainer).append(" | Notes: " + RowNotes);
                    }
                }
            });
            //Replace Question icon with Questions/Responses
            $(".exportquestionslink").each(function () {
                var Tab = $(this).data("tab");
                var RowType = $(this).data("rowtype");
                var RowId = $(this).data("id");
                var QuestionsContainer = $(this).parent();
                $(this).remove();
                DataRow = g_ProjectData[RowType].find(x => { return x.Id === RowId });
                var QuestionType = Tab + "Questions";
                var RowQuestions = DataRow.Questions[QuestionType];
                $(RowQuestions).each(function () {
                    var QuestionId = this.QuestionId;
                    var ResponseId = this.ResponseId;
                    var QuestionText = g_RuleQuestions.find(x => { return x.Id === QuestionId }).Value;
                    var ResponseText = "No response selected.";
                    $(g_QuestionResponses).each(function () {
                        if (this.Id == ResponseId) ResponseText = this.Value;
                    });
                    $(QuestionsContainer).append("&nbsp;" + htmlEncode(QuestionText) + " " + htmlEncode(ResponseText));
                });
            });

            //Make datatable with output buttons
            MakeDataTable("tblModalExport", false, ExportFilenamePrefix + " - " + g_ProjectData.ProjectClientName + " (" + g_ProjectData.ProjectManagerName + ")", true, false);

            $("#divModal").show();
            break;
        default:
    }
}

function ShowModalIssuesTab(ClickedButton, TabName) {
    $(".ModalIssuesTab").hide();
    $(".modaltablink").removeClass("w3-light-grey").removeClass("w3-nei");
    $(ClickedButton).addClass("w3-nei");
    $("#" + TabName).show();
    $(ClickedButton).addClass("w3-light-grey");
}

function SetProjectFlag(FlagName) {
    if (g_ProjectData.Project[0].Notes.length > 0) {
        var ProjectNotes = JSON.parse(g_ProjectData.Project[0].Notes);
        if (ProjectNotes[FlagName]) {
            if (ProjectNotes[FlagName] == "true") {
                $("#chk" + FlagName).prop("checked", true);
            }
        }
    }
}

function SaveProjectFlagAsNote(Checkbox) {
    var CheckboxId = $(Checkbox).attr("id");
    var FlagName = CheckboxId.replace(/chk/ig, "");
    var DataRowId = g_ProjectData.Project[0].Id;
    var ProjectNotes = {};
    if (g_ProjectData.Project[0].Notes.length > 0) {
        ProjectNotes = JSON.parse(g_ProjectData.Project[0].Notes);
    }
    ProjectNotes[FlagName] = $(Checkbox).prop("checked") ? "true" : "false";
    SaveValue(DataRowId, "Notes", JSON.stringify(ProjectNotes), "InvoicingPeriodProject").then(
        function (response) {
            if (response == "success") {
                g_ProjectData.Project[0].Notes = JSON.stringify(ProjectNotes);
            }
            else {
                alert(response);
            }
        },
        function (errormsg) {
            alert('Error saving change: ' + errormsg);
        }
    );
}

function ModalSave(SaveButton) {
    if ($(SaveButton).hasClass("w3-disabled")) {
        return;
    }
    else {
        var saveref = ($("#divModal").data("saveref"));
        var DataRow;
        var SaveToTableName = "InvoicingPeriod" + saveref.RowType;
        switch (saveref.ModalMode) {
            case "Notes":
                DataRow = g_ProjectData[saveref.RowType].find(x => { return x.Id === saveref.Id });
                var objNotes = {};
                if (DataRow.Notes.length > 0) {
                    objNotes = JSON.parse(DataRow.Notes);
                }
                objNotes[saveref.Tab] = $("#txtNotes").val();
                //Save Notes to Database
                SaveValue(DataRow.Id, "Notes", JSON.stringify(objNotes), SaveToTableName).then(
                    function (response) {
                        if (response == "success") {
                            //Update notes in g_ProjectData
                            DataRow.Notes = JSON.stringify(objNotes);
                            $('#divModal').hide();
                            //Update notes links style
                            $(".noteslink").each(function () {
                                SetNotesStyle(this);
                            });
                            //Check rules on current tab
                            switch (saveref.Tab) {
                                case "ETC":
                                    CheckRules("tblETC");
                                    break;
                                case "Project":
                                    CheckRules("tblPrjDetails");
                                    break;
                                case "Invoicing":
                                    CheckRules("tblInvoicing");
                                default:
                            }
                        }
                        else {
                            alert(response);
                        }
                    },
                    function (errormsg) {
                        alert('Error saving change: ' + errormsg);
                    }
                );
                break;
            case "Questions":
                DataRow = g_ProjectData[saveref.RowType].find(x => { return x.Id === saveref.Id });
                var QuestionType = saveref.Tab + "Questions";
                $(".questionselect").each(function () {
                    var QuestionId = $(this).data("questionid");
                    var ResponseId = ($(this).val() == "0") ? "" : $(this).val();
                    var QuestionToUpdate = DataRow.Questions[QuestionType].find(x => { return x.QuestionId === QuestionId });
                    if (QuestionToUpdate) {
                        QuestionToUpdate.ResponseId = ResponseId;
                    }
                });
                //Save Question Response to Database
                SaveValue(DataRow.Id, "Questions", JSON.stringify(DataRow.Questions), SaveToTableName).then(
                    function (response) {
                        if (response == "success") {
                            $('#divModal').hide();
                            //Check rules on current tab
                            switch (saveref.Tab) {
                                case "ETC":
                                    CheckRules("tblETC");
                                    break;
                                case "Project":
                                    CheckRules("tblPrjDetails");
                                    break;
                                case "Invoicing":
                                    CheckRules("tblInvoicing");
                                default:
                            }
                        }
                        else {
                            alert(response);
                        }
                    },
                    function (errormsg) {
                        alert('Error saving change: ' + errormsg);
                    }
                );
                break;
            default:
        }
    }
}

function CellEditOff(editbox) {
    var newvalue = $(editbox).val();
    var origvalue = $(editbox).data("origval");

    //refresh table view
    cell = $(editbox).parent();
    var tblID = $(cell).parent().parent().parent().attr("id");
    var table = $("#" + tblID).DataTable();
    var dtCell = table.cell(cell);
    try {
        var DisplayVal = newvalue;
        if ($(cell).hasClass("numcell")) DisplayVal = fmtNum(DisplayVal, 2);
        if ($(cell).hasClass("moneycell")) DisplayVal = fmtMoney(DisplayVal);
        if ($(cell).hasClass("pctcell")) {
            DisplayVal = fmtPct(DisplayVal/100);
        }
        if ($(cell).hasClass("datecell")) {
            var tmpdate = new Date(DisplayVal);
            tmpdate.setDate(tmpdate.getDate() + 1);
            DisplayVal = FormatDateDisplay(tmpdate, "short");
        }
        if ($(cell).hasClass("employeelist")) {
            $(cell).data("empid", DisplayVal);
            DisplayVal = empName(DisplayVal);
        }
        dtCell.data(DisplayVal).draw();
    } catch (e) {
        //datatables has trouble redrawing due to the modified <td>, but it updates the cell value properly
        //console.log(e);
    }

    //If new value is different, save it to database
    if (newvalue != origvalue) {
        if ($(cell).data("customcalcs") == true) {
            PerformCustomCalculation(cell);
            newvalue = rawNum($(cell).html());
        }
        var CellInfo = $(cell).attr("id").replace(/subphaseinvoicing/ig, "").replace(/subphase/ig, "").split("_");
        var FieldName = CellInfo[0];
        var RowId = CellInfo[1];
        if (!$(cell).hasClass("nodbsave")) {
            SaveValue(RowId, FieldName, newvalue).then(
                function (response) {
                    if (response == "success") {
                        CheckRuleOverrides(cell);
                        RecalculateTable(tblID);
                        NavigateToNextCell(cell);
                    }
                    else {
                        alert(response);
                    }
                },
                function (errormsg) {
                    alert('Error saving change: ' + errormsg);
                }
            );
        }
        else {
            CheckRuleOverrides(cell);
            RecalculateTable(tblID);
            NavigateToNextCell(cell);
        }
    }
    else {
        NavigateToNextCell(cell);
    }

}

function NavigateToNextCell(CurrentCell) {
    var NextRowCell = null;
    var NextColCell = null;
    var PrevRowCell = null;
    var PrevColCell = null;
    var bReachedCurrentCell = false
    $(".editable:visible").each(function () {
        var CurrentCellId = $(CurrentCell).attr("id");
        var CurrentCellColName = CurrentCellId.split("_")[0].replace(/subphase/ig, "");
        var CurrentCellDetailId = CurrentCellId.split("_")[1];
        var ThisCellId = $(this).attr("id");
        var ThisCellColName = ThisCellId.split("_")[0].replace(/subphase/ig, "");
        var ThisCellDetailId = ThisCellId.split("_")[1];

        if (ThisCellId == CurrentCellId) {
            bReachedCurrentCell = true;
        }
        if (!bReachedCurrentCell) {
            PrevRowCell = this;
            if (ThisCellColName == CurrentCellColName && ThisCellDetailId != CurrentCellDetailId) {
                PrevColCell = this;
            }
        }
        if (bReachedCurrentCell && !NextRowCell && ThisCellId != CurrentCellId) {
            NextRowCell = this;
        }
        if (bReachedCurrentCell && !NextColCell && ThisCellDetailId != CurrentCellDetailId && ThisCellColName == CurrentCellColName) {
            NextColCell = this;
        }
    });

    //Navigate to next cell in row or first cell in next row on Tab key
    if (g_PageKey == "Tab" && NextRowCell) {
        $(NextRowCell).trigger("click");
    }
    //Navigate to previous cell in row or last cell in previous row on Shift_Tab
    if (g_PageKey == "Shift_Tab" && PrevRowCell) {
        $(PrevRowCell).trigger("click");
    }
    //Navigate to cell in same column of row below on Enter and ArrowDown keys
    if (NextColCell && (g_PageKey == "Enter" || g_PageKey == "ArrowDown")) {
        $(NextColCell).trigger("click");
    }
    //Navigate to cell in same column of row above on ArrowUp key
    if (PrevColCell && g_PageKey == "ArrowUp") {
        $(PrevColCell ).trigger("click");
    }
}

function CellEditKeyup(editbox) {
    var key = g_PageKey; //event.key;
    if (key == 'Enter' || key == 'ArrowDown' || key == "ArrowUp") {
        CellEditOff(editbox);
    }
    else if (key == 'Escape') {
        var origval = $(editbox).data("origval");
        $(editbox).val(origval);
        CellEditOff(editbox);
    }
}

function NewGUID() {
    return ([1e7] + -1e3 + -4e3 + -8e3 + -1e11).replace(/[018]/g, c =>
        (c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16)
    );
}