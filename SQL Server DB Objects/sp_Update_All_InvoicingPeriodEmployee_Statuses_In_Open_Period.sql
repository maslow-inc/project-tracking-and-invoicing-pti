USE PTI
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_Update_All_InvoicingPeriodEmployee_Statuses_In_Open_Period]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].sp_Update_All_InvoicingPeriodEmployee_Statuses_In_Open_Period
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

CREATE PROCEDURE dbo.sp_Update_All_InvoicingPeriodEmployee_Statuses_In_Open_Period
AS
	set nocount on
	
	--PROJECT STATUS UPDATES
	--01. "Not Started" project status is default state, no update needed

	--02. Set Project Status to Started if any projects for the manager are started
	update InvoicingPeriodEmployee set StatusId = (select m.Id from Metadata m inner join MetadataType mt on m.typeid = mt.id where m.value = 'Started' and mt.name = 'InvoicingPeriodEmployeeStatus')
	where Id in (
		select distinct InvoicingPeriodEmployeeId from (
			select IPP.*, MP.Value
			from InvoicingPeriodProject IPP inner join Metadata MP on IPP.StatusId = MP.Id
			inner join InvoicingPeriod IP on IPP.InvoicingPeriodId = IP.Id
			inner join Metadata MPeriod on IP.StatusId = MPeriod.Id
			where MPeriod.Value = 'Open'		
			) tEmps
		where Value = 'Started'
	)

	--03. Set Project Status to "Submitted" if ALL projects are "Submitted"
	update InvoicingPeriodEmployee set StatusId = (select m.Id from Metadata m inner join MetadataType mt on m.typeid = mt.id where m.value = 'Submitted' and mt.name = 'InvoicingPeriodEmployeeStatus')
	where Id in (
		select distinct InvoicingPeriodEmployeeId from (
		select IPP.InvoicingPeriodEmployeeId, tSubmittedPMProjects.SubmittedProjectCount, count(*) as AllProjectCount
		from InvoicingPeriodProject IPP
		inner join (
			select InvoicingPeriodEmployeeId, count(*) as SubmittedProjectCount from (
				select IPP.*, MP.Value
				from InvoicingPeriodProject IPP inner join Metadata MP on IPP.StatusId = MP.Id
				inner join InvoicingPeriod IP on IPP.InvoicingPeriodId = IP.Id
				inner join Metadata MPeriod on IP.StatusId = MPeriod.Id
				where MPeriod.Value = 'Open'
				) tEmps
			where Value = 'Submitted'
			group by InvoicingPeriodEmployeeId
		) tSubmittedPMProjects
		on IPP.InvoicingPeriodEmployeeId = tSubmittedPMProjects.InvoicingPeriodEmployeeId
		group by IPP.InvoicingPeriodEmployeeId, tSubmittedPMProjects.SubmittedProjectCount
		) tMain where SubmittedProjectCount = AllProjectCount
	)

	--PROJECT APPROVAL UPDATES
	--01. "Not Reviewed" approval lstatus is default state, no update needed

	--02. Set Project Approval to "Questions" if any projects for the manager has approval status "Questions"
	update InvoicingPeriodEmployee set ApprovalStatusId = (select m.Id from Metadata m inner join MetadataType mt on m.typeid = mt.id where m.value = 'Questions' and mt.name = 'InvoicingPeriodEmployeeApprovalStatus')
	where Id in (
		select distinct InvoicingPeriodEmployeeId from (
			select IPP.*, MP.Value
			from InvoicingPeriodProject IPP inner join Metadata MP on IPP.ApprovalStatusId = MP.Id
			inner join InvoicingPeriod IP on IPP.InvoicingPeriodId = IP.Id
			inner join Metadata MPeriod on IP.StatusId = MPeriod.Id
			where MPeriod.Value = 'Open'		
			) tEmps
		where Value = 'Questions'
	)

	--03. Set Project Approval to "Approved" if ALL projects are "Approved"
	update InvoicingPeriodEmployee set ApprovalStatusId = (select m.Id from Metadata m inner join MetadataType mt on m.typeid = mt.id where m.value = 'Approved' and mt.name = 'InvoicingPeriodEmployeeApprovalStatus')
	where Id in (
		select distinct InvoicingPeriodEmployeeId from (
		select IPP.InvoicingPeriodEmployeeId, tSubmittedPMProjects.SubmittedProjectCount, count(*) as AllProjectCount
		from InvoicingPeriodProject IPP
		inner join (
			select InvoicingPeriodEmployeeId, count(*) as SubmittedProjectCount from (
				select IPP.*, MP.Value
				from InvoicingPeriodProject IPP inner join Metadata MP on IPP.ApprovalStatusId = MP.Id
				inner join InvoicingPeriod IP on IPP.InvoicingPeriodId = IP.Id
				inner join Metadata MPeriod on IP.StatusId = MPeriod.Id
				where MPeriod.Value = 'Open'
				) tEmps
			where Value = 'Approved'
			group by InvoicingPeriodEmployeeId
		) tSubmittedPMProjects
		on IPP.InvoicingPeriodEmployeeId = tSubmittedPMProjects.InvoicingPeriodEmployeeId
		group by IPP.InvoicingPeriodEmployeeId, tSubmittedPMProjects.SubmittedProjectCount
		) tMain where SubmittedProjectCount = AllProjectCount
	)

	--PROJECT INVOICING UPDATES
	--01. "Not Started" project status is default state, no update needed

	--02. Set Project Invoicing to "Questions" if any projects for the manager has approval status "Questions"
	update InvoicingPeriodEmployee set InvoicingStatusId = (select m.Id from Metadata m inner join MetadataType mt on m.typeid = mt.id where m.value = 'Questions' and mt.name = 'InvoicingPeriodEmployeeInvoicingStatus')
	where Id in (
		select distinct InvoicingPeriodEmployeeId from (
			select IPP.*, MP.Value
			from InvoicingPeriodProject IPP inner join Metadata MP on IPP.InvoicingStatusId = MP.Id
			inner join InvoicingPeriod IP on IPP.InvoicingPeriodId = IP.Id
			inner join Metadata MPeriod on IP.StatusId = MPeriod.Id
			where MPeriod.Value = 'Open'		
			) tEmps
		where Value = 'Questions'
	)

	--03. Set Project Invoicing to "Approved" if ALL projects are "Approved"
	update InvoicingPeriodEmployee set InvoicingStatusId = (select m.Id from Metadata m inner join MetadataType mt on m.typeid = mt.id where m.value = 'Approved' and mt.name = 'InvoicingPeriodEmployeeInvoicingStatus')
	where Id in (
		select distinct InvoicingPeriodEmployeeId from (
		select IPP.InvoicingPeriodEmployeeId, tSubmittedPMProjects.SubmittedProjectCount, count(*) as AllProjectCount
		from InvoicingPeriodProject IPP
		inner join (
			select InvoicingPeriodEmployeeId, count(*) as SubmittedProjectCount from (
				select IPP.*, MP.Value
				from InvoicingPeriodProject IPP inner join Metadata MP on IPP.InvoicingStatusId = MP.Id
				inner join InvoicingPeriod IP on IPP.InvoicingPeriodId = IP.Id
				inner join Metadata MPeriod on IP.StatusId = MPeriod.Id
				where MPeriod.Value = 'Open'
				) tEmps
			where Value = 'Approved'
			group by InvoicingPeriodEmployeeId
		) tSubmittedPMProjects
		on IPP.InvoicingPeriodEmployeeId = tSubmittedPMProjects.InvoicingPeriodEmployeeId
		group by IPP.InvoicingPeriodEmployeeId, tSubmittedPMProjects.SubmittedProjectCount
		) tMain where SubmittedProjectCount = AllProjectCount
	)

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO
