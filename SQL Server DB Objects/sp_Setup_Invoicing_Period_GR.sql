USE PTI
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_Setup_Invoicing_Period]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].sp_Setup_Invoicing_Period
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

CREATE PROCEDURE dbo.sp_Setup_Invoicing_Period
@currentperiod nvarchar(6)
AS
	--exec sp_Setup_Invoicing_Period @currentperiod = '202111'
	--select * from work_invoicingperiodsetup
	set nocount on
	declare @CutOffDate datetime
	
	select @cutoffdate = EndDate from InvoicingPeriod where InvoicingPeriodName = @currentperiod
	set @cutoffdate = dateadd(hour, 23, @cutoffdate)
	set @cutoffdate = dateadd(minute, 59, @cutoffdate)
	
	declare @PreviousPeriod varchar(6)
	set @PreviousPeriod = NEI_Reports.dbo.fnPreviousPeriod(@CurrentPeriod)

	if exists(select * from sysobjects where xtype='U' and name = 'work_invoicingperiodsetup')
	begin
		drop table work_invoicingperiodsetup
	end

	if exists(select * from nei_reports.dbo.sysobjects where xtype = 'u' and name = 'DevProjectTrackingData')
	begin
		/*********DEV ENVIRONMENT************/
		select * into work_invoicingperiodsetup from nei_reports.dbo.DevProjectTrackingData where len(trim(SUPV1_NAME)) > 0
		update work_invoicingperiodsetup set START_DATE = null where START_DATE = 'NULL'
		update work_invoicingperiodsetup set END_DATE = null where END_DATE = 'NULL'
		--select * from work_invoicingperiodsetup
	end
	else
	begin
		/*********ABAGNALE SERVER************/
		select * into work_invoicingperiodsetup from
		(
			select EM_SUPV1.Employee as SUPV1_EmpId, EM_SUPV1.EMail as SUPV1_Email, 
			isnull(EM_SUPV2.Employee, EM_SUPV1.Employee) as SUPV2_EmpId, isnull(EM_SUPV2.EMail, EM_SUPV1.EMail) as SUPV2_Email, 
			isnull(EM_SUPV3.Employee, isnull(EM_SUPV2.Employee, EM_SUPV1.Employee)) as SUPV3_EmpId, isnull(EM_SUPV3.EMail, isnull(EM_SUPV2.EMail, EM_SUPV1.EMail))as SUPV3_Email, 
			tmain.* from 
			(
			select 
				group1 as WBS1, min(groupDesc1) as WBS1_NAME, max(isnull(w1prgName, '')) as SUPV1_NAME,
				group2 as WBS2, min(groupDesc2) as WBS2_NAME, max(isnull(w2prgName, '')) as SUPV2_NAME, 
				group3 as WBS3, min(groupDesc3) as WBS3_NAME, max(isnull(w3prgName, '')) as SUPV3_NAME,
				concat(group1,':',group2,':',group3) as PROJ_KEY,
				Case
					When max(w3status) = 'A' Then 'Active'
					When max(w3Status) = 'I' Then 'Inactive'
					When max(w3status) = 'D' Then 'Dormant'
					Else 'Unknown'
				End as PHASE_STATUS,
				SUM(isnull(fee_b, 0) + isnull(reimbAllowExpense, 0) + isnull(reimbAllowConsultant, 0)) as TOTAL_COMPENSATION,
				SUM(isnull(fee_b, 0)) as DIRECT_LABOR,
				SUM(isnull(reimbAllowExpense, 0)) as REIMBURSABLE_EXPENSE,
				SUM(isnull(reimbAllowConsultant, 0)) as REIMBURSABLE_CONSULTANT,
				SUM(isnull(amtJTD_b, 0)) as JTD_BILLING_TOTAL_SPENT,
				SUM(isnull(amtJTDLabReg_b, 0)) as JTD_LABOR_BILLING_SPENT,
				SUM(isnull(reimbJTD_b, 0)) as JTD_REIMB_BILLING_SPENT,
				Case
					When SUM(isnull(fee_b, 0) + isnull(reimbAllow_b, 0) + isnull(reimbAllowConsultant, 0)) <> 0 Then
						SUM(isnull(amtJTD_b, 0)) / (SUM(isnull(fee_b, 0) + isnull(reimbAllow_b, 0) + isnull(reimbAllowConsultant, 0))) --JTD_BILLING_TOTAL_SPENT / TOTAL_COMPENSATION
					Else 0
				end as JTD_SPENT_PCT,
				SUM(isnull(billJTD_b, 0)) as JTD_BILLED,
				Case
					When SUM(isnull(fee_b, 0) + isnull(reimbAllow_b, 0) + isnull(reimbAllowConsultant, 0)) <> 0 Then
						SUM(isnull(billJTD_b, 0)) / (SUM(isnull(fee_b, 0) + isnull(reimbAllow_b, 0) + isnull(reimbAllowConsultant, 0))) --JTD_BILLED / TOTAL_COMPENSATION
					Else 0
				End as JTD_BILLED_PCT,
				SUM(isnull(arJTD_b, 0)) as JTD_AR_AMT,
				(Select BT.FeeMeth from NEI_Reports.dbo.BT where  tPE.group1 = isnull(BT.WBS1, '') and BT.WBS2 = '' and BT.WBS3 = '') as BILLING_TERMS_CODE,
				(
					select 
						case 
							when BT.FeeMeth = 0 then 'No Fee Billing'
							when BT.FeeMeth = 1 then 'Overall Percent Complete'
							when BT.FeeMeth = 2 then 'Pct Cmplt by Phs, Pct of Fee'
							when BT.FeeMeth = 3 then 'Pct Cmplt by Phs, Fixed Amt'
							when BT.FeeMeth = 4 then 'Cumulative Unit or Fee'
							when BT.FeeMeth = 5 then 'Current Unit or Fee'
							else 'No match [BT.FeeMeth=' + cast(isnull(BT.FeeMeth, '') as varchar(10)) + ']'
						end
					from NEI_Reports.dbo.BT where tPE.group1 = isnull(BT.WBS1, '') and BT.WBS2 = '' and BT.WBS3 = ''
				) as BILLING_TERMS,
				(Select RPStartDate from NEI_Reports.dbo.vProject_List vPL where group1 = tPE.group1 and group2 = tPE.group2 and group3 = tPE.group3) as START_DATE,
				(Select RPEndDate from NEI_Reports.dbo.vProject_List vPL where group1 = tPE.group1 and group2 = tPE.group2 and group3 = tPE.group3) as END_DATE,
				ISNULL((Select SUM(isnull(BillExt, 0)) from NEI_Reports.dbo.vLD_BILD where WBS1 = tPE.group1 and WBS2 = tPE.group2 and WBS3 = tPe.group3 and [Period] <= @PreviousPeriod and BillStatus = 'F'), 0) as FINAL_BILLED,
				ISNULL((Select SUM(isnull(BillExt, 0)) from NEI_Reports.dbo.vLD_BILD where WBS1 = tPE.group1 and WBS2 = tPE.group2 and WBS3 = tPe.group3 and [Period] <= @PreviousPeriod and BillStatus in ('B','H')), 0) as WIP_BY_PHASE_SUB_PHASE,
				ISNULL((Select SUM(isnull(BillExt, 0)) from NEI_Reports.dbo.vLD_BILD where WBS1 = tPE.group1 and WBS2 = tPE.group2 and WBS3 = tPe.group3 and [Period] <= @PreviousPeriod and BillStatus in ('B')), 0) as WIP_BILLABLE,
				ISNULL((Select SUM(isnull(BillExt, 0)) from NEI_Reports.dbo.vLD_BILD where WBS1 = tPE.group1 and WBS2 = tPE.group2 and WBS3 = tPe.group3 and [Period] <= @PreviousPeriod and BillStatus in ('H')), 0) as WIP_HELD,
				ISNULL((Select SUM(isnull(BillExt, 0)) from NEI_Reports.dbo.vLD_BILD where WBS1 = tPE.group1 and WBS2 = tPE.group2 and WBS3 = tPe.group3 and [Period] <= @PreviousPeriod and BillStatus in ('W', 'X')), 0) as JTD_WRITE_OFFS_W_AND_X,
				ISNULL((Select SUM(isnull(BillExt, 0)) from NEI_Reports.dbo.vLD_BILD where WBS1 = tPE.group1 and WBS2 = tPE.group2 and WBS3 = tPe.group3 and [Period] <= @PreviousPeriod and BillStatus in ('D', 'O')), 0) as JTD_WRITE_OFFS_D_AND_O,
				ISNULL((Select SUM(isnull(BillExt, 0)) from NEI_Reports.dbo.tkDetail where WBS1 = tPE.group1 and WBS2 = tPE.group2 and WBS3 = tPe.group3 and TransDate >= DATEADD(dd, -1, DATEADD(ww, DATEDIFF(ww, 0, getdate()) - 1, 0)) and TransDate <= DATEADD(dd,  5, DATEADD(ww, DATEDIFF(ww, 0, getdate()) - 1, 0))), 0) as BILLING_LAST_WEEK,

				ISNULL((Select SUM(isnull(BillExt, 0)) from NEI_Reports.dbo.vReimbursable_Expenses where WBS1 = tPE.group1 and WBS2 = tPE.group2 and WBS3 = tPe.group3 and [Period] <= @PreviousPeriod and BillStatus = 'F'), 0) as EXP_FINAL_BILLED,
				ISNULL((Select SUM(isnull(BillExt, 0)) from NEI_Reports.dbo.vReimbursable_Expenses where WBS1 = tPE.group1 and WBS2 = tPE.group2 and WBS3 = tPe.group3 and [Period] <= @PreviousPeriod and BillStatus in ('B','H')), 0) as EXP_UNBILLED,
				ISNULL((Select SUM(isnull(BillExt, 0)) from NEI_Reports.dbo.vReimbursable_Expenses where WBS1 = tPE.group1 and WBS2 = tPE.group2 and WBS3 = tPe.group3 and [Period] <= @PreviousPeriod and BillStatus in ('B')), 0) as EXP_BILLABLE,
				ISNULL((Select SUM(isnull(BillExt, 0)) from NEI_Reports.dbo.vReimbursable_Expenses where WBS1 = tPE.group1 and WBS2 = tPE.group2 and WBS3 = tPe.group3 and [Period] <= @PreviousPeriod and BillStatus in ('H')), 0) as EXP_HELD,
				ISNULL((Select SUM(isnull(BillExt, 0)) from NEI_Reports.dbo.vReimbursable_Expenses where WBS1 = tPE.group1 and WBS2 = tPE.group2 and WBS3 = tPe.group3 and [Period] <= @PreviousPeriod and BillStatus in ('W', 'X')), 0) as EXP_WRITE_OFF,
				ISNULL((Select SUM(isnull(BillExt, 0)) from NEI_Reports.dbo.vReimbursable_Expenses where WBS1 = tPE.group1 and WBS2 = tPE.group2 and WBS3 = tPe.group3 and [Period] <= @PreviousPeriod and BillStatus in ('D', 'O')), 0) as EXP_DELETE,

				SUM(isnull(amtETC_b, 0)) as ETC,
				SUM(isnull(amtEAC_b, 0)) as EAC,
				SUM(isnull(w3PctComp, 0)) as VANTAGEPOINT_RPC,
				'' as UPDATED_RPC, --Blank placeholder
				(  /* = TOTAL_COMPENSATION - JTD_BILLING_TOTAL_SPENT + PREVIOUS_BILLING_PERIOD_WRITE_OFFS_W_AND_X */
					SUM(isnull(fee_b, 0) + isnull(reimbAllow_b, 0) + isnull(reimbAllowConsultant, 0)) --TOTAL_COMPENSATION
					- SUM(isnull(amtJTD_b, 0)) --JTD_BILLING_TOTAL_SPENT
					+ ISNULL((Select SUM(isnull(BillExt, 0)) from NEI_Reports.dbo.LD where WBS1 = tPE.group1 and WBS2 = tPE.group2 and WBS3 = tPe.group3 and Period = @PreviousPeriod and TransType = 'TS' and BillStatus in ('W', 'X')), 0) --PREVIOUS_BILLING_PERIOD_WRITE_OFFS_W_AND_X
				) as POTENTIAL_WRITE_OFFS,
				( /* = TOTAL_COMPENSATION - JTD_BILLED */
					SUM(isnull(fee_b, 0) + isnull(reimbAllow_b, 0) + isnull(reimbAllowConsultant, 0)) --TOTAL_COMPENSATION
					- SUM(isnull(billJTD_b, 0))
				) as REMAINING_TO_BILL,
				'' as NOTES_FROM_PREVIOUS_WEEKS, --Blank placeholder
				'' AS PM_PROJECTED_WRITE_OFF, --Blank placeholder
				max(ClientName) as CLIENT_NAME,
				max(revtype) as REVENUE_METHOD,
				max(RevUpsetLimits) as REVENUE_LIMIT,
				max(isnull(PCTF.CustPhaseEAC, 0)) as PHASE_EAC,
				sum(isnull(AvgBill.RegHrs, 0)) as REG_HRS,
				sum(isnull(AvgBill.BillExt, 0)) as BILL_EXT,
				max(isnull(AvgBill.AvgBillRate, 0)) as AVG_BILL_RATE,
				max(isnull(tente.Proj_CustTotalTENTELimit,0)) as Project_TENTE_Limit


			from NEI_Reports.dbo.ProjectEarnings_PTI tPE 
				inner join NEI_Reports.dbo.PR on tPE.group1 = isnull(PR.WBS1, '') and tPE.group2 = isnull(PR.WBS2, '') and tPE.group3 = isnull(PR.WBS3, '')
				inner join NEI_Reports.dbo.ProjectCustomTabFields PCTF on tPE.group1 = isnull(PCTF.WBS1, '') and tPE.group2 = isnull(PCTF.WBS2, '') and tPE.group3 = isnull(PCTF.WBS3, '')
				left join (
					SELECT [WBS1]
						  ,MAX([CustTotalTENTELimit]) as Proj_CustTotalTENTELimit
					  FROM [NEI_Engineering].[dbo].[ProjectCustomTabFields]
					  WHERE CustTotalTENTELimit > 0 
					  GROUP BY  [WBS1]
					  ) tente 
					on tente.WBS1 = tPE.WBS1				
				left join 
					/* Get average bill rate */
					(select 
						WBS1, WBS2, WBS3, RegHrs, BillExt, BillExt/RegHrs as AvgBillRate
					from
						(select 
							WBS1, WBS2, WBS3, sum(RegHrs) as RegHrs, sum(BillExt) as BillExt
						from 
						(
							select * from (
								select BillStatus, TransType, WBS1, WBS2, WBS3, RegHrs, BillExt
									from NEI_Reports.dbo.LD
								union all
								select BillStatus, TransType, WBS1, WBS2, WBS3, RegHrs, BillExt
									from NEI_Reports.dbo.BILD
								union all
								select 'U' as BillStatus, 'TS' as TransType, WBS1, WBS2, WBS3, RegHrs, BillExt
									from NEI_Reports.dbo.tkDetail where transdate > (select max(transdate) from NEI_Reports.dbo.LD)
							) t1
							where BillStatus not in ('M', 'T', 'R') and TransType in ('TS','HL') and RegHrs >0 and BillExt >0
						) t2
					group by WBS1, WBS2,WBS3) as t3) as AvgBill
					on tPE.group1 = isnull(AvgBill.WBS1, '') and tPE.group2 = isnull(AvgBill.WBS2, '') and tPE.group3 = isnull(AvgBill.WBS3, '')  
			group by group1, group2, group3
			having left(max(isnull(group1, '')),3) <> '000' and left(max(isnull(group1, '')),1) <> 'P' and max(tPE.w1Status) = 'A'
			--order by max(isnull(w1prgName, '')), group1, group2, group3
			) tmain
			left join (select Employee, Concat(LastName, ', ', FirstName) as EmpNameKey, isnull(EMail, '') as EMail from NEI_Reports.dbo.EM) EM_SUPV1 on tmain.SUPV1_NAME = EM_SUPV1.EmpNameKey
			left join (select Employee, Concat(LastName, ', ', FirstName) as EmpNameKey, isnull(EMail, '') as EMail from NEI_Reports.dbo.EM) EM_SUPV2 on tmain.SUPV2_NAME = EM_SUPV2.EmpNameKey
			left join (select Employee, Concat(LastName, ', ', FirstName) as EmpNameKey, isnull(EMail, '') as EMail from NEI_Reports.dbo.EM) EM_SUPV3 on tmain.SUPV3_NAME = EM_SUPV3.EmpNameKey
			where len(trim(tmain.SUPV1_NAME)) > 0
		) tInvoicingData order by WBS1_NAME, WBS2_NAME, WBS3_NAME
	end
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO