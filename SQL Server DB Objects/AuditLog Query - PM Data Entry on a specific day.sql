select distinct e.LastName, e.FirstName, m.Value as [Audit Log Event], al.dts as [Date], ipp.ClientName, ipp.Name, ipp.WBS1, ipph.WBS2, ips.WBS3
from AuditLog al
inner join Metadata m on al.typeid = m.Id
inner join employee e on al.EmployeeId = e.Id
left join PTI.dbo.invoicingperioddata ipd on al.tablerowid = ipd.Id
left join pti.dbo.invoicingperiodproject ipp on ipd.invoicingperiodprojectid = ipp.id
left join pti.dbo.invoicingperiodphase ipph on ipd.invoicingperiodphaseid = ipph.id
left join pti.dbo.invoicingperiodsubphase ips on ipd.invoicingperiodsubphaseid = ips.id
where al.dts >= '2022-07-07 00:00:00'
and not ipd.id is null

union all
select distinct e.LastName, e.FirstName, m.Value as [Audit Log Event], al.dts as [Date], ipp.ClientName, ipp.Name, ipp.WBS1, '' as WBS2, '' as WBS3
from AuditLog al
inner join Metadata m on al.typeid = m.Id
inner join employee e on al.EmployeeId = e.Id
left join pti.dbo.invoicingperiodproject ipp on al.TableRowId = ipp.id
where al.dts >= '2022-07-07 00:00:00'
and not ipp.id is null

union all
select distinct e.LastName, e.FirstName, m.Value as [Audit Log Event], al.dts as [Date], ipp.ClientName, ipp.Name, ipp.WBS1, ipph.WBS2, ips.WBS3
from AuditLog al
inner join Metadata m on al.typeid = m.Id
inner join employee e on al.EmployeeId = e.Id
left join PTI.dbo.invoicingperiodsubphase ips on al.tablerowid = ips.Id
left join pti.dbo.invoicingperiodphase ipph on ips.invoicingperiodphaseid = ipph.id
left join pti.dbo.invoicingperiodproject ipp on ipph.invoicingperiodprojectid = ipp.id
where al.dts >= '2022-07-07 00:00:00'
and not ips.id is null