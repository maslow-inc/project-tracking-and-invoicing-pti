USE PTI
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_FPC_Upload]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].sp_FPC_Upload
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

CREATE PROCEDURE dbo.sp_FPC_Upload
	@EmployeeId nvarchar(50),
	@InvoicingPeriodId nvarchar(50)
AS
	set nocount on
	
	DECLARE @Test_Mode integer
	DECLARE @Test_Mode_Delay nvarchar(20)
	
	set @Test_Mode = 0
	set @Test_Mode_Delay = '00:00:10'
	
	DECLARE @SQL nvarchar(max)
	DECLARE @ErrMsg nvarchar(max)
	DECLARE @AuditLogRecordChangedId nvarchar(50)
	DECLARE @AuditLogRecord nvarchar(max)
	DECLARE @ErrorMsgs nvarchar(max)
	
	set @ErrorMsgs = ''
	
	select @AuditLogRecordChangedId = mt.id
	from Metadata m
	inner join MetadataType mt on m.TypeId = mt.Id
	where mt.Name = 'AuditLogEvent'
	and m.Value = 'Record Changed'

	--Set up Databases
	DECLARE @PTI_DB nvarchar(100)
	DECLARE @DVP_DB nvarchar(100)
	
	SELECT @PTI_DB = db_name()
	SET @DVP_DB = 'NEI_Engineering_RenameForLive'
	IF LOWER(TRIM(@PTI_DB)) = 'pti_dev' SET @DVP_DB = 'Sandbox'
	
	--Add process start AuditLog record (Note: All AuditLog records use the InvoicingPeriodId value in the TableRowId field and sp_FPC_Upload as the TableName)
	set @AuditLogRecord = '{"IsError":"false","LogEvent":"FPC Upload Process Start (DVP Database = ' + @DVP_DB + '; Test Mode = ' + cast(@Test_Mode as varchar(1)) + ')"}'
	insert into AuditLog(Id, TableName, TableRowId, TypeId, PreviousRecord, DTS, EmployeeId) values(NEWID(), 'sp_FPC_Upload', @InvoicingPeriodId, @AuditLogRecordChangedId, @AuditLogRecord, GetDate(), @EmployeeId)
	
	--Update Subphases EAC
	BEGIN TRY
	set @AuditLogRecord = '{"IsError":"false","LogEvent":"Update Subphases EAC Start"}'
	insert into AuditLog(Id, TableName, TableRowId, TypeId, PreviousRecord, DTS, EmployeeId) values(NEWID(), 'sp_FPC_Upload', @InvoicingPeriodId, @AuditLogRecordChangedId, @AuditLogRecord, GetDate(), @EmployeeId)

	set @SQL = '
	--SELECT *
    UPDATE
      t1
    SET
      t1.CustPhaseEAC = t2.[Calc EAC],
      t1.CustPhaseEACDate = t2.EndDate
    FROM 
      (
      SELECT [WBS1]
        ,[WBS2]
        ,[WBS3]
        ,CONCAT([WBS1], ''|'', [WBS2],''|'',[WBS3]) as ProjKey
        ,[Project_TENTE_Limit]
        ,[Calc EAC]
        ,EndDate
        ,CAST(ROUND([Calc FPC]*100,2) as numeric(18,2)) as [Calc FPC]
        ,CASE
          WHEN a1.New_End_Date is null THEN a1.Current_End_Date
          ELSE a1.New_End_Date END as Updated_End_Date
    FROM NEI_Reports.[dbo].vPBI_PTI_Period_ETC a1
    INNER JOIN 
      (
      SELECT MAX(EndDate) as MaxEndDate
      FROM ' + @PTI_DB + '.[dbo].[vPTI_for_DVP]
      )
      lastperiod 
        on lastperiod.MaxEndDate = a1.EndDate
    --WHERE WBS1 = ''003800.001''
      ) t2
      INNER JOIN 
        ' + @DVP_DB + '.dbo.ProjectCustomTabFields t1
          on t1.WBS1 = t2.WBS1 and t1.WBS2 = t2.WBS2 and t1.WBS3 = t2.WBS3	
	'
	print @SQL
	if @Test_Mode = 1
		waitfor delay @Test_Mode_Delay
	else
		exec sp_executesql @SQL
	
	set @AuditLogRecord = '{"IsError":"false","LogEvent":"Update Subphases EAC Complete"}'
	insert into AuditLog(Id, TableName, TableRowId, TypeId, PreviousRecord, DTS, EmployeeId) values(NEWID(), 'sp_FPC_Upload', @InvoicingPeriodId, @AuditLogRecordChangedId, @AuditLogRecord, GetDate(), @EmployeeId)
	
	END TRY
	BEGIN CATCH
		SET @ErrMsg = Concat('Update Subphases EAC Complete - Error ', ERROR_NUMBER(), ' updating Subphases EAC in procedure sp_FPC_Upload on line #', ERROR_LINE(), ': ', ERROR_MESSAGE())
		SET @ErrorMsgs = @ErrorMsgs + '"' + @ErrMsg + '", '
		set @AuditLogRecord = '{"IsError":"true","LogEvent":"' + @ErrMsg + '"}'
		insert into AuditLog(Id, TableName, TableRowId, TypeId, PreviousRecord, DTS, EmployeeId) values(NEWID(), 'sp_FPC_Upload', @InvoicingPeriodId, @AuditLogRecordChangedId, @AuditLogRecord, GetDate(), @EmployeeId)

		PRINT @ErrMsg
	END CATCH
	
	--Update Subphases FPC
	BEGIN TRY
	set @AuditLogRecord = '{"IsError":"false","LogEvent":"Update Subphases FPC Start"}'
	insert into AuditLog(Id, TableName, TableRowId, TypeId, PreviousRecord, DTS, EmployeeId) values(NEWID(), 'sp_FPC_Upload', @InvoicingPeriodId, @AuditLogRecordChangedId, @AuditLogRecord, GetDate(), @EmployeeId)

	set @SQL = '
	--SELECT *
    UPDATE
      t1
    SET
      t1.PctComp = t2.[Calc FPC],
      t1.EstCompletionDate = t2.Updated_End_Date
    FROM 
      (
      SELECT [WBS1]
        ,[WBS2]
        ,[WBS3]
        ,CONCAT([WBS1], ''|'', [WBS2],''|'',[WBS3]) as ProjKey
        ,[Project_TENTE_Limit]
        ,[Calc EAC]
        ,EndDate
        ,CAST(ROUND([Calc FPC]*100,2) as numeric(18,2)) as [Calc FPC]
        ,CASE
          WHEN a1.New_End_Date is null THEN a1.Current_End_Date
          ELSE a1.New_End_Date END as Updated_End_Date
    FROM NEI_Reports.[dbo].[vPBI_PTI_ETC_FPC_Upload] a1
    INNER JOIN 
      (
      SELECT MAX(EndDate) as MaxEndDate
      FROM ' + @PTI_DB + '.[dbo].[vPTI_for_DVP]
      )
      lastperiod 
        on lastperiod.MaxEndDate = a1.EndDate
    --WHERE WBS1 = ''003800.001''
      ) t2
      INNER JOIN 
        ' + @DVP_DB + '.dbo.PR t1
          on t1.WBS1 = t2.WBS1 and t1.WBS2 = t2.WBS2 and t1.WBS3 = t2.WBS3
	'
	print @SQL
	if @Test_Mode = 1
		waitfor delay @Test_Mode_Delay
	else
		exec sp_executesql @SQL
	
	set @AuditLogRecord = '{"IsError":"false","LogEvent":"Update Subphases FPC Complete"}'
	insert into AuditLog(Id, TableName, TableRowId, TypeId, PreviousRecord, DTS, EmployeeId) values(NEWID(), 'sp_FPC_Upload', @InvoicingPeriodId, @AuditLogRecordChangedId, @AuditLogRecord, GetDate(), @EmployeeId)
	
	END TRY
	BEGIN CATCH
		SET @ErrMsg = Concat('Update Subphases FPC Complete - Error ', ERROR_NUMBER(), ' updating Subphases FPC in procedure sp_FPC_Upload on line #', ERROR_LINE(), ': ', ERROR_MESSAGE())
		SET @ErrorMsgs = @ErrorMsgs + '"' + @ErrMsg + '", '
		set @AuditLogRecord = '{"IsError":"true","LogEvent":"' + @ErrMsg + '"}'
		insert into AuditLog(Id, TableName, TableRowId, TypeId, PreviousRecord, DTS, EmployeeId) values(NEWID(), 'sp_FPC_Upload', @InvoicingPeriodId, @AuditLogRecordChangedId, @AuditLogRecord, GetDate(), @EmployeeId)

		PRINT @ErrMsg
	END CATCH	
	
	--Update Phases EAC
	BEGIN TRY
	set @AuditLogRecord = '{"IsError":"false","LogEvent":"Update Phases EAC Start"}'
	insert into AuditLog(Id, TableName, TableRowId, TypeId, PreviousRecord, DTS, EmployeeId) values(NEWID(), 'sp_FPC_Upload', @InvoicingPeriodId, @AuditLogRecordChangedId, @AuditLogRecord, GetDate(), @EmployeeId)
	
	set @SQL = '
	--SELECT *
    UPDATE
      t1
    SET
      t1.CustPhaseEAC = t2.[Calc EAC],
      t1.CustPhaseEACDate = t2.EndDate
    FROM 
      (
      SELECT 
        [WBS1]
        ,[WBS2]
        ,CONCAT([WBS1], ''|'', [WBS2],''|'') as ProjKey
        ,SUM([Calc EAC]) as [Calc EAC]
        ,EndDate
      ,CASE
        WHEN SUM([Calc EAC]) = 0 THEN 100
        ELSE CAST((SUM([SpentTotal])/SUM([Calc EAC]))*100 as numeric(18,2)) END as [Calc FPC]
    FROM NEI_Reports.[dbo].vPBI_PTI_Period_ETC a1
    INNER JOIN 
      (
      SELECT MAX(EndDate) as MaxEndDate
      FROM ' + @PTI_DB + '.[dbo].[vPTI_for_DVP]
      )
      lastperiod 
        on lastperiod.MaxEndDate = a1.EndDate
    --WHERE WBS1 = ''003800.001''
    GROUP BY WBS1, WBS2, EndDate
      ) t2
      INNER JOIN 
        ' + @DVP_DB + '.dbo.ProjectCustomTabFields t1
          on t1.WBS1 = t2.WBS1 and t1.WBS2 = t2.WBS2 and t1.WBS3 = ''''
	'
	print @SQL
	if @Test_Mode = 1
		waitfor delay @Test_Mode_Delay
	else
		exec sp_executesql @SQL	
	
	set @AuditLogRecord = '{"IsError":"false","LogEvent":"Update Phases EAC Complete"}'
	insert into AuditLog(Id, TableName, TableRowId, TypeId, PreviousRecord, DTS, EmployeeId) values(NEWID(), 'sp_FPC_Upload', @InvoicingPeriodId, @AuditLogRecordChangedId, @AuditLogRecord, GetDate(), @EmployeeId)

	END TRY
	BEGIN CATCH
		SET @ErrMsg = Concat('Update Phases EAC Complete - Error ', ERROR_NUMBER(), ' updating Phases EAC in procedure sp_FPC_Upload on line #', ERROR_LINE(), ': ', ERROR_MESSAGE())
		SET @ErrorMsgs = @ErrorMsgs + '"' + @ErrMsg + '", '
		set @AuditLogRecord = '{"IsError":"true","LogEvent":"' + @ErrMsg + '"}'
		insert into AuditLog(Id, TableName, TableRowId, TypeId, PreviousRecord, DTS, EmployeeId) values(NEWID(), 'sp_FPC_Upload', @InvoicingPeriodId, @AuditLogRecordChangedId, @AuditLogRecord, GetDate(), @EmployeeId)
		
		PRINT @ErrMsg
	END CATCH
	
	--Update Phases FPC
	BEGIN TRY
	set @AuditLogRecord = '{"IsError":"false","LogEvent":"Update Phases FPC Start"}'
	insert into AuditLog(Id, TableName, TableRowId, TypeId, PreviousRecord, DTS, EmployeeId) values(NEWID(), 'sp_FPC_Upload', @InvoicingPeriodId, @AuditLogRecordChangedId, @AuditLogRecord, GetDate(), @EmployeeId)
	
	set @SQL = '
	--SELECT *
    UPDATE
      t1
    SET
      t1.PctComp = t2.[Calc FPC],
      t1.EstCompletionDate = t2.Updated_End_Date
    FROM 
      (
      SELECT 
        [WBS1]
        ,[WBS2]
        ,CONCAT([WBS1], ''|'', [WBS2],''|'') as ProjKey
        ,SUM([Calc EAC]) as [Calc EAC]
        ,EndDate
      ,CASE
        WHEN SUM([Calc EAC]) = 0 THEN 100
        ELSE CAST((SUM([SpentTotal])/SUM([Calc EAC]))*100 as numeric(18,2)) END as [Calc FPC]
      ,CASE
        WHEN MAX(a1.New_End_Date) is null THEN MAX(a1.Current_End_Date)
        ELSE MAX(a1.New_End_Date) END as Updated_End_Date
    FROM NEI_Reports.[dbo].[vPBI_PTI_ETC_FPC_Upload] a1
    INNER JOIN 
      (
      SELECT MAX(EndDate) as MaxEndDate
      FROM ' + @PTI_DB + '.[dbo].[vPTI_for_DVP]
      )
      lastperiod 
        on lastperiod.MaxEndDate = a1.EndDate
    --WHERE WBS1 = ''003800.001''
    GROUP BY WBS1, WBS2, EndDate
      ) t2
      INNER JOIN 
        ' + @DVP_DB + '.dbo.PR t1
          on t1.WBS1 = t2.WBS1 and t1.WBS2 = t2.WBS2 and t1.WBS3 = ''''
	'
	print @SQL
	if @Test_Mode = 1
		waitfor delay @Test_Mode_Delay
	else
		exec sp_executesql @SQL	
	
	set @AuditLogRecord = '{"IsError":"false","LogEvent":"Update Phases FPC Complete"}'
	insert into AuditLog(Id, TableName, TableRowId, TypeId, PreviousRecord, DTS, EmployeeId) values(NEWID(), 'sp_FPC_Upload', @InvoicingPeriodId, @AuditLogRecordChangedId, @AuditLogRecord, GetDate(), @EmployeeId)

	END TRY
	BEGIN CATCH
		SET @ErrMsg = Concat('Update Phases FPC Complete - Error ', ERROR_NUMBER(), ' updating Phases FPC in procedure sp_FPC_Upload on line #', ERROR_LINE(), ': ', ERROR_MESSAGE())
		SET @ErrorMsgs = @ErrorMsgs + '"' + @ErrMsg + '", '
		set @AuditLogRecord = '{"IsError":"true","LogEvent":"' + @ErrMsg + '"}'
		insert into AuditLog(Id, TableName, TableRowId, TypeId, PreviousRecord, DTS, EmployeeId) values(NEWID(), 'sp_FPC_Upload', @InvoicingPeriodId, @AuditLogRecordChangedId, @AuditLogRecord, GetDate(), @EmployeeId)
		
		PRINT @ErrMsg
	END CATCH	
	
	--Update Projects EAC
	BEGIN TRY
	set @AuditLogRecord = '{"IsError":"false","LogEvent":"Update Projects EAC Start"}'
	insert into AuditLog(Id, TableName, TableRowId, TypeId, PreviousRecord, DTS, EmployeeId) values(NEWID(), 'sp_FPC_Upload', @InvoicingPeriodId, @AuditLogRecordChangedId, @AuditLogRecord, GetDate(), @EmployeeId)
	
	set @SQL = '
	--SELECT *
    UPDATE
      t1
    SET
      t1.CustPhaseEAC = t2.[Calc EAC],
      t1.CustPhaseEACDate = t2.EndDate
    FROM 
      (
      SELECT 
        [WBS1]
        ,SUM([Calc EAC]) as [Calc EAC]
        ,EndDate
        ,CASE
          WHEN SUM([Calc EAC]) = 0 THEN 100
          ELSE CAST((SUM([SpentTotal])/SUM([Calc EAC]))*100 as numeric(18,2)) 
        END as [Calc FPC]
    FROM NEI_Reports.[dbo].vPBI_PTI_Period_ETC a1
    INNER JOIN 
      (
      SELECT MAX(EndDate) as MaxEndDate
      FROM ' + @PTI_DB + '.[dbo].[vPTI_for_DVP]
      )
      lastperiod 
        on lastperiod.MaxEndDate = a1.EndDate
    --WHERE WBS1 = ''003800.001''
    GROUP BY WBS1, EndDate
      ) t2
      INNER JOIN 
        ' + @DVP_DB + '.dbo.ProjectCustomTabFields t1
          on t1.WBS1 = t2.WBS1 and t1.WBS2 = '''' and t1.WBS3 = ''''
	'
	print @SQL
	if @Test_Mode = 1
		waitfor delay @Test_Mode_Delay
	else
		exec sp_executesql @SQL	

	set @AuditLogRecord = '{"IsError":"false","LogEvent":"Update Projects EAC Complete"}'
	insert into AuditLog(Id, TableName, TableRowId, TypeId, PreviousRecord, DTS, EmployeeId) values(NEWID(), 'sp_FPC_Upload', @InvoicingPeriodId, @AuditLogRecordChangedId, @AuditLogRecord, GetDate(), @EmployeeId)
	
	END TRY
	BEGIN CATCH
		SET @ErrMsg = Concat('Update Projects EAC Complete - Error ', ERROR_NUMBER(), ' updating Projects EAC in procedure sp_FPC_Upload on line #', ERROR_LINE(), ': ', ERROR_MESSAGE())
		SET @ErrorMsgs = @ErrorMsgs + '"' + @ErrMsg + '", '
		set @AuditLogRecord = '{"IsError":"true","LogEvent":"' + @ErrMsg + '"}'
		insert into AuditLog(Id, TableName, TableRowId, TypeId, PreviousRecord, DTS, EmployeeId) values(NEWID(), 'sp_FPC_Upload', @InvoicingPeriodId, @AuditLogRecordChangedId, @AuditLogRecord, GetDate(), @EmployeeId)

		PRINT @ErrMsg
	END CATCH

	--Update Projects FPC
	BEGIN TRY
	set @AuditLogRecord = '{"IsError":"false","LogEvent":"Update Projects FPC Start"}'
	insert into AuditLog(Id, TableName, TableRowId, TypeId, PreviousRecord, DTS, EmployeeId) values(NEWID(), 'sp_FPC_Upload', @InvoicingPeriodId, @AuditLogRecordChangedId, @AuditLogRecord, GetDate(), @EmployeeId)
	
	set @SQL = '
	--SELECT *
    UPDATE
      t1
    SET
      t1.PctComp = t2.[Calc FPC]
    FROM 
      (
      SELECT 
        [WBS1]
        ,SUM([Calc EAC]) as [Calc EAC]
        ,EndDate
        ,CASE
          WHEN SUM([Calc EAC]) = 0 THEN 100
          ELSE CAST((SUM([SpentTotal])/SUM([Calc EAC]))*100 as numeric(18,2)) 
        END as [Calc FPC]
    FROM NEI_Reports.[dbo].[vPBI_PTI_ETC_FPC_Upload] a1
    INNER JOIN 
      (
      SELECT MAX(EndDate) as MaxEndDate
      FROM ' + @PTI_DB + '.[dbo].[vPTI_for_DVP]
      )
      lastperiod 
        on lastperiod.MaxEndDate = a1.EndDate
    --WHERE WBS1 = ''003800.001''
    GROUP BY WBS1, EndDate
      ) t2
      INNER JOIN 
        ' + @DVP_DB + '.dbo.PR t1
          on t1.WBS1 = t2.WBS1 and t1.WBS2 = '''' and t1.WBS3 = ''''
	'
	print @SQL
	if @Test_Mode = 1
		waitfor delay @Test_Mode_Delay
	else
		exec sp_executesql @SQL	

	set @AuditLogRecord = '{"IsError":"false","LogEvent":"Update Projects FPC Complete"}'
	insert into AuditLog(Id, TableName, TableRowId, TypeId, PreviousRecord, DTS, EmployeeId) values(NEWID(), 'sp_FPC_Upload', @InvoicingPeriodId, @AuditLogRecordChangedId, @AuditLogRecord, GetDate(), @EmployeeId)
	
	END TRY
	BEGIN CATCH
		SET @ErrMsg = Concat('Update Projects FPC Complete - Error ', ERROR_NUMBER(), ' updating Projects FPC in procedure sp_FPC_Upload on line #', ERROR_LINE(), ': ', ERROR_MESSAGE())
		SET @ErrorMsgs = @ErrorMsgs + '"' + @ErrMsg + '", '
		set @AuditLogRecord = '{"IsError":"true","LogEvent":"' + @ErrMsg + '"}'
		insert into AuditLog(Id, TableName, TableRowId, TypeId, PreviousRecord, DTS, EmployeeId) values(NEWID(), 'sp_FPC_Upload', @InvoicingPeriodId, @AuditLogRecordChangedId, @AuditLogRecord, GetDate(), @EmployeeId)

		PRINT @ErrMsg
	END CATCH
	
	--Add process complete AuditLog record
	if @ErrorMsgs = ''
	BEGIN
		set @AuditLogRecord = '{"IsError":"false","LogEvent":"FPC Upload Process Complete"}'
	END
	ELSE
	BEGIN
		set @AuditLogRecord = concat('{"IsError":"true","LogEvent":"FPC Upload Process Complete With Errors","Errors":[', substring(trim(@ErrorMsgs), 1, len(trim(@ErrorMsgs)) - 1), ']}')
	END
	insert into AuditLog(Id, TableName, TableRowId, TypeId, PreviousRecord, DTS, EmployeeId) values(NEWID(), 'sp_FPC_Upload', @InvoicingPeriodId, @AuditLogRecordChangedId, @AuditLogRecord, GetDate(), @EmployeeId)
	
