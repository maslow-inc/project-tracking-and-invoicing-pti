--Swap Employee Ids for Testing (i.e., make Sean's Id = Andrew Ackerman's Id so Sean has invoicing period data)
declare @FromNetworkId as nvarchar(50)
declare @FromEmpId as uniqueidentifier
declare @ToNetworkId as nvarchar(50)
declare @ToEmpId as uniqueidentifier
set @FromNetworkId = 'aackerman'
set @ToNetworkId = 'sean'
declare @tempid as uniqueidentifier
select @tempid = newid()
select @FromEmpId = Id from Employee where NetworkId = @FromNetworkId
select @ToEmpId = Id from Employee where NetworkId = @ToNetworkId
update Employee set Id = @tempid where NetworkId = @FromNetworkId
update Employee set Id = @FromEmpId where NetworkId = @ToNetworkId
update Employee set Id = @ToEmpId where NetworkId = @FromNetworkId