select ptidata.id as InvoicingPeriodDataId, ptidata.ClientName, ptidata.ProjectName, ptidata.PhaseName, ptidata.SubphaseName
, ptidata.SpentTotal as PTI_SPENT_TOTAL, wrk.JTD_BILLING_TOTAL_SPENT as DVP_SPENT_TOTAL, (wrk.JTD_BILLING_TOTAL_SPENT - ptidata.SpentTotal) as SPENT_TOTAL_DIFF
, ptidata.WIP_Total as PTI_WIP_TOTAL, wrk.WIP_BY_PHASE_SUB_PHASE as DVP_WIP_TOTAL, (wrk.WIP_BY_PHASE_SUB_PHASE - ptidata.WIP_Total) as WIP_TOTAL_DIFF
, ptidata.WIP_Current as PTI_WIP_CURRENT, wrk.WIP_BILLABLE as DVP_WIP_CURRENT, (wrk.WIP_BILLABLE - ptidata.WIP_Current) as WIP_CURRENT_DIFF
, ptidata.WIP_Held as PTI_WIP_HELD, wrk.WIP_HELD as DVP_WIP_HELD, (wrk.WIP_HELD - ptidata.WIP_Held) as WIP_HELD_DIFF
from work_invoicingperiodsetup wrk
inner join (
select concat(isnull(ipprj.WBS1, ''), ':', isnull(ipp.WBS2, ''), ':', isnull(ips.WBS3, '')) as proj_key, ipprj.ClientName, concat(ipprj.WBS1, ' - ', ipprj.Name) as ProjectName, concat(ipp.WBS2, ' ' , ipp.Name) as 'PhaseName', ips.Name as 'SubphaseName'
, ipd.*
from invoicingperioddata ipd
inner join InvoicingPeriodSubphase ips on ipd.InvoicingPeriodSubphaseId = ips.id
inner join invoicingperiodphase ipp on ipd.invoicingperiodphaseid = ipp.Id
inner join InvoicingPeriodProject ipprj on ipd.invoicingperiodprojectid = ipprj.id
where ipd.invoicingperiodid = '818CC95C-4BB7-4C2D-A45D-03FE75801CCF' --(select id from invoicingperiod where invoicingperiodname = '202209')
) PTIData
on wrk.PROJ_KEY = PTIData.proj_key
where PTIData.SpentTotal <> wrk.JTD_BILLING_TOTAL_SPENT or ptidata.WIP_Total <> wrk.WIP_BY_PHASE_SUB_PHASE 
or ptidata.WIP_Current <> wrk.WIP_BILLABLE or ptidata.WIP_HELD <> wrk.WIP_HELD
order by ptidata.ClientName, ptidata.ProjectName, ptidata.PhaseName, ptidata.SubphaseName

--Code to change some work_ table values
/*
update work_invoicingperiodsetup set JTD_BILLING_TOTAL_SPENT = cast(1.05 * cast(JTD_BILLING_TOTAL_SPENT as numeric(18,4)) as varchar(255))
where cast(JTD_BILLING_TOTAL_SPENT as numeric(18,4)) > 100000

update work_invoicingperiodsetup set WIP_BY_PHASE_SUB_PHASE = cast(1.05 * cast(WIP_BY_PHASE_SUB_PHASE as numeric(18,4)) as varchar(255))
where cast(WIP_BY_PHASE_SUB_PHASE as numeric(18,4)) > 10000

update work_invoicingperiodsetup set WIP_BILLABLE = cast(1.5 * cast(WIP_BILLABLE as numeric(18,4)) as varchar(255))
where cast(WIP_BILLABLE as numeric(18,4)) > 15000

update work_invoicingperiodsetup set WIP_HELD = cast(1.18 * cast(WIP_HELD as numeric(18,4)) as varchar(255))
where cast(WIP_HELD as numeric(18,4)) > 2000
*/
select JTD_BILLING_TOTAL_SPENT, WIP_BY_PHASE_SUB_PHASE, WIP_BILLABLE, WIP_HELD from work_invoicingperiodsetup
--where cast(JTD_BILLING_TOTAL_SPENT as numeric(18,4)) > 100000
--where cast(WIP_BY_PHASE_SUB_PHASE as numeric(18,4)) > 10000
--where cast(WIP_BILLABLE as numeric(18,4)) > 15000
--where cast(WIP_HELD as numeric(18,4)) > 2000
where cast(EXP_UNBILLED as numeric(18,4)) > 2500

--Updates to change InvoicingPeriodData Spent and WIP values in Invoicing period 202211
/*
update invoicingperioddata set SpentTotal = 1.08 * SpentTotal
where invoicingperiodid = (select id from invoicingperiod where InvoicingPeriodName = '202211')
and spenttotal > 100000

update invoicingperioddata set WIP_Total = 1.13 * WIP_Total
where invoicingperiodid = (select id from invoicingperiod where InvoicingPeriodName = '202211')
and WIP_Total > 10000

update invoicingperioddata set WIP_Current = 1.5 * WIP_Current
where invoicingperiodid = (select id from invoicingperiod where InvoicingPeriodName = '202211')
and WIP_Current > 15000

update invoicingperioddata set WIP_Held = 1.18 * WIP_Held
where invoicingperiodid = (select id from invoicingperiod where InvoicingPeriodName = '202211')
and WIP_Held > 2000

update invoicingperioddata set ExpenseUnbilled = .65 * ExpenseUnbilled
where invoicingperiodid = (select id from invoicingperiod where InvoicingPeriodName = '202211')
and ExpenseUnbilled > 2000
*/