/* NEI PTI Application SQL Server DB Objects */
/* REV 14.0 2022-04-11 */

Use PTI
GO

/****** Object:  Table [dbo].[Employee] ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Employee]') and OBJECTPROPERTY(id, N'IsTable') = 1)
drop table [dbo].[Employee]

SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

CREATE TABLE [dbo].[Employee](
		[Id] [uniqueidentifier] NOT NULL,
		[NetworkId] [nvarchar](250) NOT NULL,
		[EmployeeId] [nvarchar](20) NOT NULL,
		[LastName] [nvarchar](50) NOT NULL,
		[FirstName] [nvarchar](50) NOT NULL,
		[Email] [nvarchar](50) NOT NULL
 CONSTRAINT [PK_Employee] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
))
--Developer Employee records
insert into Employee(Id, NetworkId, EmployeeId, LastName, FirstName, Email) Values(NEWID(), 'sean', '', 'Maslow', 'Sean', 'smaslow@gmail.com')
insert into Employee(Id, NetworkId, EmployeeId, LastName, FirstName, Email) Values(NEWID(), 'smaslow', '', 'Maslow', 'Sean', 'smaslow@neieng.com')
insert into Employee(Id, NetworkId, EmployeeId, LastName, FirstName, Email) Values(NEWID(), 'mlanphier', '0025', 'Lanphier', 'Mark', 'mlanphier@neieng.com')
insert into Employee(Id, NetworkId, EmployeeId, LastName, FirstName, Email) Values(NEWID(), 'pparrucci', '1610', 'Parrucci', 'Paul', 'pparrucci@neieng.com')


/****** Object:  Table [dbo].[MetadataType] ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[MetadataType]') and OBJECTPROPERTY(id, N'IsTable') = 1)
drop table [dbo].[MetadataType]

SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

CREATE TABLE [dbo].[MetadataType](
		[Id] [uniqueidentifier] NOT NULL,
		[Name] [nvarchar](50) NOT NULL
 CONSTRAINT [PK_MetadataType] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
))
--System MetadataTypes
/*AuditLog*/
DECLARE @AuditLogEventTypeGUID nvarchar(50)
set @AuditLogEventTypeGUID = NEWID()
insert into MetadataType(Id, Name) values(@AuditLogEventTypeGUID, 'AuditLogEvent')
DECLARE @MetadataPredicateTypeGUID nvarchar(50)

/*Metadata*/
set @MetadataPredicateTypeGUID = NEWID()
insert into MetadataType(Id, Name) values(@MetadataPredicateTypeGUID, 'MetadataPredicate')

/*InvoicingPeriod*/
DECLARE @InvoicingPeriodStatusTypeGUID nvarchar(50)
set @InvoicingPeriodStatusTypeGUID = NEWID()
insert into MetadataType(Id, Name) values(@InvoicingPeriodStatusTypeGUID, 'InvoicingPeriodStatus')

/*InvoicingPeriodEmployee*/
DECLARE @InvoicingPeriodEmployeeStatusTypeGUID nvarchar(50)
set @InvoicingPeriodEmployeeStatusTypeGUID = NEWID()
insert into MetadataType(Id, Name) values(@InvoicingPeriodEmployeeStatusTypeGUID, 'InvoicingPeriodEmployeeStatus')
DECLARE @InvoicingPeriodEmployeeApprovalStatusTypeGUID nvarchar(50)
set @InvoicingPeriodEmployeeApprovalStatusTypeGUID = NEWID()
insert into MetadataType(Id, Name) values(@InvoicingPeriodEmployeeApprovalStatusTypeGUID, 'InvoicingPeriodEmployeeApprovalStatus')
DECLARE @InvoicingPeriodEmployeeInvoicingStatusTypeGUID nvarchar(50)
set @InvoicingPeriodEmployeeInvoicingStatusTypeGUID = NEWID()
insert into MetadataType(Id, Name) values(@InvoicingPeriodEmployeeInvoicingStatusTypeGUID, 'InvoicingPeriodEmployeeInvoicingStatus')

/*InvoicingPeriodProject*/
DECLARE @InvoicingPeriodProjectStatusTypeGUID nvarchar(50)
set @InvoicingPeriodProjectStatusTypeGUID = NEWID()
insert into MetadataType(Id, Name) values(@InvoicingPeriodProjectStatusTypeGUID, 'InvoicingPeriodProjectStatus')
DECLARE @InvoicingPeriodProjectApprovalStatusTypeGUID nvarchar(50)
set @InvoicingPeriodProjectApprovalStatusTypeGUID = NEWID()
insert into MetadataType(Id, Name) values(@InvoicingPeriodProjectApprovalStatusTypeGUID, 'InvoicingPeriodProjectApprovalStatus')
DECLARE @InvoicingPeriodProjectInvoicingStatusTypeGUID nvarchar(50)
set @InvoicingPeriodProjectInvoicingStatusTypeGUID = NEWID()
insert into MetadataType(Id, Name) values(@InvoicingPeriodProjectInvoicingStatusTypeGUID, 'InvoicingPeriodProjectInvoicingStatus')

/*InvoicingPeriodPhase*/
DECLARE @InvoicingPeriodPhaseStatusTypeGUID nvarchar(50)
set @InvoicingPeriodPhaseStatusTypeGUID = NEWID()
insert into MetadataType(Id, Name) values(@InvoicingPeriodPhaseStatusTypeGUID, 'InvoicingPeriodPhaseStatus')

/*InvoicingPeriodSubphase*/
DECLARE @InvoicingPeriodSubphaseStatusTypeGUID nvarchar(50)
set @InvoicingPeriodSubphaseStatusTypeGUID = NEWID()
insert into MetadataType(Id, Name) values(@InvoicingPeriodSubphaseStatusTypeGUID, 'InvoicingPeriodSubphaseStatus')

/*Data Entry Rule and Rule Message Types*/
DECLARE @RuleTypeGUID nvarchar(50)
set @RuleTypeGUID = NEWID()
insert into MetadataType(Id, Name) values(@RuleTypeGUID, 'Rule')
DECLARE @RuleMessageTypeGUID nvarchar(50)
set @RuleMessageTypeGUID = NEWID()
insert into MetadataType(Id, Name) values(@RuleMessageTypeGUID, 'RuleMessage')

/*Questions and Responses*/
DECLARE @QuestionTypeGUID nvarchar(50)
set @QuestionTypeGUID = NEWID()
insert into MetadataType(Id, Name) values(@QuestionTypeGUID, 'Question')
DECLARE @ResponseTypeGUID nvarchar(50)
set @ResponseTypeGUID = NEWID()
insert into MetadataType(Id, Name) values(@ResponseTypeGUID, 'Response')

/*Rule Overrides*/
DECLARE @RuleOverrideTypeGUID nvarchar(50)
set @RuleOverrideTypeGUID = NEWID()
insert into MetadataType(Id, Name) values(@RuleOverrideTypeGUID, 'RuleOverride')


/****** Object:  Table [dbo].[Metadata] ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Metadata]') and OBJECTPROPERTY(id, N'IsTable') = 1)
drop table [dbo].[Metadata]

SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

CREATE TABLE [dbo].[Metadata](
		[Id] [uniqueidentifier] NOT NULL,
		[TypeId] [uniqueidentifier] NOT NULL,
		[Value] [nvarchar] (4000) NOT NULL,
		[Predicates] [nvarchar] (MAX) NULL,
		[ActiveFlag] [bit] NOT NULL,
		[SystemFlag] [bit] NOT NULL		
 CONSTRAINT [PK_Metadata] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
))
--AuditLog System Metadata
insert into Metadata(Id, TypeId, [Value], Predicates, ActiveFlag, SystemFlag) values(NEWID(), @AuditLogEventTypeGUID, 'Record Added', '', 1, 1)
insert into Metadata(Id, TypeId, [Value], Predicates, ActiveFlag, SystemFlag) values(NEWID(), @AuditLogEventTypeGUID, 'Record Changed', '', 1, 1)
insert into Metadata(Id, TypeId, [Value], Predicates, ActiveFlag, SystemFlag) values(NEWID(), @AuditLogEventTypeGUID, 'Record Deleted', '', 1, 1)

--InvoicingPeriod Statuses
insert into Metadata(Id, TypeId, [Value], Predicates, ActiveFlag, SystemFlag) values(NEWID(), @InvoicingPeriodStatusTypeGUID, 'Open', '', 1, 1)
insert into Metadata(Id, TypeId, [Value], Predicates, ActiveFlag, SystemFlag) values(NEWID(), @InvoicingPeriodStatusTypeGUID, 'Closed', '', 1, 1)

--InvoicingPeriodEmployee Statuses
insert into Metadata(Id, TypeId, [Value], Predicates, ActiveFlag, SystemFlag) values(NEWID(), @InvoicingPeriodEmployeeStatusTypeGUID, 'Not Started', '', 1, 1)
insert into Metadata(Id, TypeId, [Value], Predicates, ActiveFlag, SystemFlag) values(NEWID(), @InvoicingPeriodEmployeeStatusTypeGUID, 'Started', '', 1, 1)
insert into Metadata(Id, TypeId, [Value], Predicates, ActiveFlag, SystemFlag) values(NEWID(), @InvoicingPeriodEmployeeStatusTypeGUID, 'Submitted', '', 1, 1)
insert into Metadata(Id, TypeId, [Value], Predicates, ActiveFlag, SystemFlag) values(NEWID(), @InvoicingPeriodEmployeeApprovalStatusTypeGUID, 'Not Reviewed', '', 1, 1)
insert into Metadata(Id, TypeId, [Value], Predicates, ActiveFlag, SystemFlag) values(NEWID(), @InvoicingPeriodEmployeeApprovalStatusTypeGUID, 'Questions', '', 1, 1)
insert into Metadata(Id, TypeId, [Value], Predicates, ActiveFlag, SystemFlag) values(NEWID(), @InvoicingPeriodEmployeeApprovalStatusTypeGUID, 'Approved', '', 1, 1)
insert into Metadata(Id, TypeId, [Value], Predicates, ActiveFlag, SystemFlag) values(NEWID(), @InvoicingPeriodEmployeeInvoicingStatusTypeGUID, 'Not Started', '', 1, 1)
insert into Metadata(Id, TypeId, [Value], Predicates, ActiveFlag, SystemFlag) values(NEWID(), @InvoicingPeriodEmployeeInvoicingStatusTypeGUID, 'Questions', '', 1, 1)
insert into Metadata(Id, TypeId, [Value], Predicates, ActiveFlag, SystemFlag) values(NEWID(), @InvoicingPeriodEmployeeInvoicingStatusTypeGUID, 'Approved', '', 1, 1)

--InvoicingPeriodProject Statuses
insert into Metadata(Id, TypeId, [Value], Predicates, ActiveFlag, SystemFlag) values(NEWID(), @InvoicingPeriodProjectStatusTypeGUID, 'Not Started', '', 1, 1)
insert into Metadata(Id, TypeId, [Value], Predicates, ActiveFlag, SystemFlag) values(NEWID(), @InvoicingPeriodProjectStatusTypeGUID, 'Started', '', 1, 1)
insert into Metadata(Id, TypeId, [Value], Predicates, ActiveFlag, SystemFlag) values(NEWID(), @InvoicingPeriodProjectStatusTypeGUID, 'Submitted', '', 1, 1)
insert into Metadata(Id, TypeId, [Value], Predicates, ActiveFlag, SystemFlag) values(NEWID(), @InvoicingPeriodProjectApprovalStatusTypeGUID, 'Not Reviewed', '', 1, 1)
insert into Metadata(Id, TypeId, [Value], Predicates, ActiveFlag, SystemFlag) values(NEWID(), @InvoicingPeriodProjectApprovalStatusTypeGUID, 'Questions', '', 1, 1)
insert into Metadata(Id, TypeId, [Value], Predicates, ActiveFlag, SystemFlag) values(NEWID(), @InvoicingPeriodProjectApprovalStatusTypeGUID, 'Approved', '', 1, 1)
insert into Metadata(Id, TypeId, [Value], Predicates, ActiveFlag, SystemFlag) values(NEWID(), @InvoicingPeriodProjectInvoicingStatusTypeGUID, 'Not Started', '', 1, 1)
insert into Metadata(Id, TypeId, [Value], Predicates, ActiveFlag, SystemFlag) values(NEWID(), @InvoicingPeriodProjectInvoicingStatusTypeGUID, 'Questions', '', 1, 1)
insert into Metadata(Id, TypeId, [Value], Predicates, ActiveFlag, SystemFlag) values(NEWID(), @InvoicingPeriodProjectInvoicingStatusTypeGUID, 'Approved', '', 1, 1)

--InvoicingPeriodPhase Statuses
insert into Metadata(Id, TypeId, [Value], Predicates, ActiveFlag, SystemFlag) values(NEWID(), @InvoicingPeriodPhaseStatusTypeGUID, 'Not Started', '', 1, 1)
insert into Metadata(Id, TypeId, [Value], Predicates, ActiveFlag, SystemFlag) values(NEWID(), @InvoicingPeriodPhaseStatusTypeGUID, 'Questions', '', 1, 1)
insert into Metadata(Id, TypeId, [Value], Predicates, ActiveFlag, SystemFlag) values(NEWID(), @InvoicingPeriodPhaseStatusTypeGUID, 'Approved', '', 1, 1)

--InvoicingPeriodSubphase Statuses
insert into Metadata(Id, TypeId, [Value], Predicates, ActiveFlag, SystemFlag) values(NEWID(), @InvoicingPeriodSubphaseStatusTypeGUID, 'Not Started', '', 1, 1)
insert into Metadata(Id, TypeId, [Value], Predicates, ActiveFlag, SystemFlag) values(NEWID(), @InvoicingPeriodSubphaseStatusTypeGUID, 'Questions', '', 1, 1)
insert into Metadata(Id, TypeId, [Value], Predicates, ActiveFlag, SystemFlag) values(NEWID(), @InvoicingPeriodSubphaseStatusTypeGUID, 'Approved', '', 1, 1)

--Rules, Rule Messages, Rule Questions and Responses
DECLARE @RuleGUID nvarchar(50)
DECLARE @RuleMessageGUID nvarchar(50)
DECLARE @RuleQuestionGUID nvarchar(50)
DECLARE @QuestionResponseGUID nvarchar(50)
DECLARE @MetadataPredicate nvarchar(MAX)
DECLARE @HasMessagePredicateGUID nvarchar(50)
DECLARE @HasQuestionPredicateGUID nvarchar(50)
DECLARE @HasResponsePredicateGUID nvarchar(50)
set @HasMessagePredicateGUID = NEWID()
insert into Metadata(Id, TypeId, [Value], Predicates, ActiveFlag, SystemFlag) values(@HasMessagePredicateGUID, @MetadataPredicateTypeGUID, 'Has Message', '', 1, 1)
set @HasQuestionPredicateGUID = NEWID()
insert into Metadata(Id, TypeId, [Value], Predicates, ActiveFlag, SystemFlag) values(@HasQuestionPredicateGUID, @MetadataPredicateTypeGUID, 'Has Question', '', 1, 1)
set @HasResponsePredicateGUID = NEWID()
insert into Metadata(Id, TypeId, [Value], Predicates, ActiveFlag, SystemFlag) values(@HasResponsePredicateGUID, @MetadataPredicateTypeGUID, 'Has Response', '', 1, 1)

/*ETCRule1*/
set @RuleMessageGUID = NEWID()
insert into Metadata(Id, TypeId, [Value], Predicates, ActiveFlag, SystemFlag) values(@RuleMessageGUID, @RuleMessageTypeGUID, 'ETC values are not required at this stage of the project.', '', 1, 1)
set @MetadataPredicate = concat('[{"Predicate":"', @HasMessagePredicateGUID, '", "Object":"', @RuleMessageGUID, '"}]')
set @RuleGUID = NEWID()
insert into Metadata(Id, TypeId, [Value], Predicates, ActiveFlag, SystemFlag) values(@RuleGUID, @RuleTypeGUID, 'ETCRule1', @MetadataPredicate, 1, 1) --Rule
insert into Metadata(Id, TypeId, [Value], Predicates, ActiveFlag, SystemFlag) values(NEWID(), @RuleOverrideTypeGUID, 'ETCRule1', '', 1, 1) --Rule Override

/*ETCRule2*/
set @RuleMessageGUID = NEWID()
insert into Metadata(Id, TypeId, [Value], Predicates, ActiveFlag, SystemFlag) values(@RuleMessageGUID, @RuleMessageTypeGUID, 'ETC values are not required at this stage of the project, but if you have reason to believe at this stage of the project that EAC will deviate from compensation, enter values accordingly.', '', 1, 1)
set @MetadataPredicate = concat('[{"Predicate":"', @HasMessagePredicateGUID, '", "Object":"', @RuleMessageGUID, '"}]')
set @RuleGUID = NEWID()
insert into Metadata(Id, TypeId, [Value], Predicates, ActiveFlag, SystemFlag) values(@RuleGUID, @RuleTypeGUID, 'ETCRule2', @MetadataPredicate, 1, 1)
insert into Metadata(Id, TypeId, [Value], Predicates, ActiveFlag, SystemFlag) values(NEWID(), @RuleOverrideTypeGUID, 'ETCRule2', '', 1, 1) --Rule Override

/*ETCRule8*/
set @RuleMessageGUID = NEWID()
insert into Metadata(Id, TypeId, [Value], Predicates, ActiveFlag, SystemFlag) values(@RuleMessageGUID, @RuleMessageTypeGUID, 'A description must be entered in all subphase notes where risk is not $0.00.', '', 1, 1)
set @MetadataPredicate = concat('[{"Predicate":"', @HasMessagePredicateGUID, '", "Object":"', @RuleMessageGUID, '"}]')
set @RuleGUID = NEWID()
insert into Metadata(Id, TypeId, [Value], Predicates, ActiveFlag, SystemFlag) values(@RuleGUID, @RuleTypeGUID, 'ETCRule8', @MetadataPredicate, 1, 1)

/*ETCRule14*/
set @RuleMessageGUID = NEWID()
insert into Metadata(Id, TypeId, [Value], Predicates, ActiveFlag, SystemFlag) values(@RuleMessageGUID, @RuleMessageTypeGUID, 'Calc EAC is less than Spent Total. In order to proceed, update ETC.', '', 1, 1)
set @MetadataPredicate = concat('[{"Predicate":"', @HasMessagePredicateGUID, '", "Object":"', @RuleMessageGUID, '"}]')
set @RuleGUID = NEWID()
insert into Metadata(Id, TypeId, [Value], Predicates, ActiveFlag, SystemFlag) values(@RuleGUID, @RuleTypeGUID, 'ETCRule14', @MetadataPredicate, 1, 1)

/*ETCRule15*/
--Question Responses
set @QuestionResponseGUID = NEWID()
insert into Metadata(Id, TypeId, [Value], Predicates, ActiveFlag, SystemFlag) values(@QuestionResponseGUID, @ResponseTypeGUID, 'Engineering Inefficiencies', '', 1, 1)
set @MetadataPredicate = concat('[{"Predicate":"', @HasResponsePredicateGUID, '", "Object":"', @QuestionResponseGUID, '"}')
set @QuestionResponseGUID = NEWID()
insert into Metadata(Id, TypeId, [Value], Predicates, ActiveFlag, SystemFlag) values(@QuestionResponseGUID, @ResponseTypeGUID, 'Engineering Efficiencies', '', 1, 1)
set @MetadataPredicate = concat(@MetadataPredicate, ',{"Predicate":"', @HasResponsePredicateGUID, '", "Object":"', @QuestionResponseGUID, '"}')
set @QuestionResponseGUID = NEWID()
insert into Metadata(Id, TypeId, [Value], Predicates, ActiveFlag, SystemFlag) values(@QuestionResponseGUID, @ResponseTypeGUID, 'Missed scope during proposal stage', '', 1, 1)
set @MetadataPredicate = concat(@MetadataPredicate, ',{"Predicate":"', @HasResponsePredicateGUID, '", "Object":"', @QuestionResponseGUID, '"}')
set @QuestionResponseGUID = NEWID()
insert into Metadata(Id, TypeId, [Value], Predicates, ActiveFlag, SystemFlag) values(@QuestionResponseGUID, @ResponseTypeGUID, 'Scope included in proposal not needed by customer', '', 1, 1)
set @MetadataPredicate = concat(@MetadataPredicate, ',{"Predicate":"', @HasResponsePredicateGUID, '", "Object":"', @QuestionResponseGUID, '"}')
set @QuestionResponseGUID = NEWID()
insert into Metadata(Id, TypeId, [Value], Predicates, ActiveFlag, SystemFlag) values(@QuestionResponseGUID, @ResponseTypeGUID, 'Change order received from customer', '', 1, 1)
set @MetadataPredicate = concat(@MetadataPredicate, ',{"Predicate":"', @HasResponsePredicateGUID, '", "Object":"', @QuestionResponseGUID, '"}')
set @QuestionResponseGUID = NEWID()
insert into Metadata(Id, TypeId, [Value], Predicates, ActiveFlag, SystemFlag) values(@QuestionResponseGUID, @ResponseTypeGUID, 'Deductive change order received from customer.', '', 1, 1)
set @MetadataPredicate = concat(@MetadataPredicate, ',{"Predicate":"', @HasResponsePredicateGUID, '", "Object":"', @QuestionResponseGUID, '"}')
set @QuestionResponseGUID = NEWID()
insert into Metadata(Id, TypeId, [Value], Predicates, ActiveFlag, SystemFlag) values(@QuestionResponseGUID, @ResponseTypeGUID, 'Miscalulation of Prior EAC', '', 1, 1)
set @MetadataPredicate = concat(@MetadataPredicate, ',{"Predicate":"', @HasResponsePredicateGUID, '", "Object":"', @QuestionResponseGUID, '"}')
set @QuestionResponseGUID = NEWID()
insert into Metadata(Id, TypeId, [Value], Predicates, ActiveFlag, SystemFlag) values(@QuestionResponseGUID, @ResponseTypeGUID, 'Project is T&E', '', 1, 1)
set @MetadataPredicate = concat(@MetadataPredicate, ',{"Predicate":"', @HasResponsePredicateGUID, '", "Object":"', @QuestionResponseGUID, '"}')
set @QuestionResponseGUID = NEWID()
insert into Metadata(Id, TypeId, [Value], Predicates, ActiveFlag, SystemFlag) values(@QuestionResponseGUID, @ResponseTypeGUID, 'Added scope for OJT, Warranty, rework', '', 1, 1)
set @MetadataPredicate = concat(@MetadataPredicate, ',{"Predicate":"', @HasResponsePredicateGUID, '", "Object":"', @QuestionResponseGUID, '"}]')
--Rule Question
set @RuleQuestionGUID = NEWID()
insert into Metadata(Id, TypeId, [Value], Predicates, ActiveFlag, SystemFlag) values(@RuleQuestionGUID, @QuestionTypeGUID, 'Why has the project EAC changed by more than +/- 5%?', @MetadataPredicate, 1, 1)
set @MetadataPredicate = concat('[{"Predicate":"', @HasQuestionPredicateGUID, '", "Object":"', @RuleQuestionGUID, '"}]')
--Rule
set @RuleGUID = NEWID()
insert into Metadata(Id, TypeId, [Value], Predicates, ActiveFlag, SystemFlag) values(@RuleGUID, @RuleTypeGUID, 'ETCRule15', @MetadataPredicate, 1, 1)

/*InvoicingRule1*/
--Question Responses
set @QuestionResponseGUID = NEWID()
insert into Metadata(Id, TypeId, [Value], Predicates, ActiveFlag, SystemFlag) values(@QuestionResponseGUID, @ResponseTypeGUID, 'Dollar value is too low to invoice.', '', 1, 1)
set @MetadataPredicate = concat('[{"Predicate":"', @HasResponsePredicateGUID, '", "Object":"', @QuestionResponseGUID, '"}')
set @QuestionResponseGUID = NEWID()
insert into Metadata(Id, TypeId, [Value], Predicates, ActiveFlag, SystemFlag) values(@QuestionResponseGUID, @ResponseTypeGUID, 'Work completed on scope that is pending a change order.', '', 1, 1)
set @MetadataPredicate = concat(@MetadataPredicate, ',{"Predicate":"', @HasResponsePredicateGUID, '", "Object":"', @QuestionResponseGUID, '"}')
set @QuestionResponseGUID = NEWID()
insert into Metadata(Id, TypeId, [Value], Predicates, ActiveFlag, SystemFlag) values(@QuestionResponseGUID, @ResponseTypeGUID, 'Work started before receipt of a purchase order or NTP.', '', 1, 1)
set @MetadataPredicate = concat(@MetadataPredicate, ',{"Predicate":"', @HasResponsePredicateGUID, '", "Object":"', @QuestionResponseGUID, '"}')
set @QuestionResponseGUID = NEWID()
insert into Metadata(Id, TypeId, [Value], Predicates, ActiveFlag, SystemFlag) values(@QuestionResponseGUID, @ResponseTypeGUID, 'Work completed on scope for the next milestone.', '', 1, 1)
set @MetadataPredicate = concat(@MetadataPredicate, ',{"Predicate":"', @HasResponsePredicateGUID, '", "Object":"', @QuestionResponseGUID, '"}]')
--Rule Question
set @RuleQuestionGUID = NEWID()
insert into Metadata(Id, TypeId, [Value], Predicates, ActiveFlag, SystemFlag) values(@RuleQuestionGUID, @QuestionTypeGUID, 'Why are we holding WIP?', @MetadataPredicate, 1, 1)
set @MetadataPredicate = concat('[{"Predicate":"', @HasQuestionPredicateGUID, '", "Object":"', @RuleQuestionGUID, '"}]')
--Rule
set @RuleGUID = NEWID()
insert into Metadata(Id, TypeId, [Value], Predicates, ActiveFlag, SystemFlag) values(@RuleGUID, @RuleTypeGUID, 'InvoicingRule1', @MetadataPredicate, 1, 1)

/*InvoicingRule2*/
set @RuleMessageGUID = NEWID()
insert into Metadata(Id, TypeId, [Value], Predicates, ActiveFlag, SystemFlag) values(@RuleMessageGUID, @RuleMessageTypeGUID, 'Enter a note including the project number, phase and subphase where the WIP should be transferred for all subphasees having a non-zero Amt to Transfer.', '', 1, 1)
set @MetadataPredicate = concat('[{"Predicate":"', @HasMessagePredicateGUID, '", "Object":"', @RuleMessageGUID, '"}]')
set @RuleGUID = NEWID()
insert into Metadata(Id, TypeId, [Value], Predicates, ActiveFlag, SystemFlag) values(@RuleGUID, @RuleTypeGUID, 'InvoicingRule2', @MetadataPredicate, 1, 1)

/*InvoicingRule4*/
set @QuestionResponseGUID = NEWID()
insert into Metadata(Id, TypeId, [Value], Predicates, ActiveFlag, SystemFlag) values(@QuestionResponseGUID, @ResponseTypeGUID, 'Project is invoiced in full and project is closing.', '', 1, 1)
set @MetadataPredicate = concat('[{"Predicate":"', @HasResponsePredicateGUID, '", "Object":"', @QuestionResponseGUID, '"}')
set @QuestionResponseGUID = NEWID()
insert into Metadata(Id, TypeId, [Value], Predicates, ActiveFlag, SystemFlag) values(@QuestionResponseGUID, @ResponseTypeGUID, 'Work completed was for a milestone that has already been invoiced.', '', 1, 1)
set @MetadataPredicate = concat(@MetadataPredicate, ',{"Predicate":"', @HasResponsePredicateGUID, '", "Object":"', @QuestionResponseGUID, '"}')
set @QuestionResponseGUID = NEWID()
insert into Metadata(Id, TypeId, [Value], Predicates, ActiveFlag, SystemFlag) values(@QuestionResponseGUID, @ResponseTypeGUID, 'Work was related to OJT, Warranty, Rework and cannot be invoiced to customer.', '', 1, 1)
set @MetadataPredicate = concat(@MetadataPredicate, ',{"Predicate":"', @HasResponsePredicateGUID, '", "Object":"', @QuestionResponseGUID, '"}')
set @QuestionResponseGUID = NEWID()
insert into Metadata(Id, TypeId, [Value], Predicates, ActiveFlag, SystemFlag) values(@QuestionResponseGUID, @ResponseTypeGUID, 'Project or Phase is  over budget and we cannot invoice.', '', 1, 1)
set @MetadataPredicate = concat(@MetadataPredicate, ',{"Predicate":"', @HasResponsePredicateGUID, '", "Object":"', @QuestionResponseGUID, '"}]')
--Rule Question
set @RuleQuestionGUID = NEWID()
insert into Metadata(Id, TypeId, [Value], Predicates, ActiveFlag, SystemFlag) values(@RuleQuestionGUID, @QuestionTypeGUID, 'Why are we deleting WIP?', @MetadataPredicate, 1, 1)
set @MetadataPredicate = concat('[{"Predicate":"', @HasQuestionPredicateGUID, '", "Object":"', @RuleQuestionGUID, '"}]')
--Rule
set @RuleGUID = NEWID()
insert into Metadata(Id, TypeId, [Value], Predicates, ActiveFlag, SystemFlag) values(@RuleGUID, @RuleTypeGUID, 'InvoicingRule4', @MetadataPredicate, 1, 1)

/*InvoicingRule5*/
set @RuleMessageGUID = NEWID()
insert into Metadata(Id, TypeId, [Value], Predicates, ActiveFlag, SystemFlag) values(@RuleMessageGUID, @RuleMessageTypeGUID, 'Amount to Invoice and Invoiced is greater than compensation. Please correct your entry.', '', 1, 1)
set @MetadataPredicate = concat('[{"Predicate":"', @HasMessagePredicateGUID, '", "Object":"', @RuleMessageGUID, '"}]')
set @RuleGUID = NEWID()
insert into Metadata(Id, TypeId, [Value], Predicates, ActiveFlag, SystemFlag) values(@RuleGUID, @RuleTypeGUID, 'InvoicingRule5', @MetadataPredicate, 1, 1)

/*InvoicingRule6*/
set @RuleMessageGUID = NEWID()
insert into Metadata(Id, TypeId, [Value], Predicates, ActiveFlag, SystemFlag) values(@RuleMessageGUID, @RuleMessageTypeGUID, 'There are WIP and Expenses not accounted for (WIP Check < $0.00). Please revise your entries.', '', 1, 1)
set @MetadataPredicate = concat('[{"Predicate":"', @HasMessagePredicateGUID, '", "Object":"', @RuleMessageGUID, '"}]')
set @RuleGUID = NEWID()
insert into Metadata(Id, TypeId, [Value], Predicates, ActiveFlag, SystemFlag) values(@RuleGUID, @RuleTypeGUID, 'InvoicingRule6', @MetadataPredicate, 1, 1)

/****** Object:  Table [dbo].[AuditLog] ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[AuditLog]') and OBJECTPROPERTY(id, N'IsTable') = 1)
drop table [dbo].[AuditLog]

SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

CREATE TABLE [dbo].[AuditLog](
		[Id] [uniqueidentifier] NOT NULL,
		[TableName] [nvarchar] (100) NOT NULL,
		[TableRowId] [uniqueidentifier] NOT NULL,
		[TypeId] [uniqueidentifier] NOT NULL,
		[PreviousRecord] [nvarchar] (MAX) NOT NULL,
		[DTS] [datetime] NOT NULL,
		[EmployeeId] [uniqueidentifier] NOT NULL
 CONSTRAINT [PK_AuditLog] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
))

/****** Object:  Table [dbo].[InvoicingPeriod] ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[InvoicingPeriod]') and OBJECTPROPERTY(id, N'IsTable') = 1)
drop table [dbo].[InvoicingPeriod]

SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

CREATE TABLE [dbo].[InvoicingPeriod](
		[Id] [uniqueidentifier] NOT NULL,
		[InvoicingPeriodName] [nvarchar] (10) NOT NULL,
		[StartDate] [date] NOT NULL,
		[EndDate] [date] NOT NULL,
		[StatusId] [uniqueidentifier] NULL
 CONSTRAINT [PK_InvoicingPeriod] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
))
--Populate InvoicingPeriod from NEI_Reports.InvoicingPeriods
insert into invoicingperiod (id, StartDate, EndDate, InvoicingPeriodName, StatusId)
select newid() as Id, [Start] as [StartDate], [End] as [EndDate], [Name] as [InvoicingPeriodName], null as [StatusId]
from NEI_Reports.dbo.InvoicingPeriods

/****** Object:  Table [dbo].[InvoicingPeriodEmployee] ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[InvoicingPeriodEmployee]') and OBJECTPROPERTY(id, N'IsTable') = 1)
drop table [dbo].[InvoicingPeriodEmployee]

SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

CREATE TABLE [dbo].[InvoicingPeriodEmployee](
		[Id] [uniqueidentifier] NOT NULL,
		[InvoicingPeriodId] [uniqueidentifier] NOT NULL,
		[EmployeeId] [uniqueidentifier] NOT NULL,
		[StatusId] [uniqueidentifier] NOT NULL,
		[ApprovalStatusId] [uniqueidentifier] NULL,
		[InvoicingStatusId] [uniqueidentifier] NULL
 CONSTRAINT [PK_InvoicingPeriodEmployee] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
))

/****** Object:  Table [dbo].[InvoicingPeriodProject] ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[InvoicingPeriodProject]') and OBJECTPROPERTY(id, N'IsTable') = 1)
drop table [dbo].[InvoicingPeriodProject]

SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

CREATE TABLE [dbo].[InvoicingPeriodProject](
		[Id] [uniqueidentifier] NOT NULL,
		[InvoicingPeriodId] [uniqueidentifier] NOT NULL,
		[InvoicingPeriodEmployeeId] [uniqueidentifier] NOT NULL,
		[WBS1] [nvarchar] (30) NOT NULL,
		[Name] [nvarchar] (50) NOT NULL,
		[ClientName] [nvarchar] (100) NOT NULL,
		[StatusId] [uniqueidentifier] NOT NULL,
		[ApprovalStatusId] [uniqueidentifier] NULL,
		[InvoicingStatusId] [uniqueidentifier] NULL,
		[Notes] [nvarchar] (MAX) NULL,
		[Questions] [nvarchar] (MAX) NULL
 CONSTRAINT [PK_InvoicingPeriodProject] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
))

/****** Object:  Table [dbo].[InvoicingPeriodPhase] ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[InvoicingPeriodPhase]') and OBJECTPROPERTY(id, N'IsTable') = 1)
drop table [dbo].[InvoicingPeriodPhase]

SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

CREATE TABLE [dbo].[InvoicingPeriodPhase](
		[Id] [uniqueidentifier] NOT NULL,
		[InvoicingPeriodId] [uniqueidentifier] NOT NULL,
		[InvoicingPeriodEmployeeId] [uniqueidentifier] NOT NULL,
		[InvoicingPeriodProjectId] [uniqueidentifier] NOT NULL,
		[WBS2] [nvarchar] (30) NOT NULL,
		[Name] [nvarchar] (50) NOT NULL,
		[StatusId] [uniqueidentifier] NOT NULL,
		[Notes] [nvarchar] (MAX) NULL,
		[Questions] [nvarchar] (MAX) NULL
 CONSTRAINT [PK_InvoicingPeriodPhase] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
))

/****** Object:  Table [dbo].[InvoicingPeriodSubphase] ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[InvoicingPeriodSubphase]') and OBJECTPROPERTY(id, N'IsTable') = 1)
drop table [dbo].[InvoicingPeriodSubphase]

SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

CREATE TABLE [dbo].[InvoicingPeriodSubphase](
		[Id] [uniqueidentifier] NOT NULL,
		[InvoicingPeriodId] [uniqueidentifier] NOT NULL,
		[InvoicingPeriodEmployeeId] [uniqueidentifier] NOT NULL,
		[InvoicingPeriodPhaseId] [uniqueidentifier] NOT NULL,
		[WBS3] [nvarchar] (30) NOT NULL,
		[Name] [nvarchar] (50) NOT NULL,
		[StatusId] [uniqueidentifier] NOT NULL,
		[Notes] [nvarchar] (MAX) NULL,
		[Questions] [nvarchar] (MAX) NULL
 CONSTRAINT [PK_InvoicingPeriodSubphase] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
))

/****** Object:  Table [dbo].[InvoicingPeriodData] ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[InvoicingPeriodData]') and OBJECTPROPERTY(id, N'IsTable') = 1)
drop table [dbo].[InvoicingPeriodData]

SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

CREATE TABLE [dbo].[InvoicingPeriodData](
		[Id] [uniqueidentifier] NOT NULL,
		[InvoicingPeriodId] [uniqueidentifier] NOT NULL,
		[InvoicingPeriodEmployeeId] [uniqueidentifier] NOT NULL,
		[InvoicingPeriodProjectId] [uniqueidentifier] NOT NULL,
		[InvoicingPeriodPhaseId] [uniqueidentifier] NOT NULL,
		[InvoicingPeriodSubphaseId] [uniqueidentifier] NOT NULL,
		[PhaseStatus] [nvarchar] (30) NOT NULL,
		[NewPhaseStatus] [nvarchar] (30) NULL,
		[Compensation] [decimal] (19,4) NOT NULL,
		[DirectLaborBudget] [decimal] (19,4) NOT NULL,
		[ExpenseBudget] [decimal] (19,4) NOT NULL,
		[ConsultantBudget] [decimal] (19,4) NOT NULL,
		[SpentTotal] [decimal] (19,4) NOT NULL,
		[SpentLabor] [decimal] (19,4) NOT NULL,
		[SpentReimbExpense] [decimal] (19,4) NOT NULL,
		[HoursToComplete] [decimal] (19,4) NULL,
		[EstimateExpenses] [decimal] (19,4) NULL,
		[Risk] [decimal] (19,4) NULL,
		[InvoicedTotal] [decimal] (19,4) NOT NULL,
		[NewInvoicedPct] [decimal] (19,4) NULL,
		[AR_Amount] [decimal] (19,4) NOT NULL,
		[BillingTerms] [nvarchar] (50) NULL,
		[StartDate] [datetime] NULL,
		[EstEndDate] [datetime] NULL,
		[EstEndDate_Updated] [datetime] NULL,
		[RevenueMethod] [nvarchar] (10) NULL,
		[RevenueMethod_Updated] [nvarchar] (10) NULL,
		[PhaseMgrEmployeeId_Updated] [uniqueidentifier] NULL,
		[WIP_Total] [decimal] (19,4) NOT NULL,
		[WIP_Current] [decimal] (19,4) NOT NULL,
		[WIP_Held] [decimal] (19,4) NOT NULL,
		[WIP_To_Close] [decimal] (19,4) NULL,
		[ExpenseUnbilled] [decimal] (19,4) NOT NULL,
		[ETC] [decimal] (19,4) NOT NULL,
		[PhaseEAC] [decimal] (19,4) NOT NULL,
		[AvgBillRate] [decimal] (19,4) NOT NULL,
		[TENTELimit] [decimal] (19,4) NOT NULL,
		[AmountToInvoice] [decimal] (19,4) NULL,
		[AmountToBill] [decimal] (19,4) NULL,
		[AmountToHold] [decimal] (19,4) NULL,
		[AmountToDelete] [decimal] (19,4) NULL,
		[AmountToTransfer] [decimal] (19,4) NULL,
		[MetadataExtension] [nvarchar] (MAX) NULL
 CONSTRAINT [PK_InvoicingPeriodData] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
))