﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace PTI.Models
{
    public static class PTIAuthADGroups
    {
        //Group Higherarchy Low to High: PMs, PMTL, ACCT, ADMINISTRATOR, OWNER
        public const string OWNER = @"NEIENG\PTI_Owner,MicrosoftAccount\sean@maslowinc.com";
        public const string ADMINISTRATOR = @"NEIENG\PTI_Administrator,NEIENG\PTI_Owner,MicrosoftAccount\sean@maslowinc.com";
        public const string ACCT = @"NEIENG\PTI_ACCT,NEIENG\PTI_Administrator,NEIENG\PTI_Owner,MicrosoftAccount\sean@maslowinc.com";
        public const string PMs = @"NEIENG\PTI_PMs,NEIENG\PTI_ACCT,NEIENG\PTI_Administrator,NEIENG\PTI_Owner,MicrosoftAccount\sean@maslowinc.com";

        //PMTL, aka PM Team Lead, is a superset of PMs. Members of PMTL may perform certain project approval actions of their team members
        public const string PMTL = @"NEIENG\PTI_PMTL,MicrosoftAccount\seanx@maslowinc.com";

        public enum GROUP
        {
            NONE = 0,
            PM = 1,
            ACCT = 2,
            ADMINISTRATOR = 3,
            OWNER = 4
        }

        public static void SetUserHighestGroupPermission()
        {
            string[] UserRoles = (string[])HttpContext.Current.Session["PTIUserRoles"];
            GROUP HighestGroup = GROUP.NONE;
            string HighestGroupName = "NONE";
            HttpContext.Current.Session["PTIUserIsPMTL"] = false;

            for (int i = 0; i < UserRoles.Length; i++)
            {
                //Standard Group Permissions
                if (HighestGroup == GROUP.NONE)
                {
                    if (PMs.Contains(UserRoles[i]))
                    {
                        HighestGroup = GROUP.PM;
                        HighestGroupName = "PM";
                    }
                }
                if (HighestGroup == GROUP.PM)
                {
                    if (ACCT.Contains(UserRoles[i]))
                    {
                        HighestGroup = GROUP.ACCT;
                        HighestGroupName = "ACCT";
                    }
                }
                if (HighestGroup == GROUP.ACCT)
                {
                    if (ADMINISTRATOR.Contains(UserRoles[i]))
                    {
                        HighestGroup = GROUP.ADMINISTRATOR;
                        HighestGroupName = "ADMINISTRATOR";
                    }
                }
                if (HighestGroup == GROUP.ADMINISTRATOR)
                {
                    if (OWNER.Contains(UserRoles[i]))
                    {
                        HighestGroup = GROUP.OWNER;
                        HighestGroupName = "OWNER";
                    }
                }

                //Special Groups
                if (PMTL.Contains(UserRoles[i]))
                {
                    HttpContext.Current.Session["PTIUserIsPMTL"] = true;
                }
            }

            HttpContext.Current.Session["PTIUserHighestGroupPermission"] = HighestGroup;
            HttpContext.Current.Session["PTIUserHighestGroupPermissionName"] = HighestGroupName;
        }

        public static GROUP UserHighestGroupPermission()
        {
            if (HttpContext.Current.Session["PTIUserHighestGroupPermission"] == null)
            {
                SetUserHighestGroupPermission();
            }
            return (GROUP)HttpContext.Current.Session["PTIUserHighestGroupPermission"];
        }
    }

    public class AuthorizeNEIAD : AuthorizeAttribute
    {
        public string Groups { get; set; }
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            if (base.AuthorizeCore(httpContext))
            {
                /* Return true immediately if the authorization is not locked down to any particular AD group */
                if (Groups.Length == 0) return true;

                bool bAuthorized = false;

                // Get the AD groups
                var groups = Groups.Split(',').ToList();

                // Verify that the user is in the given AD group (if any)
                string[] UserRoles = (string[])HttpContext.Current.Session["PTIUserRoles"];
                if (UserRoles == null)
                {
                    HttpContext.Current.Response.Redirect("~/home");
                }

                for (int i = 0; i < UserRoles.Length; i++)
                {
                    if (!bAuthorized)
                    {
                        if (groups.Contains(UserRoles[i].Trim()))
                        {
                            bAuthorized = true;
                        }
                    } 
                }

                return bAuthorized;

            }
            return false;
        }

        protected override void HandleUnauthorizedRequest(
        AuthorizationContext filterContext)
        {
            if (filterContext.HttpContext.User.Identity.IsAuthenticated)
            {
                var result = new ViewResult();
                result.ViewName = "NotAuthorized";
                result.MasterName = "_Layout";
                filterContext.Result = result;
            }
            else
                base.HandleUnauthorizedRequest(filterContext);
        }
    }
}