﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PTI.Models
{
    public class ProjectTrackingViewModel : _BaseViewModel
    {
        public bool AllProjectsMode;

        public void UpdateInvoicingPeriodEmployeeStatuses()
        {
            InvoicingPeriodEmployee.UpdateAllStatuses();
        }
    }
}