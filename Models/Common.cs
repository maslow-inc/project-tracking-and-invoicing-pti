﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace PTI.Models
{
    public class Common
    {
        public static string FormatDTS(DateTime dts)
        {
            return dts.ToString("yyyy-MM-dd HH:mm:ss");
        }

        public static string FormatDateForDisplay(DateTime dts)
        {
            return dts.ToString("MMM dd, yyyy");
        }

        public static bool IsNumeric(string value)
        {
            value = value.Replace(".", "").Replace("-", ""); //Allow decimals and negatives
            return value.All(char.IsNumber);
        }

        /// <summary>
        /// Get Employee.Id using the logged in User.Identity.Name
        /// </summary>
        /// <returns>Employee.Id</returns>
        public static Guid LoggedInEmployeeId()
        {
            if (HttpContext.Current.Session["LoggedInEmployeeId"] == null)
            {
                string NetworkNameFull = HttpContext.Current.User.Identity.Name;
                string NetworkNameNoDomain = NetworkNameFull.Substring(NetworkNameFull.IndexOf(@"\") + 1);
                string sql = "select Id from Employee where NetworkId = @NetworkId";
                string EmployeeGuid = Data.GetScalar(sql, new string[] { NetworkNameNoDomain }, "Id");
                HttpContext.Current.Session["LoggedInEmployeeId"] = EmployeeGuid;
            }
            return new Guid(HttpContext.Current.Session["LoggedInEmployeeId"].ToString());
        }

        public static string ConnectedDatabaseName()
        {
            string dbName = "Not Found";
            string Connstr = ConfigurationManager.ConnectionStrings["PTI"].ConnectionString;
            string[] ConnstrProps = Connstr.Split(';');
            for (int i = 0; i < ConnstrProps.Length; i++)
            {
                if (ConnstrProps[i].Trim().ToLower().StartsWith("initial catalog="))
                {
                    dbName = ConnstrProps[i].Trim().Substring(16);
                }
            }

            return dbName;
        }
    }
}