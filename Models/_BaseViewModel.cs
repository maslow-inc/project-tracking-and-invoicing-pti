﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;

namespace PTI.Models
{
    public class _BaseViewModel
    {
        public string id { get; set; }
        public string ThisPageName { get; set; }
        public string wwwroot = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot");
    }
}