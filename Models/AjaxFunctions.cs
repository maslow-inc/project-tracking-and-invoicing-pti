﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Web.Script.Serialization;
using System.Threading;
using System.Diagnostics;
using System.Data;

namespace PTI.Models
{
    public class AjaxFunctions
    {
        public string EventCommand { get; set; }
        public string EventArgument { get; set; }
        public string EventData { get; set; }
        public string HTMLResponse { get; set; }
		public virtual void HandleRequest()
		{
			//This is the main method to call from the controller after setting the EventCommand
			JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();
			string TableName;
			Guid RowId;
			dynamic data;
			string FieldName;
			string NewValue;
			string CurrentRowJSON;
			dynamic ThisRow;
			string UpdatedRowJSON;
			List<string> ColsToUpdate;
			switch ((String.Empty + EventCommand).ToLower())
			{
				case "getallrows":
					TableName = EventArgument;
					HTMLResponse = Data.GetAllTableRowsJSON(TableName);
					break;
				case "getmetadatabytype":
					string MetadataTypeName = EventArgument;
					HTMLResponse = Metadata.GetMetadataJSONByTypeName(MetadataTypeName);
					break;
				case "saverecord":
					TableName = EventArgument;
					HTMLResponse = Data.SaveTableRow(TableName, EventData);
					break;
				case "savevalue":
					data = jsonSerializer.Deserialize<object>(EventData);
					TableName = data["TableName"].ToString();
					RowId = new Guid(data["Id"]);
					FieldName = data["FieldName"].ToString();
					NewValue = data["NewValue"].ToString();
					//Save Change
					if (TableName == "InvoicingPeriodData")
					{
						//InvoicingPeriodData.UpdateDataValue has some status changing logic
						HTMLResponse = InvoicingPeriodData.UpdateDataValue(RowId, FieldName, NewValue);
					}
					else
					{
						CurrentRowJSON = Data.GetTableRowJSON(TableName, RowId);
						ThisRow = jsonSerializer.Deserialize<object>(CurrentRowJSON);
						ThisRow[FieldName] = NewValue;
						UpdatedRowJSON = jsonSerializer.Serialize(ThisRow);
						ColsToUpdate = new List<string>();
						ColsToUpdate.Add(FieldName); //Update individual fields since JavascriptSerializer breaks some JSON data when saving the entire row
						HTMLResponse = Data.SaveTableRow(TableName, UpdatedRowJSON, ColsToUpdate);
						//TODO: Figure out logic for updating InvoicingPeriodProject and InvoicingPeriodEmployee status here...
					}
					break;
				case "deleterecord":
					TableName = EventArgument;
					if (TableName.Trim().ToLower() == "metadata")
                    {
						HTMLResponse = Metadata.DeleteRow(new Guid(EventData));
					}
					else
                    {
						HTMLResponse = Data.DeleteTableRow(TableName, new Guid(EventData));
					}
					
					break;
				case "help":
                    switch (EventArgument.Trim().ToLower())
                    {
                        case "allpagehelp":
							HTMLResponse = Help.GetHelpForPage(EventData);
                            break;
                        default:
                            break;
                    }
                    break;
				case "invoicingperiod":
                    switch (EventArgument.Trim().ToLower())
                    {
						case "openperiod":
							data = jsonSerializer.Deserialize<object>(EventData);
							HTMLResponse = InvoicingPeriod.OpenInvoicingPeriod(data["PeriodName"].ToString());
							break;
						case "closecurrent":
							HTMLResponse = InvoicingPeriod.CloseCurrentInvoicingPeriod();
							break;
						case "setperiodforsession":
							HttpContext.Current.Session["UserSelectedInvoicingPeriod"] = EventData;
							HTMLResponse = "success";
							break;
						case "resetopen":
							HTMLResponse = InvoicingPeriod.ResetOpenInvoicingPeriod();
							break;
						case "returntocurrentinvoicingperiod":
							HttpContext.Current.Session["UserSelectedInvoicingPeriod"] = null;
							HTMLResponse = "success";
							break;
						case "getdvpupdates":
							HTMLResponse = InvoicingPeriod.GetDVPUpdatesForCurrentOpenPeriod();
							break;
						case "runfpcupload":
							string EmpId = Employee.LoggedInUserEmployeeId().ToString();
							string InvPrdId = InvoicingPeriod.CurrentOpenPeriodId();
                            if (InvPrdId != "")
							{
                                Thread trd = new Thread(() => InvoicingPeriod.RunFPCUpload(EmpId, InvPrdId));
                                trd.IsBackground = true;
                                trd.Start();
                                HTMLResponse = "started";
                            }
							else
							{
								HTMLResponse = "This function is only available when there is an open Invoicing Period.";
                            }
                            break;
                        case "getfpcuploadstatus":
                            HTMLResponse = InvoicingPeriod.GetFPCUploadStatus();
                            break;
						case "getlastfpcuploaddtsforopenperiod":
                            HTMLResponse = InvoicingPeriod.GetLastFPCUploadDTSForOpenPeriod();
                            break;
                        default:
                            break;
                    }
                    break;
				case "project":
                    switch (EventArgument.Trim().ToLower())
                    {
						case "allpmswithprojectsinperiod":
							HTMLResponse = InvoicingPeriodEmployee.GetAllPMsWithProjectsInPeriodJSON();
							break;
						case "allprojects":
							HTMLResponse = InvoicingPeriodProject.GetAllProjectsInPeriodJSON();
							break;
						case "allprojectsforemployee":
							HTMLResponse = InvoicingPeriodProject.GetEmployeeProjectsInPeriodJSON(new Guid(EventData));
							break;
						case "employeename":
							HTMLResponse = InvoicingPeriodEmployee.DisplayName(new Guid(EventData));
							break;
						case "alldata":
							HTMLResponse = InvoicingPeriodData.AllDataInProjectJSON(new Guid(EventData));
							break;
						case "updatestatus":
							data = jsonSerializer.Deserialize<object>(EventData);
							RowId = new Guid(data["Id"]);
							string StatusType = data["StatusType"].ToString();
							string Status = data["Status"].ToString();
							HTMLResponse = InvoicingPeriodProject.UpdateProjectStatus(RowId, StatusType, Status);
							break;
						case "undostatus":
							data = jsonSerializer.Deserialize<object>(EventData);
							RowId = new Guid(data["Id"]);
							string UndoAction = data["UndoAction"].ToString();
							HTMLResponse = InvoicingPeriodProject.UndoProjectStatus(RowId, UndoAction);
							break;
						case "carrynotesforward":
                            data = jsonSerializer.Deserialize<object>(EventData);
							string InvoicingPeriodProjectId = data["InvoicingPeriodProjectId"].ToString();
                            string NotesType = data["NotesType"].ToString();
                            HTMLResponse = InvoicingPeriodProject.CarryNotesForward(InvoicingPeriodProjectId, NotesType);
                            break;
						default:
                            break;
                    }
                    break;
				default:
					HTMLResponse = "Unknown command.";
					break;
			}
		}
    }
}