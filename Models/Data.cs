﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Text.RegularExpressions;
using System.Reflection;
using System.Web.Script.Serialization;
using Newtonsoft.Json;
using System.Text;

namespace PTI.Models
{
    #region Table Classes
    public class AuditLog
    {
        //System Ids
        public static Guid RecordAddedId()
        {
            if (HttpContext.Current.Session["AuditLogRecordAddedId"] == null)
            {
                HttpContext.Current.Session["AuditLogRecordAddedId"] = Metadata.GetMetadataIdByTypeAndValue("AuditLogEvent", "Record Added");
            }

            return new Guid(HttpContext.Current.Session["AuditLogRecordAddedId"].ToString());
        }

        public static Guid RecordChangedId()
        {
            if (HttpContext.Current.Session["AuditLogRecordChangedId"] == null)
            {
                HttpContext.Current.Session["AuditLogRecordChangedId"] = Metadata.GetMetadataIdByTypeAndValue("AuditLogEvent", "Record Changed");
            }

            return new Guid(HttpContext.Current.Session["AuditLogRecordChangedId"].ToString());
        }

        public static Guid RecordDeletedId()
        {
            if (HttpContext.Current.Session["AuditLogRecordDeletedId"] == null)
            {
                HttpContext.Current.Session["AuditLogRecordDeletedId"] = Metadata.GetMetadataIdByTypeAndValue("AuditLogEvent", "Record Deleted");
            }

            return new Guid(HttpContext.Current.Session["AuditLogRecordDeletedId"].ToString());
        }

        //Class properties
        private static JavaScriptSerializer _jsonSerializer = new JavaScriptSerializer();
        private Guid _id;
        private string _tableName;
        private Guid _tableRowId;
        private Guid _typeId;
        private string _previousRecord;
        private DateTime _DTS;
        private Guid _employeeId;

        public Guid Id { get { return _id; } set { _id = value; } }
        public string TableName { get { return _tableName; } set { _tableName = value; } }
        public Guid TableRowId { get { return _tableRowId; } set { _tableRowId = value; } }
        public Guid TypeId { get { return _typeId; } set { _typeId = value; } }
        public string PreviousRecord { get { return _previousRecord; } set { _previousRecord = value; } }
        public DateTime DTS { get { return _DTS; } set { _DTS = value; } }
        public Guid EmployeeId { get { return _employeeId; } set { _employeeId = value; } }

        public AuditLog()
        {
            //Default constructor initializes a new type
            Id = Guid.NewGuid();
        }

        public AuditLog(Guid Id)
        {
            //Default constructor loads record by Id
            InitWithId(Id);
        }

        private void InitWithId(Guid Id)
        {
            _id = Id;
            string RowJSON = Data.GetTableRowJSON("AuditLog", Id);
            AuditLog FoundRecord = _jsonSerializer.Deserialize<AuditLog>(RowJSON);
            if (FoundRecord != null)
            {
                _id = FoundRecord.Id;
                _tableName = FoundRecord.TableName;
                _tableRowId = FoundRecord.TableRowId;
                _typeId = FoundRecord.TypeId;
                _previousRecord = FoundRecord.PreviousRecord;
                _DTS = FoundRecord.DTS;
                _employeeId = FoundRecord.EmployeeId;
            }
        }

        /// <summary>
        /// Record changes in a table before adding/updating/deleting
        /// </summary>
        /// <param name="TableName">Name of table being changed</param>
        /// <param name="ChangedRecordJSON">JSON string of new record data</param>
        /// <param name="ChangeTypeId">Id of Audit Log change type</param>
        /// <returns></returns>
        public static string LogChange(string TableName, string ChangedRecordJSON, Guid ChangeTypeId)
        {
            string response = "success";
            string sql;
            JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();

            //Make JSON string of Current Record
            dynamic DataRow = jsonSerializer.Deserialize<object>(ChangedRecordJSON);
            Guid TableRowId = new Guid(DataRow["Id"]);
            string CurrentRecordJSON = Data.GetTableRowJSON(TableName, TableRowId);

            //If row has been changed, save an AuditLog record
            if (CurrentRecordJSON != ChangedRecordJSON || ChangeTypeId == AuditLog.RecordDeletedId())
            {
                sql = "Insert Into AuditLog(Id, TableName, TableRowId, TypeId, PreviousRecord, DTS, EmployeeId) values(@Id, @TableName, @TableRowId, @TypeId, @PreviousRecord, @DTS, @EmployeeId)";
                response = Data.RunActionQuery(sql, new string[] { Guid.NewGuid().ToString(), TableName, TableRowId.ToString(), ChangeTypeId.ToString(), CurrentRecordJSON, Common.FormatDTS(DateTime.Now), Common.LoggedInEmployeeId().ToString() });
                if (Common.IsNumeric(response)) response = "success";
            }

            return response;
        }

    }

    public class Employee
    {
        public static Guid LoggedInUserEmployeeId()
        {
            Guid ReturnId = new Guid();
            string UserNetworkName = HttpContext.Current.User.Identity.Name;
            UserNetworkName = UserNetworkName.Substring(UserNetworkName.LastIndexOf("\\") + 1);
            string sql = "select Id from Employee where NetworkId = @NetworkId";
            string EmpId = Data.GetScalar(sql, new string[] { UserNetworkName}, "Id");
            if (EmpId != null)
            {
                ReturnId = new Guid(EmpId);
            }
            return ReturnId;
        }

        public static Guid InvoicingPeriodEmployeeIdForCurrentPeriod()
        {
            Guid ReturnId = new Guid();
            if (InvoicingPeriod.CurrentSessionPeriodId() != "")
            {
                string sql = "select Id from InvoicingPeriodEmployee where InvoicingPeriodId = @InvoicingPeriodId and EmployeeId = @EmployeeId";
                string InvoicingPeriodEmployeeId = Data.GetScalar(sql, new string[] { InvoicingPeriod.CurrentOpenPeriodId(), Employee.LoggedInUserEmployeeId().ToString() }, "Id");
                if (InvoicingPeriodEmployeeId != null)
                {
                    ReturnId = new Guid(InvoicingPeriodEmployeeId);
                }
            }
            return ReturnId;
        }

        public static bool IsPMSupervisor()
        {
            bool response = false;

            Guid SupervisorEmployeeId = LoggedInUserEmployeeId();
            string sql = @"select count(*) as cnt 
                            from Employee S inner join Employee E on S.EmployeeId = E.SupervisorId
                            Where S.Id = @SupervisorId";
            int PMCount = Convert.ToInt32(Data.GetScalar(sql, new string[] { SupervisorEmployeeId.ToString() }, "cnt"));
            if (PMCount > 0) response = true;
            
            return response;
        }

        public static bool IsTeamLeadForProject(string InvoicingPeriodProjectId)
        {
            bool response = false;
            if (Convert.ToBoolean(HttpContext.Current.Session["PTIUserIsPMTL"]))
            {
                string sql = @"select eSup.Id as SupervisorEmployeeId
                            from InvoicingPeriodEmployee IPE inner join InvoicingPeriodProject IPP on IPE.Id = IPP.InvoicingPeriodEmployeeId 
                            inner join Employee e on IPE.EmployeeId = e.Id
                            left join Employee eSup on e.SupervisorId = eSup.EmployeeId
                            where IPP.Id = @InvoicingPeriodProjectId";
                string SupervisorEmployeeId = Convert.ToString(Data.GetScalar(sql, new string[] { InvoicingPeriodProjectId }, "SupervisorEmployeeId"));
                response = (SupervisorEmployeeId == Convert.ToString(LoggedInUserEmployeeId()));
            }
            return response;
        }
    }

    public class InvoicingPeriod
    {
        public static Guid InvoicingPeriodStatus(string StatusName)
        {
            return Metadata.GetMetadataIdByTypeAndValue("InvoicingPeriodStatus", StatusName);
        }

        public static string CurrentOpenPeriodDisplay()
        {
            string PeriodDisplay = "Not Set";
            string currentOpenPeriodId = CurrentOpenPeriodId();
            if (currentOpenPeriodId.Trim().Length > 0)
            {
                PeriodDisplay = FormatInvoicingPeriodForDisplay(new Guid(currentOpenPeriodId));
            }
            return PeriodDisplay;
        }

        public static string CurrentOpenPeriodName()
        {
            Guid OpenStatusId = Metadata.GetMetadataIdByTypeAndValue("InvoicingPeriodStatus", "Open");
            string sql = "select InvoicingPeriodName from InvoicingPeriod where StatusId = @StatusId";

            string OpenPeriodName = Data.GetScalar(sql, new string[] { OpenStatusId.ToString() }, "InvoicingPeriodName");
            if (OpenPeriodName == null) OpenPeriodName = "Not Set";

            return OpenPeriodName;
        }

        public static string CurrentOpenPeriodId()
        {
            Guid OpenStatusId = Metadata.GetMetadataIdByTypeAndValue("InvoicingPeriodStatus", "Open");
            string sql = "select Id from InvoicingPeriod where StatusId = @StatusId";
            string Id = Data.GetScalar(sql, new string[] { OpenStatusId.ToString() }, "Id");
            if (Id == null) Id = "";
            return Id;
        }

        public static string CurrentSessionPeriodDisplay()
        {
            string PeriodDisplay;
            if (HttpContext.Current.Session["UserSelectedInvoicingPeriod"] != null)
            {
                JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
                dynamic UserSelectedInvoicingPeriod = jsSerializer.Deserialize<object>(HttpContext.Current.Session["UserSelectedInvoicingPeriod"].ToString());
                Guid InvoicingPeriodId = new Guid(UserSelectedInvoicingPeriod["Id"]);
                PeriodDisplay = FormatInvoicingPeriodForDisplay(InvoicingPeriodId);
            }
            else
            {
                PeriodDisplay = CurrentOpenPeriodDisplay();
            }
            return PeriodDisplay;
        }

        private static string FormatInvoicingPeriodForDisplay(Guid InvoicingPeriodId)
        {
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            string InvoicingPeriodJSON = Data.GetTableRowJSON("InvoicingPeriod", InvoicingPeriodId);
            dynamic InvoicingPeriod = jsSerializer.Deserialize<object>(InvoicingPeriodJSON);
            string PeriodStartDate = Common.FormatDateForDisplay(Convert.ToDateTime(InvoicingPeriod["StartDate"]));
            string PeriodEndDate = Common.FormatDateForDisplay(Convert.ToDateTime(InvoicingPeriod["EndDate"]));
            return string.Format("{0} ({1} - {2})", InvoicingPeriod["InvoicingPeriodName"], PeriodStartDate, PeriodEndDate);
        }

        public static string CurrentSessionPeriodId()
        {
            if (HttpContext.Current.Session["UserSelectedInvoicingPeriod"] != null)
            {
                JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
                dynamic InvoicingPeriod = jsSerializer.Deserialize<object>(HttpContext.Current.Session["UserSelectedInvoicingPeriod"].ToString());
                return InvoicingPeriod["Id"];
            }
            else
            {
                return CurrentOpenPeriodId();
            }
        }

        public static string OpenInvoicingPeriod(string InvoicingPeriodNameToOpen)
        {
            string response = "success";
            string sql;
            string CurrentActivePeriod = CurrentOpenPeriodName();
            Guid StatusId;

            //Can only open if no other period is already open
            if (Common.IsNumeric(CurrentActivePeriod))
            {
                response = string.Format("Can't open another invoicing period while period '{0}' is already open.", CurrentActivePeriod);
            }
            else
            {
                sql = "select Id from InvoicingPeriod where InvoicingPeriodName = @InvoicingPeriodName";
                string InvoicingPeriodIdToOpen = Data.GetScalar(sql, new string[] { InvoicingPeriodNameToOpen }, "Id");
                if (InvoicingPeriodIdToOpen == null)
                {
                    response = "Invalid Invoicing Period";
                }
                else
                {
                    //Load InvoicingPeriod Tables if no data exists
                    sql = "select count(*) as cnt from InvoicingPeriodData where InvoicingPeriodId = @InvoicingPeriodId";
                    string RowCount = Data.GetScalar(sql, new string[] { InvoicingPeriodIdToOpen }, "cnt");
                    if (Convert.ToInt32(RowCount) == 0)
                    {
                        //Load work_invoicingperiodsetup table
                        response = Data.RunActionQuery("sp_Setup_Invoicing_Period @currentperiod", new string[] { InvoicingPeriodNameToOpen }, CommandType.StoredProcedure);
                        response = (Common.IsNumeric(response)) ? "success" : response;

                        if (response == "success")
                        {
                            //Employee table
                            if (response == "success")
                            {
                                //Add employee records that don't exist yet
                                sql = @"insert into employee(Id, NetworkId, EmployeeId, LastName, FirstName, Email, SupervisorId)
                                        select Newid() as Id, 
                                        replace(replace(lower(substring(trim(EM.FirstName), 1, 1) + trim(EM.LastName)), ' ', ''), '-', '') as NetworkId, 
                                        trim(EM.Employee) as EmployeeId, trim(EM.LastName) as LastName, trim(EM.FirstName) as FirstName, 
                                        case when EM.EMail is null then lower(substring(trim(EM.FirstName), 1, 1) + trim(EM.LastName)) + '@neieng.com' else EM.EMail end as Email, Supervisor
                                        from nei_reports.dbo.EM EM left join employee e on EM.Employee = e.EmployeeId
                                        where e.Id is null";
                                response = Data.RunActionQuery(sql, new string[] { });
                                response = (Common.IsNumeric(response)) ? "success" : response;
                                if (response == "success")
                                {
                                    //Update existing Employee records if NetworkId, LastName, FirstName or Email values have changed
                                    sql = @"update employee set LastName = tUpdate.LastName, FirstName = tUpdate.FirstName, Email = tUpdate.Email, SupervisorId = tUpdate.Supervisor
                                            from employee e inner join
                                            (
	                                            select tEM.*, /*'_____' as Separator, e.*,*/
	                                            iif (lower(trim(e.LastName)) <> lower(trim(tEM.LastName)), 'Y', 'N') as LastNameChanged,
	                                            iif (lower(trim(e.FirstName)) <> lower(trim(tEM.FirstName)), 'Y', 'N') as FirstNameChanged,
	                                            iif (lower(trim(e.Email)) <> lower(trim(tEM.Email)), 'Y', 'N') as EmailChanged,
                                                iif (lower(trim(e.SupervisorId)) <> lower(trim(tEM.Supervisor)), 'Y', 'N') as SupervisorChanged
	                                            from employee e
	                                            inner join
	                                            (
		                                            select replace(replace(lower(substring(trim(EM.FirstName), 1, 1) + trim(EM.LastName)), ' ', ''), '-', '') as NetworkId, 
		                                            trim(EM.Employee) as EmployeeId, trim(EM.LastName) as LastName, trim(EM.FirstName) as FirstName, 
		                                            case when EM.EMail is null then lower(substring(trim(EM.FirstName), 1, 1) + trim(EM.LastName)) + '@neieng.com' else EM.EMail end as Email,
                                                    Supervisor
		                                            from nei_reports.dbo.EM EM
	                                            ) tEM on e.EmployeeId = tEM.EmployeeId
	                                            where lower(trim(e.LastName)) <> lower(trim(tEM.LastName)) or lower(trim(e.FirstName)) <> lower(trim(tEM.FirstName)) or lower(trim(e.Email)) <> lower(trim(tEM.Email)) or lower(trim(e.SupervisorId)) <> lower(trim(tEM.Supervisor))
                                            ) tUpdate on e.EmployeeId = tUpdate.EmployeeId
                                            where len(trim(tUpdate.EmployeeId)) > 0";
                                    response = Data.RunActionQuery(sql, new string[] { });
                                    response = (Common.IsNumeric(response)) ? "success" : response;
                                }
                            }

                            //InvoicingPeriodEmployee table
                            if (response == "success")
                            {
                                StatusId = Metadata.GetMetadataIdByTypeAndValue("InvoicingPeriodEmployeeStatus", "Not Started");
                                sql = @"insert into InvoicingPeriodEmployee(Id, InvoicingPeriodId, EmployeeId, StatusId, ApprovalStatusId, InvoicingStatusId)
                                select newid() as Id, '" + InvoicingPeriodIdToOpen + @"' as InvoicingPeriodId, tPeriodEmps.Id as EmployeeId, '" + StatusId.ToString() + @"' as StatusId, null as ApprovalStatusId, null as InvoicingStatusId
                                from (
	                                select distinct em.Id, tRaw.* from
	                                (
	                                    select distinct SUPV1_EmpId as EmployeeId, substring(SUPV1_Email, 1, charindex('@', SUPV1_Email)-1) as NetworkId,
	                                    substring(SUPV1_NAME, 1, charindex(',', SUPV1_NAME)-1) as LastName, right(SUPV1_NAME, len(SUPV1_NAME) - charindex(',', SUPV1_NAME)) as FirstName,
	                                    SUPV1_Email as Email
	                                    from work_invoicingperiodsetup
	                                    union all
	                                    select distinct SUPV2_EmpId as EmployeeId, substring(SUPV2_Email, 1, charindex('@', SUPV2_Email)-1) as NetworkId,
	                                    substring(SUPV2_NAME, 1, charindex(',', SUPV2_NAME)-1) as LastName, right(SUPV2_NAME, len(SUPV2_NAME) - charindex(',', SUPV2_NAME)) as FirstName,
	                                    SUPV2_Email as Email
	                                    from work_invoicingperiodsetup where len(trim(SUPV2_NAME)) > 0
	                                    union all
	                                    select distinct SUPV3_EmpId as EmployeeId, substring(SUPV3_Email, 1, charindex('@', SUPV3_Email)-1) as NetworkId,
	                                    substring(SUPV3_NAME, 1, charindex(',', SUPV3_NAME)-1) as LastName, right(SUPV3_NAME, len(SUPV3_NAME) - charindex(',', SUPV3_NAME)) as FirstName,
	                                    SUPV3_Email as Email
	                                    from work_invoicingperiodsetup where len(trim(SUPV3_NAME)) > 0
	                                ) tRaw
	                                inner join Employee em on tRaw.EmployeeId = em.EmployeeId
	                                where not tRaw.LastName is null and not tRaw.FirstName is null
                                ) tPeriodEmps";
                                response = Data.RunActionQuery(sql, new string[] { });
                                response = (Common.IsNumeric(response)) ? "success" : response;
                            }

                            //InvoicingPeriodProject table
                            if (response == "success")
                            {
                                StatusId = Metadata.GetMetadataIdByTypeAndValue("InvoicingPeriodProjectStatus", "Not Started");
                                sql = @"insert into InvoicingPeriodProject(Id, InvoicingPeriodId, InvoicingPeriodEmployeeId, WBS1, Name, ClientName, StatusId, ApprovalStatusId, InvoicingStatusId, OnHold)
                                select newid() as Id, tInserts.* from (
                                select distinct IPE.InvoicingPeriodId as InvoicingPeriodId, IPE.Id as InvoicingPeriodEmployeeId, 
                                wrkIPS.WBS1 as WBS1, wrkIPS.WBS1_NAME as Name, wrkIPS.CLIENT_NAME as ClientName,
                                '" + StatusId.ToString() + @"' as StatusId, null as ApprovalStatusId, null as InvoicingStatusId, wrkIPS.Project_On_Hold
                                from
                                InvoicingPeriodEmployee IPE inner join Employee e on IPE.EmployeeId = e.Id
                                inner join work_invoicingperiodsetup wrkIPS on e.EmployeeId = wrkIPS.SUPV1_EmpId
                                where IPE.InvoicingPeriodId = @InvoicingPeriodId
                                ) tInserts";
                                response = Data.RunActionQuery(sql, new string[] { InvoicingPeriodIdToOpen });
                                response = (Common.IsNumeric(response)) ? "success" : response;
                            }

                            //InvoicingPeriodPhase table
                            if (response == "success")
                            {
                                StatusId = Metadata.GetMetadataIdByTypeAndValue("InvoicingPeriodPhaseStatus", "Not Started");
                                sql = @"insert into InvoicingPeriodPhase(Id, InvoicingPeriodId, InvoicingPeriodEmployeeId, InvoicingPeriodProjectId, WBS2, Name, StatusId)
                                select newid() as Id, tInserts.* from (
                                select distinct IPPrj.InvoicingPeriodId as InvoicingPeriodId, IPE.Id as InvoicingPeriodEmployeeId, IPPrj.Id as InvoicingPeriodProjectid,
                                wrkIPS.WBS2 as WBS2, wrkIPS.WBS2_NAME as Name, '" + StatusId.ToString() + @"' as StatusId
                                from
                                InvoicingPeriodEmployee IPE inner join Employee e on IPE.EmployeeId = e.Id
                                inner join work_invoicingperiodsetup wrkIPS on e.EmployeeId = isnull(wrkIPS.SUPV2_EmpId, wrkIPS.SUPV1_EmpId)
                                inner join InvoicingPeriodProject IPPrj on wrkIPS.wbs1 = IPPrj.WBS1 and IPPrj.InvoicingPeriodId = IPE.InvoicingPeriodId
                                where IPPrj.InvoicingPeriodId = @InvoicingPeriodId) tInserts";
                                response = Data.RunActionQuery(sql, new string[] { InvoicingPeriodIdToOpen });
                                response = (Common.IsNumeric(response)) ? "success" : response;
                            }

                            //InvoicingPeriodSubphase table
                            if (response == "success")
                            {
                                StatusId = Metadata.GetMetadataIdByTypeAndValue("InvoicingPeriodSubphaseStatus", "Not Started");
                                sql = @"insert into InvoicingPeriodSubphase(Id, InvoicingPeriodId, InvoicingPeriodEmployeeId, InvoicingPeriodPhaseId, WBS3, Name, StatusId)
                                select newid() as Id, tInserts.* from (
                                select distinct IPPrj.InvoicingPeriodId as InvoicingPeriodId, IPE.Id as InvoicingPeriodEmployeeId, IPPhs.id as InvoicingPeriodPhaseId,
                                wrkIPS.WBS3 as WBS3, wrkIPS.WBS3_NAME as Name, '" + StatusId.ToString() + @"' as StatusId
                                from
                                InvoicingPeriodEmployee IPE inner join Employee e on IPE.EmployeeId = e.Id
                                inner join work_invoicingperiodsetup wrkIPS on e.EmployeeId = isnull(wrkIPS.SUPV3_EmpId, isnull(wrkIPS.SUPV2_EmpId, wrkIPS.SUPV1_EmpId))
                                inner join InvoicingPeriodProject IPPrj on wrkIPS.wbs1 = IPPrj.WBS1 and IPPrj.InvoicingPeriodId = IPE.InvoicingPeriodId
                                inner join InvoicingPeriodPhase IPPhs on IPPhs.InvoicingPeriodProjectId = IPPrj.Id and IPPhs.WBS2 = wrkIPS.WBS2
                                where IPPrj.InvoicingPeriodId = @InvoicingPeriodId) tInserts";
                                response = Data.RunActionQuery(sql, new string[] { InvoicingPeriodIdToOpen });
                                response = (Common.IsNumeric(response)) ? "success" : response;
                            }

                            //InvoicingPeriodData table
                            if (response == "success")
                            {
                                sql = @"insert into InvoicingPeriodData(Id, InvoicingPeriodId, InvoicingPeriodEmployeeId, InvoicingPeriodProjectId, InvoicingPeriodPhaseId, InvoicingPeriodSubphaseId,
                                PhaseStatus, Compensation, DirectLaborBudget, ExpenseBudget, ConsultantBudget, 
                                SpentTotal, SpentLabor, SpentReimbExpense, HoursToComplete, EstimateExpenses, Risk, InvoicedTotal, AR_Amount, 
                                BillingTerms, StartDate, EstEndDate, EstEndDate_Updated, RevenueMethod, RevenueMethod_Updated, PhaseMgrEmployeeId_Updated, WIP_Total, WIP_Current, 
                                WIP_Held, WIP_To_Close, ExpenseUnbilled, ETC, PhaseEAC, AvgBillRate, TENTELimit,
                                AmountToInvoice, AmountToBill, AmountToHold, AmountToDelete, AmountToTransfer)
                                select newid() as Id, tInserts.* from (
                                select distinct IPPrj.InvoicingPeriodId as InvoicingPeriodId, IPE.Id as InvoicingPeriodEmployeeId, IPPrj.Id as InvoicingPeriodProjectId, IPPhs.id as InvoicingPeriodPhaseId, IPPSub.Id as InvoicingPeriodSubphaseId,
                                wrkIPS.PHASE_STATUS as PhaseStatus, wrkIPS.TOTAL_COMPENSATION as Compensation, wrkIPS.DIRECT_LABOR as DirectLaborBudget, wrkIPS.REIMBURSABLE_EXPENSE as ExpenseBudget, wrkIPS.REIMBURSABLE_CONSULTANT as ConsultantBudget,
                                wrkIPS.JTD_BILLING_TOTAL_SPENT as SpentTotal, wrkIPS.JTD_LABOR_BILLING_SPENT as SpentLabor, wrkIPS.JTD_REIMB_BILLING_SPENT as SpentReimbExpense, null as HoursToComplete, null as EstimateExpenses, null as Risk, wrkIPS.JTD_BILLED as InvoicedTotal, wrkIPS.JTD_AR_AMT as AR_Amount,
                                wrkIPS.BILLING_TERMS as BillingTerms, wrkIPS.START_DATE as StartDate, wrkIPS.END_DATE as EstEndDate, null as EstEndDate_Updated, wrkIPS.REVENUE_METHOD as RevenueMethod, null as RevenueMethod_Updated, null as PhaseMgrEmployeeId_Updated, wrkIPS.WIP_BY_PHASE_SUB_PHASE as WIP_Total, wrkIPS.WIP_BILLABLE as WIP_Current,
                                wrkIPS.WIP_HELD as WIP_Held, null as WIP_To_Close, wrkIPS.EXP_UNBILLED as ExpenseUnbilled, wrkIPS.ETC as ETC, wrkIPS.PHASE_EAC as PhaseEAC, wrkIPS.AVG_BILL_RATE as AvgBillRate, wrkIPS.Project_TENTE_Limit as TNTELimit,
                                null as AmountToInvoice, null as AountToBill, null as AmountToHold, null as AmountToDelete, null as AmountToTransfer
                                from
                                InvoicingPeriodEmployee IPE inner join Employee e on IPE.EmployeeId = e.Id
                                inner join work_invoicingperiodsetup wrkIPS on e.EmployeeId = isnull(wrkIPS.SUPV2_EmpId, wrkIPS.SUPV1_EmpId)
                                inner join InvoicingPeriodProject IPPrj on wrkIPS.wbs1 = IPPrj.WBS1 and IPPrj.InvoicingPeriodId = IPE.InvoicingPeriodId
                                inner join InvoicingPeriodPhase IPPhs on IPPhs.InvoicingPeriodProjectId = IPPrj.Id and IPPhs.WBS2 = wrkIPS.WBS2
                                inner join InvoicingPeriodSubphase IPPSub on IPPSub.InvoicingPeriodPhaseId = IPPHs.Id and IPPSub.WBS3 = wrkIPS.WBS3
                                where IPPrj.InvoicingPeriodId = @InvoicingPeriodId) tInserts";
                                response = Data.RunActionQuery(sql, new string[] { InvoicingPeriodIdToOpen });
                                response = (Common.IsNumeric(response)) ? "success" : response;
                            }
                        }
                    }

                    //Set InvoicingPeriod row to Open Status
                    if (response == "success")
                    {
                        string OriginalRowJSON = Data.GetTableRowJSON("InvoicingPeriod", new Guid(InvoicingPeriodIdToOpen));
                        Guid OpenStatusId = InvoicingPeriodStatus("Open");
                        sql = "update InvoicingPeriod set StatusId = @StatusId where Id = @Id";
                        response = Data.RunActionQuery(sql, new string[] { OpenStatusId.ToString(), InvoicingPeriodIdToOpen });
                        if (Common.IsNumeric(response))
                        {
                            if (Convert.ToInt32(response) == 1)
                            {
                                AuditLog.LogChange("InvoicingPeriod", OriginalRowJSON, AuditLog.RecordChangedId());
                                response = "success";
                            }
                        }
                    }
                }
            }
            return response;
        }

        public static string CloseCurrentInvoicingPeriod()
        {
            string response;
            //TODO: Check that all current period table statuses are set properly

            //Close period if Ok
            Guid ClosedStatusId = Metadata.GetMetadataIdByTypeAndValue("InvoicingPeriodStatus", "Closed");
            string ActivePeriodId = CurrentOpenPeriodId();
            if (ActivePeriodId.Trim().Length > 0)
            {
                string OriginalRowJSON = Data.GetTableRowJSON("InvoicingPeriod", new Guid(ActivePeriodId));
                string sql = "update InvoicingPeriod set StatusId = @ClosedStatusId where Id = @Id";
                response = Data.RunActionQuery(sql, new string[] { ClosedStatusId.ToString(), ActivePeriodId.ToString() });
                if (Common.IsNumeric(response))
                {
                    if (Convert.ToInt32(response) == 1)
                    {
                        AuditLog.LogChange("InvoicingPeriod", OriginalRowJSON, AuditLog.RecordChangedId());
                        response = "success";
                    }
                }
            }
            else
            {
                response = "No active period to close.";
            }
            return response;
        }

        public static string ResetOpenInvoicingPeriod()
        {
            string response = "success";
            string OpenPeriodId = CurrentOpenPeriodId();
            if (OpenPeriodId.Trim().Length > 0)
            {
                string OriginalInvoicingPeriodRowJSON = Data.GetTableRowJSON("InvoicingPeriod", new Guid(OpenPeriodId));
                string sql = "delete InvoicingPeriodData where InvoicingPeriodId = @InvoicingPeriodId";
                response = Data.RunActionQuery(sql, new string[] { OpenPeriodId });
                if (Common.IsNumeric(response))
                {
                    sql = "delete InvoicingPeriodSubphase where InvoicingPeriodId = @InvoicingPeriodId";
                    response = Data.RunActionQuery(sql, new string[] { OpenPeriodId });
                }
                if (Common.IsNumeric(response))
                {
                    sql = "delete InvoicingPeriodPhase where InvoicingPeriodId = @InvoicingPeriodId";
                    response = Data.RunActionQuery(sql, new string[] { OpenPeriodId });
                }
                if (Common.IsNumeric(response))
                {
                    sql = "delete InvoicingPeriodProject where InvoicingPeriodId = @InvoicingPeriodId";
                    response = Data.RunActionQuery(sql, new string[] { OpenPeriodId });
                }
                if (Common.IsNumeric(response))
                {
                    sql = "delete InvoicingPeriodEmployee where InvoicingPeriodId = @InvoicingPeriodId";
                    response = Data.RunActionQuery(sql, new string[] { OpenPeriodId });
                }
                if (Common.IsNumeric(response))
                {
                    sql = "update InvoicingPeriod set StatusId = null where Id = @InvoicingPeriodId";
                    response = Data.RunActionQuery(sql, new string[] { OpenPeriodId });
                }
                if (Common.IsNumeric(response))
                {
                    response = AuditLog.LogChange("InvoicingPeriod", OriginalInvoicingPeriodRowJSON, AuditLog.RecordChangedId());
                }
                response = (Common.IsNumeric(response)) ? "success" : response;
            }
            else
            {
                response = "There is no open period to reset.";
            }
            return response;
        }

        public static string GetDVPUpdatesForCurrentOpenPeriod()
        {
            string DVPUpdates;
            string _currentOpenPeriodID = CurrentOpenPeriodId();
            if (_currentOpenPeriodID != "")
            {
                //Load work_invoicingperiodsetup table
                string ActionResponse = Data.RunActionQuery("sp_Setup_Invoicing_Period @currentperiod", new string[] { CurrentOpenPeriodName() }, CommandType.StoredProcedure);
                ActionResponse = (Common.IsNumeric(ActionResponse)) ? "success" : ActionResponse;

                if (ActionResponse == "success") {
                    string sql = @"select ptidata.id as InvoicingPeriodDataId, ptidata.ClientName, ptidata.ProjectName, ptidata.PhaseName, ptidata.SubphaseName
                            , ptidata.SpentTotal as PTI_SPENT_TOTAL, wrk.JTD_BILLING_TOTAL_SPENT as DVP_SPENT_TOTAL, (wrk.JTD_BILLING_TOTAL_SPENT - ptidata.SpentTotal) as SPENT_TOTAL_DIFF
							, ptidata.ExpenseUnbilled as PTI_EXP_UNBILLED, wrk.EXP_UNBILLED as DVP_EXP_UNBILLED, (wrk.EXP_UNBILLED - ptidata.ExpenseUnbilled) as EXP_UNBILLED_DIFF
                            , ptidata.WIP_Total as PTI_WIP_TOTAL, wrk.WIP_BY_PHASE_SUB_PHASE as DVP_WIP_TOTAL, (wrk.WIP_BY_PHASE_SUB_PHASE - ptidata.WIP_Total) as WIP_TOTAL_DIFF
                            , ptidata.WIP_Current as PTI_WIP_CURRENT, wrk.WIP_BILLABLE as DVP_WIP_CURRENT, (wrk.WIP_BILLABLE - ptidata.WIP_Current) as WIP_CURRENT_DIFF
                            , ptidata.WIP_Held as PTI_WIP_HELD, wrk.WIP_HELD as DVP_WIP_HELD, (wrk.WIP_HELD - ptidata.WIP_Held) as WIP_HELD_DIFF
                            from work_invoicingperiodsetup wrk
                            inner join (
                            select concat(isnull(ipprj.WBS1, ''), ':', isnull(ipp.WBS2, ''), ':', isnull(ips.WBS3, '')) as proj_key, ipprj.ClientName, concat(ipprj.WBS1, ' - ', ipprj.Name) as ProjectName, concat(ipp.WBS2, ' ' , ipp.Name) as 'PhaseName', ips.Name as 'SubphaseName'
                            , ipd.*
                            from invoicingperioddata ipd
                            inner join InvoicingPeriodSubphase ips on ipd.InvoicingPeriodSubphaseId = ips.id
                            inner join invoicingperiodphase ipp on ipd.invoicingperiodphaseid = ipp.Id
                            inner join InvoicingPeriodProject ipprj on ipd.invoicingperiodprojectid = ipprj.id
                            where ipd.invoicingperiodid = @InvoicingPeriodId
                            ) PTIData
                            on wrk.PROJ_KEY = PTIData.proj_key
                            where PTIData.SpentTotal <> wrk.JTD_BILLING_TOTAL_SPENT or PTIData.ExpenseUnbilled <> wrk.EXP_UNBILLED or ptidata.WIP_Total <> wrk.WIP_BY_PHASE_SUB_PHASE 
                            or ptidata.WIP_Current <> wrk.WIP_BILLABLE or ptidata.WIP_HELD <> wrk.WIP_HELD
                            order by ptidata.ClientName, ptidata.ProjectName, ptidata.PhaseName, ptidata.SubphaseName";
                    DVPUpdates = Data.RunSelectQueryJSON(sql, new string[] { _currentOpenPeriodID });
                }
                else
                {
                    DVPUpdates = ActionResponse;
                }
            }
            else
            {
                DVPUpdates = "This function is only available when there is an open Invoicing Period.";
            }

            return DVPUpdates;
        }

        /// <summary>
        /// Run FPC Upload for Current Open Invoicing Period
        /// </summary>
        public static void RunFPCUpload(string EmployeeId, string InvoicingPeriodId)
        {
            Data.RunActionQuery("sp_FPC_Upload @EmployeeId, @InvoicingPeriodId", new string[] { EmployeeId, InvoicingPeriodId }, CommandType.StoredProcedure);
        }

        public static string GetFPCUploadStatus()
        {
            string FPCUploadStatus = "";
            string _currentOpenPeriodID = CurrentOpenPeriodId();
            if (_currentOpenPeriodID != "")
            {
                string sql = @"select * from AuditLog al
                            where TableName = 'sp_FPC_Upload' 
                            and TableRowId = @InvoicingPeriodId
                            and DTS >= (select max(DTS) from AuditLog where TableName = al.TableName and TableRowId = al.TableRowId and PreviousRecord like '%FPC Upload Process Start%')
                            and not exists(select * from AuditLog where TableName = al.TableName and TableRowId = al.TableRowId and PreviousRecord like '%FPC Upload Process Complete%'
                            and DTS > (select max(DTS) from AuditLog where TableName = al.TableName and TableRowId = al.TableRowId and PreviousRecord like '%FPC Upload Process Start%')
                            )
                            order by DTS";
                FPCUploadStatus = Data.RunSelectQueryJSON(sql, new string[] { _currentOpenPeriodID });
            }

            return FPCUploadStatus;
        }

        public static string GetLastFPCUploadDTSForOpenPeriod()
        {
            string FPCUploadDTS = "";
            string _currentOpenPeriodID = CurrentOpenPeriodId();
            if (_currentOpenPeriodID != "")
            {
                string sql = @"select top 1 CONCAT(e.FirstName, ' ', e.LastName) as EmployeeName, al.DTS from AuditLog al inner join Employee e on al.EmployeeId = e.Id
                            where TableName = 'sp_FPC_Upload' 
                            and TableRowId = @InvoicingPeriodId
                            and PreviousRecord like '%FPC Upload Process Complete%'
                            order by DTS desc";
                FPCUploadDTS = Data.RunSelectQueryJSON(sql, new string[] { _currentOpenPeriodID });
            }

            return FPCUploadDTS;
        }
    }

    public class InvoicingPeriodEmployee
    {
        public static string GetEmployeeIdForCurrentPeriodByAnotherPeriod(string InvoicingPeriodEmployeeId)
        {
            string sql = @"select Id 
                            from InvoicingPeriodEmployee 
                            where EmployeeId = (
	                            select e.Id
	                            from InvoicingPeriodEmployee IPE inner join Employee e on IPE.EmployeeId = e.Id
	                            where IPE.id = @InvoicingPeriodEmployeeId
                            )
                            and InvoicingPeriodId = @InvoicingPeriodId";
            string CurrentPeriodId = InvoicingPeriod.CurrentSessionPeriodId();
            if (CurrentPeriodId.Length == 0)
            {
                CurrentPeriodId = Guid.NewGuid().ToString();
            }
            return Data.GetScalar(sql, new string[] { InvoicingPeriodEmployeeId, CurrentPeriodId }, "Id");
        }

        public static string GetAllPMsWithProjectsInPeriodJSON()
        {
            string sql = @"select distinct CONCAT(trim(e.LastName), ', ', trim(e.FirstName)) as EmployeeName, e.Id as EmployeeId, IPE.Id as InvoicingPeriodEmployeeId, 
                        IPE.StatusId, IPE.ApprovalStatusId, IPE.InvoicingStatusId
						, (select count(*) from InvoicingPeriodProject where InvoicingPeriodId = IPE.InvoicingPeriodId and InvoicingPeriodEmployeeId = IPE.Id) as ProjectCount
						, (select count(*) from InvoicingPeriodProject IPPA inner join Metadata m on IPPA.ApprovalStatusId = m.Id where IPPA.InvoicingPeriodId = IPE.InvoicingPeriodId and IPPA.InvoicingPeriodEmployeeId = IPE.Id and m.Value = 'Approved') as ApprovedProjectCount
                        from InvoicingPeriodEmployee IPE
                        inner join InvoicingPeriodProject IPP on IPE.Id = IPP.InvoicingPeriodEmployeeId
                        inner join Employee e on IPE.EmployeeId = e.Id
                        where IPE.InvoicingPeriodId = @InvoicingPeriodId
                        order by 1";
            if (InvoicingPeriod.CurrentSessionPeriodId() != "")
            {
                return Data.RunSelectQueryJSON(sql, new string[] { InvoicingPeriod.CurrentSessionPeriodId() });
            }
            else
            {
                return "";
            }
        }

        public static string DisplayName(Guid InvoicingPeriodEmployeeId)
        {
            string sql = @"select CONCAT(trim(e.LastName), ', ', trim(e.FirstName)) as Name 
                    from InvoicingPeriodEmployee IPE inner join Employee e on IPE.EmployeeId = e.Id
                    where IPE.Id = @InvoicingPeriodEmployeeId";
            return Data.GetScalar(sql, new string[] { InvoicingPeriodEmployeeId.ToString() }, "Name");
        }

        public static Guid LoggedInUserInvoicingPeriodEmployeeId()
        {
            Guid ReturnId = new Guid();
            Guid EmpId = Employee.LoggedInUserEmployeeId();
            string InvoicingPeriodId = InvoicingPeriod.CurrentSessionPeriodId();
            if (InvoicingPeriodId != "")
            {
                string sql = "select Id from InvoicingPeriodEmployee where InvoicingPeriodId = @InvoicingPeriodId and EmployeeId = @EmployeeId";
                string InvoicingPeriodEmployeeId = Data.GetScalar(sql, new string[] { InvoicingPeriodId, EmpId.ToString() }, "Id");
                if (InvoicingPeriodEmployeeId != null)
                {
                    ReturnId = new Guid(InvoicingPeriodEmployeeId);
                }
            }
            return ReturnId;
        }

        public static bool CanViewProjects(Guid InvoicingPeriodEmployeeId)
        {
            bool CanView = false;

            //All users can view all projects in past invoicing periods
            if (InvoicingPeriod.CurrentSessionPeriodId() != InvoicingPeriod.CurrentOpenPeriodId())
            {
                CanView = true;
            }
            else if (Convert.ToInt32(HttpContext.Current.Session["PTIUserHighestGroupPermission"]) > 1)
            {
                //ACCT and above can view all projects
                CanView = true;
            }
            else if (InvoicingPeriodEmployeeId.ToString().ToLower() == LoggedInUserInvoicingPeriodEmployeeId().ToString().ToLower())
            {
                //PM can view their own projects
                CanView = true;
            }
            else if (Employee.IsPMSupervisor())
            {
                //Supervisors can view their reporting PMs' projects
                string sql = @"select e.SupervisorId from Employee e inner join InvoicingPeriodEmployee ipe on e.id = ipe.EmployeeId where ipe.id = @InvoicingPeriodEmployeeId";
                string SupervisorId = Data.GetScalar(sql, new string[] { InvoicingPeriodEmployeeId.ToString() }, "SupervisorId");
                sql = @"select count(*) as cnt
                        from Employee
                        where Id = @EmployeeId and EmployeeId = @SupervisorId";
                int count = Convert.ToInt32(Data.GetScalar(sql, new string[] { Employee.LoggedInUserEmployeeId().ToString(), SupervisorId }, "cnt"));
                if (count > 0) CanView = true;
            }
            return CanView;
        }

        public static bool CanAccessProjectData(Guid InvoicingPeriodProjectId)
        {
            bool CanAccess = false;
            string sql;

            //All users can view all projects in past invoicing periods
            if (InvoicingPeriod.CurrentSessionPeriodId() != InvoicingPeriod.CurrentOpenPeriodId())
            {
                CanAccess = true;
            }
            else if (Convert.ToInt32(HttpContext.Current.Session["PTIUserHighestGroupPermission"]) > 1)
            {
                //ACCT and above can access all project data
                CanAccess = true;
            }
            else
            {
                //PMs can only access their project data. Supervisors can access their project data and their PMs' project data
                if (Employee.IsPMSupervisor())
                {
                    sql = "select InvoicingPeriodEmployeeId from InvoicingPeriodProject where Id = @InvoicingPeriodProjectId";
                    string InvoicingPeriodEmployeeId = Data.GetScalar(sql, new string[] { InvoicingPeriodProjectId.ToString() }, "InvoicingPeriodEmployeeId");
                    CanAccess = CanViewProjects(new Guid(InvoicingPeriodEmployeeId));
                }
                else
                {
                    Guid LoggedInUserId = LoggedInUserInvoicingPeriodEmployeeId();
                    sql = "select count(*) as cnt from InvoicingPeriodProject where Id = @InvoicingPeriodProjectId and InvoicingPeriodEmployeeId = @InvoicingPeriodEmployeeId";
                    string RecordCount = Data.GetScalar(sql, new string[] { InvoicingPeriodProjectId.ToString(), LoggedInUserId.ToString() }, "cnt");
                    if (Convert.ToInt32(RecordCount) > 0)
                    {
                        CanAccess = true;
                    }
                }
            }
            return CanAccess;
        }

        public static void UpdateAllStatuses()
        {
            Data.RunActionQuery("sp_Update_All_InvoicingPeriodEmployee_Statuses_In_Open_Period", new string[] {}, CommandType.StoredProcedure);
        }
    }

    public class InvoicingPeriodProject
    {
        public static string GetWBS1(string InvoicingPeriodProjectId)
        {
            return Data.GetScalar("select WBS1 from InvoicingPeriodProject where Id = @InvoicingPeriodProjectId", new string[]{ InvoicingPeriodProjectId}, "WBS1");
        }

        public static string GetInvoicingProjectIdForSelectedPeriodByCurrentPeriodId(string InvoicingPeriodProjectId)
        {
            string WBS1 = GetWBS1(InvoicingPeriodProjectId);
            string CurrentPeriod = InvoicingPeriod.CurrentSessionPeriodId();
            string sql = "select id from InvoicingPeriodProject where InvoicingPeriodID = @InvoicingPeriodID and WBS1 = @WBS1";
            string ReturnId = Data.GetScalar(sql, new string[] { CurrentPeriod, WBS1 }, "id");
            return ReturnId;
        }

        public static string GetAllProjectsInPeriodJSON()
        {
            string responseJSON = "{}";
            string sql;
            string[] QueryParams;
            if (Convert.ToInt32(PTIAuthADGroups.UserHighestGroupPermission()) >= 2 || InvoicingPeriod.CurrentSessionPeriodId() != InvoicingPeriod.CurrentOpenPeriodId())
            {
                //Accounting and Higher can see all PMs' projects. Additionally, all users can see all projects if viewing a prior period
                sql = @"select IPP.*, e.LastName, e.FirstName
                            ,(select case when notes like('%""InvoiceSentToPM"":""true""%') then 'true' else 'false' end from InvoicingPeriodProject where Id = IPP.Id) as InvoiceSentToPM
                            ,(select case when notes like('%""DVPDataEntryComplete"":""true""%') then 'true' else 'false' end from InvoicingPeriodProject where Id = IPP.Id) as DVPDataEntryComplete
                            from InvoicingPeriodProject IPP inner join InvoicingPeriodEmployee IPE on IPP.InvoicingPeriodEmployeeId = IPE.Id
                            inner join Employee e on IPE.EmployeeId = e.Id
                            where IPP.InvoicingPeriodId = @InvoicingPeriodId 
                            order by IPP.WBS1, IPP.Name, e.LastName, e.FirstName";
                QueryParams = new string[] { InvoicingPeriod.CurrentSessionPeriodId() };
            }
            else
            {
                //Supervisors can see their projects and PMs who report to them
                sql = @"select IPP.*, e.LastName, e.FirstName, e.id
                            ,(select case when notes like('%""InvoiceSentToPM"":""true""%') then 'true' else 'false' end from InvoicingPeriodProject where Id = IPP.Id) as InvoiceSentToPM
                            ,(select case when notes like('%""DVPDataEntryComplete"":""true""%') then 'true' else 'false' end from InvoicingPeriodProject where Id = IPP.Id) as DVPDataEntryComplete
                            from InvoicingPeriodProject IPP inner join InvoicingPeriodEmployee IPE on IPP.InvoicingPeriodEmployeeId = IPE.Id
                            inner join Employee e on IPE.EmployeeId = e.Id
                            where IPP.InvoicingPeriodId = @InvoicingPeriodId
							and e.id in (
								select id from employee where id = @SupervisorEmployeeId1
								union all
								select E.id
								from Employee S inner join Employee E on S.EmployeeId = E.SupervisorId
								Where S.Id = @SupervisorEmployeeId2
							)
                            order by IPP.WBS1, IPP.Name, e.LastName, e.FirstName";
                QueryParams = new string[] { InvoicingPeriod.CurrentSessionPeriodId(), Employee.LoggedInUserEmployeeId().ToString(), Employee.LoggedInUserEmployeeId().ToString() };
            }
            string CurrentSessionPeriodId = InvoicingPeriod.CurrentSessionPeriodId();
            if (CurrentSessionPeriodId != "")
            {
                responseJSON = Data.RunSelectQueryJSON(sql, QueryParams);
            }
            
            return responseJSON;
        }

        public static string GetEmployeeProjectsInPeriodJSON(Guid EmployeeId)
        {
            string sql = @"select IPP.*
                        ,(select case when notes like('%""InvoiceSentToPM"":""true""%') then 'true' else 'false' end from InvoicingPeriodProject where Id = IPP.Id) as InvoiceSentToPM
                        ,(select case when notes like('%""DVPDataEntryComplete"":""true""%') then 'true' else 'false' end from InvoicingPeriodProject where Id = IPP.Id) as DVPDataEntryComplete
                        from InvoicingPeriodProject IPP
                        where IPP.InvoicingPeriodId = @InvoicingPeriodId and InvoicingPeriodEmployeeId = @EmployeeId
                        order by WBS1, Name";
            return Data.RunSelectQueryJSON(sql, new string[] { InvoicingPeriod.CurrentSessionPeriodId(), EmployeeId.ToString() });
        }

        public static Guid GetInvoicingPeriodEmployeeIdForProject(Guid RowId)
        {
            string sql = "select InvoicingPeriodEmployeeId from InvoicingPeriodProject where Id = @Id";
            string RawGuid = Data.GetScalar(sql, new string[] { RowId.ToString() }, "InvoicingPeriodEmployeeId");
            return (RawGuid == null) ? Guid.NewGuid() : new Guid(RawGuid);
        }

        public static string UpdateProjectStatus(Guid RowId, string StatusType, string Status)
        {
            string response;

            response = Data.UpdateTableStatus("InvoicingPeriodProject", RowId, StatusType, Status);
            //Update other table statuses depending on what status is being set (i.e., set Employee status to Submitted once all projects are submitted, etc.)
            if (response == "success")
            {
                if (StatusType == "Status" && Status == "Submitted")
                {
                    //Update Project ApprovalStatus to "Not Reviewed" after setting project to "Submitted"
                    response = Data.UpdateTableStatus("InvoicingPeriodProject", RowId, "ApprovalStatus", "Not Reviewed");
                }
                if (StatusType == "ApprovalStatus" && Status == "Approved")
                {
                    //Update Project InvoicingStatus to "Not Started" after setting project ApprovalStatus to "Approved"
                    response = Data.UpdateTableStatus("InvoicingPeriodProject", RowId, "InvoicingStatus", "Not Started");
                }
            }
            return response;
        }

        /// <summary>
        /// Undo current status based on current status of project
        /// Unsubmit: PM user can set project Status to "Started" Approval status <> "Approved"
        /// Return to PM: Admin user can set project Status to "Started" if Approval status <> "Approved" 
        /// Unapprove: Admin user can set project Invoicing status to "Not Started" if current invoicing period is open and Invoicing status = "Approved"
        /// </summary>
        /// <param name="RowId">InvoicingPeriodProjectId</param>
        /// <param name="UndoAction">Unsubmit, ReturnToPM, Unapprove</param>
        /// <returns>success or error message</returns>
        public static string UndoProjectStatus(Guid RowId, string UndoAction)
        {
            string response = "";
            Guid InvoicingPeriodEmployeeId = InvoicingPeriodProject.GetInvoicingPeriodEmployeeIdForProject(RowId);
            int UserGroup = Convert.ToInt32(HttpContext.Current.Session["PTIUserHighestGroupPermission"]);
            string ProjectStatus = (Data.GetTableStatus("InvoicingPeriodProject", RowId, "Status") == null) ? "" : Data.GetTableStatus("InvoicingPeriodProject", RowId, "Status").ToLower();
            string ProjectApproval = (Data.GetTableStatus("InvoicingPeriodProject", RowId, "ApprovalStatus") == null) ? "" : Data.GetTableStatus("InvoicingPeriodProject", RowId, "ApprovalStatus").ToLower();
            string ProjectInvoicing = (Data.GetTableStatus("InvoicingPeriodProject", RowId, "InvoicingStatus") == null) ? "" : Data.GetTableStatus("InvoicingPeriodProject", RowId, "InvoicingStatus").ToLower();
            
            switch (UndoAction.Trim().ToLower())
            {
                case "unsubmit":
                    if (UserGroup == 1 && ProjectStatus == "submitted" && ProjectApproval != "approved")
                    {
                        //Update InvoicingPeriodProject Status
                        response = UpdateProjectStatus(RowId, "Status", "Started");
                        //Update InvoicingPeriodEmployee Statuses
                        if (response == "success")
                        {
                            response = Data.UpdateTableStatus("InvoicingPeriodEmployee", InvoicingPeriodEmployeeId, "Status", "Started");
                            if (response == "success")
                            {
                                response = Data.UpdateTableStatus("InvoicingPeriodEmployee", InvoicingPeriodEmployeeId, "ApprovalStatus", "Not Reviewed");
                            }
                        }
                    }
                    else
                    {
                        response = "Current user permissions and/or project status prevented this action from being completed successfully.";
                    }
                    break;
                case "returntopm":
                    if (UserGroup > 2 && ProjectStatus == "submitted" && ProjectApproval != "approved")
                    {
                        //Update InvoicingPeriodProject Status
                        response = UpdateProjectStatus(RowId, "Status", "Started");
                        //Update InvoicingPeriodEmployee Statuses
                        if (response == "success")
                        {
                            response = Data.UpdateTableStatus("InvoicingPeriodEmployee", InvoicingPeriodEmployeeId, "Status", "Started");
                            if (response == "success")
                            {
                                response = Data.UpdateTableStatus("InvoicingPeriodEmployee", InvoicingPeriodEmployeeId, "ApprovalStatus", "Not Reviewed");
                            }
                        }
                    }
                    else
                    {
                        response = "Current user permissions and/or project status prevented this action from being completed successfully.";
                    }
                    break;
                case "unapprove":
                    bool CurrentSessionIsOpen = (InvoicingPeriod.CurrentSessionPeriodDisplay() == InvoicingPeriod.CurrentOpenPeriodDisplay());
                    if (UserGroup > 2 && CurrentSessionIsOpen && ProjectInvoicing == "approved")
                    {
                        //Update InvoicingPeriodProject Status
                        response = UpdateProjectStatus(RowId, "InvoicingStatus", "Not Started");
                        //Update InvoicingPeriodEmployee Statuses
                        if (response == "success")
                        {
                            response = Data.UpdateTableStatus("InvoicingPeriodEmployee", InvoicingPeriodEmployeeId, "InvoicingStatus", "Not Started");
                        }
                    }
                    else
                    {
                        response = "Current user permissions and/or project status prevented this action from being completed successfully.";
                    }
                    break;
                default:
                    break;
            }
            return response;
        }

        public static string CarryNotesForward(string InvoicingPeriodProjectId, string NotesType)
        {
            string CurrentInvoicingPeriodId = InvoicingPeriod.CurrentOpenPeriodId().ToString();
            JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();
            string response = "success";
            List<string> SaveErrors = new List<string>();
            if (CurrentInvoicingPeriodId.Length > 0)
            {
                //Get Prior Period Id
                string sql = @"select top 1 Id from InvoicingPeriod
                            where Id <> @CurrentInvoicingPeriodId1
                            and StartDate < (select StartDate from InvoicingPeriod where Id = @CurrentInvoicingPeriodId2)
                            and StatusId is not null
                            order by StartDate desc";
                string PriorInvoicingPeriodId = Data.GetScalar(sql, new string[] { CurrentInvoicingPeriodId, CurrentInvoicingPeriodId }, "Id");
                string PriorInvoicingPeriodName = Data.GetScalar("select InvoicingPeriodName from InvoicingPeriod where Id = @InvoicingPeriodId", new string[] { PriorInvoicingPeriodId }, "InvoicingPeriodName");

                //Get Prior InvoicingPeriodProjectId
                sql = @"select Id 
                        from InvoicingPeriodProject 
                        where WBS1 = (select WBS1 from InvoicingPeriodProject where id = @InvoicingPeriodProjectId) 
                        and InvoicingPeriodId = @PriorInvoicingPeriodId";
                string PriorInvoicingPeriodProjectId = Data.GetScalar(sql, new string[] { InvoicingPeriodProjectId, PriorInvoicingPeriodId }, "Id");

                //Get all notes from prior period
                sql = @"select * from (
                        select 'Project' as TableName, concat(isnull(WBS1, ''), '||') as ProjKey, Name, Notes
                        from InvoicingPeriodProject ipp
                        where InvoicingPeriodId = @PriorInvoicingPeriodId1
                        and id = @InvoicingPeriodProjectId1
                        and Notes is not null
                        UNION ALL
                        select 'Phase' as TableName, concat(isnull(WBS1, ''), '|', IPP.WBS2, '|') as ProjKey, IPP.Name, IPP.Notes 
                        from InvoicingPeriodPhase IPP inner join InvoicingPeriodProject IPPrj on IPP.InvoicingPeriodProjectId = IPPrj.Id
                        where IPP.InvoicingPeriodId = @PriorInvoicingPeriodId2
                        and IPP.InvoicingPeriodProjectId = @InvoicingPeriodProjectId2
                        and IPP.Notes is not null
                        UNION ALL
                        select 'Subphase' as TableName, concat(isnull(WBS1, ''), '|', IPP.WBS2, '|', IPS.WBS3) as ProjKey, IPS.Name, IPS.Notes 
                        from InvoicingPeriodSubphase IPS inner join InvoicingPeriodPhase IPP on IPS.InvoicingPeriodPhaseId = IPP.Id
                        inner join InvoicingPeriodProject IPPrj on IPP.InvoicingPeriodProjectId = IPPrj.Id
                        where IPS.InvoicingPeriodId = @PriorInvoicingPeriodId3
                        and IPP.Id in (select Id from InvoicingPeriodPhase where InvoicingPeriodId = @PriorInvoicingPeriodId4 and InvoicingPeriodProjectId = @InvoicingPeriodProjectId3)
                        and IPS.Notes is not null) t1
                        where Notes like '%""' + @NotesType + '""%'";
                List<DataRow> rows = Data.RunSelectQuery(sql, new string[] { PriorInvoicingPeriodId, PriorInvoicingPeriodProjectId, PriorInvoicingPeriodId, PriorInvoicingPeriodProjectId, PriorInvoicingPeriodId, PriorInvoicingPeriodId, PriorInvoicingPeriodProjectId, NotesType });
                foreach (DataRow row in rows)
                {
                    string TableName = row.Val("TableName");
                    string ProjKey = row.Val("ProjKey");
                    string Name = row.Val("Name");
                    
                    dynamic NotesJSON = jsonSerializer.Deserialize<object>(row.Val("Notes"));
                    string PriorNoteValue = NotesJSON[NotesType].ToString();

                    //Get current period record
                    switch (TableName)
                    {
                        case "Project":
                            sql = @"select Id, Notes 
                                    from InvoicingPeriodProject 
                                    where concat(isnull(WBS1, ''), '||') = @ProjKey and InvoicingPeriodId = @InvoicingPeriodId";
                            break;
                        case "Phase":
                            sql = @"select IPP.Id, IPP.Notes 
                                    from InvoicingPeriodPhase IPP inner join InvoicingPeriodProject IPPrj on IPP.InvoicingPeriodProjectId = IPPrj.Id 
                                    where concat(isnull(IPPrj.WBS1, ''), '|', IPP.WBS2, '|') = @ProjKey and IPP.InvoicingPeriodId = @InvoicingPeriodId";
                            break;
                        case "Subphase":
                            sql = @"select IPS.Id, IPS.Notes 
                                    from InvoicingPeriodSubphase IPS inner join InvoicingPeriodPhase IPP on IPS.InvoicingPeriodPhaseId = IPP.Id 
                                    inner join InvoicingPeriodProject IPPrj on IPP.InvoicingPeriodProjectId = IPPrj.Id
                                    where concat(isnull(WBS1, ''), '|', IPP.WBS2, '|', IPS.WBS3) = @ProjKey
                                    and IPS.InvoicingPeriodId = @InvoicingPeriodId";
                            break;
                        default:
                            break;
                    }
                    List<DataRow> CurrentRows = Data.RunSelectQuery(sql, new string[] { ProjKey, CurrentInvoicingPeriodId });
                    foreach (DataRow CurrentRow in CurrentRows)
                    {
                        string NewNoteVal = string.Format("{0} [{1}]", PriorNoteValue, PriorInvoicingPeriodName);
                        string CurrentRowId = CurrentRow.Val("Id");
                        string CurrentRowJSON = Data.GetTableRowJSON("InvoicingPeriod" + TableName, new Guid(CurrentRowId));
                        dynamic ThisRow = jsonSerializer.Deserialize<object>(CurrentRowJSON);
                        if (ThisRow["Notes"].ToString().Trim().Length == 0)
                        {
                            ThisRow["Notes"] = "{}";
                        }
                        dynamic ThisRowNotes = jsonSerializer.Deserialize<object>(ThisRow["Notes"]);
                        bool SaveChanges = false;
                        if (!ThisRow["Notes"].Contains("\"" + NotesType + "\":")) //No previous note, just add the note from the prior period
                        {
                            ThisRowNotes[NotesType] = NewNoteVal;
                            SaveChanges = true;
                        }
                        else //A note has already been entered for this period, append the new from prior period to end of existing notes with carriage return between
                        {
                            if (!ThisRowNotes[NotesType].ToString().Contains(NewNoteVal)) //Don't add duplicates to notes
                            {
                                ThisRowNotes[NotesType] = string.Format("{0}{1}{2}", ThisRowNotes[NotesType], Environment.NewLine, NewNoteVal);
                                SaveChanges = true;
                            }                            
                        }
                        
                        //Update Notes in Current Row
                        if (SaveChanges)
                        {
                            ThisRow["Notes"] = jsonSerializer.Serialize(ThisRowNotes);
                            string UpdatedRowJSON = jsonSerializer.Serialize(ThisRow);
                            //Save Change
                            string SaveResponse = Data.SaveTableRow("InvoicingPeriod" + TableName, UpdatedRowJSON);
                            if (SaveResponse != "success")
                            {
                                SaveErrors.Add(SaveResponse);
                            }
                        }
                    }
                }
            }
            if (SaveErrors.Count == 0)
            {
                //Update Project Notes with flag indicating notes have been carried forward
                string ProjectRowJSON = Data.GetTableRowJSON("InvoicingPeriodProject", new Guid(InvoicingPeriodProjectId));
                dynamic ProjectRow = jsonSerializer.Deserialize<object>(ProjectRowJSON);
                if (ProjectRow["Notes"].ToString().Trim().Length == 0)
                {
                    ProjectRow["Notes"] = "{}";
                }
                dynamic ProjectRowNotes = jsonSerializer.Deserialize<object>(ProjectRow["Notes"]);
                ProjectRowNotes[NotesType + "CarryNotesForward"] = "true";
                ProjectRow["Notes"] = jsonSerializer.Serialize(ProjectRowNotes);
                string UpdatedProjectRowJSON = jsonSerializer.Serialize(ProjectRow);
                response = Data.SaveTableRow("InvoicingPeriodProject", UpdatedProjectRowJSON);
            }
            else
            {
                response = "The following save errors occurred: " + string.Join(",", SaveErrors.ToArray());
            }

            return response;
        }
    }

    public class InvoicingPeriodData
    {
        public static string AllDataInProjectJSON(Guid InvoicingPeriodProjectId)
        {
            string response;
            string sql;
            JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();

            dynamic AllData = jsonSerializer.Deserialize<object>("{}");

            if (InvoicingPeriodEmployee.CanAccessProjectData(InvoicingPeriodProjectId))
            {
                //InvoicingPeriodProject Records
                sql = @"select concat(trim(e.LastName), ', ', trim(e.firstname)) as ManagerName, IPPrj.* 
                from InvoicingPeriodProject IPPrj
                inner join InvoicingPeriodEmployee IPE on IPPrj.InvoicingPeriodEmployeeId = IPE.id
                inner join Employee e on IPE.EmployeeId = e.Id
                where IPPrj.Id = @ProjectId";
                string ProjectJSON = Data.RunSelectQueryJSON(sql, new string[] { InvoicingPeriodProjectId.ToString() });
                dynamic Project = jsonSerializer.Deserialize<object>(ProjectJSON);
                foreach (dynamic ProjectRow in Project)
                {
                    AllData["ProjectManagerName"] = ProjectRow["ManagerName"];
                    AllData["ProjectClientName"] = ProjectRow["ClientName"];
                }
                AllData["Project"] = Project;

                //InvoicingPeriodPhase Records
                sql = @"select concat(trim(e.LastName), ', ', trim(e.firstname)) as ManagerName, IPPh.* 
                from InvoicingPeriodPhase IPPh
                inner join InvoicingPeriodEmployee IPE on IPPh.InvoicingPeriodEmployeeId = IPE.id
                inner join Employee e on IPE.EmployeeId = e.Id
                where InvoicingPeriodProjectId = @ProjectId";
                string PhaseJSON = Data.RunSelectQueryJSON(sql, new string[] { InvoicingPeriodProjectId.ToString() });
                AllData["Phase"] = jsonSerializer.Deserialize<object>(PhaseJSON);

                //InvoicingPeriodSubPhase Records
                sql = @"select concat(trim(e.LastName), ', ', trim(e.firstname)) as ManagerName, IPSub.* 
                from InvoicingPeriodSubphase IPSub
                inner join InvoicingPeriodEmployee IPE on IPSub.InvoicingPeriodEmployeeId = IPE.id
                inner join Employee e on IPE.EmployeeId = e.Id
                where InvoicingPeriodPhaseId in (select Id from InvoicingPeriodPhase where InvoicingPeriodProjectId = @ProjectId)";
                string SubphaseJSON = Data.RunSelectQueryJSON(sql, new string[] { InvoicingPeriodProjectId.ToString() });
                AllData["Subphase"] = jsonSerializer.Deserialize<object>(SubphaseJSON);

                //InvoicingPeriodData Records
                sql = @"select concat(trim(e.LastName), ', ', trim(e.firstname)) as ManagerName, IPD.* 
                from InvoicingPeriodData IPD
                inner join InvoicingPeriodEmployee IPE on IPD.InvoicingPeriodEmployeeId = IPE.id
                inner join Employee e on IPE.EmployeeId = e.Id
                where InvoicingPeriodProjectId = @ProjectId";
                string DetailJSON = Data.RunSelectQueryJSON(sql, new string[] { InvoicingPeriodProjectId.ToString() });
                AllData["Detail"] = jsonSerializer.Deserialize<object>(DetailJSON);
            }
            response = jsonSerializer.Serialize(AllData);
            return response;
        }

        public static string PreviousPeriodProjectLevelDataJSON(string InvoicingPeriodProjectId)
        {
            string JSONData = "";
            string sql = @"select * from InvoicingPeriodData where InvoicingPeriodProjectId = (
	                        select Id from invoicingperiodproject where 
	                        wbs1 = (
		                        select wbs1 from InvoicingPeriodProject where Id = @InvoicingPeriodProjectId
	                        )
	                        and InvoicingPeriodId = (
		                        select top 1 Id from InvoicingPeriod
		                        where Id <> @InvoicingPeriod1
		                        and StartDate < (select StartDate from InvoicingPeriod where Id = @InvoicingPeriod2)
		                        and StatusId is not null
		                        order by StartDate desc
	                        )
                        )";
            string CurrentSessionPeriodId = InvoicingPeriod.CurrentSessionPeriodId();
            JSONData = Data.RunSelectQueryJSON(sql, new string[] {InvoicingPeriodProjectId.ToString(), CurrentSessionPeriodId, CurrentSessionPeriodId });
            return JSONData;
        }
        
        public static string UpdateDataValue(Guid RowId, string FieldName, string NewValue)
        {
            string response;
            if (Data.IsValidTableColumn("InvoicingPeriodData", FieldName))
            {
                JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();
                string CurrentRowJSON = Data.GetTableRowJSON("InvoicingPeriodData", RowId);
                dynamic ThisRow = jsonSerializer.Deserialize<object>(CurrentRowJSON);
                ThisRow[FieldName] = NewValue;
                string UpdatedRowJSON = jsonSerializer.Serialize(ThisRow);
                //Save Change
                response = Data.SaveTableRow("InvoicingPeriodData", UpdatedRowJSON);

                //Set Appropriate Statuses
                if (response == "success")
                {
                    //TODO: Build status update logic based on current status and data value that was updated
                    Guid InvoicingPeriodProjectId = new Guid(ThisRow["InvoicingPeriodProjectId"]);
                    Guid InvoicingPeriodEmployeeId = new Guid(ThisRow["InvoicingPeriodEmployeeId"]);

                    //Update InvoicingPeriodProject to started (needs conditional logic)
                    string InvoicingPeriodProjectStatus = Data.GetTableStatus("InvoicingPeriodProject", InvoicingPeriodProjectId, "Status");
                    string InvoicingPeriodProjectApproval = Data.GetTableStatus("InvoicingPeriodProject", InvoicingPeriodProjectId, "ApprovalStatus");
                    string InvoicingPeriodProjectInvoicing = Data.GetTableStatus("InvoicingPeriodProject", InvoicingPeriodProjectId, "InvoicingStatus");

                    if (InvoicingPeriodProjectStatus == "Not Started")
                    {
                        Data.UpdateTableStatus("InvoicingPeriodProject", InvoicingPeriodProjectId, "Status", "Started");
                    }

                    //Update InvoicingPeriodEmployee to started (needs conditional logic)
                    string InvoicingPeriodEmployeeStatus = Data.GetTableStatus("InvoicingPeriodEmployee", InvoicingPeriodEmployeeId, "Status");
                    string InvoicingPeriodEmployeeApproval = Data.GetTableStatus("InvoicingPeriodEmployee", InvoicingPeriodEmployeeId, "ApprovalStatus");
                    string InvoicingPeriodEmployeeInvoicing = Data.GetTableStatus("InvoicingPeriodEmployee", InvoicingPeriodEmployeeId, "InvoicingStatus");

                    if (InvoicingPeriodEmployeeStatus == "Not Started")
                    {
                        response = Data.UpdateTableStatus("InvoicingPeriodEmployee", InvoicingPeriodEmployeeId, "Status", "Started");
                    }                    
                }
            }
            else
            {
                response = "Invalid data field.";
            }

            return response;
        }


    }

    public class MetadataType
    {
        public static Guid GetIdByName(string MetadataTypeName)
        {
            string sql = "select Id from MetadataType where Name = @name";
            string rawGuid = Data.GetScalar(sql, new string[] { MetadataTypeName }, "Id");
            return new Guid(rawGuid);
        }
    }

    public class Help
    {
        public static string GetHelpForPage(string PageName)
        {
            JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();
            
            //Initialize return object
            dynamic PageHelp = jsonSerializer.Deserialize<object>("{}");
            PageHelp["PageName"] = PageName;
            List<dynamic> HelpSections = new List<dynamic>();

            //Set up help predicates
            string HasHelpSectionPredicateId = Data.GetScalar("select id from metadata where value = 'Has Help Section'", new string[] { }, "id");
            string HasHelpTextPredicateId = Data.GetScalar("select id from metadata where value = 'Has Help Text'", new string[] { }, "id");

            //Get help sections on page
            List<DataRow> PagePredicateRows = Data.RunSelectQuery("Select Predicates from Metadata where Value = @Value", new string[] { PageName });
            foreach (DataRow PagePredicateRow in PagePredicateRows)
            {
                dynamic PagePredicates = jsonSerializer.Deserialize<object>(PagePredicateRow.Val("Predicates"));
                foreach(dynamic PagePredicate in PagePredicates)
                {
                    string PagePredicateId = PagePredicate["Predicate"].ToString();
                    string PageObjectId = PagePredicate["Object"].ToString();
                    if (PagePredicateId == HasHelpSectionPredicateId)
                    {
                        //Help Section Headings
                        dynamic HelpSection = jsonSerializer.Deserialize<object>("{}");
                        string SectionHeading = Data.GetScalar("select value from metadata where id = @Id", new string[] { PageObjectId }, "value");
                        HelpSection["SectionHeading"] = SectionHeading;
                        StringBuilder SectionText = new StringBuilder();
                        List < DataRow> SectionHeadingPredicateRows = Data.RunSelectQuery("Select Predicates from Metadata where Id = @Id", new string[] { PageObjectId });
                        foreach(DataRow SectionHeadingPredicateRow in SectionHeadingPredicateRows)
                        {
                            //Help Section Text
                            dynamic SectionHeadingPredicates = jsonSerializer.Deserialize<object>(SectionHeadingPredicateRow.Val("Predicates"));
                            foreach(dynamic SectionHeadingPredicate in SectionHeadingPredicates)
                            {
                                string SectionHeadingPredicateId = SectionHeadingPredicate["Predicate"].ToString();
                                string SectionHeadingObjectId = SectionHeadingPredicate["Object"].ToString();
                                if (SectionHeadingPredicateId == HasHelpTextPredicateId)
                                {
                                    string HelpText = Data.GetScalar("select value from metadata where id = @Id", new string[] { SectionHeadingObjectId }, "value");
                                    SectionText.Append(HelpText);
                                }
                            }
                            HelpSection["SectionText"] = SectionText.ToString();
                        }
                        HelpSections.Add(HelpSection);
                    }
                }
            }
            if (HelpSections.Count > 0)
            {
                PageHelp["HelpSections"] = HelpSections.ToArray();
            }

            return jsonSerializer.Serialize(PageHelp);
        }
    }

    public class Metadata
    {
        //Class Methods
        /// <summary>
        /// Geta Metadata table Id by Metadata Type Name and Metadata Value
        /// </summary>
        /// <param name="MetadataType">MetadataType.Name</param>
        /// <param name="MetadataValue">Metadata.Value</param>
        /// <returns>Guid</returns>
        public static Guid GetMetadataIdByTypeAndValue(string MetadataType, string MetadataValue)
        {
            string sql = "select Id from metadata where Value = @Value and TypeId = (select Id from MetadataType where Name = @Name)";
            string rawGuid = Data.GetScalar(sql, new string[] { MetadataValue, MetadataType }, "Id");
            return new Guid(rawGuid);
        }

        public static string GetMetadataJSONByTypeName(string MetadataTypeName)
        {
            Guid TypeId = MetadataType.GetIdByName(MetadataTypeName);
            string sWhere = string.Format("where TypeId = '{0}'", TypeId.ToString());
            return Data.GetAllTableRowsJSON("Metadata", sWhere);
        }

        public static string GetTableMetadataJSON(string TableName)
        {
            string response = "";
            if (Data.IsValidDatatable(TableName))
            {
                string sql = @"select m.TypeId as TypeId, mt.Name as MetadataTypeName, m.Id as MetadataId, m.Value as MetadataName, m.ActiveFlag, m.SystemFlag  
                            from Metadata m inner join MetadataType mt on m.TypeId = mt.Id
                            where mt.Name like '" + TableName + @"' + '%'";
                response = Data.RunSelectQueryJSON(sql, new string[] { });
            }
            return response;
        }

        public static string DeleteRow(Guid RowId)
        {
            string response;
            string sql = @"select count(*) as cnt from Metadata where predicates like '%""Object"":""" + RowId.ToString() + "%'";
            if (Convert.ToInt32(Data.GetScalar(sql, new string[] { }, "cnt")) != 0) {
                response = "Cannot delete record as one or more related records exist.";
            }
            else
            {
                response = Data.DeleteTableRow("Metadata", RowId);
            }
            return response;
        }
    }

    #endregion

    #region Data Classes
    public class QueryParameter
    {
        public string Name { get; set; }
        public string Value { get; set; }
    }

    public class DataCell
    {
        public int ColIndex { get; set; }
        public string ColName { get; set; }
        public string Value { get; set; }
    }

    public class DataRow
    {
        public List<DataCell> Data { get; set; }

        public DataRow()
        {
            //Default constructor initializes Data list
            Data = new List<DataCell>();
        }

        /// <summary>
        /// Get value by column index
        /// </summary>
        /// <param name="ColIndex">Column Index</param>
        /// <returns>DataCell value</returns>
        public string Val(int ColIndex)
        {
            return Data.Find(x => x.ColIndex == ColIndex).Value;
        }

        /// <summary>
        /// Get value by column name
        /// </summary>
        /// <param name="ColName"></param>
        /// <returns>DataCell value</returns>
        public string Val(string ColName)
        {
            return Data.Find(x => x.ColName == ColName).Value;
        }
    }

    public class Data
    {

        /// <summary>
        /// Validates that a table is available for CRUD
        /// </summary>
        /// <param name="TableName">Name of table being checked</param>
        /// <returns>boolean</returns>
        public static bool IsValidDatatable(string TableName)
        {
            return (AllTables().Contains(TableName) ? true : false);
        }

        /// <summary>
        /// Use to validate that table being requested is a valid database table
        /// </summary>
        /// <returns>string list of table names</returns>
        public static List<string> AllTables()
        {
            List<string> allTables = new List<string>();
            if (HttpContext.Current.Session["DatabaseTables"] == null)
            {
                string sql = "select name from sysobjects where xtype = 'U'";
                List<DataRow> rows = RunSelectQuery(sql, new string[] { });
                foreach (DataRow row in rows)
                {
                    allTables.Add(row.Val("name"));
                }
                HttpContext.Current.Session["DatabaseTables"] = allTables;
            }
            else
            {
                allTables = (List<string>)HttpContext.Current.Session["DatabaseTables"];
            }
            return allTables;
        }

        /// <summary>
        /// Check to see if a column name is valid in a table
        /// </summary>
        /// <param name="TableName">Name of table being checked</param>
        /// <param name="ColumnName">Name of column to check in table</param>
        /// <returns>true if exists otherwise false</returns>
        public static bool IsValidTableColumn(string TableName, string ColumnName)
        {
            string sql = @"select count(*) as cnt
                from sys.columns c join sys.tables t on c.object_id = t.object_id
                where t.name = @TableName and c.name = @ColumnName";
            string ColCount = Data.GetScalar(sql, new string[] { TableName, ColumnName }, "cnt");
            return (Convert.ToInt32(ColCount) == 1) ? true : false;
        }

        /// <summary>
        /// Use to simplify passing Query Parameters
        /// </summary>
        /// <param name="Parameter">Send array of colon-delimited name|value strings ["@Id:12345","@Name:SampleName"...]</param>
        /// <returns>List of QueryParameter</returns>
        public static List<QueryParameter> ParameterList(string[] Params)
        {
            List<QueryParameter> ParamList = new List<QueryParameter>();
            foreach (string Param in Params)
            {
                string ParamName = Param.Split(':')[0];
                string ParamValue = Param.Split(':')[1];
                ParamList.Add(new QueryParameter { Name = ParamName, Value = ParamValue });
            }
            return ParamList;
        }

        /// <summary>
        /// Returns string list of parameter names from a query where param names are like @ParamName
        /// </summary>
        /// <param name="sql">SQL Query</param>
        /// <returns>List of String</returns>
        public static string[] GetParamNamesFromSQL(string sql)
        {
            List<string> ParamNames = new List<string>();
            string pattern = @"@\w*";
            Regex rgx = new Regex(pattern);

            foreach (Match match in rgx.Matches(sql))
            {
                ParamNames.Add(match.Value);
            }

            return ParamNames.ToArray();
        }

        /// <summary>
        /// Create a list of QueryParameters from a sql string and string array of parameter values
        /// </summary>
        /// <param name="sql">Parameterized SQL statement @-prefixed parameters</param>
        /// <param name="ParamValues">String array of parameter values in the order they appear in the SQL statement</param>
        /// <returns></returns>
        public static List<QueryParameter> BuildQueryParams(string sql, string[] ParamValues)
        {
            List<QueryParameter> Params = new List<QueryParameter>();
            if (ParamValues.Length > 0)
            {
                string[] ParamNames = GetParamNamesFromSQL(sql);
                for (int i = 0; i < ParamNames.Length; i++)
                {
                    Params.Add(new QueryParameter { Name = ParamNames[i], Value = ParamValues[i] });
                }
            }
            return Params;
        }

        /// <summary>
        /// Return a single table row by Table Name and Row Id
        /// </summary>
        /// <param name="TableName">Name of table to select from</param>
        /// <param name="Id">Guid Row Id</param>
        /// <returns>Table DataRow</returns>
        public static DataRow GetTableRow(string TableName, Guid Id)
        {
            DataRow OutputRow = new DataRow();
            string sql = string.Format("select * from {0} where Id = @Id", TableName);
            List<DataRow> Rows = RunSelectQuery(sql, new string[] { Id.ToString() });
            if (Rows.Count > 0) OutputRow = Rows[0];
            return OutputRow;
        }

        /// <summary>
        /// Use when getting a single value or count from a database table
        /// </summary>
        /// <param name="sql">SQL Query</param>
        /// <param name="QueryParams">List of QueryParameter</param>
        /// <param name="ColName">Name of column in query result that contains the scalar result</param>
        /// <returns>Scalar value in ColName of the sql query</returns>
        public static string GetScalar(string sql, string[] ParameterValues, string ColName)
        {
            string response = null;

            List<DataRow> rows = Data.RunSelectQuery(sql, ParameterValues);
            foreach (DataRow row in rows)
            {
                response = row.Val(ColName);
            }

            return response;
        }

        /// <summary>
        /// Returns JSON of all rows from datatable by its name
        /// </summary>
        /// <param name="TableName">Table Name</param>
        /// <returns>JSON string of all rows in a table</returns>
        public static string GetAllTableRowsJSON(string TableName, string WhereAndOrderBy = "")
        {
            string OutputRows = "";
            if (IsValidDatatable(TableName))
            {
                string sql = string.Format("select * from {0} {1}", TableName, WhereAndOrderBy);
                OutputRows = RunSelectQueryJSON(sql, new string[] { });
            }
            return OutputRows;
        }

        /// <summary>
        /// Returns JSON of single row from datatable by its Id
        /// </summary>
        /// <param name="TableName">Table Name</param>
        /// <param name="Id">Table row Id</param>
        /// <returns>JSON string of values in a single table row</returns>
        public static string GetTableRowJSON(string TableName, Guid Id)
        {
            string TableRowJSON = "";
            if (IsValidDatatable(TableName))
            {
                string sql = string.Format("select * from {0} where Id = @Id", TableName);
                string OutputRows = RunSelectQueryJSON(sql, new string[] { Id.ToString() });
                //Only one row returned, so remove array brackets []
                TableRowJSON = OutputRows.Replace("[{", "{").Replace("}]", "}");
            }
            return TableRowJSON;
        }

        /// <summary>
        /// Save a row by table name and row data
        /// </summary>
        /// <param name="TableName">Name of table to save to</param>
        /// <param name="RowDataJSON">JSON row data</param>
        /// <returns>"success" or error message</returns>
        public static string SaveTableRow(string TableName, string RowDataJSON, List<string> ColumnsToUpdate = null)
        {
            string response = "success";
            JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();
            string sql;
            string count;
            if (IsValidDatatable(TableName))
            {
                //Deserialize Row Data
                dynamic RowData = jsonSerializer.Deserialize<object>(RowDataJSON);
                if (Convert.ToString(RowData["Id"]) == "0")
                {
                    //Passing an Id of "0" is a new record, so create a new Guid for the Id and update RowDataJSON
                    RowData["Id"] = Guid.NewGuid().ToString();
                    RowDataJSON = jsonSerializer.Serialize(RowData);
                }

                //Check for duplicates as needed depending on table
                count = "0";
                switch (TableName.Trim().ToLower())
                {
                    case "metadatatype":
                        sql = "select count(*) as cnt from MetadataType where lower(Name) = lower(@Name) and Id <> @Id";
                        count = Data.GetScalar(sql, new string[] { RowData["Name"], RowData["Id"].ToString() }, "cnt");
                        break;
                    case "metadata":
                        sql = "select count(*) as cnt from Metadata where lower(Value) = lower(@Value) and TypeId = @TypeId and Id <> @Id";
                        count = Data.GetScalar(sql, new string[] { RowData["Value"], RowData["TypeId"].ToString(), RowData["Id"].ToString() }, "cnt");
                        break;
                    default:
                        break;
                }
                if (Convert.ToInt32(count) > 0)
                {
                    response = "Cannot save as doing so would create a duplicate.";
                }

                //Save row data if no dupes
                if (response == "success")
                {
                    List<string> TableCols = GetTableColumns(TableName);
                    List<string> QueryParams = new List<string>();
                    Guid AuditLogChangeTypeId;
                    //see if this is an insert or update by Id
                    sql = string.Format("select count(*) as cnt from {0} where Id = @Id", TableName);
                    count = Data.GetScalar(sql, new string[] { RowData["Id"].ToString() }, "cnt");
                    if (Convert.ToInt32(count) > 0)
                    {
                        //Update existing row
                        AuditLogChangeTypeId = AuditLog.RecordChangedId();
                        sql = "update " + TableName + " set ";
                        foreach (string ColName in TableCols)
                        {
                            bool IncludeColumn = true;
                            if (ColumnsToUpdate != null)
                            {
                                if (!ColumnsToUpdate.Contains(ColName)) IncludeColumn = false;
                            }
                            if (IncludeColumn)
                            {
                                if (!sql.Trim().EndsWith("set")) sql += ", ";
                                sql += ColName + " = @" + ColName;
                                try
                                {
                                    QueryParams.Add(RowData[ColName]);
                                }
                                catch (Exception)
                                {
                                    QueryParams.Add("");
                                }
                            }
                        }
                        sql += " where Id = @Id";
                        QueryParams.Add(RowData["Id"]);
                    }
                    else
                    {
                        //Insert new row
                        AuditLogChangeTypeId = AuditLog.RecordAddedId();
                        string InsertValues = " values(@Id";
                        sql = "insert into " + TableName + "(Id";
                        QueryParams.Add(RowData["Id"]);
                        foreach (string ColName in TableCols)
                        {
                            InsertValues += ", @" + ColName;
                            sql += ", " + ColName;
                            try
                            {
                                QueryParams.Add(RowData[ColName]);
                            }
                            catch (Exception)
                            {
                                QueryParams.Add("");
                            }
                        }
                        InsertValues += ")";
                        sql += ")" + InsertValues;
                    }
                    try
                    {
                        AuditLog.LogChange(TableName, RowDataJSON, AuditLogChangeTypeId);
                        response = RunActionQuery(sql, QueryParams.ToArray());
                        if (Common.IsNumeric(response)) response = "success";
                    }
                    catch (Exception ex)
                    {
                        response = ex.Message;
                    }

                }
            }

            return response;
        }

        /// <summary>
        /// Retrieve a record's status description for a particular status type in a table
        /// </summary>
        /// <param name="TableName">Name of table</param>
        /// <param name="RowId">Id of row of interest</param>
        /// <param name="StatusType">Status, InvoicingStatus, ApprovalStatus</param>
        /// <returns>Status description, i.e., Started, Submitted, Approved, etc.</returns>
        public static string GetTableStatus(string TableName, Guid RowId, string StatusType)
        {
            string response = "";
            string MetadataType = TableName + StatusType;
            string StatusColumnName = string.Format("{0}Id", StatusType);
            if (IsValidDatatable(TableName))
            {
                if (IsValidTableColumn(TableName, StatusColumnName))
                {
                    string sql = "select Value from Metadata where Id = (Select " + StatusColumnName + " from " + TableName + " where Id = @Id)";
                    response = GetScalar(sql, new string[] { RowId.ToString() }, "Value");
                }
                else
                {
                    response = "Invalid status column name.";
                }
            }
            else
            {
                response = "Invalid table name.";
            }

            return response;
        }

        /// <summary>
        /// Update appropriate Table status
        /// </summary>
        /// <param name="TableName">Table being updated</param>
        /// <param name="RowId">Row Id from table</param>
        /// <param name="StatusType">Status, InvoicingStatus, ApprovalStatus</param>
        /// <param name="Status">Not Started, Started, Submitted, Questions, Approved, etc.</param>
        /// <returns>success or error message</returns>
        public static string UpdateTableStatus(string TableName, Guid RowId, string StatusType, string Status)
        {
            string response = "success";
            string MetadataType = TableName + StatusType;
            string StatusColumnName = string.Format("{0}Id", StatusType);
            Guid StatusId = Metadata.GetMetadataIdByTypeAndValue(MetadataType, Status);
            if (IsValidDatatable(TableName))
            {
                if (IsValidTableColumn(TableName, StatusColumnName))
                {
                    string sql = "update " + TableName + " set " + StatusColumnName + " = @Status where Id = @Id";
                    RunActionQuery(sql, new string[] { StatusId.ToString(), RowId.ToString() });
                }
                else
                {
                    response = "Invalid status column name.";
                }
            }
            else
            {
                response = "Invalid table name.";
            }

            return response;
        }

        /// <summary>
        /// Delete a record in a data table
        /// </summary>
        /// <param name="TableName">Name of data table</param>
        /// <param name="Id">Row id of record to delete</param>
        /// <returns>"success" or error message</returns>
        public static string DeleteTableRow(string TableName, Guid Id)
        {
            string response = "success";
            string sql;

            //Check MetadataType in Use
            if (Data.RecordIsInUse(TableName, Id))
            {
                response = "Can't delete record. It is in use.";
            }
            else
            {
                sql = string.Format("delete {0} where Id = @Id", TableName);
                try
                {
                    string DeleteJSON = "{\"Id\":\"" + Id.ToString() + "\"}";
                    AuditLog.LogChange(TableName, DeleteJSON, AuditLog.RecordDeletedId());
                    response = Data.RunActionQuery(sql, new string[] { Id.ToString() });
                    if (Common.IsNumeric(response)) response = "success";
                }
                catch (Exception ex)
                {
                    response = ex.Message;
                }
            }
            return response;
        }

        /// <summary>
        /// Returns true/false if a table row is in use in a foreign key table
        /// </summary>
        /// <param name="TableName">Name of table</param>
        /// <param name="RowId">Record Id</param>
        /// <returns>True if other records use this Id as a foreign key, otherwise false.</returns>
        public static bool RecordIsInUse(string TableName, Guid RowId)
        {
            Dictionary<string, string[]> AllTableForeignKeys = new Dictionary<string, string[]>();
            //Employee table foreign keys
            AllTableForeignKeys.Add("Employee", new string[] { "AuditLog.EmployeeId", "InvoicingPeriodEmployee.EmployeeId" });
            //MetadataType table foreign keys
            AllTableForeignKeys.Add("MetadataType", new string[] { "Metadata.TypeId" });
            //Metadata table foreign keys
            AllTableForeignKeys.Add("Metadata", new string[] { "AuditLog.TypeId", "InvoicingPeriod.StatusId", "InvoicingPeriodEmployee.StatusId", "InvoicingPeriodEmployee.StatusId", "InvoicingPeriodEmployee.ApprovalStatusId", "InvoicingPeriodEmployee.InvoicingStatusId", "InvoicingPeriodProject.StatusId", "InvoicingPeriodProject.ApprovalStatusId", "InvoicingPeriodProject.InvoicingStatusId", "InvoicingPeriodPhase.StatusId", "InvoicingPeriodSubphase.StatusId" });
            //InvoicingPeriod table foreign keys
            AllTableForeignKeys.Add("InvoicingPeriod", new string[] { "InvoicingPeriodProject.InvoicingPeriodId", "InvoicingPeriodPhase.InvoicingPeriodId", "InvoicingPeriodSubphase.InvoicingPeriodId", "InvoicingPeriodData.InvoicingPeriodId", "InvoicingPeriodEmployee.InvoicingPeriodId" });
            //InvoicingPeriodEmployee table foreign keys
            AllTableForeignKeys.Add("InvoicingPeriodEmployee", new string[] { "InvoicingPeriodProject.InvoicingPeriodEmployeeId", "InvoicingPeriodPhase.InvoicingPeriodEmployeeId", "InvoicingPeriodPhase.InvoicingPeriodEmployeeId", "InvoicingPeriodSubphase.InvoicingPeriodEmployeeId", "InvoicingPeriodData.InvoicingPeriodEmployeeId" });
            //InvoicingPeriodProject table foreign keys
            AllTableForeignKeys.Add("InvoicingPeriodProject", new string[] { "InvoicingPeriodPhase.InvoicingPeriodProjectId", "InvoicingPeriodData.InvoicingPeriodProjectId" });
            //InvoicingPeriodPhase table foreign keys
            AllTableForeignKeys.Add("InvoicingPeriodPhase", new string[] { "InvoicingPeriodSubphase.InvoicingPeriodPhaseId", "InvoicingPeriodData.InvoicingPeriodPhaseId" });
            //InvoicingPeriodSubphase table foreign keys
            AllTableForeignKeys.Add("InvoicingPeriodSubphase", new string[] { "InvoicingPeriodData.InvoicingPeriodSubphaseId" });

            bool InUse = false;
            string TotalCount;
            string sql = string.Empty;
            string[] ThisTableForeignKeys = AllTableForeignKeys[TableName];
            //Check Foreign Keys
            foreach (string ForeignKey in ThisTableForeignKeys)
            {
                string ForeignKeyTable = ForeignKey.Split('.')[0];
                string ForeignKeyColumn = ForeignKey.Split('.')[1];
                if (sql.Length > 0) sql += " UNION All ";
                sql += string.Format("select count(*) as cnt from {0} where {1} = '{2}'", ForeignKeyTable, ForeignKeyColumn, RowId.ToString());
            }
            if (sql.Length > 0)
            {
                sql = "select sum(cnt) as TotalCount from (" + sql + ") tmain";
                TotalCount = GetScalar(sql, new string[] { }, "TotalCount");
                InUse = (Convert.ToInt32(TotalCount) > 0);
            }
            //Check Questions/Responses Metadata Ids
            if (TableName.Trim().ToLower() == "metadata" && !InUse)
            {
                sql = @"select count(*) as cnt from
                        (
                        select Questions from invoicingperiodproject where not questions is null
                        union all
                        select Questions from invoicingperiodphase where not questions is null
                        union all
                        select Questions from InvoicingPeriodSubphase where not questions is null
                        ) tQuestions
                        where Questions like '%" + RowId.ToString() + "%'";
                TotalCount = GetScalar(sql, new string[] { }, "cnt");
                InUse = (Convert.ToInt32(TotalCount) > 0);
            }
            return InUse;
        }


        /// <summary>
        /// Get column names from table
        /// </summary>
        /// <param name="TableName">Name of table</param>
        /// <returns>string list of column names</returns>
        public static List<string> GetTableColumns(string TableName)
        {
            List<string> TableColumns = new List<string>();
            if (IsValidDatatable(TableName))
            {
                string sql = "select name from syscolumns where id in (select Id from sysobjects where xtype = 'U' and lower(name) = @name) and lower(name) <> 'id'";
                List<DataRow> Columns = RunSelectQuery(sql, new string[] { TableName.Trim().ToLower() });
                foreach (DataRow row in Columns)
                {
                    TableColumns.Add(row.Val("name"));
                }
            }
            return TableColumns;
        }

        /// <summary>
        /// Get object properties for an object
        /// </summary>
        /// <param name="ThisObject">Object of interest</param>
        /// <returns>dictionary of string,object with key being property name</returns>
        public static Dictionary<string, object> ObjectProperties(object ThisObject)
        {
            Dictionary<string, object> ReturnDictionary = new Dictionary<string, object>();
            foreach (PropertyInfo prop in ThisObject.GetType().GetProperties())
            {
                ReturnDictionary.Add(prop.Name, prop.GetValue(ThisObject, null));
            }
            return ReturnDictionary;
        }

        /// <summary>
        /// Run a select SQL statement or Stored Procedure
        /// </summary>
        /// <param name="SQLorProcName">SQL Select string or Stored Proc name that returns a recordset</param>
        /// <param name="ParameterValues">String array of parameter values in order they appear in query</param>
        /// <param name="CmdType">CommandType.Text or CommandType.StoredProcedure</param>
        /// <returns>List of DataRow</returns>
        public static List<DataRow> RunSelectQuery(string SQLorProcName, string[] ParameterValues, CommandType CmdType = CommandType.Text)
        {
            List<DataRow> Rows = new List<DataRow>();
            string Connstr = ConfigurationManager.ConnectionStrings["PTI"].ConnectionString;
            List<QueryParameter> QueryParams = BuildQueryParams(SQLorProcName, ParameterValues);
            using (SqlConnection con = new SqlConnection(Connstr))
            {
                using (SqlCommand cmd = new SqlCommand(SQLorProcName, con))
                {
                    cmd.CommandType = CmdType;
                    cmd.CommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings["SQLCommandTimeoutInSeconds"]);
                    foreach (QueryParameter param in QueryParams)
                    {
                        cmd.Parameters.AddWithValue(param.Name, param.Value);
                    }
                    try
                    {
                        con.Open();
                    }
                    catch (Exception ex)
                    {
                        HttpContext.Current.Response.Write("<h2>Database Connection Error</h2>");
                        HttpContext.Current.Response.Write("<p style='color:red;'>" + ex.Message + "</p>");
                        HttpContext.Current.Response.Write("<p>Please contact an administrator</p>");
                        HttpContext.Current.Response.End();
                    }
                    
                    SqlDataReader rdr = cmd.ExecuteReader();
                    while (rdr.Read())
                    {
                        DataRow row = new DataRow();
                        for (int i = 0; i < rdr.FieldCount; i++)
                        {
                            row.Data.Add(new DataCell { ColIndex = i, ColName = rdr.GetName(i), Value = rdr.GetValue(i).ToString() });
                        }
                        Rows.Add(row);
                    }
                }
            }

            return Rows;
        }

        /// <summary>
        /// Run a select SQL statement or Stored Procedure and return results in a JSON string
        /// </summary>
        /// <param name="SQLorProcName">SQL Select string or Stored Proc name that returns a recordset</param>
        /// <param name="ParameterValues">String array of parameter values in order they appear in query</param>
        /// <param name="CmdType">CommandType.Text or CommandType.StoredProcedure</param>
        /// <returns>JSON string of data</returns>
        public static string RunSelectQueryJSON(string SQLorProcName, string[] ParameterValues, CommandType CmdType = CommandType.Text)
        {
            JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();
            List<Dictionary<string, string>> Rows = new List<Dictionary<string, string>>();

            List<DataRow> ResultRows = RunSelectQuery(SQLorProcName, ParameterValues, CmdType);
            if (ResultRows.Count > 0)
            {
                foreach (DataRow Row in ResultRows)
                {
                    Dictionary<string, string> OutputRow = new Dictionary<string, string>();
                    foreach (DataCell Field in Row.Data)
                    {
                        OutputRow.Add(Field.ColName, Field.Value);
                    }
                    Rows.Add(OutputRow);
                }
            }
            string OutputRowsJSON = jsonSerializer.Serialize(Rows);
            return OutputRowsJSON;
        }

        /// <summary>
        /// Run an Update or Delete SQL statement or stored procedure
        /// </summary>
        /// <param name="SQLorProcName">SQL action query string or stored proc name</param>
        /// <param name="ParameterValues">String array of parameter values in order they appear in query</param>
        /// <param name="CmdType"></param>
        /// <returns>CommandType.Text or CommandType.StoredProcedure</returns>
        public static string RunActionQuery(string SQLorProcName, string[] ParameterValues, CommandType CmdType = CommandType.Text)
        {
            string Response = "success";
            string Connstr = ConfigurationManager.ConnectionStrings["PTI"].ConnectionString;
            List<QueryParameter> QueryParams = BuildQueryParams(SQLorProcName, ParameterValues);

            //Strip parameters off stored proc name if any exist
            if (CmdType == CommandType.StoredProcedure)
            {
                if (SQLorProcName.IndexOf(" ") > -1)
                {
                    SQLorProcName = SQLorProcName.Substring(0, SQLorProcName.IndexOf(" "));
                }
            }
            using (SqlConnection con = new SqlConnection(Connstr))
            {
                using (SqlCommand cmd = new SqlCommand(SQLorProcName, con))
                {
                    cmd.CommandType = CmdType;
                    cmd.CommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings["SQLCommandTimeoutInSeconds"]);
                    foreach (QueryParameter param in QueryParams)
                    {
                        if (param.Value.Length > 0)
                        {
                            cmd.Parameters.AddWithValue(param.Name, param.Value);
                        }
                        else
                        {
                            cmd.Parameters.AddWithValue(param.Name, DBNull.Value);
                        }
                    }
                    con.Open();
                    try
                    {
                        Response = cmd.ExecuteNonQuery().ToString();
                    }
                    catch (Exception ex)
                    {
                        Response = ex.Message;
                    }
                }
            }
            return Response;
        }
    }

    #endregion

}