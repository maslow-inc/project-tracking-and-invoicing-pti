﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PTI.Models;
using System.Web.Security;

namespace PTI.Controllers
{
    public class HomeController : _BaseController
    {
        public ActionResult Index()
        {
            //User Role Session
            if (Session["PTIUserRoles"] == null)
            {
                Session["PTIUserRoles"] = Roles.GetRolesForUser();
                PTIAuthADGroups.SetUserHighestGroupPermission();
            }

            return View();
        }
    }
}