﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PTI.Models;

namespace PTI.Controllers
{
    public class SystemToolsController : _BaseController
    {
        [AuthorizeNEIAD(Groups = PTIAuthADGroups.OWNER)]
        public ActionResult InvoicingPeriods()
        {
            return View();
        }

        [AuthorizeNEIAD(Groups = PTIAuthADGroups.OWNER)]
        public ActionResult Metadata()
        {
            return View();
        }

        [AuthorizeNEIAD(Groups = PTIAuthADGroups.OWNER)]
        public ActionResult UpdateCurrentInvoicingPeriod()
        {
            return View();
        }
    }
}