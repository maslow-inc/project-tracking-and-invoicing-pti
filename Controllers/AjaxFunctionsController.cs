﻿using System;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PTI.Models;

namespace PTI.Controllers
{
    public class AjaxFunctionsController : _BaseController
    {
		[AuthorizeNEIAD(Groups = PTIAuthADGroups.PMs)]
		public ActionResult Index(string EventCommand, string EventArgument)
		{
			Models.AjaxFunctions vm = new Models.AjaxFunctions();

			vm.EventArgument = EventArgument;
			vm.EventCommand = EventCommand;
			StreamReader streamReader = new StreamReader(Request.InputStream);
			vm.EventData = streamReader.ReadToEnd();
			vm.HandleRequest();
			return View(vm);
		}

	}
}