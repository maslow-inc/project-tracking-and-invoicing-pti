﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PTI.Models;

namespace PTI.Controllers
{
    public class ProjectTrackingController : _BaseController
    {
        [AuthorizeNEIAD(Groups = PTIAuthADGroups.ACCT)]
        public ActionResult ProjectManagers()
        {
            ProjectTrackingViewModel vm = new ProjectTrackingViewModel();
            vm.UpdateInvoicingPeriodEmployeeStatuses();
            return View(vm);
        }

        [AuthorizeNEIAD(Groups = PTIAuthADGroups.PMs)]
        public ActionResult Projects(string id)
        {
            bool AllProjects = false;
            //All users can see all projects if viewing a closed invoicing period
            if (InvoicingPeriod.CurrentSessionPeriodId() != InvoicingPeriod.CurrentOpenPeriodId())
            {
                if (id == null) 
                {
                    //Get employee ID for logged in user
                    id = InvoicingPeriodEmployee.LoggedInUserInvoicingPeriodEmployeeId().ToString(); // Employee.LoggedInUserEmployeeId().ToString();
                }
                AllProjects = true;
            }
            else
            {
                //Validate ID
                if (id == null)
                {
                    //Get employee ID for logged in user
                    id = InvoicingPeriodEmployee.LoggedInUserInvoicingPeriodEmployeeId().ToString();

                    //ACCT and higher can see all Projects for all PMs, Supervisors can see all projects for themselves and PMs who report to them
                    if (Convert.ToInt32(PTIAuthADGroups.UserHighestGroupPermission()) >= 2 || Employee.IsPMSupervisor())
                    {
                        id = InvoicingPeriodEmployee.LoggedInUserInvoicingPeriodEmployeeId().ToString();
                        AllProjects = true;
                    }
                }
                else
                {
                    //PMs can only see their own Projects. Supervisors can see their own projects and projects for PMs who report to them
                    if (Convert.ToInt32(PTIAuthADGroups.UserHighestGroupPermission()) < 2)
                    {
                        if (!Employee.IsPMSupervisor())
                        {
                            //If not supervisor, then can only see their own projects
                            id = InvoicingPeriodEmployee.LoggedInUserInvoicingPeriodEmployeeId().ToString();
                        }
                        else
                        {
                            //Confirm that supervisor can see this PM's projects
                            if (!InvoicingPeriodEmployee.CanViewProjects(new Guid(id)))
                            {
                                id = InvoicingPeriodEmployee.LoggedInUserInvoicingPeriodEmployeeId().ToString();
                            }
                        }
                    }
                }
            }
            
            ProjectTrackingViewModel vm = new ProjectTrackingViewModel
            {
                id = id,
                AllProjectsMode = AllProjects
            };

            return View(vm);
        }

        [AuthorizeNEIAD(Groups = PTIAuthADGroups.PMs)]
        public ActionResult ProjectData(string id)
        {
            ProjectTrackingViewModel vm = new ProjectTrackingViewModel
            {
                id = id
            };

            return View(vm);
        }

    }
}