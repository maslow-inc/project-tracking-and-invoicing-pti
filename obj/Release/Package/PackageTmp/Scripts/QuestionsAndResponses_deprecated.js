﻿//Question and Response variables
var AllQuestionsJSON;
var AllResponsesJSON;
var QuestionTable; //variable for datatable
var QuestionHTMLTemplate = "<tr><td><a href='#expandcollapse' class='expand-collapse' title='Show/Hide Responses' data-rowid='~rowid~'><i class='fa fa-plus'></i></a><a href='#edit' title='Edit Question' data-rowid='~rowid~' style='margin-left:10px;' onclick='OpenEditForm(\"Question\", this);'><i class='fa fa-pencil-square-o' aria-hidden='true'></i></a><a href='#delete' title='Delete Question' style='margin-left:10px;' data-rowid='~rowid~' onclick='DeleteRecord(this, \"Question\");'><i class='fa fa-times' aria-hidden='true'></i></a></td><td>~value~</td><td style='text-align:center;'>~activeflag~</td></tr>";
var MetadataJSON;

var rowCheckIcon = "<i class='fa fa-check' aria-hidden='true'></i>";

$(function () {
    //Page ready

    //Load Questions and Responses
    GetMetadataByType("Question").then(
        function (QuestionJSON) {
            //Load Questions Table
            $("#QuestionAndResponseBody").html("");
            AllQuestionsJSON = QuestionJSON;
            $(AllQuestionsJSON).each(function () {
                var thisQuestion = QuestionHTMLTemplate.replace(/~rowid~/ig, this.Id);
                thisQuestion = thisQuestion.replace(/~value~/ig, this.Value);
                thisQuestion = thisQuestion.replace(/~activeflag~/ig, (this.ActiveFlag == "True") ? rowCheckIcon : "");
                $("#QuestionAndResponseBody").append(thisQuestion);
            });
            InitQuestionTable();
        },
        function (errormsg) {
            console.log('Error loading questions: ' + errormsg);
        }
    );

    GetMetadataByType("Response").then(
        function (ResponseJSON) {
            AllResponsesJSON = ResponseJSON;
        },
        function (errormsg) {
            console.log('Error loading responses: ' + errormsg);
        }
    );
});

function InitQuestionTable() {
    QuestionTable = $("#tblQuestionsAndResponses").DataTable({
        scrollY: 340,
        scrollCollapse: true,
        stateSave: true,
        "columnDefs": [
            { "orderable": false, "targets": [0,2] },
            { "width": "50px", "targets": [0] }
        ]
    });
    //Setup Expand/Collapse Responses Rows
    $('#tblQuestionsAndResponses tbody').on('click', 'a.expand-collapse', function () {
        var QuestionId = $(this).data("rowid");
        var tr = $(this).closest('tr');
        var row = QuestionTable.row(tr);
        var cmdCellHTML = $(this).html();

        if (row.child.isShown()) {
            // Close opened row
            row.child.hide();
            $(this).html(cmdCellHTML.replace(/-minus/ig, "-plus"));
        }
        else {
            // Open closed row
            row.child(ShowQuestionResponses(QuestionId)).show();
            $(this).html(cmdCellHTML.replace(/-plus/ig, "-minus"));
        }
    });
}

function ShowQuestionResponses(QuestionId) {
    var ResponseHTMLTemplate = "<td>~value~</td><td style='text-align:center;'>~activeflag~</td>";
    var NewResponseButtonHTML = '<button type="button" data-rowid="0" class="w3-button w3-teal w3-hover-nei w3-small w3-round" data-rowid="0" data-questionid="' + QuestionId + '" onclick="OpenEditForm(\'Response\', this);">Add New Response</button>';
    var responseActionButtons = "<td style='width:50px;'><a href='#edit' title='Edit Response' data-rowid='~rowid~' style='margin-left:10px;' data-rowid='~rowid~' data-questionid='~questionid~' onclick='OpenEditForm(\"Response\", this);'><i class='fa fa-pencil-square-o' aria-hidden='true'></i></a><a href='#delete' title='Delete Response' style='margin-left:10px;' data-rowid='~rowid~' onclick='DeleteRecord(this, \"Response\");'><i class='fa fa-times' aria-hidden='true'></i></a></td>";

    var tblResponses = "<table class='w3-table' style='width:75%;'><tr><th style='width:50px;'>&nbsp;</th><th>Response</th><th style='text-align:center;'>Active</th></tr>";
    $(AllResponsesJSON).each(function () {
        var bIsResponseForThisQuestion = false;
        var ResponsePredicates = JSON.parse(this.Predicates)
        $(ResponsePredicates).each(function () {
            if (this.Predicate == g_AnswersQuestionPredicateId && this.Object == QuestionId) {
                bIsResponseForThisQuestion = true;
            }
        });
        if (bIsResponseForThisQuestion) {
            var thisResponseActionButtons = responseActionButtons.replace(/~rowid~/ig, this.Id).replace(/~questionid~/ig, QuestionId);
            var thisResponse = ResponseHTMLTemplate.replace(/~value~/ig, this.Value);
            thisResponse = thisResponse.replace(/~activeflag~/ig, (this.ActiveFlag == "True") ? rowCheckIcon : "");
            tblResponses += "<tr>" + thisResponseActionButtons + thisResponse + "</tr>";
        }
    });
    tblResponses += "</table>";

    var responseHTML = "<div style='margin-left:100px;'>" + tblResponses + NewResponseButtonHTML + "</div>";
    return responseHTML;
}

function OpenEditForm(QuestionOrResponse, FormOpener) {
    var bResponse = (QuestionOrResponse == "Response") ? true : false;

    //Set headings and labels appropriately
    $("#headingQuestionOrResponse").html(QuestionOrResponse + " Details");
    $("#lblQuestionOrResponse").html(QuestionOrResponse);

    //Set up fields
    var AllRecordData = AllQuestionsJSON;
    if (bResponse) AllRecordData = AllResponsesJSON;

    var RowId = $(FormOpener).data("rowid");
    if (RowId == "0") {
        //New Record
        $("#txtRowId").val("0");
        $("#txtTypeId").val(g_QuestionTypeId);
        if (bResponse) $("#txtTypeId").val(g_ResponseTypeId);
        $("#txtValue").val("");
        var Predicates = "";
        if (bResponse) {
            var QuestionId = $(FormOpener).data("questionid");
            Predicates = [{ "Predicate": g_AnswersQuestionPredicateId, "Object": QuestionId }];
        }
        $("#txtPredicates").val(JSON.stringify(Predicates));
        $("#chkActiveFlag").prop("checked", true);
        $("#chkSystemFlag").prop("checked", false);
    }
    else {
        //existing record
        var thisRecord = AllRecordData.find(x => { return x.Id === RowId });
        $("#txtRowId").val(thisRecord.Id);
        $("#txtTypeId").val(thisRecord.TypeId);
        $("#txtValue").val(thisRecord.Value);
        $("#txtPredicates").val(thisRecord.Predicates);
        $("#chkActiveFlag").prop("checked", (thisRecord.ActiveFlag == "True"));
        $("#chkSystemFlag").prop("checked", (thisRecord.SystemFlag == "True"));
    }
    $('#EditForm').show();
}

function SaveRecord() {
    var Id = $("#txtRowId").val();
    var TypeId = $("#txtTypeId").val();
    var Value = $("#txtValue").val();
    var Predicates = $("#txtPredicates").val();
    var ActiveFlag = ($("#chkActiveFlag").prop("checked")) ? "1" : "0";
    var SystemFlag = ($("#chkSystemFlag").prop("checked")) ? "1" : "0";
    if (Value.trim().length == 0) {
        alert("Text for this record is required.");
        return;
    }
    if (SystemFlag == "1") {
        if (!confirm("System values are not modifiable. Do you want to continue?")) {
            return;
        }
    }
    var RowData = { "Id": Id, "TypeId": TypeId, "Value": Value, "Predicates": Predicates, "ActiveFlag": ActiveFlag, "SystemFlag": SystemFlag };

    $.ajax({
        url: g_root + "/AjaxFunctions/SaveRecord/Metadata",
        type: "POST",
        data: JSON.stringify(RowData),
        contentType: "text/plain; charset=UTF-8",
        success: function (data) {
            if (data.trim().toLowerCase() == "success") {
                //Save Successful. Hide form and reload Metadata table
                $('#EditForm').hide();
                ReloadPage();
            }
            else {
                alert(data.trim());
            }
        }
    });
}

function DeleteRecord(DeleteElement, QuestionOrResponse) {
    if (!$(DeleteElement).hasClass("w3-disabled")) {
        if (confirm("Are you sure you want to delete this record?")) {
            var RowId = $(DeleteElement).data("rowid");
            $.ajax({
                url: g_root + "/AjaxFunctions/DeleteRecord/Metadata",
                type: "POST",
                data: RowId,
                contentType: "text/plain; charset=UTF-8",
                success: function (data) {
                    data = data.trim();
                    if (data.toLowerCase() == "success") {
                        //Save Successful
                        ReloadPage();
                    }
                    else {
                        alert(data);
                    }
                }
            });
        }
    }
}

function ReloadPage() {
    window.location = g_root + "/AdminTools/QuestionsAndResponses";
}